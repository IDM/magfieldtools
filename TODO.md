# To do

- [ ] Unit simplification
- [ ] Circular2D
  - [ ] Set units and force units
- [ ] Drift correction
- [ ] Trajectories SampleVectors
- [ ] Integration
- [ ] Acquisition parameters
- [ ] Finite integration length
- [ ] Elliptic2D
- [ ] Circular3D
- [ ] Cauchy integrals
- [ ] Flux



