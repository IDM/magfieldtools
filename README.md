# Magnetic Field Tools
This library provides models and tools for magnetic field analysis.

## Compilation
### Pre requisiteries 
#### Dependencies

This program relies on the Eigen matrix C++ library. This library is free and available online: https://eigen.tuxfamily.org. 

It should be pre-installed before compilation. The following repository structure is assumed:

- *source/repos*
  - */external_libs/eigen-eigen*
  - */this project*

#### SWIG

This package uses the SWIG wrapper: http://www.swig.org/

SWIG must be installed before compiling the package. The package was tested with SWIG 4.

#### Python version

Tests were done with Python 3.8 and later. This package was not tested with Python 2.7.x.

### Compile with Make

Activate your Conda environment if needed.

Check the SWIG variable in the provided *makefile* (a Windows .exe suffix is assumed) and make the project. This will install the Python module in the *site_packages*.

### Compile with Visual Studio or Visual Studio Code 

The following environment variables are needed by the SWIG wrapper:
-  PYTHON_INCLUDE should link to your Python include directory (*<YourPythonDir\include>*)
-  PYTHON_LIB should link to your Python library (*<YourPythonDir\libs\pythonxx.lib>*)

Activate your Conda environment if needed.

A *.vcxproj* file located in */vc* can be used for building the project. Please check the links to include directories and libraries. 

Build tasks are defined for VSC. 

Please note that the release versions are much faster than the debug ones.

If the project is built from the *.vcxproj* file, the outputs are built in the *./python* directory and the library is not installed in the *site_packages*.  To use it, you will need to navigate to the directory or add it in the Python Path.
