/*!
   \file		symmetries.h
   \brief		Symmetry classes interface
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <Eigen/Dense>
#include <string>
#include <vector>
#include "mft_errors.h"

using namespace std;

// -------------------------------------------
// Symmetries
// -------------------------------------------
//! Symmetry class.
/*! 
Class for symmetries. A symmetry contains:
- a centre (Eigen::Vector3d)
- a type (string)

Most of symmetries contains also a normal vector defining symmetry plane
*/
class Symmetry {

public:
	//! Set the symmetry centre 
	/*! 
	@param v centre point  
	*/
	void set_centre(const Eigen::Vector3d& v) { centre = v; }; 

	//! Set the symmetry centre 
	/*! 
	@param v centre point
	*/
	void set_centre(const vector<double>& v); 

	//! Get the symmetry centre 
	/*! 
	@param[out] v centre point
	*/
	void get_centre(Eigen::Vector3d& v) { v = centre; };

	//! Get the symmetry centre 
	/*! 
	@return v centre point 
	*/
	vector<double> get_centre();

	void get_type(string& s) { s = sym_type; }; //!< Get the symmetry type
	string get_type() { return sym_type; } //!< Return the symmetry type

	//! Test the symmetry type
	/*!
	@param s symmetry type
	@return true if the type is equal to s, else false 
	*/
	bool is_type(const string c) { if (!sym_type.compare(c)) return true; return false; };

protected:
	Eigen::Vector3d centre;
	string sym_type = "undefined";
};

// -------------------------------------------
// Symmetry planes
// -------------------------------------------
//! Symmetry plane class.
/*! 
A symmetry plane contains
- a centre: any point located on the plane (inherited from Symmetry)
- a normal vector
- a bool indicating if the plane is a symmetry or an antisymmetry plane
- a type (inherited from Symmetry) 
*/
class SymmetryPlane : public Symmetry {
public:

	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry plane (antisymmetry == false) passing through the origin (0, 0, 0)
	*/
	SymmetryPlane() {
		normal_vector << 1, 0, 0; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
	};
	
	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry plane (antisymmetry == false) passing through the origin (0, 0, 0)
	@param n normal vector 
	*/
	SymmetryPlane(const Eigen::Vector3d& n) {
		normal_vector = n; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
	};

	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry or antisymmetry plane  passing through the origin (0, 0, 0)
	@param n normal vector 
	@param s antisymmetry status (true: antisymetry plane, false: symmetry plane) 
	*/
	SymmetryPlane(const Eigen::Vector3d& n, bool s) : SymmetryPlane(n) { antisymmetry = s; }
	
	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry or antisymmetry plane
	@param n normal vector 
	@param c centre 
	*/
	SymmetryPlane(const Eigen::Vector3d& n, const Eigen::Vector3d& c) : SymmetryPlane(n) { centre = c; };

	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry or antisymmetry plane
	@param n normal vector 
	@param c centre
	@param s antisymmetry status (true: antisymetry plane, false: symmetry plane) */
	SymmetryPlane(const Eigen::Vector3d& n, const Eigen::Vector3d& c, bool s) : SymmetryPlane(n, s) { centre = c; };
	
	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry plane (antisymmetry == false) passing through the origin (0, 0, 0)
	@param n normal vector 
	*/
	SymmetryPlane(const vector<double>& n);

	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry or antisymmetry plane passing through the origin (0, 0, 0)
	@param n normal vector 
	@param s antisymmetry status (true: antisymetry plane, false: symmetry plane) 
	*/
	SymmetryPlane(const vector<double>& n, bool s) : SymmetryPlane(n) { antisymmetry = s; };

	//! Symmetry plane constructor
	/*! 
	This constructor builds a symmetry or antisymmetry plane 
	@param n normal vector (vector of 3 doubles)
	@param c centre (vector of 3 doubles)
	@param s antisymmetry status (true: antisymetry plane, false: symmetry plane) 
	*/
	SymmetryPlane(const vector<double>& n, const vector<double>& c, bool s) : SymmetryPlane(n, s) {
		if (c.size() != 3)
			throw MFTError("SymmetryPlane::SymmetryPlane", "Centre must have 3 components");
		for (int k = 0; k < 3; k++)
			centre(k) = c[k];
	};

	//! Symmetry plane constructor
	/*! This constructor builds a symmetry or antisymmetry plane
		@param n normal vector (vector of 3 doubles)
		@param c centre (vector of 3 doubles) */
	SymmetryPlane(const vector<double>& n, const vector<double>& c) : SymmetryPlane(n) {
		if (c.size() != 3)
			throw MFTError("SymmetryPlane::SymmetryPlane", "Centre must have 3 components");
		for (int k = 0; k < 3; k++)
			centre(k) = c[k];
	};

	//! Set the normal vector 
	/*! @param v normal vector (Eigen::Vector3d) */
	void set_normal_vector(const Eigen::Vector3d& v) { normal_vector = v; };

	//! Set the normal vector 
	/*! @param v normal vector (vector of 3 doubles) */
	void set_normal_vector(const vector<double>& v);

	//! Get the normal vector
	/*! @param[out] v normal vector (Eigen::Vector3d)*/
	void get_normal_vector(Eigen::Vector3d& v) { v = normal_vector; }

	//! Get the normal vector components
	/*! @param[out] nx normal vector x component 
		@param[out] ny normal vector y component
		@param[out] nz normal vector z component*/
	void get_normal_vector(double& nx, double& ny, double& nz) { 
		nx = normal_vector(0); ny = normal_vector(1); nz = normal_vector(2);
	}

	//! Get the normal vector
	/*! @return v normal vector (vector of doubles)*/
	vector<double> get_normal_vector() { vector<double> v(3); 
		v[0] = normal_vector(0); v[1] = normal_vector(1); v[2] = normal_vector(2); 
		return v; 
	}

	//! Get the symmetry centre 
	/*! @param[out] v centre point (Eigen::Vector3d) */
	void get_centre(Eigen::Vector3d& v) { v = centre; };

	//! Set the antisymmetry status
	/*! @param s antisymmetry status (true: antisymmetry plane, false: symmetry plane) */
	void set_antisymmetry(bool b) { antisymmetry = b; };

	//! Get the antisymmetry status
	/*! @return b antisymmetry status (true: antisymmetry plane, false: symmetry plane) */
	bool is_antisymmetric() { return antisymmetry; };

	bool is_zero_para() { return zero_para; };
	bool is_zero_perp() { return zero_perp; };

protected:
	Eigen::Vector3d normal_vector;
	bool antisymmetry = false;
	bool zero_para = false;
	bool zero_perp = false;
};

//! Symmetry plane with zero parallel field class.
class SymmetryPlaneZeroPara : public SymmetryPlane{

public:

	//! Symmetry plane constructor with zero parallel field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component parallel to the plane 1, 0, 0
	*/
	SymmetryPlaneZeroPara() {
		normal_vector << 1, 0, 0; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_para = true;
	};

	//! Symmetry plane constructor with zero parallel field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component parallel to the plane
	@param n normal vector 
	*/
	SymmetryPlaneZeroPara(const Eigen::Vector3d& n) {
		normal_vector = n; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_para = true;
	};

	//! Symmetry plane constructor with zero parallel field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component parallel to the plane
	@param n normal vector 
	*/
	SymmetryPlaneZeroPara(const vector<double>& n) {
		normal_vector << n[0], n[1], n[2]; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_para = true;
	};

	//! Symmetry plane constructor with zero parallel field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component parallel to the plane
		@param n normal vector (vector of 3 doubles)
		@param c centre (vector of 3 doubles) */
	SymmetryPlaneZeroPara(const vector<double>& n, const vector<double>& c) : SymmetryPlaneZeroPara(n) {
		if (c.size() != 3)
			throw MFTError("SymmetryPlaneZeroPara::SymmetryPlaneZeroPara", "Centre must have 3 components");
		for (int k = 0; k < 3; k++)
			centre(k) = c[k];
	};

};


//! Symmetry plane with zero perpendicular field class.
class SymmetryPlaneZeroPerp : public SymmetryPlane{

public:

	//! Symmetry plane constructor with zero perpendicular field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component perpendicular to the plane 1, 0, 0
	*/
	SymmetryPlaneZeroPerp() {
		normal_vector << 1, 0, 0; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_perp = true;
	};


	//! Symmetry plane constructor with zero perpendicular field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component perpendicular to the plane
	@param n normal vector 
	*/
	SymmetryPlaneZeroPerp(const Eigen::Vector3d& n) {
		normal_vector = n; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_para = true;
	};

	//! Symmetry plane constructor with zero perpendicular field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component perpendicular to the plane
	@param n normal vector 
	*/
	SymmetryPlaneZeroPerp(const vector<double>& n) {
		normal_vector << n[0], n[1], n[2]; 
		centre << 0, 0, 0; 
		antisymmetry = false;
		sym_type = "symmetry plane";
		zero_perp = true;
	};

	//! Symmetry plane constructor with zero perpendicular field
	/*! 
	This constructor builds a symmetry plane passing through the origin (0, 0, 0) 
	with zero component parallel to the plane
		@param n normal vector (vector of 3 doubles)
		@param c centre (vector of 3 doubles) */
	SymmetryPlaneZeroPerp(const vector<double>& n, const vector<double>& c) : SymmetryPlaneZeroPerp(n) {
		if (c.size() != 3)
			throw MFTError("SymmetryPlaneZeroPerp::SymmetryPlaneZeroPerp", "Centre must have 3 components");
		for (int k = 0; k < 3; k++)
			centre(k) = c[k];
	};

};



// -------------------------------------------
// Symmetry centre
// -------------------------------------------
//! Symmetry centre class.
/*! A symmetry centre contains
	- a centre: any point located on the plane (inherited from Symmetry)
	- a type (inherited from Symmetry) */
class SymmetryCentre : public Symmetry {
public:
	//! Symmetry centre constructor
	/*! This constructor builds a symmetry centre
		@param c centre (Eigen::Vector3d) */
	SymmetryCentre(const Eigen::Vector3d& c) { centre = c; sym_type = "symmetry centre"; };

	//! Symmetry centre constructor
	/*! This constructor builds a symmetry centre
		@param c centre (vector of 3 doubles) */
	SymmetryCentre(const vector<double>& c);

private:

};