/*!
   \file		multipole.cpp
   \brief		Multipole and derivated classes implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "multipole.h"
#include "sample.h"
#include <iostream>

using namespace std;

// ---------------------------------------------
// Multipole2D: base class for multipole objects
// -------------------------------------------- -

// Normalize the multipole coefficients
void Multipole2D::normalize_coef(int n, unsigned char mode, double unity) {
	if (!coef.size()) throw MFTError("Multipole::normalize_coef", "No coefficient", 0);

	complex<double> cmax = 0;
	// Find the max multipole component if n is not specified
	if (n < 0) {
		size_t i = 0;
		for (int k = 0; k < coef.size(); k++) {
			if (abs(coef[k]) > abs(cmax)) {
				cmax = coef[k];
				i = k;
			}
		}
		n = i;
	}
	else {
		cmax = coef[n];
	}
	// Normalize
	complex<double> cnorm = 0;
	if (mode == 0) cnorm = abs(cmax);
	if (mode == 1) cnorm = cmax.real();
	if (mode == 2) cnorm = cmax.imag();
	if (mode == 3) cnorm = cmax;
	for (size_t k = 0; k < coef.size(); k++)
		coef_norm[k] = unity / cnorm * coef[k];

}

// Set the field units
void Multipole2D::set_unit_field(Unit& u) {
	
	// Check the compatibility of the units and get the scaling factor
	double f;
	try {
		f = unit_field.get_scale_factor_to_unit(u);
	}
	catch (MFTError e) {
		throw e;
	}

	// Change the coefficients
	for (size_t i = 0; i < coef.size(); i++)
		coef[i] *= f;

	// Set the new unit
	unit_field.copy(u);
}

// Set the distance units
void Multipole2D::set_unit_distance(Unit& u) {

	// Check the compatibility of the units and get the scaling factor
	double f;
	try {
		f = unit_distance.get_scale_factor_to_unit(u);
	}
	catch (MFTError e) {
		throw e;
	}

	// Change the coefficients
	for (size_t i = 0; i < ref.size(); i++)
		ref[i] *= f;

	// Set the new unit
	unit_distance.copy(u);
}

// Set a coefficient
void Multipole2D::set_coef(size_t k, complex<double> v) {
	
	if (k < degree) {
		coef[k] = v;
		normalize_coef();
	}
	else
		throw MFTError("Multipole::set_coef", "Index out of range", 0);

}

// Get the normalized coefficients
vector<double> Multipole2D::get_coef(string s) {

	vector<double> res;

	if (!s.compare("real"))		
		for (complex<double> c : coef)
			res.push_back(c.real());
	
	else if (!s.compare("imag")) 
		for (complex<double> c : coef)
			res.push_back(c.imag());
	
	else
		throw MFTError("Multipole::get_coef", "Unknown specification", 0);

	return res;
}

// Get the normalized coefficients
vector<double> Multipole2D::get_coef_norm(string s) {

	vector<double> res;

	if (!s.compare("real"))
		for (complex<double> c : coef_norm)
			res.push_back(c.real());

	else if (!s.compare("imag"))
		for (complex<double> c : coef_norm)
			res.push_back(c.imag());

	else
		throw MFTError("Multipole::get_coef", "Unknown specification", 0);

	return res;
}

// ---------------------------------------------
// Multipole2D
// ---------------------------------------------
// Multipole2D default constructor
Multipole2D::Multipole2D() {

	// This is a 2D model
	is_2D = true;

	// Initialize the multipole coefficients
	coef.resize(degree);
	coef_norm.resize(degree);
	for (size_t k = 0; k < degree; k++) {
		coef[k] = 0;
		coef_norm[k] = 0;
	}

	// Initialize the reference radius
	ref.resize(1);
	ref[0] = 10;

}

// Multipole2D constructor
Multipole2D::Multipole2D(CoordinateSystem & cs, size_t d, const vector<double> &r) : Multipole2D() {
	
	degree = d;
	coord_system = cs;
	ref = r;
}

// Initialize a Circular object from a sample set s_set
// The 2D plane is defined by the coordinate system
void Multipole2D::init_from_set(SampleVector& s_set) {

	// Set the model units
	unit_field = s_set.get_unit_sample();
	unit_distance = s_set.get_unit_distance();
	
	// Get the number of samples 
	size_t len = s_set.get_size();
	
	// Reference radius
	vector<double> ref;
	get_ref(ref);
	double r0 = ref[0];
	
	// Misc. initializations
	Eigen::Vector3d v3, v3_multi;
	double ar3[3];
	double vx, vy;
	size_t k = 0;

	// Initialize the matrix
	complex<double> ii(0.0, 1.0), z;
	Eigen::MatrixXd m = multipole_matrix(s_set);

	// Complex field
	Eigen::VectorXd b(m.rows());

	// Complex field
	for (size_t i = 0; i < len; i++) {
		// Get the sample i
		Sample s_i;
		s_set.get_sample(i, s_i);

		// The field value is complex if all field components are defined in the sample
		if (!s_i.is_proj()) {

			// project the field direction in the multipole coord system
			s_i.get_unit_vector(ar3);
			v3 << ar3[0], ar3[1], ar3[2];
			v3_multi = coord_system.proj_vector(v3);
			vx = v3_multi[0];
			vy = v3_multi[1];

			// Field values
			b(k) = s_i.get_value() * vy;
			k++;
			b(k) = s_i.get_value() * vx;

		}
		else
			b(k) = s_i.get_value();
		
		k++;
	}

	// Solve for the multipoles
	Eigen::MatrixXd an_bn_tmp;
	try{
		an_bn_tmp = m.bdcSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(b);
	}
	catch(...){
		throw MFTError("Multipole2D::init_from_set", "Cannot invert the multipole matrix", 0);
	}
	coef.resize(degree);
	for (size_t i = 0; i < degree; i++)
		coef[i] = an_bn_tmp(i) + ii * an_bn_tmp(i + degree);
	// Normalize
	normalize_coef();

	// Update the metadata
	Metadata s_md;
	s_set.get_metadata(s_md);
	metadata.set_comment("Built from " + s_md.get_label() + " SampleVector.");
	metadata.set_creation_time();
	metadata.set_modification_time();

}

// Initialize from several sample sets
void Multipole2D::init_from_set(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute the multipoles and initialize the coefficients
	init_from_set(s_set);
}

// Scale the coefficients
void Multipole2D::scale(double d){

	size_t n = coef.size();
	for (size_t k = 0; k < n; k++)
		coef[k] = d * coef[k];
}

// ---------------------------------------------
// Circular2D
// ---------------------------------------------

// Build the multipole matrix from a sample array
Eigen::MatrixXd Circular2D::multipole_matrix(SampleVector& s_set) {

	// Check that the sample array has all attributes for multipole computations
	if (!s_set.get_values_enable() || !s_set.get_coord_enable() || !s_set.get_vector_enable())
		throw MFTError("Circular2D::multipole_matrix", "SampleVector not suitable", 0);
	
	// Get the number of samples 
	size_t len = s_set.get_size();
	if (!len)
		throw MFTError("Circular2D::multipole_matrix", "Empty SampleVector");

	// Get the reference frame attached to the sample set
	CoordinateSystem cs_set;
	s_set.get_coordinate_system(cs_set);

	// Reference radius
	vector<double> ref;
	get_ref(ref);
	double r0 = ref[0];

	// Various initializations
	Eigen::Vector3d v3, v3_multi;
	complex<double> ii(0.0, 1.0), z;
	Sample s_i;
	double x, y, vx, vy, theta;
	unsigned int k;
	Eigen::Vector3d axis_x, axis_y;
	Eigen::Vector3d axis_x_set, axis_y_set, axis_z_set;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	cs_set.get_axis_x(axis_x_set);
	cs_set.get_axis_y(axis_y_set);
	cs_set.get_axis_z(axis_z_set);

	// Number of rows in the matrix
	// One row if projected values, else two rows (one for each component)
	size_t rows = len;
	for (k = 0; k < len; k++)
		if (!s_set.is_proj(k))
			rows++; 

	if (rows < 2 * degree)
		degree = rows / 2;

	// Compute the multipole matrix
	Eigen::MatrixXd m(rows, 2 * degree);

	k = 0;
	for (size_t i = 0; i < len; i++) {

		// Sample i
		s_set.get_sample(i, s_i);

		// Extract the coordinates of sample i
		s_i.get_coordinates(v3);

		// Compute the coordinates in the multipole coord system
		v3_multi = coord_system.compute_new_coordinates(v3, cs_set);
		x = v3_multi(0);
		y = v3_multi(1);

		// Extract the vector direction
		s_i.get_unit_vector(v3);
		v3_multi = (v3(0) * axis_x_set + v3(1) * axis_y_set + v3(2) * axis_z_set).dot(axis_x) * axis_x;
		v3_multi += (v3(0) * axis_x_set + v3(1) * axis_y_set + v3(2) * axis_z_set).dot(axis_y) * axis_y;
		vx = v3_multi(0);
		vy = v3_multi(1);

		// Angle between the field direction and the x axis
		theta = atan2(vy, vx);

		// Fill the matrix
		// Add one row if the sample is a projection
		// B_proj = (Re Z, -Im Z) (...b...a...)^T
		if (s_i.is_proj()) {
			for (size_t j = 0; j < degree; j++) {
				if (x == 0 && y == 0 && j == 0)
					z = -ii * exp(ii * theta);
				else
					z = - ii * exp(ii * theta) * pow((x + ii * y) / r0, j);
				m(k, j) = z.real(); 
				m(k, j + degree) = -z.imag();
			}
			k++;
		}
		// Add two lines if the sample specifies the 2 components of the field
		// Re B = (Re Z,  -Im Z) (...b...a...)^T
		// Im B = (Im Z, Re Z) (...b...a...)^T
		else {	
			for (size_t j = 0; j < degree; j++) {
				if (x == 0 && y == 0 && j == 0)
					z = 1;
				else
					z = pow((x + ii * y) / r0, j);
				m(k, j) = z.real();;
				m(k, j + degree) = -z.imag();
				m(k + 1, j) = z.imag();;
				m(k + 1, j + degree) = z.real();
			}
			k += 2;
		}

	}
	return m;
}

// Build the multipole matrix from a sample array
Eigen::MatrixXd Circular2D::multipole_matrix(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute and return the matrix
	return multipole_matrix(s_set);
}

// Wrap the multipole matrix to vectors
vector<vector<double>> Circular2D::multipole_matrix_std(SampleVector& s_set) {
	// Compute the multipole matrix
	Eigen::MatrixXd m;
	m = multipole_matrix(s_set);
	
	// Put it in a vector
	vector<vector<double>> v;
	vector<double> vk;
	for (size_t k = 0; k < (size_t)m.rows(); k++) {
		vk.clear();
		for (size_t n = 0; n < (size_t)m.cols(); n++)
			vk.push_back(m(k, n));
		v.push_back(vk);
	}
	
	return v;
}

// Wrap the multipole matrix to vectors
vector<vector<double>> Circular2D::multipole_matrix_std(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute and return the matrix
	return multipole_matrix_std(s_set);

}

// Change the reference radius and recompute the multipole coefficients
void Circular2D::change_ref_radius(double r0) {

	// Get the previous reference radius
	double r_old = ref[0];
	complex<double> cn;

	// Compute the new multipoles
	for (size_t k = 0; k < degree; k++)
		coef[k] *= pow(r0 / r_old, k);

	// Compute the normalized coefficients
	normalize_coef();
	
	// Set the new reference radius
	ref[0] = r0;

	metadata.set_modification_time();
}

// Compute the sample field
void Circular2D::field(Sample& sample, CoordinateSystem& cs) {

	// Reference radius
	double r0 = ref[0];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Direction vector
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);
	vect = vect_dir(0) * sample_x_axis + vect_dir(1) * sample_y_axis + vect_dir(2) * sample_z_axis;
	vect_dir = vect;

	// Coordinates in the Circular2D coordinate system
	Eigen::Vector3d point_proj;
	coord_system.proj_point(point_proj, coord);
	complex<double> z(point_proj(0), point_proj(1));

	// Compute the field
	complex<double> b = coef[0];
	for (size_t k = 1; k < degree; k++) 
		b += coef[k] * pow(z / r0, k);
		
	// Compute the field in the Circular2D coordinate system
	Eigen::Vector3d b_vect_tmp;
	Eigen::Vector3d axis_x, axis_y;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	
	b_vect_tmp = b.imag() * axis_x + b.real() * axis_y;

	// The value is the projection of b_vect_tmp to vect_dir
	if (sample.is_proj()) 		
		sample.set_value(b_vect_tmp.dot(vect_dir));

	else {
		if (b.real() != 0 || b.imag() != 0) {
			// Set the vector direction
			sample.set_unit_vector(b_vect_tmp(0), b_vect_tmp(1), 0);
			// Set the value
			sample.set_value(b_vect_tmp.norm());
		}
		else {
			// This defines the direction to y for zero valued samples
			sample.set_unit_vector(axis_y(0), axis_y(1), 0);
			sample.set_value(0);
		}
		
	}
}

// Compute the sample field
void Circular2D::field(Sample& sample) {
	field(sample, coord_system);
}


// Compute the complex potential
complex<double> Circular2D::complex_potential(Sample& sample, CoordinateSystem& cs) {

	// Reference radius
	double r0 = ref[0];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Direction vector
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);
	vect = vect_dir(0) * sample_x_axis + vect_dir(1) * sample_y_axis + vect_dir(2) * sample_z_axis;
	vect_dir = vect;

	// Coordinates in the Circular2D coordinate system
	Eigen::Vector3d point_proj;
	coord_system.proj_point(point_proj, coord);
	complex<double> z(point_proj(0), point_proj(1));

	// Compute the potential
	complex<double> a = coef[0];
	for (size_t k = 1; k < degree; k++) 
		a -= coef[k] * pow(z, k + 1) / pow(z, k);
		
	return a;
}

// Scalar potential
void  Circular2D::scalar_potential(Sample& sample, CoordinateSystem& cs) {

	complex<double> a = complex_potential(sample, cs);
	sample.set_value(a.imag());

}

// Compute the scalar potential
void Circular2D::scalar_potential(Sample& sample) {
	scalar_potential(sample, coord_system);
}

// Multipole strengths
void Circular2D::strength(Strength& s) {

	// Get the coefficients and reference radius
	vector<complex<double>> vs;
	get_coef(vs);
	double r0 = ref[0];

	// Compute the strengths
	for (size_t k = 0; k < degree; k++) 
		vs[k] /= pow(r0, k);

	vector<double> s_norm(degree), s_skew(degree);
	for (size_t k = 0; k < degree; k++) {
		s_norm[k] = vs[k].real();
		s_skew[k] = vs[k].imag();
	}
	
	s.set(s_norm, s_skew, unit_field, unit_distance);

}

// Multipole strengths
void Circular2D::normalized_strength(double b_rho, Unit& u, Strength& s) {

	// Check the units
	if (!u.is_physical_quantity("magnetic field x distance"))
		throw MFTError("Circular2D::normalized_strength", "b_rho must be a magnetic field times a distance");

	if (!unit_field.is_physical_quantity("magnetic field x distance"))
		throw MFTError("Circular2D::normalized_strength", "The dipole strength must be a magnetic field times a distance");

	b_rho *= u.get_scale_factor_to_unit(unit_field);

	strength(s);

	// Normalize
	vector<double> vs;
	s.get_values(vs);
	size_t n = vs.size();

	for (size_t k = 0; k < n; k++)
		vs[k] /= b_rho;

	s.set_values(vs);

	// Update the unit
	Unit u_s;
	s.get_unit_sample(u_s);
	u_s.divide(unit_field);
	u_s.simplify();
	s.set_unit_sample(u_s, true);

}

// ---------------------------------------------
// Elliptic2D
// ---------------------------------------------
// Reference coordinates
void Elliptic2D::calc_set_ref_coord(double a, double b){
	if (a <= b)
		throw MFTError("Elliptic2D::calc_set_ref_coord", "The semi-major axis must be larger than the semi-minor axis", 0);		
	vector<double> ell_coord(2);
	ell_coord[0] = pow(a * a - b * b, 0.5);
	ell_coord[1] = atanh(b / a);
	set_ref(ell_coord);
};

// Build the multipole matrix from a sample array
Eigen::MatrixXd Elliptic2D::multipole_matrix(SampleVector& s_set) {

	// Check that the sample array has all attributes for multipole computations
	if (!s_set.get_values_enable() || !s_set.get_coord_enable() || !s_set.get_vector_enable())
		throw MFTError("Elliptic2D::multipole_matrix", "SampleVector not suitable", 0);
	
	// Get the number of samples 
	size_t len = s_set.get_size();
	if (!len)
		throw MFTError("Elliptic2D::multipole_matrix", "Empty SampleVector");

	// Get the reference frame attached to the sample set
	CoordinateSystem cs_set;
	s_set.get_coordinate_system(cs_set);

	// Reference radius
	//vector<double> ref;
	//get_ref(ref);
	//double r0 = ref[0];

	// Various initializations
	Eigen::Vector3d v3, v3_multi;
	complex<double> ii(0.0, 1.0), z;
	Sample s_i;
	double x, y, vx, vy, theta;
	size_t k;
	Eigen::Vector3d axis_x, axis_y;
	Eigen::Vector3d axis_x_set, axis_y_set, axis_z_set;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	cs_set.get_axis_x(axis_x_set);
	cs_set.get_axis_y(axis_y_set);
	cs_set.get_axis_z(axis_z_set);

	// Number of rows in the matrix
	// One row if projected values, else two rows (one for each component)
	size_t rows = len;
	for (k = 0; k < len; k++)
		if (!s_set.is_proj(k))
			rows++; 

	if (rows < 2 * degree)
		degree = rows / 2;

	// Compute the multipole matrix
	Eigen::MatrixXd m(rows, 2 * degree);

	k = 0;
	for (size_t i = 0; i < len; i++) {

		// Sample i
		s_set.get_sample(i, s_i);

		// Extract the coordinates of sample i
		s_i.get_coordinates(v3);

		// Compute the coordinates in the multipole coord system
		v3_multi = coord_system.compute_new_coordinates(v3, cs_set);
		x = v3_multi(0);
		y = v3_multi(1);

		// Elliptic coordinates
		complex<double> w = acosh((x + ii * y) / ref[0]);

		// Extract the vector direction
		s_i.get_unit_vector(v3);
		v3_multi = (v3(0) * axis_x_set + v3(1) * axis_y_set + v3(2) * axis_z_set).dot(axis_x) * axis_x;
		v3_multi += (v3(0) * axis_x_set + v3(1) * axis_y_set + v3(2) * axis_z_set).dot(axis_y) * axis_y;
		vx = v3_multi(0);
		vy = v3_multi(1);

		// Angle between the field direction and the x axis
		theta = atan2(vy, vx);

		// Fill the matrix
		// Add one row if the sample is a projection
		// B_proj = (Re Z, -Im Z) (...b...a...)^T
		if (s_i.is_proj()) {
			for (size_t j = 0; j < degree; j++) {

				if (j == 0)
					z = 0.5;
				else{
					double jd = (double)j;
					z = - ii * exp(ii * theta) * cosh(jd * w) / cosh(jd * ref[1]);
				}
				m(k, j) = z.real(); 
				m(k, j + degree) = -z.imag();
			}
			k++;
		}
		// Add two lines if the sample specifies the 2 components of the field
		// Re B = (Re Z,  -Im Z) (...b...a...)^T
		// Im B = (Im Z, Re Z) (...b...a...)^T
		else {	
			for (size_t j = 0; j < degree; j++) {
				
				if (j == 0)
					z = 0.5;
				else{
					double jd = (double)j;
					z = cosh(jd * w) / cosh(jd * ref[1]);	
				}
				m(k, j) = z.real();;
				m(k, j + degree) = -z.imag();
				m(k + 1, j) = z.imag();;
				m(k + 1, j + degree) = z.real();
			}
			k += 2;
		}
	}
	return m;
}

// Build the multipole matrix from a sample array
Eigen::MatrixXd Elliptic2D::multipole_matrix(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute and return the matrix
	return multipole_matrix(s_set);
}

// Compute the sample field
void Elliptic2D::field(Sample& sample, CoordinateSystem& cs) {

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d v;
	coord_system.compute_new_coordinates(v, coord, cs);
	double x = v[0], y = v[1];

	// Direction vector
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);
	v = vect_dir(0) * sample_x_axis + vect_dir(1) * sample_y_axis + vect_dir(2) * sample_z_axis;
	vect_dir = v;

	// Compute the field
	complex<double> ii(0.0, 1.0), z;
	complex<double> b = coef[0] / 2.;
	complex<double> w = acosh((x + ii * y) / ref[0]);
	for (size_t j = 1; j < degree; j++) {
		double jd = (double)j;
		b += coef[j] * cosh(jd * w) / cosh(jd * ref[1]);	
	}

	// Compute the field in the Elliptic2D coordinate system
	Eigen::Vector3d b_vect_tmp;
	Eigen::Vector3d axis_x, axis_y;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
 
	b_vect_tmp = b.imag() * axis_x + b.real() * axis_y;

	// The value is the projection of b_vect_tmp to vect_dir
	if (sample.is_proj()) 		
		sample.set_value(b_vect_tmp.dot(vect_dir));

	else {
		if (b.real() != 0 || b.imag() != 0) {
			// Set the vector direction
			sample.set_unit_vector(b_vect_tmp(0), b_vect_tmp(1), 0);
			// Set the value
			sample.set_value(b_vect_tmp.norm());
		}
		else {
			// This defines the direction to y for zero valued samples
			sample.set_unit_vector(axis_y(0), axis_y(1), 0);
			sample.set_value(0);
		}
	}
}

// Semi-axes
pair<double, double> Elliptic2D::get_semi_axes(){

	pair<double, double> ab;
	ab.first = ref[0] / pow( 1 - tanh(ref[1] * tanh(ref[1])), 0.5); // Semi-major axis
	ab.second = ab.first * tanh(ref[1]); // Semi-minor axis

	return ab;
}

complex<double> Elliptic2D::complex_potential(Sample& sample, CoordinateSystem& cs) {

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d v;
	coord_system.compute_new_coordinates(v, coord, cs);
	double x = v[0], y = v[1];

	// Direction vector
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);
	v = vect_dir(0) * sample_x_axis + vect_dir(1) * sample_y_axis + vect_dir(2) * sample_z_axis;
	vect_dir = v;

	// Compute the field
	complex<double> ii(0.0, 1.0);
	complex<double> z = x + ii * y;
	// complex<double> b = coef[0] / 2.;
	double e = ref[0];
	complex<double> w = acosh((x + ii * y) / e);
	complex<double> a = - coef[0] / 2. * z;
	for (size_t j = 1; j < degree; j++) {
		double jd = (double)j;
		a -= coef[j] / cosh(jd * ref[1]) / (jd * jd - 1) * (jd * (e + z) * sqrt((z - e)/(z + e)) * sinh(jd * w) - z * cosh(jd * w));
		//b += coef[j] * cosh(jd * w) / cosh(jd * ref[1]);	
	}

	return a;
}

// Scalar potential
void Elliptic2D::scalar_potential(Sample& s, CoordinateSystem& cs){

	complex<double> a = complex_potential(s, cs);
	s.set_value(a.imag());
}

// Compute the scalar potential
void Elliptic2D::scalar_potential(Sample& sample) {
	scalar_potential(sample, coord_system);
}


// ---------------------------------------------
// Multipole3D: base class for multipole objects
// -------------------------------------------- -

// Set the field units
void Multipole3D::set_unit_field(Unit& u) {
	
	// Check the compatibility of the units and get the scaling factor
	double f;
	try {
		f = unit_field.get_scale_factor_to_unit(u);
	}
	catch (MFTError e) {
		throw e;
	}

	// Change the coefficients
	for (size_t i = 0; i < coef.size(); i++)
		for (size_t j = 0; j < coef[i].size(); j++)
		coef[i][j] *= f;

	// Set the new unit
	unit_field.copy(u);
}

// Set the distance units
void Multipole3D::set_unit_distance(Unit& u) {

	// Check the compatibility of the units and get the scaling factor
	double f;
	try {
		f = unit_distance.get_scale_factor_to_unit(u);
	}
	catch (MFTError e) {
		throw e;
	}

	// Change the coefficients
	for (size_t i = 0; i < ref.size(); i++)
		ref[i] *= f;

	// Set the new unit
	unit_distance.copy(u);
}

// Initialize from several sample sets
void Multipole3D::init_from_set(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute the multipoles and initialize the coefficients
	init_from_set(s_set);
}

// Field from numerical derivative of the potential
void Multipole3D::field_from_scalar_potential_grad(Sample& s, CoordinateSystem& cs){

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	s.get_coordinates(coord);
	s.get_unit_vector(vect_dir);

	// Sample coordinate system
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);

	// dr
	double d = dref_ref * ref[0];
	
	// If is_proj the potential is derivated only in the direction of the vector
	Sample s0, s1;
	if (s.is_proj()){
		// Finite differences
		s0.set_coordinates(coord - d / 2 * vect_dir);
		scalar_potential(s0, cs);	
		
		s1.set_coordinates(coord + d / 2 * vect_dir);
		scalar_potential(s1, cs);	
		
		// B = - grad V
		s.set_value((s1.get_value() - s0.get_value()) / d);
	}
	// Else compute the 3 derivatives
	else {
		// bx
		s0.set_coordinates(coord - d / 2 * sample_x_axis);
		scalar_potential(s0, cs);	
		
		s1.set_coordinates(coord + d / 2 * sample_x_axis);
		scalar_potential(s1, cs);	

		double bx = (s1.get_value() - s0.get_value()) / d;

		// by
		s0.set_coordinates(coord - d / 2 * sample_y_axis);
		scalar_potential(s0, cs);	
		
		s1.set_coordinates(coord + d / 2 * sample_y_axis);
		scalar_potential(s1, cs);	

		double by = (s1.get_value() - s0.get_value()) / d;

		// bz
		s0.set_coordinates(coord - d / 2 * sample_z_axis);
		scalar_potential(s0, cs);	
		
		s1.set_coordinates(coord + d / 2 * sample_z_axis);
		scalar_potential(s1, cs);	

		double bz = (s1.get_value() - s0.get_value()) / d;

		// Set the vector value
		double b = sqrt(bx * bx + by * by + bz * bz);
		s.set_value(b);

		// Set the direction
		double b3[3]={bx, by, bz};
		s.set_unit_vector(b3);
	}
}

// Field from numerical derivative of the potential
void Multipole3D::field_from_scalar_potential_grad(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m){

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	s.get_coordinates(coord);
	s.get_unit_vector(vect_dir);

	// Sample coordinate system
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);

	// dr
	double d = dref_ref * ref[0];
	
	// If is_proj the potential is derivated only in the direction of the vector
	Sample s0, s1;
	if (s.is_proj()){
		// Finite differences
		s0.set_coordinates(coord - d / 2 * vect_dir);
		scalar_potential(s0, cs, coef_lm, l, m);	
		
		s1.set_coordinates(coord + d / 2 * vect_dir);
		scalar_potential(s1, cs, coef_lm, l, m);	
		
		// B = - grad V
		s.set_value((s1.get_value() - s0.get_value()) / d);
	}
	// Else compute the 3 derivatives
	else {
		// bx
		s0.set_coordinates(coord - d / 2 * sample_x_axis);
		scalar_potential(s0, cs, coef_lm, l, m);	
		
		s1.set_coordinates(coord + d / 2 * sample_x_axis);
		scalar_potential(s1, cs, coef_lm, l, m);	

		double bx = (s1.get_value() - s0.get_value()) / d;

		// by
		s0.set_coordinates(coord - d / 2 * sample_y_axis);
		scalar_potential(s0, cs, coef_lm, l, m);	
		
		s1.set_coordinates(coord + d / 2 * sample_y_axis);
		scalar_potential(s1, cs, coef_lm, l, m);	

		double by = (s1.get_value() - s0.get_value()) / d;

		// bz
		s0.set_coordinates(coord - d / 2 * sample_z_axis);
		scalar_potential(s0, cs, coef_lm, l, m);	
		
		s1.set_coordinates(coord + d / 2 * sample_z_axis);
		scalar_potential(s1, cs, coef_lm, l, m);	

		double bz = (s1.get_value() - s0.get_value()) / d;

		// Set the vector value
		double b = sqrt(bx * bx + by * by + bz * bz);
		s.set_value(b);

		// Set the direction
		double b3[3]={bx, by, bz};
		s.set_unit_vector(b3);
	}
}

// Build the multipole matrix from a sample array
Eigen::MatrixXd Multipole3D::multipole_matrix(SampleVector& s_set) {

	// Check that the sample array has all attributes for multipole computations
	if (!s_set.get_values_enable() || !s_set.get_coord_enable() || !s_set.get_vector_enable())
		throw MFTError("Multipole3D::multipole_matrix", "SampleVector not suitable", 0);
	
	// Get the number of samples 
	size_t len = s_set.get_size();
	if (!len)
		throw MFTError("Multipole3D::multipole_matrix", "Empty SampleVector");

	// Get the reference frame attached to the sample set
	CoordinateSystem cs_set;
	s_set.get_coordinate_system(cs_set);

	// Reference radius
	vector<double> ref;
	get_ref(ref);
	double r0 = ref[0];

	// Misc. initializations
	Eigen::Vector3d v3, v3_multi;
	size_t l;
	Eigen::Vector3d axis_x, axis_y;
	Eigen::Vector3d axis_x_set, axis_y_set, axis_z_set;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	cs_set.get_axis_x(axis_x_set);
	cs_set.get_axis_y(axis_y_set);
	cs_set.get_axis_z(axis_z_set);

	// Number of rows in the matrix
	// One row if projected values, else three rows (one for each component)
	size_t rows = len;
	for (unsigned int k = 0; k < len; k++)
		if (!s_set.is_proj(k))
			rows += 2; 

	// Compute the multipole matrix
	//coef.resize(degree); 
	size_t n_coefs = get_coef_size();

	Eigen::MatrixXd m(rows, n_coefs);

	l = 0;

	for (size_t i = 0; i < len; i++) {

		Sample s_i;
		size_t idx = 0;

		// Sample i
		s_set.get_sample(i, s_i);

		// Fill the matrix
		// Add one row if the sample is a projection
		for (size_t j = 0; j < coef.size(); j++) 
			for (size_t k = 0; k < coef[j].size(); k++){

				field(s_i, 1, j, k);

				if (s_i.is_proj()){
					// The  projected field value is the matrix coefficient
					// TO BE CHECKED'
					double val = s_i.get_value();
					if (!isnan(val))
						m(l, idx) = val;
					else
						m(l, idx) = 0;
				}
				else{
					// Store the 3 vector components 
					// TO BE CHECKED
					vector<double> bl = s_i.get_vector_values();
					if (!isnan(bl[0]))
						m(l, idx) = bl[0];
					else
						m(l, idx) = 0;
					
					if (!isnan(bl[1]))
						m(l + 1, idx) = bl[1];
					else
						m(l + 1, idx) = 0;

					if (!isnan(bl[2]))
						m(l + 2, idx) = bl[2];
					else
						m(l + 2, idx) = 0;

				}
				idx ++;
			}

		if (s_i.is_proj())
			l++;
		else
			l += 3;
	}

	return m;
}

// Build the multipole matrix from a sample array
Eigen::MatrixXd Multipole3D::multipole_matrix(vector<SampleVector*> vs_set) {

	// Concatenate all sample sets
	SampleVector s_set;
	for (SampleVector* s : vs_set)
		s_set.add(*s);

	// Compute and return the matrix
	return multipole_matrix(s_set);
}

// Compute the multipole matrix and return is as a vector
vector<vector<double>> Multipole3D::multipole_matrix_std(SampleVector& s_set){

	Eigen::MatrixXd m;
	m = multipole_matrix(s_set);

	vector<vector<double>> v;
	v.resize(m.rows());
	for (size_t k = 0; k < (size_t)m.rows(); k++)
		v[k].resize(m.cols());

	for (size_t k = 0; k < (size_t)m.rows(); k++)
		for (size_t l = 0; l < (size_t)m.cols(); l++)
			v[k][l] = m(k, l);

	return v;
}

// Initialize a Circular object from a sample set s_set
void Multipole3D::init_from_set(SampleVector& s_set) {

	// Set the model units
	unit_field = s_set.get_unit_sample();
	unit_distance = s_set.get_unit_distance();
	
	// Get the number of samples 
	size_t len = s_set.get_size();
	
	// Reference radius
	vector<double> ref;
	get_ref(ref);
	double r0 = ref[0];
	
	// Misc. initializations
	Eigen::Vector3d v3, v3_multi;
	size_t k = 0;

	// Initialize the matrix
	Eigen::MatrixXd m = multipole_matrix(s_set);

	// Field values
	Eigen::VectorXd b(m.rows());

	for (size_t i = 0; i < len; i++) {
		// Get the sample i
		Sample s_i;
		s_set.get_sample(i, s_i);

		if (!s_i.is_proj()) {

			vector<double> bk = s_i.get_vector_values();
			b(k) = bk[0];
			k++;
			b(k) = bk[1];
			k++;
			b(k) = bk[2];
		}
		else
			b(k) = s_i.get_value();
		
		k++;
	}

	Eigen::MatrixXd coef_tmp;
	try{
		//Eigen::MatrixXd coef_tmp = m.bdcSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(b);
		Eigen::MatrixXd coef_tmp = m.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);

		// coef.resize(degree);
		k = 0;
		for (size_t i = 0; i < coef.size(); i++){
			coef[i].resize(get_coef_size(i));
			for(size_t j = 0; j < get_coef_size(i); j++){
				coef[i][j] = coef_tmp(k);
				k++;
		}

		update_coef_idx();
	}
	}
	catch(...){
		throw MFTError("Multipole3D::init_from_set", "Cannot invert the multipole matrix");	
	}


	// Update the metadata
	Metadata s_md;
	s_set.get_metadata(s_md);
	metadata.set_comment("Built from " + s_md.get_label() + " SampleVector.");
	metadata.set_creation_time();
	metadata.set_modification_time();

}

// Find non null coefficient indices
void Multipole3D::update_coef_idx(){

	size_t n = coef.size();
	coef_idx.clear();
	pair<size_t, size_t> idx;
	for (size_t k = 0; k < n; k++)
		for (size_t l = 0; l < coef[k].size(); l++)
			if (abs(coef[k][l]) > coef_eps){
				idx.first = k;
				idx.second = l;
				coef_idx.push_back(idx);
			}
}

//Scale the coefficients by a scalar
void Multipole3D::scale(double d){

	size_t n = coef.size();
	for (size_t k = 0; k < n; k++){
		size_t m = coef[k].size();
		for (size_t l = 0; l < m; l++)
				coef[k][l] = d * coef[k][l];
	}
}
// ---------------------------------------------
// Spherical3D: spherical harmonics objects
// -------------------------------------------- -

// Set the degree of the model
void Spherical3D::set_degree(size_t d) { 
	degree = d; 
	coef.resize(d); 
	for (size_t k = 0; k < degree; k++)
		coef[k].resize(2 * k + 1);
}

// Number of coefficients
size_t Spherical3D::get_coef_size(){

	size_t n_coefs = 0;
	for (size_t k = 0; k < degree; k++){
		n_coefs += get_coef_size(k);
	}
	return n_coefs;
}

// Set the multipole coefficients
void Spherical3D::set_coef(const vector<vector<double>>& v) { 

	// Check the dimensions
	size_t d = v.size();
	for (size_t k = 0; k < d; k++)
		if (v[k].size() != 2 * k + 1)
			throw MFTError("Spherical3D::set_coef", "Coefficient vector not compatible with spherical harmonics", 0);

	// Set degree and coefs
	degree = d;	
	coef = v;
}

// Set the multipole coefficient
void Spherical3D::set_coef(size_t l, int m, double v){

	// Check the dimensions
	if (l >= degree) throw MFTError("Spherical3D::set_coef", "wrong l index", 0);
	if ((m > 2 * l + 1) || (-m > (int)l)) throw MFTError("Spherical3D::set_coef", "wrong m index", 0);

	// Treat negative l index
	size_t n;
	if (m >= 0) n = m;
	else n = l - m; 

	coef[l][n] = v;

} 

// Compute the potential
void Spherical3D::scalar_potential(Sample& sample, CoordinateSystem& cs) {

	// Reference radius
	double r0 = ref[0];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Coordinates in the Circular2D coordinate system
	Eigen::Vector3d point_proj;
	coord_system.proj_point(point_proj, coord);

	// Spherical coordinates
	double r = point_proj.norm(); // Radius vector
	double theta, phi;
	if (r == 0){
		theta = 0; 
		phi = 0;
	}
	else{
		theta = acos(point_proj(2) / r); // Colatitude
		phi = atan2(point_proj(1), point_proj(0)); // longitude
	}

	// Compute the scalar potential
	double a = 0;
	for (size_t l = 0; l < degree ; l++ ){
		// order 0
		double rl;
		if ((abs(r) > 0) && (l > 0)) 
			rl = pow((r / r0), l);
		else
			rl = 1;
		a += coef[l][0] * rl * sph_legendre(l, 0, theta);
		
		// Higher orders
		for (size_t m = 1; m <=l; m++ ){
			a += coef[l][m] * rl * sph_legendre(l, m, theta) * cos(m * phi);
			a += coef[l][l+m] * rl * sph_legendre(l, -(int)m, theta) * sin(m * phi);
		}
	}

	sample.set_value(a);
}

// Compute the scalar potential
void Spherical3D::scalar_potential(Sample& sample) {
	scalar_potential(sample, coord_system);
}

// Compute the potential
void Spherical3D::scalar_potential(Sample& sample, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) {

	// Reference radius
	double r0 = ref[0];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Coordinates in the Spheriral3D coordinate system
	Eigen::Vector3d point_proj;
	coord_system.proj_point(point_proj, coord);

	// Spherical coordinates
	double r = point_proj.norm(); // Radius vector
	double theta, phi;
	if (r == 0){
		theta = 0; 
		phi = 0;
	}
	else{
		theta = acos(point_proj(2) / r); // Colatitude
		phi = atan2(point_proj(1), point_proj(0)); // longitude
	}

	// Compute the scalar potential
	double a;

	// Check degree and order
	if ((l < 0) || (l >= degree))
		throw MFTError("Spherical3D::scalar_potential", "Wrong degree", 0);

	// Check degree and order
	if ((m < 0) || (l > 2 * degree))
		throw MFTError("Spherical3D::scalar_potential", "Wrong order", 0);

	if (l == 0)
		a = coef_lm * sph_legendre(l, 0, theta);
	
	else if (m <= l)
		a = coef_lm * pow((r / r0), l) *  sph_legendre(l, m, theta) * cos(m * phi);

	else
		a = coef_lm * pow((r / r0), l) *  sph_legendre(l, m, theta) * cos(m * phi);

	sample.set_value(a);
}

// Compute the scalar potential
void Spherical3D::scalar_potential(Sample& sample, double coef_lm, size_t l, size_t m) {
	scalar_potential(sample, coord_system, coef_lm, l, m);
}

// ---------------------------------------------
// Cylindrical3D: cylindrical harmonics objects
// ---------------------------------------------
// Set the degree of the model
void Cylindrical3D::set_degree(size_t d) { 
	degree = d; 
	coef.resize(2 * d + 1); 
	normalization_coef.resize(2 * d + 1); 
	for (size_t k = 0; k <= 2 * degree; k++){
		coef[k].resize(2 * order + 1);
		normalization_coef[k].resize(2 * order + 1);
	}
	compute_normalization_coef();
}

// Set the order of the model
void Cylindrical3D::set_order(size_t o) { 
	order = o; 
	for (size_t k = 0; k <= 2 * degree; k++){
		coef[k].resize(2 * order + 1);
		normalization_coef[k].resize(2 * order + 1);
	}
	compute_normalization_coef();
}

// Number of coefficients
size_t Cylindrical3D::get_coef_size(){
	return (2 * degree + 1) * (2 * order + 1);
}

// Set the multipole coefficients
void Cylindrical3D::set_coef(const vector<vector<double>>& v) { 

	// Check the dimensions
	size_t d = v.size();
	if (!d / 2)
		// This number should be odd
		throw MFTError("Cylindrical3D::set_coef", "Coefficient vector not compatible with cylindrical harmonics", 0);

	size_t o;
	if (v.size() > 0){
		o = v[0].size();
		if ( !o / 2)
			throw MFTError("Cylindrical3D::set_coef", "Coefficient vector not compatible with cylindrical harmonics", 0);
	}
	for (size_t k = 0; k < d; k++)
		if (v[k].size() != o)
			throw MFTError("Cylindrical3D::set_coef", "Coefficient vector not compatible with cylindrical harmonics", 0);

	// Set degree and coefs
	degree = d / 2;
	order = (o + 1)  / 2;	
	coef = v;

	// Update the list of indices
	update_coef_idx();

	// Normalization coefficients
	compute_normalization_coef();

}

// Set the multipole coefficient
void Cylindrical3D::set_coef(size_t l, size_t m, double v, bool update_list){

	// Check the dimensions
	if (l > 2 * degree) throw MFTError("Cylindrical3D::set_coef", "wrong l index", 0);
	if (m > 2 * order) throw MFTError("Cylindrical3D::set_coef", "wrong m index", 0);

	coef[l][m] = v;

	// Update the list of indices
	if (update_list)
		update_coef_idx();

} 

// Set the multipole coefficients to zero
void Cylindrical3D::set_coef_zero(){

	for (size_t k = 0; k <= 2 * degree; k++)
		for (size_t l = 0; l <= 2 * order; l ++)
			coef[k][l] = 0;

	coef_idx.clear();
} 

// Compute the potential
void Cylindrical3D::scalar_potential(Sample& sample, CoordinateSystem& cs) {

	// Reference radius
	double r0 = ref[0];
	double l0 = ref[1];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Coordinates
	double r = sqrt(coord(0) * coord(0) + coord(1) * coord(1)); // Radius vector
	double theta, z = coord(2);
	if (r == 0){
		theta = 0; 
	}
	else{
		theta = atan2(coord(1), coord(0)); 
	}

	// Compute the scalar potential
	double a = 0;

	size_t n_idx = coef_idx.size();
	for (size_t j = 0; j < n_idx; j++){

		// Get indices from the list
		size_t k, l;
		k = coef_idx[j].first;
		l = coef_idx[j].second;

		// Normalization coefficient
		double a0 = normalization_coef[k][l];

		// Longitudinal dipole and solenoidal field
		if (k == 0){
			if (l == 0)
				// a += coef[0][0] * z / l0;
				a += coef[0][0] * z / a0;
			else if (l <= order){
				//double a0 = l * pi / l0 * (cyl_bessel_i(- 1, 2 * pi * l * r0 / l0) + cyl_bessel_i(1, 2 * pi * l * r0 / l0));
				a += coef[0][l] * cyl_bessel_i(0, 2 * pi * l * r / l0) / a0 * sin(2 * pi * l * z / l0);
			}
			else{
				size_t l1 = l - order;
				//double a0 = l1 * pi / l0 * (cyl_bessel_i(- 1, 2 * pi * l1 * r0 / l0) + cyl_bessel_i(1, 2 * pi * l1 * r0 / l0));
				a += coef[0][l] * cyl_bessel_i(0, 2 * pi * l1 * r / l0) / a0 * cos(2 * pi * l1 * z / l0);
			}
		}

	 	// Multipoles
		else {

			size_t k1 = k;
			if (k > degree)
				k1 = k - degree;

			// Zero order (homogeneous along z)
			if (l == 0){
				double rk;
				if (abs(r) > 0)
					//rk = pow(r, k1) / (k1 * pow(r0, k1 - 1));		
					rk = pow(r, k1);
				else 
					rk = 1;

				if (k <= degree)
					a += coef[k][0] * rk * sin(k1 * theta) / a0;
				else
					a += coef[k][0] * rk * cos(k1 * theta) / a0;
			}

			// Higher orders
			else{
				size_t l1 = l;
				if (l > order)
					l1 = l - order;

				// Normalization coefficient
				// double a0 = l1 * pi / l0 * (cyl_bessel_i(k1 - 1, 2 * pi * l1 * r0 / l0) + cyl_bessel_i(k1 + 1, 2 * pi * l1 * r0 / l0));
				double cs_k, cs_l;
				if (k <= degree)
					cs_k = sin(k1 * theta);
				else
					cs_k = cos(k1 * theta);

				if (l <= order)
					cs_l = sin(2 * pi * l1 * z / l0);
				else
					cs_l = cos(2 * pi * l1 * z / l0);

				a += coef[k][l] * cyl_bessel_i(k1, 2 * pi * l1 * r / l0) / a0 * cs_k * cs_l;
			}

		}
	}

	sample.set_value(a);
}

// Compute the scalar potential
void Cylindrical3D::scalar_potential(Sample& sample) {
	scalar_potential(sample, coord_system);
}

// Compute the potential
void Cylindrical3D::scalar_potential(Sample& sample, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) {

	// Reference radius and length
	double r0 = ref[0];
	double l0 = ref[1];

	// Check the dimensions
	if (l > 2 * degree) throw MFTError("Cylindrical3D::scalar_potential", "wrong l index", 0);
	if (m > 2 * order) throw MFTError("Cylindrical3D::scalar_potential", "wrong m index", 0);

	// Normalization coefficient
	double a0 = normalization_coef[l][m];

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Coordinates
	double r = sqrt(coord(0) * coord(0) + coord(1) * coord(1)); // Radius vector
	double theta, z = coord(2);
	if (r == 0){
		theta = 0; 
	}
	else{
		theta = atan2(coord(1), coord(0)); 
	}

	// Compute the scalar potential
	double a;

	// Check degree and order
	if (l > 2 * degree)
		throw MFTError("Cylindrical3D::scalar_potential", "Wrong degree", 0);

	if (m > 2 * order)
		throw MFTError("Cylindrical3D::scalar_potential", "Wrong order", 0);

	double rl;

	size_t ld;
	size_t mo;

	ld = l;
	if (l > degree)
		ld = l - degree;

	mo = m;
	if (m > order)
		mo = m - order;

	if ((abs(r) > 0) && (ld > 0)) 
			//rl = pow(r, ld) / (ld * pow(r0, ld - 1));
			rl = pow(r, ld);
	else
		rl = 1;

	// Longitudinal dipole and solenoidal field
	if (l == 0){
		if (m == 0)
			a = coef_lm * z / a0;
		else if (m <= order){
			a = coef_lm * cyl_bessel_i(0, 2 * pi * mo * r / l0) / a0 * sin(2 * pi * mo * z / l0);
		}
		else{
			a = coef_lm * cyl_bessel_i(0, 2 * pi * mo * r / l0) / a0 * cos(2 * pi * mo * z / l0);
		}
	}

	else if (m == 0)
		if (l <= degree)
			a = rl * sin(ld * theta) / a0;
		else
			a = rl * cos(ld * theta) / a0;
	
	else if ((l <= degree) && (m <= order))
		a = coef_lm * cyl_bessel_i(ld, 2 * pi * mo * r / l0) * sin(ld * theta) * sin(2 * pi * mo * z / l0) / a0;

	else if ((l <= degree) && (m > order))
		a = coef_lm * cyl_bessel_i(ld, 2 * pi * mo * r / l0) * sin(ld * theta) * cos(2 * pi * mo * z / l0) / a0;

	else if ((l > degree) && (m <= order))
		a = coef_lm * cyl_bessel_i(ld, 2 * pi * mo * r / l0) * cos(ld * theta) * sin(2 * pi * mo * z / l0) / a0;
	else
		a = coef_lm * cyl_bessel_i(ld, 2 * pi * mo * r / l0) * cos(ld * theta) * cos(2 *pi * mo * z / l0) / a0;

	sample.set_value(a);
}

// Compute the scalar potential
void Cylindrical3D::scalar_potential(Sample& sample, double coef_lm, size_t l, size_t m) {
	scalar_potential(sample, coord_system, coef_lm, l, m);
}

// Normalization coefficients
void Cylindrical3D::compute_normalization_coef(){

	// Reference radius
	double r0 = ref[0];
	double l0 = ref[1];

	// Longitudinal dipole
	normalization_coef[0][0] = l0;

	// Solenoidal field
	for (size_t l = 1; l <= order; l++){
		double a0 = l * pi / l0 * (cyl_bessel_i(- 1, 2 * pi * l * r0 / l0) + cyl_bessel_i(1, 2 * pi * l * r0 / l0));
		normalization_coef[0][l] = a0;
		normalization_coef[0][l + order] = a0;
	}

	// 2D multipoles
	for (size_t k = 1; k <= degree; k++){
		double a0 = k * pow(r0, k - 1);
		normalization_coef[k][0] = a0;
		normalization_coef[k + degree][0] = a0;
	}

	// 3D Multipoles
	for (size_t l = 1; l <= order; l++)
		for (size_t k = 1; k <= degree; k++){
				double a0 = l * pi / l0 * (cyl_bessel_i(k - 1, 2 * pi * l * r0 / l0) + cyl_bessel_i(k + 1, 2 * pi * l * r0 / l0));
				normalization_coef[k][l] = a0;
				normalization_coef[k][l + order] = a0;
				normalization_coef[k + degree][l] = a0;
				normalization_coef[k + degree][l + order] = a0;
			}
}

vector<vector<double>> Cylindrical3D::get_normalization_coef_std(){

	vector<vector<double>> norm_coef;
	size_t N = normalization_coef.size();
	size_t M = normalization_coef[0].size();
	norm_coef.resize(N);
	for (size_t k = 0; k < N; k++){
		norm_coef[k].resize(M);
		for (size_t l = 0; l < M; l++)
			norm_coef[k][l] = normalization_coef[k][l];
	}
	return norm_coef;
}