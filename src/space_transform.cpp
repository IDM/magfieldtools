/*!
   \file		coord_system.cpp
   \brief		Space transform classes implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "space_transform.h"

// -------------------------------------------
// Space transforms
// -------------------------------------------
// --- Set and get
vector<double> SpaceTransform::get_axis() {
	vector<double> v(3);
	v[0] = axis(0);
	v[1] = axis(1);
	v[2] = axis(2);
	return v;
}

vector<double> SpaceTransform::get_centre() {
	vector<double> v(3);
	v[0] = centre(0);
	v[1] = centre(1);
	v[2] = centre(2);
	return v;
}

// --- Rotations ---
Rotation::Rotation(double a, const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Rotation::Rotation", "Axis vector must have 3 components");
	axis << v[0], v[1], v[2];
	transform_type = "rotation"; 
	centre << 0, 0, 0; 
	angle = a;
}

Rotation::Rotation(double a, const vector<double>& v, const vector<double>& o) {
	if (v.size() != 3)
		throw MFTError("Rotation::Rotation", "Axis avector must have 3 components");
	if (o.size() != 3)
		throw MFTError("Rotation::Rotation", "Origin vector must have 3 components");
	axis << v[0], v[1], v[2];
	centre << o[0], o[1], o[2];
	transform_type = "rotation";
	angle = a;
}

vector<double> Rotation::get_centre() {
	vector<double> v(3); 
	v[0] = centre(0);
	v[1] = centre(1);
	v[2] = centre(2);
	return v;
}

void Rotation::matrix(Eigen::Matrix3d& m, bool covariant) {
	// Normalize the axis
	Eigen::Vector3d axis_norm = axis;
	axis_norm /= axis.norm();
	// Compute the rotation matrix
	if (covariant)
		m = Eigen::AngleAxisd(angle, axis_norm);
	else
		m = Eigen::AngleAxisd(-angle, axis_norm);
}

// Apply a rotation to coordinates
void Rotation::transform_coordinates(Eigen::Vector3d& v) {
	Eigen::Matrix3d m;
	matrix(m, true);
	v = centre + m * (v - centre);
};

// Apply a rotation to coordinates
void Rotation::transform_coordinates(vector<double> & v) {
	if (v.size() != 3)
		throw MFTError("Rotation::transform_coordinates", "Coordinates vector must have 3 components");
	Eigen::Vector3d v3;
	v3 << v[0], v[1], v[2];
	transform_coordinates(v3);
	v[0] = v3(0);
	v[1] = v3(1);
	v[2] = v3(2);
};

// Apply to a covariant vector
void Rotation::transform_vector_cov(Eigen::Vector3d& v) {
	Eigen::Matrix3d m;
	matrix(m, true);
	v = m * v;
};

// Apply to a covariant vector
void Rotation::transform_vector_cov(vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Rotation::transform_vector_cov", "Vectors must have 3 components");
	Eigen::Vector3d v3;
	v3 << v[0], v[1], v[2];
	transform_vector_cov(v3);
	v[0] = v3(0);
	v[1] = v3(1);
	v[2] = v3(2);
};

// Apply to a contravariant vector
void Rotation::transform_vector_contrav(Eigen::Vector3d& v) {
	Eigen::Matrix3d m;
	matrix(m, false);
	v = m * v;
};

// Apply to a contravariant vector
void Rotation::transform_vector_contrav(vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Rotation::transform_vector_cov", "Vectors must have 3 components");
	Eigen::Vector3d v3;
	v3 << v[0], v[1], v[2];
	transform_vector_contrav(v3);
	v[0] = v3(0);
	v[1] = v3(1);
	v[2] = v3(2);
};

// --- Translations ---
// Apply a translation to coordinates
void Translation::transform_coordinates(Eigen::Vector3d& v) {
	v -= axis;
};

// Apply a translation to coordinates
void Translation::transform_coordinates(vector<double>& v) {
	v[0] -= axis(0);
	v[1] -= axis(1);
	v[2] -= axis(2);
};