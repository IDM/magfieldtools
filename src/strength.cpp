/*!
   \file		strength.cpp
   \brief		Strength class implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "strength.h"

// Configuration
void Strength::init() {

	set_values_enable(true);
	set_time_enable(false);
	set_coord_enable(false);
	set_vector_enable(false);

	clear();

	Metadata m;
	get_metadata(m);
	m.set_modification_time();
	m.set_comment("strengths");
	set_metadata(m);

}

// Get the strength k
Strength Strength::get(size_t k) {
	
	Strength s;
	s.resize(2);
	
	size_t n = size / 2;

	// Get the strengths at k
	vector<double> v(2);
	v[0] = value_array[k]; // Normal 
	v[1] = value_array[k + n]; // Skew 

	s.set_values(v);

	// Compute and set the unit the unit
	Unit u_s, u_d;
	get_unit_sample(u_s);
	get_unit_distance(u_d);
	for (size_t i = 0; i < k; i++)
		u_s.divide(u_d);

	//s.set_unit_sample(u_s);
	s.unit_sample = u_s;

	return s;

}


// Set
void Strength::set(const vector<double>& s_norm, const vector<double>& s_skew, Unit& u_s, Unit& u_d) {

	if (s_norm.size() != s_skew.size())
		throw MFTError("Strength::Strength", "Incompatible strengths dimensions");

	init();
	
	// Set the units
	unit_sample = u_s;
	unit_distance = u_d;

	// Set the strength values
	size_t n = s_norm.size();
	resize(2 * n);
	for (size_t k = 0; k < n; k++) {
		set_values(k, s_norm[k]);
		set_values(k + n, s_skew[k]);
	}

}