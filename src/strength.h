/*!
   \file		strength.h
   \brief		Strength class interface
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once

#include "sample_vector.h"

 // -----------------------------------------
 // Strengths
 // -----------------------------------------
 //! Strengths
 /*! 
 The strengths are defined as 
     1   d^n B
S = --- ------- 
     n!  d z^n

This class is derived from the SampleVector class. The vector values are
[0] Normal field (or integrated field)
[1] Normal field gradient (or integrated field gradient)
[2] Normal sextupole strength (or integrated sextupole strength)
...
[N] Skew field (or integrated field)
[N + 1] Skew field gradient (or integrated field gradient)
...

The sample unit is for the field (or integrated field)
 */
class Strength : public SampleVector {
public:
	Strength() { init(); }; //!< Default constructor
	
	//! Constructor
	/*! 
	@param v vector of normal strengths 
	@param v vector of skew strengths  
	@param u_sample unit of the first (dipolar) strength 
	@param u_length unit of length
	*/
	Strength(const vector<double>& s_norm, const vector<double>& s_skew, Unit& u_sample, Unit& u_length) { set(s_norm, s_skew, u_sample, u_length); };

	void init(); //!< Configurate the SampleVector for strengths

	//! Return the strength k
	Strength get(size_t k);

	//! Set the strengths
	/*! 
	@param v vector of normal strengths 
	@param v vector of skew strengths
	@param u_sample unit of the first (dipolar) strength
	@param u_length unit of length 
	*/
	void set(const vector<double>& s_norm, const vector<double>& s_skew, Unit& u_sample, Unit& u_lenght);

private:

};