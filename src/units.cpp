/*!
   \file		units.cpp
   \brief		Unit class implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "units.h"

using namespace std;

// ---------------------------------------------
// Units
// ---------------------------------------------
Unit::Unit() {
	string u = "m";
	init(u);

}
Unit::Unit(string s) {
	init(s);
}

void Unit::init(string name) {
	
	symbol_up.clear();
	symbol_down.clear();
	physical_quantity_up.clear();
	physical_quantity_down.clear();

	// --- Distance
	if (!name.compare("Meter") || !name.compare("meter") || !name.compare("m")) {
		physical_quantity = "distance";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("m");
		prefix = "";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Centimeter") || !name.compare("centimeter") || !name.compare("cm")) {
		physical_quantity = "distance";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("m");
		prefix = "c";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Millimeter") || !name.compare("millimeter") || !name.compare("mm")) {
		physical_quantity = "distance";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("m");
		prefix = "m";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Micrometer") || !name.compare("micrometer") || !name.compare("um")) {
		physical_quantity = "distance";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("m");
		prefix = "u";
		unit_to_si_ratio = 1;
	}
	// --- Time
	else if (!name.compare("Second") || !name.compare("second") || !name.compare("s")) {
		physical_quantity = "time";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("s");
		prefix = "";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Millisecond") || !name.compare("millisecond") || !name.compare("ms")) {
		physical_quantity = "time";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("s");
		prefix = "m";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Microsecond") || !name.compare("microsecond") || !name.compare("us")) {
		physical_quantity = "time";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("s");
		prefix = "u";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Nanosecond") || !name.compare("nanosecond") || !name.compare("ns")) {
		physical_quantity = "time";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("s");
		prefix = "n";
		unit_to_si_ratio = 1;
	}
	// --- Magnetic fiels units
	// Tesla
	else if (!name.compare("Tesla") || !name.compare("tesla") || !name.compare("T")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("T");
		prefix = "";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Millitesla") || !name.compare("millitesla") || !name.compare("mT")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("T");
		prefix = "m";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Microtesla") || !name.compare("microtesla") || !name.compare("uT")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("T");
		prefix = "u";
		unit_to_si_ratio = 1;
	}
	// Gauss
	else if (!name.compare("Gauss") || !name.compare("gauss") || !name.compare("G")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("G");
		prefix = "";
		unit_to_si_ratio = 1e-4;
	}
	else if (!name.compare("Milligauss") || !name.compare("milligauss") || !name.compare("mG")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("G");
		prefix = "m";
		unit_to_si_ratio = 1e-4;
	}
	else if (!name.compare("Kilogauss") || !name.compare("kilogauss") || !name.compare("kG")) {
		physical_quantity = "magnetic field";
		physical_quantity_up.push_back(physical_quantity);
		symbol_up.push_back("G");
		prefix = "k";
		unit_to_si_ratio = 1e-4;
	}
	// --- Integrated magnetic field units
	// Tesla x meter
	else if (!name.compare("Tesla x meter") || !name.compare("tesla x meter") || !name.compare("Tm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("T");
		symbol_up.push_back("m");
		prefix = "";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Tesla x millimeter") || !name.compare("tesla x millimeter") || !name.compare("Tmm")
		|| !name.compare("Millitesla x meter") || !name.compare("millitesla x meter") || !name.compare("mTm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("T");
		symbol_up.push_back("m");
		prefix = "m";
		unit_to_si_ratio = 1;
	}
	else if (!name.compare("Tesla x micrometer") || !name.compare("tesla x micrometer") || !name.compare("Tum")
	|| !name.compare("Microtesla x meter") || !name.compare("microtesla x meter") || !name.compare("uTm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("T");
		symbol_up.push_back("m");
		prefix = "u";
		unit_to_si_ratio = 1;
	}
	// Gauss x meter
	else if (!name.compare("Gauss x meter") || !name.compare("gauss x meter") || !name.compare("Gm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("G");
		symbol_up.push_back("m");
		prefix = "";
		unit_to_si_ratio = 1e-4;
	}
	else if (!name.compare("Gauss x centimeter") || !name.compare("gauss x centimeter") || !name.compare("Gcm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("G");
		symbol_up.push_back("m");
		prefix = "c";
		unit_to_si_ratio = 1e-4;
	}
	else if (!name.compare("Gauss x millimeter") || !name.compare("gauss x millimeter") || !name.compare("Gmm")
	|| !name.compare("Milligauss x meter") || !name.compare("milligauss x meter") || !name.compare("mGm")) {
		physical_quantity = "magnetic field x distance";
		physical_quantity_up.push_back("magnetic field");
		physical_quantity_up.push_back("distance");
		symbol_up.push_back("G");
		symbol_up.push_back("m");
		prefix = "m";
		unit_to_si_ratio = 1e-4;
	}
	else if (!name.compare("1") || !name.compare("one") || !name.compare("One")) {
	physical_quantity = "1";
	physical_quantity_up.push_back("");
	symbol_up.push_back("");
	prefix = "";
	unit_to_si_ratio = 1;
	}
	else 
	throw MFTError(" Unit::init", "Unknown unit specification", 0);
}

void Unit::get_symbol(string& str_s) {

	/*str_s = prefix;
	for (string s : symbol_up)
		str_s += s + " ";
	for (string s : symbol_down)
		str_s += s + "^-1 ";*/

	// Lambda for counting the occurances of a symbol string in a vector<string>
	auto count_symbol = [](string symbol, vector<string> v) {
		size_t n = 0;
		for (string s : v)
			if (!symbol.compare(s))
				n++;
		return n;
	};

	size_t k = 0, count;
	string s;
	while (k < symbol_up.size()) {
		s = symbol_up[k];
		count = count_symbol(s, symbol_up);
		str_s += s;
		if (count > 1)
			str_s += "^" + to_string(count);
		str_s += " ";
		k += count;
	}
	
	k = 0;
	while (k < symbol_down.size()) {
		s = symbol_down[k];
		count = count_symbol(s, symbol_down);
		str_s += s;
		if (count > 1)
			str_s += "^-" + to_string(count);
		str_s += " ";
		k += count;
	}

	// Remove the space at the end
	if (str_s.size())
		str_s.pop_back();
}

double Unit::get_prefix_value() {
	double m = 1;
	if (!prefix.compare("f"))
		m = 1e-15;
	else if (!prefix.compare("p"))
		m = 1e-12;
	else if (!prefix.compare("n"))
		m = 1e-9;
	else if (!prefix.compare("u"))
		m = 1e-6;
	else if (!prefix.compare("m"))
		m = 1e-3;
	else if (!prefix.compare("c"))
		m = 1e-2;
	else if (!prefix.compare("d"))
		m = 0.1;
	else if (!prefix.compare(""))
		m = 1;
	else if (!prefix.compare("k"))
		m = 1000;
	else if (!prefix.compare("M"))
		m = 1e6;
	else if (!prefix.compare("G"))
		m = 1e9;
	return m;
}

string Unit::get_prefix_from_value(double d, double multiplier, bool extended) {
	
	string m;
	double md = d * multiplier;
	if (md == 1e-15)
		m = "f";
	else if (md == 1e-12)
		m = "p";
	else if (md == 1e-9)
		m = "n";
	else if (md == 1e-6)
		m = "u";
	else if (md == 1e-3)
		m = "m";
	else if (md == 1e-2 && extended)
		m = "c";
	else if (md == 1e-1 && extended)
		m = "d";
	else if (md == 1)
		m = "";
	else if (md == 1e1 && extended)
		m = "da";
	else if (md == 1e2 && extended)
		m = "h";
	else if (md == 1e3)
		m = "k";
	else if (md == 1e6)
		m = "M";
	else if (md == 1e9)
		m = "G";
	else if (md == 1e12)
		m = "T";
	else m = "-";

	return m;
}

double Unit::get_scale_factor_to_unit(Unit& u) {

	string physical_quantity_u;
	u.get_physical_quantity(physical_quantity_u);

	if (physical_quantity.compare(physical_quantity_u))
		throw MFTError("Unit::get_scale_factor_to_unit", "Incompatible units"); // Incompatible units

	return get_unit_to_si_ratio() / u.get_unit_to_si_ratio();
}

void Unit::copy(Unit& u) {
	physical_quantity = u.physical_quantity;
	symbol_up.clear();
	for (string s : u.symbol_up)
		symbol_up.push_back(s);
	for (string s : u.symbol_down)
		symbol_down.push_back(s);
	for (string s : u.physical_quantity_up)
		physical_quantity_up.push_back(s);
	for (string s : u.physical_quantity_down)
		physical_quantity_down.push_back(s);
	prefix = u.prefix;
	unit_to_si_ratio = u.unit_to_si_ratio;
}

void Unit::multiply(Unit& u) {

	string s;
	// New physical quantity
	for (string s : u.physical_quantity_up)
		physical_quantity_up.push_back(s);
	for (string s : u.physical_quantity_down)
		physical_quantity_down.push_back(s);

	// Add new symbols
	for (string s : u.symbol_up)
		symbol_up.push_back(s);
	for (string s : u.symbol_down)
		symbol_down.push_back(s);

	sort_symbols();
	update_physical_quantity();

	// Ratio to SI
	unit_to_si_ratio *= (u.get_unit_to_si_ratio() / u.get_prefix_value()) / get_prefix_value();
	
	// Prefix
	double p = get_prefix_value() * u.get_prefix_value();
	string m = get_prefix_from_value(p);
	if (!m.compare("-"))
		throw MFTError("Unit::multiply", "Unit prefix not found", 0);
	else
		prefix = m;
	
	simplify();
}

void Unit::divide(Unit& u) {

	string s;
	// New physical quantity
	for (string s : u.physical_quantity_up)
		physical_quantity_down.push_back(s);
	for (string s : u.physical_quantity_down)
		physical_quantity_up.push_back(s);

	// Add new symbols
	for (string s : u.symbol_down)
		symbol_up.push_back(s);
	for (string s : u.symbol_up)
		symbol_down.push_back(s);
	
	sort_symbols();
	update_physical_quantity();

	// Ratio to SI
	unit_to_si_ratio /= ((u.get_unit_to_si_ratio() / u.get_prefix_value()) / get_prefix_value());

	// Prefix
	double p = get_prefix_value() / u.get_prefix_value();
	string m = get_prefix_from_value(p);
	if (!m.compare("-"))
		throw MFTError("Unit::multiply", "Unit prefix not found", 0);
	else
		prefix = m;

	// Simplify
	simplify();
}

void Unit::sort_symbols() {
	
	vector<pair<string, size_t>> v;

	// --- Upper symbols
	for (size_t i = 0; i < symbol_up.size(); i++)
		v.push_back(make_pair(symbol_up[i], i));	

	// Define a lamba for comparing the points
	auto comp = [](pair<string, size_t> p0, pair<string, size_t> p1) { return (p0.first < p1.first); };

	// Sort 
	std::stable_sort(v.begin(), v.end(), comp);

	// Apply to the symbols and physical quantities
	vector<string> w;

	for (size_t k = 0; k < symbol_up.size(); k++) {
		symbol_up[k] = v[k].first;
		w.push_back(physical_quantity_up[v[k].second]);
	}
	physical_quantity_up.assign(w.begin(), w.end());
	
	// --- Lower symbols
	v.clear();
	for (size_t i = 0; i < symbol_down.size(); i++)
		v.push_back(make_pair(symbol_down[i], i));

	// Sort 
	std::stable_sort(v.begin(), v.end(), comp);

	// Apply to the symbols and physical quantities
	w.clear();
	for (size_t k = 0; k < symbol_down.size(); k++) {
		symbol_down[k] = v[k].first;
		w.push_back(physical_quantity_down[v[k].second]);
	}
	physical_quantity_down.assign(w.begin(), w.end());
}

// Simplify 
void Unit::simplify() {
	
	sort_symbols();

	string s_up;
	size_t count_up, count_down, k = 0;
	long int count;

	// Lambda for counting the occurances of a symbol string in a vector<string>
	auto count_symbol = [](string symbol, vector<string> v) {
		size_t n = 0;
		for (string s : v)
			if (!symbol.compare(s))
				n++;
		return n;
	};

	vector<string> new_symbol_up, new_symbol_down;
	vector<string> new_physical_quantity_up, new_physical_quantity_down;
	string s, q;
	while (k < symbol_up.size()) {
		// Count the occurances of this symbol in the symbol vectors
		s = symbol_up[k];
		q = physical_quantity_up[k];
		count_up = count_symbol(s, symbol_up);
		count_down = count_symbol(s, symbol_down);
		count = count_up - count_down;
		// Push back the difference up or down
		if (count > 0) 
			for (size_t n = 0; n < count; n++) {
				new_symbol_up.push_back(s);
				new_physical_quantity_up.push_back(q);
			}
		else
			for (size_t n = 0; n < -count; n++) {
				new_symbol_down.push_back(s);
				new_physical_quantity_down.push_back(q);
			}
		// Go to the next symbol
		k += count_up;
	}

	// Overwrite symbols and quantities
	symbol_up.clear();
	symbol_down.clear();
	physical_quantity_up.clear();
	physical_quantity_down.clear();

	for (k = 0; k < new_symbol_up.size(); k++) {
		symbol_up.push_back(new_symbol_up[k]);
		physical_quantity_up.push_back(new_symbol_up[k]);
	}

	for (k = 0; k < new_symbol_down.size(); k++) {
		symbol_down.push_back(new_symbol_down[k]);
		physical_quantity_down.push_back(new_symbol_down[k]);
	}

	sort_symbols();
}

// Update the physical quantity
void Unit::update_physical_quantity() {

	physical_quantity = "";

	if (!physical_quantity_up.size())
		physical_quantity = "1";
	else
		for (size_t i = 0; i < physical_quantity_up.size(); i++) {
			physical_quantity += physical_quantity_up[i];
			if (i < physical_quantity_up.size() - 1)
				physical_quantity += " x ";
		}
		
	if (physical_quantity_down.size())
		for (size_t i = 0; i < physical_quantity_down.size(); i++) {
			physical_quantity += " / ";
			physical_quantity += physical_quantity_down[i];
		}

}
