/*!
   \file		signature.cpp
   \brief		Signature class
   \author		Gael Le Bec
   \version		1
   \date		2022
   \copyright	GNU Public License.
 */

#pragma once

#include "signature.h"

using namespace std;

//--------------------------------------------
// Signature
//--------------------------------------------
// Move
void Signature::move(SpaceTransform* tr) { 
  coord_system.transform(tr); 
  if (tr->is_type("rotation"))
    ((Rotation*)tr)->transform_vector_contrav(vector_dir);
}

// Move
void Signature::move(vector<SpaceTransform*> tr) { 
  for (size_t k = 0; k < tr.size(); k++){
    coord_system.transform(tr[k]); 
    if (tr[k]->is_type("rotation"))
      ((Rotation*)tr[k])->transform_vector_contrav(vector_dir);
    }
}

// Compute the coordinate system
void Signature::compute_coord_system(SampleVector & s){

  // Get the coordinates
  vector<double> x, y, z;
  s.get_coordinates(x, y, z);

  // Compute and set the origin
  double x0 = 0, y0 = 0, z0 = 0;
    size_t n = s.get_size();
  for (size_t k = 0; k < n; k++){
    x0 += x[k];
    y0 += y[k];
    z0 += z[k];
  }
  vector<double> xyz0{x0 / n, y0 / n, z0 / n};
  coord_system.set_origin(xyz0);
  
  // Determine the axis
  Eigen::Vector3d u, v, w, vk;
  coord_system.get_axis_z(u);
  v << x[1] - x[0], y[1] - y[0], z[1] - z[0]; 
  w = u.cross(v);
  v /= v.norm();
  vector<double> vv(3);
  vector<double> vu(3);
  vector<double> vw(3);

  if (w.norm() > 1e-6){  
    // If the samples are not aligned with the z axis
    w /= w.norm();
    u = v.cross(w);
    u /= u.norm();
  }
  else{
    coord_system.get_axis_x(u);
    w = v.cross(u);
    w /= w.norm();
    u = v.cross(w);
    u /= u.norm();
  }

  vv[0] = v(0); vv[2] =  v(1); vv[2] = v(2);
  vu[0] = u(0); vu[1] = u(1); vu[2] = u(2); 
  vw[0] = w(0); vw[1] =  w(1); vw[2] = w(2);

  coord_system.set_axis_x(vv);
  coord_system.set_axis_y(vw);
  coord_system.set_axis_z(vu);
}

// Compute the range from the interpolator values
void Signature::compute_range(){

  if (values_interpolator.is_empty())
      throw MFTError("Signature::compute_range", "The interpolattors are not initialized", 0);

  // Get the set of points points
  vector<vector<double>> p;
  values_interpolator.get_points(p);
  size_t n = p.size();

  // Set the range
  range.first = p[0][0];
  range.second = p[n - 1][0];

}

// Field at Sample location
void Signature::field(Sample& s){

  // Create a sample vector 
  SampleVector sv;
  sv.push_back(s);

  // Compute the field and update the sample
  field(sv);
  sv.get_sample(0, s);
}

//--------------------------------------------
// Signature2D
//--------------------------------------------
Signature2D::Signature2D(){
  set_model_type("signature_2d");
}

Signature2D::Signature2D(SampleVector & s, SampleVector & ds, double c){
  set_model_type("signature_2d");
  init_from_set(s, ds, c);
}

// Initialize from a sample set
void Signature2D::init_from_set(SampleVector & s, SampleVector & ds, double c){
  // Check the number of points
  size_t n = s.get_size();
  if (n < 3)
    throw MFTError("Signature2D::init_from_set", "At least three points are needed", 0);

  if (n != ds.get_size())
    throw MFTError("Signature2D::init_from_set", "s and ds must have the same number of points", 0);
  
  // Coordinate system
  compute_coord_system(s);

  vector<double> x, y, z;
  s.get_coordinates(x, y, z);
  Eigen::Vector3d v, vk;
  v << x[1] - x[0], y[1] - y[0], z[1] - z[0]; 

  // Are the points aligned?
  for (size_t k = 1; k < n; k++){
    vk << x[k] - x[k - 1], y[k] - y[k - 1], z[k] - z[k - 1];
    if (v.cross(vk).norm() > 1e-3)
      throw MFTError("Signature2D::init_from_set", "The points must be aligned", 0);
  }

  // Positions
  vector<double> xval(n), yval(n), zval(n);
  vector<double> u3(3), v3(3);
  for (size_t k = 0; k < n; k++){
    u3[0] = x[k];
    u3[1] = y[k];
    u3[2] = z[k];
    v3 = coord_system.proj_point(u3);
    xval[k] = v3[0];
    yval[k] = v3[1];
    zval[k] = v3[2];
  }

  // Vector direction
  Eigen::Vector3d v_dir;
  s.get_unit_vector(0, v_dir);
  vector_dir = v_dir;

  if ((isnan(vector_dir(0)))||(isnan(vector_dir(1)))||(isnan(vector_dir(2)))||(!vector_dir.norm()))
    vector_dir << 0, 1, 0;

  // Units
  s.get_unit_distance(unit_distance);
  s.get_unit_sample(unit_field);

  // Interpolators
  vector<double> val, dval;
  s.get_values(val);
  ds.get_values(dval);    
  values_interpolator.set_points(xval, val);
  derivatives_interpolator.set_points(xval, dval);

  // Range
  compute_range();

  // Signature coefficient
  coef = c;

}

// Initialize from a sample set
void Signature2D::init_from_set(SampleVector& s){

  SampleVector ds;
  ds.resize(s.get_size());
  vector<double> zero(s.get_size());
  ds.set_values(zero);

  init_from_set(s, ds, 1);
}

// Compute the field
void Signature2D::field(SampleVector & s){

  size_t n = s.get_size();

  // Coordinates
  vector<double> zero(0);
  vector<double> & cx = zero, cy = zero, cz = zero;
  s.get_coordinates(cx, cy, cz);

  // Origin
  Eigen::Vector3d o;
  coord_system.get_origin(o);
  double x0 = o(0), y0 = o(1), z0 = o(2);

  // Positions
  vector<double> u(3), v(3), xval(n), yval(n);
  for (size_t k = 0; k < n; k++){
    u[0] = cx[k];
    u[1] = cy[k];
    u[2] = cz[k];
    v = coord_system.proj_point(u);
    xval[k] = v[0];
    yval[k] = v[1];
  }
  
  // Interpolation
  double val;
  Eigen::Vector3d vk;
  for (size_t k = 0; k < n; k++){

    // Field value
    val = coef * (values_interpolator.spline(xval[k], false) 
        + derivatives_interpolator.spline(xval[k], false) * yval[k]);

    // Projection
    if (s.is_proj(k)){
      s.get_unit_vector(k, vk);
      val *= vector_dir.dot(vk);
    }

    // Set the value
    s.set_values(k, val);
  }

}

// Compute the field
void Signature2D::field(Sample & s){

  SampleVector sv;
  sv.resize(1);
  sv.set_sample(0, s);
  
  field(sv);
  sv.get_sample(0, s);
}

// Set the field units
void Signature2D::set_unit_field(Unit& u){

  double f = unit_field.get_scale_factor_to_unit(u);
  
  // Get the coefficients for the values interpolator
  vector<double> coef_l;
  vector<vector<double>> coef_s;
  values_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] *= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] *= l;

  // Get the coefficients for the derivative interpolator
  derivatives_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] *= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] *= l;

  unit_field.copy(u);

}

// Set the distance units
void Signature2D::set_unit_distance(Unit& u){

  double f = unit_distance.get_scale_factor_to_unit(u);
  
  // Get the coefficients
  vector<vector<double>> p;
  values_interpolator.get_points(p);
  for (size_t k = 0; k < p.size(); k++)
    for (size_t l = 0; l < p[k].size(); k++)
      p[k][l] *= l;

  derivatives_interpolator.get_points(p);
    for (size_t k = 0; k < p.size(); k++)
      for (size_t l = 0; l < p[k].size(); k++)
        p[k][l] *= l;

        
  vector<double> coef_l;
  vector<vector<double>> coef_s;
  derivatives_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] /= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] /= l;

  unit_field.copy(u);

}

// Scale the signature coefficients
void Signature2D::scale(double d){

  vector<double> coef_lin;
  vector<vector<double>> coef_sp;
  
  // Values interpolator
  values_interpolator.get_coefs(coef_lin, coef_sp);
  
  // Linear coefs
  for (size_t k = 0; k < coef_lin.size(); k++)
    coef_lin[k] = d * coef_lin[k];

  // Spline coefs
  for (size_t k = 0; k < coef_sp.size(); k++)
    for (size_t l = 0; l < coef_sp[k].size(); l++)
      coef_sp[k][l] = d * coef_sp[k][l];

  // Derivatives interpolator
  derivatives_interpolator.get_coefs(coef_lin, coef_sp);
  
  // Linear coefs
  for (size_t k = 0; k < coef_lin.size(); k++)
    coef_lin[k] = d * coef_lin[k];

  // Spline coefs
   for (size_t k = 0; k < coef_sp.size(); k++)
    for (size_t l = 0; l < coef_sp[k].size(); l++)
      coef_sp[k][l] = d * coef_sp[k][l];

}

//--------------------------------------------
// Signature3D
//--------------------------------------------
Signature3D::Signature3D(){
  set_model_type("signature_3d");
}

// Initialize from a sample set
void Signature3D::init_from_set(SampleVector & s, SampleVector & ds_dy, SampleVector & ds_dz, double c){
  // Check the number of points
  size_t n = s.get_size();
  if (n < 3)
    throw MFTError("Signature3D::init_from_set", "At least three points are needed", 0);

  if (n != ds_dy.get_size())
    throw MFTError("Signature3D::init_from_set", "s and ds/dy must have the same number of points", 0);
  
  if (n != ds_dz.get_size())
    throw MFTError("Signature3D::init_from_set", "s and ds/dz must have the same number of points", 0);
 
  // Coordinate system
  compute_coord_system(s);

  vector<double> x, y, z;
  s.get_coordinates(x, y, z);
  Eigen::Vector3d v, vk;
  v << x[1] - x[0], y[1] - y[0], z[1] - z[0]; 

  // Are the points aligned?
  for (size_t k = 1; k < n; k++){
    vk << x[k] - x[k - 1], y[k] - y[k - 1], z[k] - z[k - 1];
    if (v.cross(vk).norm() > 1e-3)
      throw MFTError("Signature3D::init_from_set", "The points must be aligned", 0);
  }

  // Positions
  vector<double> xval(n), yval(n), zval(n);
  vector<double> u3(3), v3(3);
  for (size_t k = 0; k < n; k++){
    u3[0] = x[k];
    u3[1] = y[k];
    u3[2] = z[k];
    v3 = coord_system.proj_point(u3);
    xval[k] = v3[0];
    yval[k] = v3[1];
    zval[k] = v3[2];
  }

  // Vector direction
  Eigen::Vector3d v_dir;
  s.get_unit_vector(0, v_dir);
  vector_dir = v_dir;

  if ((isnan(vector_dir(0)))||(isnan(vector_dir(1)))||(isnan(vector_dir(2)))||(!vector_dir.norm()))
    vector_dir << 0, 1, 0;

  // Units
  s.get_unit_distance(unit_distance);
  s.get_unit_sample(unit_field);

  // Interpolators
  vector<double> val, dval_dy, dval_dz;
  s.get_values(val);
  ds_dy.get_values(dval_dy);    
  ds_dz.get_values(dval_dz);    
  values_interpolator.set_points(xval, val);
  derivatives_interpolator.set_points(xval, dval_dy);
  derivatives_interpolator2.set_points(xval, dval_dz);

  // Range
  compute_range();

  // Signature coefficient
  coef = c;

}

// Initialize from a sample set
void Signature3D::init_from_set(SampleVector& s){

  SampleVector ds;
  ds.resize(s.get_size());
  vector<double> zero(s.get_size());
  ds.set_values(zero);

  init_from_set(s, ds, ds, 1);
}


// Constructor
Signature3D::Signature3D(SampleVector & s, SampleVector & ds_dy, SampleVector & ds_dz, double c){
  set_model_type("signature_3d");
  init_from_set(s, ds_dy, ds_dz, c);
}

// Compute the field
void Signature3D::field(SampleVector& s){

 size_t n = s.get_size();

  // Coordinates
  vector<double> zero(0);
  vector<double> & cx = zero, cy = zero, cz = zero;
  s.get_coordinates(cx, cy, cz);

  // Origin
  Eigen::Vector3d o;
  coord_system.get_origin(o);
  double x0 = o(0), y0 = o(1), z0 = o(2);

  // Positions
  vector<double> u(3), v(3), xval(n), yval(n), zval(n);
  for (size_t k = 0; k < n; k++){
    u[0] = cx[k];
    u[1] = cy[k];
    u[2] = cz[k];
    v = coord_system.proj_point(u);
    xval[k] = v[0];
    yval[k] = v[1];
    zval[k] = v[2];
  }
  
  // Interpolation
  double val;
  Eigen::Vector3d vk;
  for (size_t k = 0; k < n; k++){

    // Field value
    val = coef * (values_interpolator.spline(xval[k], false) 
        + derivatives_interpolator.spline(xval[k]) * yval[k]
        + derivatives_interpolator2.spline(xval[k]) * zval[k]);

    // Projection
    if (s.is_proj(k)){
      s.get_unit_vector(k, vk);
      val *= vector_dir.dot(vk);
    }

    // Set the value
    s.set_values(k, val);
  }
}

// Set the field units
void Signature3D::set_unit_field(Unit& u){

  double f = unit_field.get_scale_factor_to_unit(u);
  
  // Get the coefficients for the values interpolator
  vector<double> coef_l;
  vector<vector<double>> coef_s;
  values_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] *= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] *= l;

  // Get the coefficients for the derivative interpolator
  derivatives_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] *= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] *= l;

  // Get the coefficients for the 2nd derivative interpolator
  derivatives_interpolator2.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] *= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] *= l;

  unit_field.copy(u);

}

// Set the distance units
void Signature3D::set_unit_distance(Unit& u){

  double f = unit_distance.get_scale_factor_to_unit(u);
  
  // Get the coefficients
  vector<vector<double>> p;
  values_interpolator.get_points(p);
  for (size_t k = 0; k < p.size(); k++)
    for (size_t l = 0; l < p[k].size(); k++)
      p[k][l] *= l;

  derivatives_interpolator.get_points(p);
    for (size_t k = 0; k < p.size(); k++)
      for (size_t l = 0; l < p[k].size(); k++)
        p[k][l] *= l;

  derivatives_interpolator2.get_points(p);
    for (size_t k = 0; k < p.size(); k++)
      for (size_t l = 0; l < p[k].size(); k++)
        p[k][l] *= l;
        
  vector<double> coef_l;
  vector<vector<double>> coef_s;
  derivatives_interpolator.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] /= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] /= l;

  derivatives_interpolator2.get_coefs(coef_l, coef_s);

  // Update the coefs
  for (size_t k = 0; k < coef_l.size(); k++)
    coef_l[k] /= f;

  for (size_t k = 0; k < coef_s.size(); k++)
    for (size_t l = 0; l < coef_s[k].size(); k++)
      coef_s[k][l] /= l;

  unit_field.copy(u);

}

// Scale the signature coefficients
void Signature3D::scale(double d){

  vector<double> coef_lin;
  vector<vector<double>> coef_sp;
  
  // Values interpolator
  values_interpolator.get_coefs(coef_lin, coef_sp);
  
  // Linear coefs
  for (size_t k = 0; k < coef_lin.size(); k++)
    coef_lin[k] = d * coef_lin[k];

  // Spline coefs
  for (size_t k = 0; k < coef_sp.size(); k++)
    for (size_t l = 0; l < coef_sp[k].size(); l++)
      coef_sp[k][l] = d * coef_sp[k][l];

  // Derivatives interpolator
  derivatives_interpolator.get_coefs(coef_lin, coef_sp);
  
  // Linear coefs
  for (size_t k = 0; k < coef_lin.size(); k++)
    coef_lin[k] = d * coef_lin[k];

  // Spline coefs
   for (size_t k = 0; k < coef_sp.size(); k++)
    for (size_t l = 0; l < coef_sp[k].size(); l++)
      coef_sp[k][l] = d * coef_sp[k][l];

  // Derivatives interpolator #2
  derivatives_interpolator2.get_coefs(coef_lin, coef_sp);
  
  // Linear coefs
  for (size_t k = 0; k < coef_lin.size(); k++)
    coef_lin[k] = d * coef_lin[k];

  // Spline coefs
   for (size_t k = 0; k < coef_sp.size(); k++)
    for (size_t l = 0; l < coef_sp[k].size(); l++)
      coef_sp[k][l] = d * coef_sp[k][l];

}

