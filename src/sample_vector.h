/*!
   \file		sample_vector.h
   \brief		SampleVector class
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <sample.h>


class MagFieldModel;

 // -----------------------------------------
 // Sample vectors 
 // -----------------------------------------
 //! Vectors of Samples
 /*! 
 SampleVectors are sets of samples. The sample attributes are stored in C++ vectors. A SampleVector has the following attributes:
 - C++ vectors of doubles for
 	- Values
 	- Time
 	- X, Y and Z coordinates
 	- VX, VY and VZ components of unit vectors
 - a C++ vector of bools indicating the projection state of samples
 - Units for the samples physical values, distances and time
 - a size
 - booleans indicating the SampleVector configuration:
   - a boolean indicating if the SampleVector contains values (false for e.g. trajectories)
   - a boolean indicating if it contains time stamps
   - a boolean indicating if it contains coordinates (false for e.g. currents)
   - a boolean indicating that the sample has vector values (false for scalar samples)
 The C++ vectors are initialized accordingly.
 - a coordinate system
 - Metadata
 @see Sample
 @see Unit
 @see CoordinateSystem
 */
class SampleVector {

public:
		
	//! SampleVector constructor
	SampleVector() { init_units(); clear(); metadata.set_creation_time(); }; /*!< Default constructor*/

	//! SampleVector copy constructor
	SampleVector(const SampleVector & s);

	//! SampleVector constructor.
	/*! 
	@param n size 
	*/
	SampleVector(size_t n) { init_units(); config(true, true, true, true, n); metadata.set_creation_time(); }

	//! SampleVector constructor
	/*! 
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.) 
	*/
	SampleVector(string u[3]) : SampleVector() { init_units(u); };

	//! SampleVector constructor
	/*! 
	@param n size 
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.) 
	*/
	SampleVector(size_t n, string u[3]) : SampleVector(n) { init_units(u); };

	//! SampleVector constructor
	/*! 
	@param u units string vector [sample, distance, time ] ({"T"}, {"G", "mm"}, {"T", "m", "s"} etc.) 
	*/
	SampleVector(const vector<string>& u);

	//! SampleVector constructor.
	/*! 
	@param n size
	@param values_on a bool enableling the sample values
	@param  time_on a bool enableling the time stamps
	@param coord_on a bool enableling the coordinates
	@param vect_on a bool enableling vector samples 
	*/
	SampleVector(size_t n, bool val_on, bool time_on, bool coord_on, bool vect_on) {
		init_units(); config(val_on, time_on, coord_on, vect_on, n); metadata.set_creation_time();
	}

	//!	SampleVector constructor.
	/*!	
	@param val vector of sample values
	@param xyz vector of triplets of coordinates
	@param vect_xyz vector of triplets of unit vectors
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param p a bool indicating the projection state of the samples (true: projection, false: vector) 
	*/
	SampleVector(vector< double>& val, vector<double[3]>& xyz, vector<double[3]>& vect_xyz, string u[3], bool p);

	//!	SampleVector constructor.
	/*!	
	@param val vector of sample values
	@param xyz vector of triplets of coordinates
	@param vect_xyz vector of triplets of unit vectors
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param p a bool indicating the projection state of the samples (true: projection, false: vector)
	@param cs Coordinate system for the sample positions and field components 
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double[3]>& vect_xyz, string u[3], bool p,
		CoordinateSystem& cs)
		: SampleVector(val, xyz, vect_xyz, u, p) {
		coord_system = cs;
	};

	//!	SampleVector constructor.
	/*!	
	@param val vector of sample values
	@param xyz vector of triplets of coordinates
	@param vect_xyz vector of triplets of unit vectors
	@param t vector of time stamps
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param p a bool indicating the projection state of the samples (true: projection, false: vector) 
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double[3]>& vect_xyz, vector<double> t, string u[3], bool p)
		: SampleVector(val, xyz, vect_xyz, u, p) {
		size_t s_val = val.size();
		if (s_val == t.size()) {
			time_array.resize(s_val);
			time_enable = true;
			for (size_t i = 0; i < s_val; i++)
				time_array[i] = t[i];
		}
	};

	//! SampleVector constructor.
	/*!	
	@param val vector of sample values
	@param xyz vector of triplets of coordinates
	@param vect_xyz vector of triplets of unit vectors
	@param t vector of time stamps
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param p a bool indicating the projection state of the samples (true: projection, false: vector)
	@param cs Coordinate system
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double[3]>& vect_xyz, vector<double> t, string u[3], bool p,
		CoordinateSystem& cs)
		: SampleVector(val, xyz, vect_xyz, t, u, p) {
		coord_system = cs;
	};

	//! SampleVector constructor for scalar samples.
	/*!	
	@param val Vector of sample values
	@param xyz Vector of triplets of coordinate
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, string u[3]);

	//!	SampleVector constructor for scalar samples.
	/*! 
	@param val Vector of sample values
	@param xyz Vector of triplets of coordinates
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param cs Coordinate system for the sample positions and field components 
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, string u[3], CoordinateSystem& cs)
		: SampleVector(val, xyz, u) {
		coord_system = cs;
	};

	//! SampleVector constructor for scalar samples.
	/*! 
	@param val Vector of sample values
	@param xyz Vector of triplets of coordinates
	@param t Vector of time stamps
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.) 
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double> t, string u[3])
		: SampleVector(val, xyz, u) {
		size_t s_val = val.size();
		if (s_val == t.size()) {
			time_array.resize(s_val);
			time_enable = true;
			for (size_t i = 0; i < s_val; i++)
				time_array[i] = t[i];
		}
	};

	//! SampleVector constructor for scalar samples.
	/*! 
	@param val Vector of sample values
	@param xyz Vector of triplets of coordinates
	@param t Vector of time stamps
	@param u units string array [sample, distance, time ] ({"T", "m", "s"} etc.)
	@param cs Coordinate system
	*/
	SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double> t, string u[3], CoordinateSystem& cs)
		: SampleVector(val, xyz, t, u) {
		coord_system = cs;
	};

	void init_units(); /*!< Initialize units*/

	//! Initialize units.
	/*! 
	@param s Array of strings for [sample, distance, time ] (e.g. {"T", "m", "s"} etc.)
	*/
	void init_units(string s[3]);

	//! Set the values of the samples
	/*! 
	@param val Vector of sample valus
	@warning The size of val must be equal to the size of the SampleVector. 
	*/
	void set_values(const vector<double>& val);

	//! Set the kth value of the samples
	/*! 
	@param k index
	@param val value 
	*/
	void set_values(size_t k, double val);

	//! Set the values of the samples
	/*! 
	@param val Pointer to an array of values
	@warning The size of val must be equal to the size of the SampleVector (the size of the array is not checked). 
	*/
	void set_values(double* val);

	//! Set the values of the samples
	/*!
	@param val value of all the samples
	*/
	void set_values(double val);

	//! Set all values to zero
	void zeros() { set_values(0.); };

	//! Set all values to ones
	void ones() { set_values(1.); };

	//! Get values of the samples.
	/*! 
	@param[out] val Vector of values 
	*/
	void get_values(vector<double>& val);

	//! Get the values of the samples.
	/*! 
	@return a vector of values 
	*/
	vector<double> get_values();

	//! Get values of a vector component.
	/*! 
	@param[in] s specification of the vector component ("x", "y" or "z")
	@param[out] val Vector of values 
	*/
	void get_values(string s, vector<double>& val);

	//! Get values of a vector component.
	/*! 
	@param s  specification of the vector component ("x", "y" or "z")
	@return Vector of values 
	*/
	vector<double> get_values(string s);

	//! Get the value of a sample
	/*! 
	@param k Index of the sample
	@return a value 
	*/
	double get_values(size_t k) { return value_array[k]; }

	//! Get the vector values of a sample
	/*! 
	@param[in] k Index of the sample
	@param[out] v a triplet of vector components 
	*/
	void get_vector_values(size_t k, vector<double>& v);

	//! Get the vector values of a sample
	/*! 
	@param k Index of the sample
	@return a triplet of vector components 
	*/
	vector<double> get_vector_values(size_t k);

	//! Get values of a vector component (same as get_values()).
	/*! 
	@param s specification of the vector component ("x", "y" or "z")
	@return Vector of values 
	*/
	vector<double> get_vector_values(string s) { return get_values(s); }

	//! Set vector values for sample k
	/*! 
	@param k sample index
	@param v vector values of sample k
	*/
	void set_vector_values_ref(size_t k, vector<double> & v);

	//! Set vector values for sample k
	/*! 
	@param k sample index
	@param v vector values of sample k
	*/
	void set_vector_values(size_t k, vector<double> v) { set_vector_values_ref(k, v); };

	//! Set vector values 
	/*! 
	@param vx x vector values 
	@param vy y vector values 
	@param vz z vector values 
	*/
	void set_vector_values_ref(vector<double> & vx, vector<double> & vy, vector<double> & vz);

	//! Set vector values 
	/*! 
	@param vx x vector values 
	@param vy y vector values 
	@param vz z vector values 
	*/
	void set_vector_values(vector<double> vx, vector<double> vy, vector<double> vz) {  set_vector_values_ref(vx, vy, vz); };

	//! Set the time from a vector.
	/*! 
	@param t Vector of time stamps
	@warning The size of val must be equal to the size of the SampleVector 
	*/
	void set_time_ref(const vector<double> & t);

	//! Set the time from a vector.
	/*! 
	@param t Vector of time stamps
	@warning The size of val must be equal to the size of the SampleVector 
	*/
	void set_time(const vector<double> & t) { set_time_ref(t); };

	//! Set the time of the kth sample.
	/*! 
	@param k sample index
	@param t Vector of time stamps
	@warning The size of val must be equal to the size of the SampleVector 
	*/
	void set_time(size_t k, double t);

	//! Set the time from a vector.
	/*! 
	@param t Pointer to an array of time stamps
	@warning The size of val must be equal to the size of the SampleVector (the size of the array is not checked). 
	*/
	void set_time(double* t);

	//! Get the time stamps
	/*! 
	@param[out] Vector of time stamps 
	*/
	void get_time(vector<double>& t) { t = time_array; }

	//! Get the time stamps
	/*! 
	@return Vector of time stamps 
	*/
	vector <double> get_time() { return time_array; }

	//! Get the time stamps
	/*! 
	@return Vector of time stamps 
	*/
	double get_time(size_t k);

	//! Set coordinates from a vector of triplets.
	/*! 
	@param c Vector of triplets of coordinates
	@warning The size of c must be equal to the size of the SampleVector
	*/
	void set_coordinates(vector<double[3]>& c);

	//! Set the coordinates of a given sample from an array.
	/*! 
	@param k Index of the sample
	@param c array of coordinates
	@warning The index must be smaller than the size of the SampleVector
	*/
	void set_coordinates(const size_t k, double c[3]);

	//! Set the coordinates of a given sample from an Eigen vector.
	/*! 
	@param k Index of the sample
	@param c Eigen vector of coordinates
	@warning The index must be smaller than the size of the SampleVector
	*/
	void set_coordinates(const size_t k, const Eigen::Vector3d& c);

	//! Set the coordinates of a given sample from a vector
	/*! 
	@param k Index of the sample
	@param c Vector of coordinates
	@warning The index must be smaller than the size of the SampleVector
	@warning The size of c must be 3
	*/
	void set_coordinates(const size_t k, const vector<double>& c);

	//! Set the coordinates of samples from vectors.
	/*! 
	@param x Vector of x coordinates
	@param y Vector of y coordinates
	@param z Vector of z coordinates
	@warning The size of x, y and z must be equal to the size of the SampleVector 
	*/
	void set_coordinates(const vector<double>& x, const vector<double>& y, const vector<double>& z);

	//! Set the coordinates of samples from vectors.
	/*! 
	@param s String "x", "y" or "z" specification of coordinates
	@param x Vector of x, y or z coordinates
	@warning The size of the vector must be equal to the size of the SampleVector, if its size is not null
	*/
	void set_coordinates(string s, const vector<double>& v);

	//! Set the coordinates of samples from arrays.
	/*! 
	@param x Pointer to an array of x coordinates
	@param y Pointer to an array of y coordinates
	@param z Pointer to an array of z coordinates
	@warning The size of x, y and z must be equal to the size of the SampleVector (the size of the arrays are not checked) 
	*/
	void set_coordinates(double* x, double* y, double* z);

	//! Set the coordinates of samples from a single array.
	/*! 
	@param xyz Pointer to an array of coordinates [x_0, ..., x_n-1, y_0, ..., y_n-1, z_0, ..., z_n-1]
	*/
	void set_coordinates(double* xyz);

	//! Get the coordinates.
	/*! 
	@param[out] x Vector of x coordinates
	@param[out] y Vector of y coordinates
	@param[out] z Vector of z coordinates 
	*/
	void get_coordinates(vector<double>& x, vector<double>& y, vector<double>& z);

	//! Get the coordinates.
	/*! 
	@param s specification for the coordinates ("x", "y", "z")
	@return Vector of coordinates 
	*/
	vector<double> get_coordinates(string s);

	//! Get the coordinates of a given sample.
	/*! 
	@param[in] k Index of sample
	@param[out] x coordinate
	@param[out] y coordinate
	@param[out] z coordinate 
	*/
	void get_coordinates(size_t k, double& x, double& y, double& z);

	//! Get the coordinates of a given sample.
	/*!
	@param[in] k Index of the sample
	@param[out] c Array of coordinates 
	*/
	void get_coordinates(size_t k, double c[3]);

	//! Get the coordinates of a given sample.
	/*! 
	@param[in] k Index of the sample
	@param[out] c coordinates
	*/
	void get_coordinates(size_t k, Eigen::Vector3d& c);

	//! Get the coordinates of a given sample
	/*! 
	@param k Index of the sample
	@return coordinates 
	*/
	vector<double> get_coordinates(size_t k);

	//! Set the projection states.
	/*! 
	@param p Vector of bools indicating the projection state of the samples (true: projection, false: vector) 
	*/
	void set_proj(const vector<bool>& p);

	//! Set the projection states.
	/*! 
	@param p Pointer to an array of bools indicating the projection state of the samples (true: projection, false: vector)
	@warning The size of p must be equal to the size of the SampleVector 
	*/
	void set_proj(bool* p);

	//! Set the projection states.
	/*! 
	@param p a bool indicating the projection state of all samples (true: projection, false: vector) 
	*/
	void set_proj(bool p);

	//! Set the projection states of sample k
	/*! 
	@param k Sample index
	@param p a bool indicating the projection state of all samples (true: projection, false: vector) 
	*/
	void set_proj(size_t k, bool p);

	//! Get the projection state/
	/*! 
	@param[out] Vector of projection states (true: projection, false: vector)
	*/
	void get_proj(vector<bool>& p) { p = proj_array; };

	//! Get the projection state/
	/*! 
	@param[out] Vector of projection states (true: projection, false: vector)
	*/
	vector<bool> get_proj() { return proj_array; };

	//! Set the unit vectors.
	/*! 
	@param v Vector of triplets of unit vector coordinates. The unit vectors are normalized.
	@warning the size of v must be equal to the size of the SampleVector 
	*/
	void set_unit_vector(vector<double[3]>& c);

	//! Set the unit vectors.
	/*! 
	@param vx projection on x
	@param vy projection on y
	@param vz projecton on z
	The unit vectors are normalized.
	*/
	void set_unit_vector(double vx, double vy, double vz);

	//! Set the unit vectors, and normalize them
	/*! 
	@param vx Vector of unit vector x coordinates.
	@param vy Vector of unit vector y coordinates.
	@param vz Vector of unit vectors z coordinates.
	@warning the size of v must be equal to the size of the SampleVector 
	*/
	void set_unit_vector(const vector<double>& vx, const vector<double>& vy, const vector<double>& vz);

	//! Set one component of the unit vectors
	/*! 
	@param s string specification for vector component ("x", "y" or "z")
	@param v Array of unit vector compoents. The unit vector is normalized. 
	*/
	void set_unit_vector(string s, const vector<double>& v);

	//! Set the unit vector of Sample k.
	/*! 
	@param k Index of the sample.
	@param v array of unit vector coordinates. The unit vector is normalized. 
	*/
	void set_unit_vector(const size_t k, double v[3]);

	//! Set the unit vector of Sample k.
	/*! 
	@param k Index of the sample.
	@param v Eigen vector of unit vector coordinates. The unit vector is normalized. 
	*/
	void set_unit_vector(const size_t k, const Eigen::Vector3d& v);

	//! Set the unit vector of Sample k.
	/*! 
	@param k Index of the sample.
	@param v Vector of unit vector coordinates. The unit vector is normalized. 
	*/
	void set_unit_vector(const size_t k, const vector<double>& v);

	// !Set the unit vectors. The unit vectors are normalized.
	/*! 
	@param vx Pointer to an array of unit vector x coordinates.
	@param vy Pointer to an array of unit vector y coordinates.
	@param vz Pointer to an array of unit vectors z coordinates.	
	@warning the sizes of vx, vy and vz must be equal to the size of the SampleVector 
	*/
	void set_unit_vector(double* vx, double* vy, double* vz);

	//! Set the unit vectors.
	/*! 
	@param vxyz Pointer to an array of unit vector coordinates [vx_0, ..., vx_n-1, vy_0, ..., vz_0, ...]. The unit vector is normalized. 
	*/
	void set_unit_vector(double* vxyz);

	void normalize_unit_vector(); //!< Normalize the unit vectors
	
	//! Get the unit vectors.
	/*!
	@param[out] vx Vector of unit vector x coordinates
	@param[out] vy Vector of unit vector y coordinates
	@param[out] vz Vector of unit vector z coordinates
	*/
	void get_unit_vector(vector<double>& vx, vector<double>& vy, vector<double>& vz);

	//! Get unit vector coordinates.
	/*! 
	@param s String specification of the coordinates ("x", "y" or "z")
	@return Vector of unit vector coordinates
	*/
	vector<double> get_unit_vector(string s);

	//! Get the unit vector of a given sample
	/*! 
	@param[in] k Index of the sample
	@param[out] v Array of vector coordinates 
	*/
	void get_unit_vector(size_t k, double c[3]);

	//! Get the unit vector of Sample k.
	/*! 
	@param[in] k Index of the sample.
	@param[out] v Eigen vector of unit vector coordinates. 
	*/
	void get_unit_vector(size_t k, Eigen::Vector3d& c);

	//! Get the unit vector of Sample k.
	/*! 
	@param k Index of the sample.
	@result v Vector of unit vector coordinates. 
	*/
	vector<double> get_unit_vector(size_t k);

	//! Configure the SampleVector
	/*! 
	@param values_on The value vector is initialized if true
	@param time_on The time stamp vector is initialized if true
	@param coord_on The coordinate vectors are initialized if true
	@param vector_on The unit vectors are initialized if true
	@param n the size of the SampleVector 
	*/
	void config(bool values_on, bool time_on, bool coord_on, bool vector_on, size_t n);

	//! Get the sample k.
	/*! 
	@param[in] Index of the sample
	@param[out] s Sample 
	*/
	void get_sample(size_t k, Sample& s);

	//! Get the sample k.
	/*! 
	@param Index of the sample
	@result the sample
	*/
	Sample get_sample(size_t k);

	//! Set the sample k.
	/*! 
	@param k Index of the sample
	@param s Sample 
	*/
	void set_sample(size_t k, Sample& s);

	//! Push back a sample to the set.
	/*! 
	@param s Sample 
	*/
	void push_back(Sample& s);

	//! Remove the last element of the set
	void pop_back();

	//! Erase the element k
	/*!
	@param k index of element to remove 
	*/
	void erase(size_t k);

	//! Add a SampleVector to this SampleVector.
	/*! 
	@param s SampleVector 
	*/
	void add(SampleVector& set_1);

	//! Resize the SampleVector.
	/*! 
	@param n New size 
	*/
	void resize(size_t n);

	//! Reserve space for the SampleVector.
	/*! 
	@param n size 
	*/
	void reserve(size_t n);

	size_t get_size() { return size; }; //!< Get the size of the SampleVector.

	void clear(); //!< Clear the SampleVector.

	//! Set value_enable.
	/*! 
	@param b value_enable bit (values initialized if true) 
	*/
	void set_values_enable(bool b) { values_enable = b; metadata.set_modification_time(); }

	//! Get value_enable.
	/*! 
	@return the value_enable bit (values initialized if true) 
	*/
	bool get_values_enable() { return values_enable; }

	//! Set time_enable.
	/*! 
	@param b time_enable bit (time stamps initialized if true) 
	*/
	void set_time_enable(bool b) { time_enable = b; metadata.set_modification_time(); }

	//! Get time_enable.
	/*! 
	@return the time_enable bit (values initialized if true) 
	*/
	bool get_time_enable() { return time_enable; }

	//! Set coord_enable.
	/*! 
	@param b coord_enable bit (coordinates initialized if true) 
	*/
	void set_coord_enable(bool b) { coord_enable = b; metadata.set_modification_time(); }

	//! Get coord_enable.
	/*!
	@return the coord_enable bit (coordinates initialized if true) 
	*/
	bool get_coord_enable() { return coord_enable; }

	//! Set vector_enable.
	/*! 
	@param b vector_enable bit (vectors initialized if true) 
	*/
	void set_vector_enable(bool b) { vector_enable = b; metadata.set_modification_time(); }

	//! Get vector_enable.
	/*! 
	@return the vector_enable bit (vectors initialized if true) 
	*/
	bool get_vector_enable() { return vector_enable; }

	//! Set the coordinate system.
	/*! 
	@param c CoordinateSystem 
	*/
	void set_coordinate_system(CoordinateSystem& c);

	//! Get the coordinate system.
	/*! 
	@param[out] c Coordinate system 
	*/
	void get_coordinate_system(CoordinateSystem& c);

	//! Get the coordinate system.
	/*! 
	@return Coordinate system 
	*/
	CoordinateSystem get_coordinate_system();

	//! Set the sample unit.
	/*! 
	@param u Unit
	@param b do not check the unit compatibility if true (default is false)
	*/
	void set_unit_sample(Unit& u, bool do_not_check=false);

	//! Set the distance unit.
	/*! 
	@param u Unit
	@param b do not check the unit compatibility if true (default is false)
	*/
	void set_unit_distance(Unit& u, bool do_not_check=false);

	//! Set the time unit.
	/*! 
	@param u Unit 
	@param b do not check the unit compatibility if true (default is false)
	*/
	void set_unit_time(Unit& u, bool do_not_check=false);

	//! Get the sample unit.
	/*! 
	@param[out] u Unit 
	*/
	void get_unit_sample(Unit& u) { u = unit_sample; };

	//! Get the sample unit.
	/*! 
	@return u Unit 
	*/
	Unit get_unit_sample() { return unit_sample; };

	//! Get the distance unit.
	/*! 
	@param[out] u Unit 
	*/
	void get_unit_distance(Unit& u) { u = unit_distance; };

	//! Get the distance unit.
	/*! 
	@return u Unit 
	*/
	Unit get_unit_distance() { return unit_distance; };

	//! Get the time unit.
	/*! 
	@param[out] u Unit 
	*/
	void get_unit_time(Unit& u) { u = unit_time; };

	//! Get the time unit.
	/*! 
	@return u Unit 
	*/
	Unit get_unit_time() { return unit_time; };

	//! Get the physical quantity from the sample unit.
	/*! 
	@param[out] s String filled with the physical quantity 
	*/
	void get_physical_quantity(string& s) { unit_sample.get_physical_quantity(s); }

	//! Get the physical quantity from the sample unit.
	/*! 
	@return String filled with the physical quantity 
	*/
	string get_physical_quantity() { return unit_sample.get_physical_quantity(); }

	//! Return the projection state of Sample k.
	/*! 
	@return the projection state (true if projected, else false) 
	*/
	bool is_proj(size_t k) { if (k < size) return proj_array[k]; throw MFTError("SampleVector::is_proj", "Index out of range", 0); }

	//! Set the metadata
	/*! 
	@param md Metadata 
	*/
	void set_metadata(const Metadata& d) { metadata = d; metadata.set_modification_time(); }

	//! Get the metadata
	/*! 
	@param[out] md Metadata 
	*/
	void get_metadata(Metadata& md) { md = metadata; }

	// Get the metadata
	/*! 
	@return the metadata 
	*/
	Metadata get_metadata() { return metadata; }

	//! Transform the coordinate system.
	/*! 
	@param tr Space transform 
	*/
	void transform_coordinate_system(SpaceTransform* tr);

	//! Apply a vector of space transformations (tr0, tr1, etc) to the coordinate system.
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void transform_coordinate_system(vector<SpaceTransform*> v_tr);

	//! Move the sample
	/*! 
	@param tr Space transform 
	*/
	void move(SpaceTransform* tr);

	//! Apply a vector of space transformations (tr0, tr1, etc) to the sample
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void move(vector<SpaceTransform*> v_tr);

	//! Build a set of null samples distributed on a circle
	/*!	
	@param r Radius
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system
	@param proj boolean defining the sample projection state
	@param cs Coordinate system defining the centre (origin) and orientation in space (in the plane defined by the first and secondary axes) 
	*/
	void samples_on_circle(double r, size_t n_samples, const char& c_xyzrt, bool proj, const CoordinateSystem& cs);

	//! Build a set of null samples distributed on a circle
	/*! 
	@param r Radius
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_circle(double r, size_t n_samples, const char& c_xyzrt, bool proj) { samples_on_circle(r, n_samples, c_xyzrt, proj, CoordinateSystem()); };

	//! Build a set of null samples distributed on a circle
	/*! 
	@param r Radius
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system 
	*/
	void samples_on_circle(double r, size_t n_samples, const char& c_xyzrt) { samples_on_circle(r, n_samples, c_xyzrt, false, CoordinateSystem()); };

	//! Build a set of null samples distributed on a circle
	/*! 
	@param r Radius
	@param n_samples Number of samples
	@param proj boolean defining the sample projection state
	*/
	void samples_on_circle(double r, size_t n_samples, bool proj) { samples_on_circle(r, n_samples, 'y', proj, CoordinateSystem()); };

	//! Build a set of null samples distributed on a disk
	/*!	
	@param r disk radius
	@param  n_samples_x number of samples on axis
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system
	@param proj boolean defining the sample projection state
	@param cs Coordinate system defining the centre(origin) and orientation in space(in the plane defined by the firstand secondary axes) 
	*/
	void samples_on_disk(double r, size_t n_samples_x, const char& c_xyz, bool proj, const CoordinateSystem& cs);

	//! Build a set of null samples distributed on a disk
	/*!	
	@param r disk radius
	@param n_samples_x number of samples on axis
	@param c_xyz String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system
	*/
	void samples_on_disk(double r, size_t n_samples_x, const char& c_xyz) { samples_on_disk(r, n_samples_x, c_xyz, false, CoordinateSystem()); };

	//! Build a set of null samples distributed on a disk
	/*!	
	@param r disk radius
	@param n_samples_x number of samples on axis
	@param c_xyz String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system
	@param proj boolean defining the sample projection state
	*/
	void samples_on_disk(double r, size_t n_samples_x, const char& c_xyz, bool proj) { samples_on_disk(r, n_samples_x, c_xyz, proj, CoordinateSystem()); };

	//! Build a set of null samples distributed on a disk
	/*!	
	@param r disk radius
	@param  n_samples_x number of samples on axis
	*/
	void samples_on_disk(double r, size_t n_samples_x) { samples_on_disk(r, n_samples_x, 'y', false, CoordinateSystem()); };

	//! Build a set of null samples distributed on a disk
	/*!	
	@param r disk radius
	@param  n_samples_x number of samples on axis
	@param proj boolean defining the sample projection state
	*/
	void samples_on_disk(double r, size_t n_samples_x, bool proj) { samples_on_disk(r, n_samples_x, 'y', proj, CoordinateSystem()); };

	//! Build a set of null samples distributed on an ellipse
	/*!	
	@param a semi-major axis
	@param b semi-minor axis
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system
	@param proj boolean defining the sample projection state
	@param cs Coordinate system defining the centre (origin) and orientation in space (in the plane defined by the first and secondary axes) 
	*/
	void samples_on_ellipse(double a, double b, size_t n_samples, const char& c_xyzr, bool proj, const CoordinateSystem& cs);

	//! Build a set of null samples distributed on an ellipse
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_ellipse(double a, double b, size_t n_samples, const char& c_xyzrt, bool proj) { samples_on_ellipse(a, b, n_samples, c_xyzrt, proj, CoordinateSystem()); };

	//! Build a set of null samples distributed on an ellipse
	/*! 
	@param a semi-major axis
	@param b semi-minor axis 
	@param n_samples Number of samples
	@param c_xyrt String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal, 'r' radial, 't' tangential in the cs coordinate system 
	*/
	void samples_on_ellipse(double a, double b, size_t n_samples, const char& c_xyzrt) { samples_on_ellipse(a, b, n_samples, c_xyzrt, false, CoordinateSystem()); };

	//! Build a set of null samples distributed on an ellipse
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param n_samples Number of samples
	@param proj boolean defining the sample projection state
	*/
	void samples_on_ellipse(double a, double b, size_t n_samples, bool proj) { samples_on_ellipse(a, b, n_samples, 'y', proj, CoordinateSystem()); };	

	//! Build a set of null projected samples distributed on an ellipse
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param n_samples Number of samples
	*/
	void samples_on_ellipse(double a, double b, size_t n_samples) { samples_on_ellipse(a, b, n_samples, 'y', true, CoordinateSystem()); };

	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param proj boolean defining the sample projection state
	@param cs Coordinate system defining the centre(origin) and orientation in space(in the plane defined by the firstand secondary axes) 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, const char& c_xyzr, bool proj, const CoordinateSystem& cs);
	
	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, const char& c_xyzr, bool proj) { samples_on_sphere(r, n_samples_theta, n_samples_phi, c_xyzr, proj, CoordinateSystem());};

	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param cs Coordinate system defining the centre(origin) and orientation in space(in the plane defined by the firstand secondary axes) 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, const char& c_xyzr, const CoordinateSystem& cs) { samples_on_sphere(r, n_samples_theta, n_samples_phi, c_xyzr, true, cs);};

	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, const char& c_xyzr) { samples_on_sphere(r, n_samples_theta, n_samples_phi, c_xyzr, true, CoordinateSystem());};

	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, bool proj) { samples_on_sphere(r, n_samples_theta, n_samples_phi, 'r', proj, CoordinateSystem());};

	//! Build a set of null samples distributed on a sphere
	/*!	
	@param r sphere radius
	@param n_samples_theta number of colatitude angles
	@param n_samples_phi number of longitude angles 
	*/
	void samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi) { samples_on_sphere(r, n_samples_theta, n_samples_phi, 'r', true, CoordinateSystem());};

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal angles
	@param n_samples_z number of longitudinal points
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param proj boolean defining the sample projection state
	@param cs Coordinate system defining the centre(origin) and orientation in space(in the plane defined by the firstand secondary axes) 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z, const char& c_xyzr, bool proj, const CoordinateSystem& cs);

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal points
	@param n_samples_z number of longitudinal points
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z, const char& c_xyzr, bool proj) { samples_on_cylinder(r, l, n_samples_theta, n_samples_z, c_xyzr, proj, CoordinateSystem());};

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal points
	@param n_samples_z number of longitudinal points
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial
	@param cs Coordinate system defining the centre(origin) and orientation in space(in the plane defined by the firstand secondary axes) 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z, const char& c_xyzr, const CoordinateSystem& cs) { samples_on_cylinder(r, l, n_samples_theta, n_samples_z, c_xyzr, true, cs);};

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal points
	@param n_samples_z number of longitudinal points
	@param c_xyzr String specification of the orientation of the field: 'x' horizontal, 'y' vertical, 'z' longitudinal in the cs coordinate system, 'r' radial 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z, const char& c_xyzr) { samples_on_cylinder(r, l, n_samples_theta, n_samples_z, c_xyzr, true, CoordinateSystem());};

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal points
	@param n_samples_z number of longitudinal points
	@param proj boolean defining the sample projection state 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z, bool proj) { samples_on_cylinder(r, l, n_samples_theta, n_samples_z, 'r', proj, CoordinateSystem());};

	//! Build a set of null samples distributed on a cylinder
	/*!	
	@param r cylinder radius
	@param l cylinder length
	@param n_samples_theta number of azimutal points
	@param n_samples_z number of longitudinal points 
	*/
	void samples_on_cylinder(double r, double l, size_t n_samples_theta, size_t n_samples_z) { samples_on_cylinder(r, l, n_samples_theta, n_samples_z, 'r', true, CoordinateSystem());};

	//! Build a set of null samples distributed on a rectangle
	/*!	
	@param x0 horizontal position of first corner
	@param y0 vertical position of first corner
	@param x1 horizontal position of second corner
	@param y1 vertical position of second corner
	@param nx number of horizontal samples 
	@param ny number of vertical samples 
	@param c_xyz vector direction specification
	@param proj projected values or not 
	@param cs Coordinate system 
	*/
	void samples_on_rectange(double x0, double y0, double x1, double y1, size_t nx, size_t ny, const char& c_xyzr, bool proj, const CoordinateSystem& cs);

	//! Build a set of null samples distributed on a rectangle
	/*!	
	@param x0 horizontal position of first corner
	@param y0 vertical position of first corner
	@param x1 horizontal position of second corner
	@param y1 vertical position of second corner
	@param nx number of horizontal samples 
	@param ny number of vertical samples 
	@param c_xyz vector direction specification
	@param proj projected values or not 
	*/
	void samples_on_rectange(double x0, double y0, double x1, double y1, size_t nx, size_t ny, const char& c_xyzr, bool proj) {
		samples_on_rectange(x0, y0, x1, y1, nx, ny, c_xyzr, proj, CoordinateSystem());	};

	//! Build a set of null samples distributed on a rectangle
	/*!	
	@param x0 horizontal position of first corner
	@param y0 vertical position of first corner
	@param x1 horizontal position of second corner
	@param y1 vertical position of second corner
	@param nx number of horizontal samples 
	@param ny number of vertical samples 
	@param c_xyz vector direction specification
	*/
	void samples_on_rectange(double x0, double y0, double x1, double y1, size_t nx, size_t ny, const char& c_xyzr) {
		samples_on_rectange(x0, y0, x1, y1, nx, ny, c_xyzr, true, CoordinateSystem());	};

	//! Build a set of null samples distributed on a rectangle
	/*!	
	@param x0 horizontal position of first corner
	@param y0 vertical position of first corner
	@param x1 horizontal position of second corner
	@param y1 vertical position of second corner
	@param nx number of horizontal samples 
	@param ny number of vertical samples 
	*/
	void samples_on_rectange(double x0, double y0, double x1, double y1, size_t nx, size_t ny) {
		samples_on_rectange(x0, y0, x1, y1, nx, ny, 'y', true, CoordinateSystem());	};

	//! Build samples on a racetrack from samples on a segment
	/*! 
	This function assumes that the samples are ordered and regularly sampled
	@param[out] res result
	@param l length of the segment
	@param r radius 
	@param amb ambient field model
	@param a build the arc above (true) or below the samples
	@param eps tolerance 
	*/
	void segment_to_racetrack(SampleVector & res, double l, double r, MagFieldModel & amb, bool above=true, double eps=1e-3);

	//! Build samples on a racetrack from samples on a segment
	/*! 
	This function assumes that the samples are ordered and regularly sampled
	@param[out] res result
	@param l length of the segment
	@param r radius 
	@param amb ambient field model
	@param a build the arc above (true) or below the samples
	@param eps tolerance 
	*/
	SampleVector segment_to_racetrack(double l, double r, MagFieldModel & amb, bool above=true, double eps=1e-3){
		SampleVector res; 
		segment_to_racetrack(res, l, r, amb, above, eps);
		return res;
	};

	//! Subtract a field to the sample
	/*!
	@param fmod field model
	*/
	void subtract_field(MagFieldModel & fmod);

	//! Build a SampleVector with symmetric samples with respect to sym
	/*! 
	@param[in] sym pointer to a symmetry 
	@param[out] s_vect symmetric sample vector 
	*/
	void symmetry(Symmetry* sym, SampleVector& s_vect);

	//! Build a SampleVector with symmetric samples with respect to sym
	/*! 
	@param sym pointer to a symmetry
	@return s_vect symmetric sample vector 
	*/
	SampleVector symmetry(Symmetry* sym) { SampleVector s_vect; symmetry(sym, s_vect); return s_vect; };

	//! Build a SampleVector with symmetric samples with respect to the symmetry plane 
	/*! 
	@param[in] sym pointer to a symmetry plane 
	@param[out] s_vect symmetric sample vector 
	*/
	void symmetry_plane(Symmetry* sym, SampleVector& s_vect);

	//! Build a SampleVector with symmetric samples with respect to the symmetry plane 
	/*! 
	@param sym pointer to a symmetry plane
	@return s_vect symmetric sample vector 
	*/
	SampleVector symmetry_plane(Symmetry* sym) { SampleVector s_vect; symmetry_plane(sym, s_vect); return s_vect; };

	//! Build a SampleVector with symmetric samples with respect to the centre of symmetry
	/*! 
	@param[in] sym pointer to a centre of symmetry 
	@param[out] s_vect symmetric sample vector
	*/
	void symmetry_centre(Symmetry* sym, SampleVector& s_vect);

	//! Build a SampleVector with symmetric samples with respect to the centre of symmetry
	/*! 
	@param sym pointer to a centre of symmetry
	@return s_vect symmetric sample vector
	*/
	SampleVector symmetry_centre(Symmetry* sym) { SampleVector s_vect; symmetry_centre(sym, s_vect); return s_vect; };

	//! Compute the mean value
	/*! 
	@return the average
	*/
	double mean_value();

	//! Subtract the mean value
	void subtract_mean_value();

	//! Compute the mean value of a component
	/*! 
	@param s component specification ("x", "y" or "z")
	@return the average 
	*/
	double mean_value(string s);

	//! Subtract the mean vector value
	void subtract_mean_vector_value();

	//! Subtract the mean normal value
	void subtract_mean_normal_value();

	//! Mean sample
	/*! 
	@param[out] s mean sample
	*/
	void mean(Sample& s);

	//! Build unit curvilinear vectors
	/*! 
	@param[out] vect_t Tangent unit vectors
	@param[out] vect_n Normal unit vectors
	@param[out] vect_b BiNormal unit vectors
	@param tol tolerance for aligned points
	*/
	void frenet_basis(SampleVector& vect_t, SampleVector& vect_n, SampleVector& vect_b,double tol=0.1);

	//! Return the tangent vectors
	/*! 
	@param tol tolerance for aligned points 
	@return a SampleVector with the tangent vectors 
	*/
	SampleVector tangent_vectors(double tol=0.1);

	//! Return the normal vectors
	/*! 
	@param tol tolerance for aligned points 
	@return a SampleVector with the normal vectors 
	*/
	SampleVector normal_vectors(double tol=0.1);

	//! Return the binormal vector
	/*! 
	@param tol tolerance for aligned points 
	@return a SampleVector with the binormal vectors 
	*/
	SampleVector binormal_vectors(double tol=0.1);

	//! Dot product of s and this
	/*! 
	@param[out] res dot product 
	@param s input 
	*/
	void dot(SampleVector& res, SampleVector& s);

	//! Dot product
	/*! 
	@param s other sample vector 
	@return the dot product 
	*/
	SampleVector dot(SampleVector& s);

	//! Cross product of s and this
	/*! 
	@param[out] res dot product 
	@param s input 
	*/
	void cross(SampleVector& res, SampleVector& s);

	//! Cross product
	/*! 
	@param s other sample vector 
	@return the cross product 
	*/
	SampleVector cross(SampleVector& s);

	//! Norm of this
	/*! 
	@param[out] res SampleVector with norm 
	*/
	void norm(SampleVector& res);

	//! Norm of this
	/*! 
	@return a SampleVector with norm 
	*/
	SampleVector norm();

	//! Integral of the normal component along a contour
	void integral_normal(SampleVector& s);

	//! Integral of the normal component along a contour
	SampleVector integral_normal();

	//! Integral of the tangent component along a contour
	void integral_tangent(SampleVector& s);

	//! Integral of the tangent component along a contour
	SampleVector integral_tangent();

	//! Integral of the binormal component along a contour
	void integral_binormal(SampleVector& s);

	//! Integral of the binormal component along a contour
	SampleVector integral_binormal();

	//! Integral of the scalar values along a contour
	void integral(SampleVector& s);

	//! Integral of the scalar values along a contour
	SampleVector integral();

	//! Set the close contour integral of normal component to zero
	void zero_integral_normal();

	//! Set the close contour integral of tangent component to zero
	void zero_integral_tangent();

	//! Set the close contour integral of binormal component to zero
	void zero_integral_binormal();

	//! Multiply the values of this SampleVector by a scalar
	/*
	@param d factor
	*/
	void times(double d);

protected:

	Unit unit_sample; // Sample unit
	Unit unit_distance; // Distance unit
	Unit unit_time; // Time unit

	vector<double> value_array;
	vector<double> time_array;
	vector<double> x_coord_array;
	vector<double> y_coord_array;
	vector<double> z_coord_array;
	vector<double> x_unit_vector_array;
	vector<double> y_unit_vector_array;
	vector<double> z_unit_vector_array;
	vector<bool> proj_array;

	size_t size = 0;

	bool values_enable = true; // Sample values enabled (true in most cases, except e.g. trajectories)
	bool time_enable = true; // Sample timing enabled
	bool coord_enable = true; // Coordinates enabled (true in most cases except e.g. current, temperature, etc.)
	bool vector_enable = true; // 3D vector values (true: vector / false: scalar)

	CoordinateSystem coord_system = CoordinateSystem(); // Main coordinate system
	Metadata metadata; // Metadata

};
