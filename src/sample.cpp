/*!
   \file		sample.cpp
   \brief		Sample, SampleVector, Acquisition parameter and MetaData classes implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "sample.h"

// ---------------------------------------------
// Sample
// ---------------------------------------------

// Constructor
Sample::Sample(double val, Eigen::Vector3d& c, Eigen::Vector3d& u) {
	value = val; 
	coordinates = c; 
	double n = u.norm();
	unit_vector = u / n;
}

// Constructor
Sample::Sample(double val, const vector<double>& c, const vector<double>& u) {
	if (c.size() != 3 || u.size() != 3)
		throw MFTError("Sample::Sample", "Vectors must have 3 components", 0);
	value = val;
	double n = sqrt(u[0] * u[0] + u[1] * u[1] + u[2]* u[2]);
	if (n > 0)
		for (int k = 0; k < 3; k++) {
			coordinates(k) = c[k];
			unit_vector(k) = u[k] / n;
		}
	else{
		unit_vector(0) = 0;
		unit_vector(1) = 0;
		unit_vector(2) = 1;
		for (int k = 0; k < 3; k++) {
			coordinates(k) = c[k];
		}
	}
	
}

// Set sample value
void Sample::set_value(double val) {
	value = val;
}

// Get sample value
double Sample::get_value() {
	return value;
}

// Set vector values
void Sample::set_vector_values(double vx, double vy, double vz){

	// Norm
	double n = sqrt(vx * vx + vy * vy + vz * vz);
	
	// Amplitude
	value = n;

	// Unit vector
	if (n > 0){
		unit_vector[0] = vx / n;
		unit_vector[1] = vy / n;
		unit_vector[2] = vz / n;
	}
	else{
		unit_vector[0] = 0;
		unit_vector[1] = 0;
		unit_vector[2] = 1;
	}
}

// Set vector values
void Sample::set_vector_values(vector<double>& v){

	if (v.size() != 3)
		throw MFTError("Sample::set_vector_values", "Vectors must have 3 components", 0);

	set_vector_values(v[0], v[1], v[2]);
}

// Get vector values
void Sample::get_vector_values(Eigen::Vector3d& v) {
	if (value == 0){
		v(0) = 0;
		v(1) = 0;
		v(2) = 0;
	}
	else
		v = value * unit_vector;
}


// Get vector value
vector<double> Sample::get_vector_values(){ 
	vector<double> v(3, 0); 
	v[0] = value * unit_vector[0]; 
	v[1] = value * unit_vector[1]; 
	v[2] = value * unit_vector[2]; 
	return v; 
};

// Get vector value
void Sample::get_vector_values(vector<double> v){ 
	v.resize(3); 
	if (value != 0){
		v[0] = value * unit_vector[0]; 
		v[1] = value * unit_vector[1]; 
		v[2] = value * unit_vector[2]; 
	}
	else{
		v[0] = 0;
		v[1] = 0;
		v[2] = 0;
	}
};

// Set sample coordinates
void Sample::set_coordinates(const Eigen::Vector3d& v) {
	for (int i = 0; i < 3; i++) coordinates(i) = v(i);
}

// Set sample coordinates
void Sample::set_coordinates(const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Sample::set_coordinates", "Coordinates vectors must have 3 components", 0);
	for (int i = 0; i < 3; i++) coordinates(i) = v[i];
}

// Set sample coordinates
void Sample::set_coordinates(const double v[3]) {
	for (int i = 0; i < 3; i++) coordinates(i) = v[i];
}

// Set sample coordinates
void Sample::set_coordinates(double x, double y, double z) {
	coordinates(0) = x;
	coordinates(1) = y;
	coordinates(2) = z;
}

// Get sample coordinates
void Sample::get_coordinates(Eigen::Vector3d& v) {
	v = coordinates;
}

// Get sample coordinates
void Sample::get_coordinates(double v[3]) {
	for (int i = 0; i < 3; i++) v[i] = coordinates(i);
}

// Get sample coordinates
void Sample::get_coordinates(double& x, double& y, double& z) {
	x = coordinates(0);
	y = coordinates(1);
	z = coordinates(2);
}

// Get sample coordinates
vector<double> Sample::get_coordinates() {
	vector<double> v(3);
	v[0] = coordinates(0);
	v[1] = coordinates(1);
	v[2] = coordinates(2);
	return v;
}

// Set sample vector direction
void Sample::set_unit_vector(const Eigen::Vector3d& v) {
	double n = v.norm();
	if (n > 0)
		for (int i = 0; i < 3; i++) unit_vector(i) = v(i) / n;
	else{
		unit_vector(0) = 0;
		unit_vector(1) = 0;
		unit_vector(2) = 1;
	}

}

// Set sample vector direction
void Sample::set_unit_vector(const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Sample::set_unit_vector", "Sample coordinates must have 3 components");
	double n = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	if (n > 0)
		for (int i = 0; i < 3; i++) unit_vector(i) = v[i] / n;
	else{
		unit_vector(0) = 0;
		unit_vector(1) = 0;
		unit_vector(2) = 1;
	}
}

// Set sample vector direction
void Sample::set_unit_vector(const double v[3]) {
	double n = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	if (n > 0)
		for (int i = 0; i < 3; i++) unit_vector(i) = v[i] / n;
	else{
		unit_vector(0) = 0;
		unit_vector(1) = 0;
		unit_vector(2) = 1;
	}
}

// Set sample vector direction
void Sample::set_unit_vector(double vx, double vy, double vz) {
	double n = sqrt(vx * vx + vy * vy + vz * vz);
	if (n > 0){
		unit_vector(0) = vx / n;
		unit_vector(1) = vy / n;
		unit_vector(2) = vz / n;
	}
	else{
		unit_vector(0) = 0;
		unit_vector(1) = 0;
		unit_vector(2) = 1;
	}
}

// Get sample vector direction
void Sample::get_unit_vector(Eigen::Vector3d& v) {
	v = unit_vector;
}

// Get sample vector direction
vector<double> Sample::get_unit_vector() {
	vector<double> v(3);
	v[0] = unit_vector[0];
	v[1] = unit_vector[1];
	v[2] = unit_vector[2];
	return v;
}

// Get sample vector direction
void Sample::get_unit_vector(double& vx, double& vy, double& vz) {
	vx = unit_vector(0);
	vy = unit_vector(1);
	vz = unit_vector(2);
}

// Get sample vector direction
void Sample::get_unit_vector(double v[3]) {
	for (int i = 0; i < 3; i++) v[i] = unit_vector(i);
}

// Transform the coordinates and unit vectors of the sample
void Sample::transform(SpaceTransform* tr) {
	
	if (tr->is_type("translation"))	
		translate(tr);
		
	else if (tr->is_type("rotation"))
		rotate(tr);
	
	else
		throw MFTError("Sample::transform", "Unknown transform specification", 0);
}

// Translate the sample
void Sample::translate(SpaceTransform* tr) {

	Eigen::Vector3d trans;
	tr->get_axis(trans);
	coordinates -= trans;
}


// Translate the sample using a pre-defined vector
void Sample::translate(Eigen::Vector3d& tr) {
	coordinates -= tr;
}

// Translate the sample using a pre-defined vector
void Sample::translate(vector<double>& tr) {
	coordinates(0) -= tr[0];
	coordinates(1) -= tr[1];
	coordinates(2) -= tr[2];
}

// Rotate the sample
void Sample::rotate(SpaceTransform* tr) {

	Rotation* rot = (Rotation*)tr;
	// Centre of the rotation
	Eigen::Vector3d centre;
	rot->get_centre(centre);
	// New position of the sample
	Eigen::Matrix3d m;
	rot->matrix(m, true);
	coordinates = centre + m * (coordinates - centre);
	// New pointing vector
	rot->matrix(m, false);
	unit_vector = m * unit_vector;
}

// Rotate the sample using pre-defined matrices and vectors
void Sample::rotate(Eigen::Vector3d& centre, Eigen::Matrix3d& m, Eigen::Matrix3d& im) {

	// New position of the sample
	coordinates = centre + m * (coordinates - centre);
	// New pointing vector
	unit_vector = m * unit_vector;
}

// Make a symmetric sample
void Sample::symmetry(Symmetry* sym, Sample& s) {

	if (sym->is_type("symmetry plane"))
		symmetry_plane(sym, s);
	else if (sym->is_type("symmetry centre"))
		symmetry_centre(sym, s);

	else
		throw MFTError("Sample::symmetry", "Unknown symmetry specification", 0);
}

// Make a symmetric sample
void Sample::symmetry_plane(Symmetry* sym, Sample& s) {

	if (!sym->is_type("symmetry plane"))
		throw MFTError("Sample::symmetry_plane", "Not a symmetry plane");

	SymmetryPlane* sym_plane = (SymmetryPlane*)sym;

	// --- Coordinate system
	// Build a vector (vx, vy, vz) in the symmetry plane
	double nx = 0, ny = 0, nz = 0;
	double vx, vy, vz;
	sym_plane->get_normal_vector(nx, ny, nz);

	if (nx != 0) {
		vy = 0;
		vz = 1;
		vx = -nz / nx;
	}
	else if (ny != 0) {
		vx = 0;
		vz = 1;
		vy = -nz / ny;
	}
	else if (nz != 0) {
		vy = 0;
		vx = 1;
		vz = -nx / nz;
	}

	else
		throw MFTError("Sample::symmetry_plane", "Null normal vector", 0);

	// Build a coordinate system attached to the symmetry plane
	Eigen::Vector3d v, n, c;
	n << nx, ny, nz;
	v << vx, vy, vz;
	sym->get_centre(c);
	CoordinateSystem cs(n, v, c);

	// --- New coordinates
	Eigen::Vector3d p_proj, new_coordinates;
	cs.proj_point(p_proj, coordinates); // Projection of the coordinates in the symmetry plane
	
	Eigen::Vector3d new_unit_vector, axis_x, axis_y, axis_z, o;
	cs.get_axis_x(axis_x);
	cs.get_axis_y(axis_y);
	cs.get_axis_z(axis_z);
	cs.get_origin(o);

	new_coordinates = o - p_proj(0) * axis_x + p_proj(1) * axis_y + p_proj(2) * axis_z;
	s.set_coordinates(new_coordinates);

	// --- new vector direction
	cs.proj_vector(p_proj, unit_vector);

	if (sym_plane->is_antisymmetric())
		new_unit_vector = -p_proj(0) * axis_x - p_proj(1) * axis_y - p_proj(2) * axis_z;
	else
		new_unit_vector = p_proj(1) * axis_y + p_proj(2) * axis_z - p_proj(0) * axis_x;

	s.set_unit_vector(new_unit_vector);

	// --- Other attributes
	s.set_value(value);
	s.set_proj(proj);
	s.set_time(t);

}

// Make a symmetric sample
void Sample::symmetry_centre(Symmetry* sym, Sample& s) {

	if (!sym->is_type("symmetry centre"))
		throw MFTError("Sample::symmetry_centre", "Not a symmetry centre");

	SymmetryCentre* sym_centre = (SymmetryCentre*)sym;

	// Centre of symmetry
	Eigen::Vector3d c;
	sym->get_centre(c);

	// --- New coordinates
	Eigen::Vector3d new_coordinates;
	new_coordinates = 2 * c - coordinates;
	s.set_coordinates(new_coordinates);

	// --- new vector direction
	Eigen::Vector3d new_unit_vector;
	new_unit_vector = - unit_vector;
	
	s.set_unit_vector(new_unit_vector);

	// --- Other attributes
	s.set_value(value);
	s.set_proj(proj);
	s.set_time(t);

}
