/*!
   \file		coord_system.cpp
   \brief		Coordinate system and space transform classes implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "coord_system.h"

// --------------------------------------------------
// Coordinate system
// --------------------------------------------------
// Constructor: 1st axis is v1, 2nd axis is v2 and origin is o
CoordinateSystem::CoordinateSystem(const Eigen::Vector3d& v1, const Eigen::Vector3d& v2, const Eigen::Vector3d& o) {
	axis_x = v1;
	axis_y = v2;
	origin = o;
	// Make the system orthonormal
	make_orthonormal();
}

CoordinateSystem::CoordinateSystem(const vector<double>& v1, const vector<double>& v2, const vector<double>& o) {
	// Check vector sizes
	if ((v1.size() != 3) || (v2.size() != 3) || (o.size() != 3))
		throw MFTError("CoordinateSystem::CoordinateSystem", "Vectors must have 3 components");
	// Parse
	axis_x << v1[0], v1[1], v1[2];
	axis_y << v2[0], v2[1], v2[2];
	origin << o[0], o[1], o[2];
	// Make the system orthonormal
	make_orthonormal();
}

CoordinateSystem::CoordinateSystem(const vector<double>& v1, const vector<double>& v2, const vector<double>& v3, const vector<double>& o) {
	// Check vector sizes
	if ((v1.size() != 3) || (v2.size() != 3) || (v3.size() != 3) || (o.size() != 3))
		throw MFTError("CoordinateSystem::CoordinateSystem", "Vectors must have 3 components");
	// Parse
	axis_x << v1[0], v1[1], v1[2];
	axis_y << v2[0], v2[1], v2[2];
	axis_z << v3[0], v3[1], v3[2];
	origin << o[0], o[1], o[2];
	set_ortho();
}


CoordinateSystem::CoordinateSystem(string s1, const vector<double>& v1, string s2, const vector<double>& v2, const vector<double>& o) {
	// Check vector sizes
	if ((v1.size() != 3) || (v2.size() != 3) || (o.size() != 3))
		throw MFTError("CoordinateSystem::CoordinateSystem", "Vectors must have 3 components");
	
	// Parse to Eigen vectors
	Eigen::Vector3d V1, V2, O;
	V1 << v1[0], v1[1], v1[2];
	V2 << v2[0], v2[1], v2[2];
	O << o[0], o[1], o[2];

	// Build the coordinate system
	init(s1, V1, s2, V2, O);

}

void CoordinateSystem::init(string s1, const Eigen::Vector3d& v1, string s2, const Eigen::Vector3d& v2, const Eigen::Vector3d& o) {

	// Primary axis
	if (!s1.compare("x") || !s1.compare("X"))
		axis_x = v1;
	else if (!s1.compare("y") || !s1.compare("Y"))
		axis_y = v1;
	else if (!s1.compare("z") || !s1.compare("Z"))
		axis_z = v1;
	else
		throw MFTError("CoordinateSystem::CoordinateSystem", "Unknown axis specification");

	// Secondary axis
	if (!s2.compare("x") || !s2.compare("X"))
		axis_x = v2;
	else if (!s2.compare("y") || !s2.compare("Y"))
		axis_y = v2;
	else if (!s2.compare("z") || !s2.compare("Z"))
		axis_z = v2;
	else
		throw MFTError("CoordinateSystem::CoordinateSystem", "Unknown axis specification");

	// Build an orthonormal system
	make_orthonormal(s1, s2);

	// Origin
	origin = o;

}


// Set axis
void CoordinateSystem::set_axis(string s, const Eigen::Vector3d& v) {

	if (!s.compare("x") || (!s.compare("X")))
		axis_x = v;
	else if (!s.compare("y") || (!s.compare("Y")))
		axis_y = v;
	else if (!s.compare("z") || (!s.compare("Z")))
		axis_z = v;
	else 
		throw MFTError("CoordinateSystem::set_axis", "Unknown axis specification", 0);
}

// Get axis
void CoordinateSystem::get_axis(string s, Eigen::Vector3d& v) {

	if (!s.compare("x") || (!s.compare("X")))
		v = axis_x;
	else if (!s.compare("y") || (!s.compare("Y")))
		v = axis_y;
	else if (!s.compare("z") || (!s.compare("Z")))
		v = axis_z;
	else
		throw MFTError("CoordinateSystem::get_axis", "Unknown axis specification", 0);
}

// Get axis
vector<double> CoordinateSystem::get_axis(string s) {

	vector<double> v(3);
	if (!s.compare("x") || (!s.compare("X")))
		for (int k = 0; k < 3; k++)
			v[k] = axis_x(k);
	else if (!s.compare("y") || (!s.compare("Y")))
		for (int k = 0; k < 3; k++)
			v[k] = axis_y(k);
	else if (!s.compare("z") || (!s.compare("Z")))
		for (int k = 0; k < 3; k++)
			v[k] = axis_z(k);
	else
		throw MFTError("CoordinateSystem::get_axis", "Unknown axis specification", 0);

	return v;
}

// Build an orthonormal axis. Error codes: 0: no error, 1: the first axis and the second axis are collinears or null vectors
void CoordinateSystem::make_orthonormal() {
	
	// x axis
	Eigen::Vector3d v1 = axis_x;
	axis_x = v1 / v1.norm();
	// z axis
	Eigen::Vector3d v2 = axis_x.cross(axis_y);
	axis_z = v2 / v2.norm();
	// y axis
	axis_y = axis_z.cross(axis_x);

	// Check the coordinate system
	ortho = false;
	if (axis_y.norm() == 0 || axis_z.norm() == 0) 
		throw MFTError("CoordinateSystem::make_orthogonal", "Cannot create orthogonal vector", 0);
	ortho = true;

}

void CoordinateSystem::make_orthonormal(string primary_axis, string secondary_axis) {

	if (!primary_axis.compare("x") || (!primary_axis.compare("X"))) {
		// Normalize the x axis
		axis_x = axis_x / axis_x.norm(); 
		if (!secondary_axis.compare("y") || (!secondary_axis.compare("Y"))) {
			// The z axis is orthogonal to x and y, and has unit norm
			axis_z = axis_x.cross(axis_y);
			axis_z = axis_z / axis_z.norm();
			// and the y axis is the product of the two others
			axis_y = axis_z.cross(axis_x);
		}
		else if (!secondary_axis.compare("z") || (!secondary_axis.compare("Z"))) {
			// The y axis is orthogonal to x and z, and has unit norm
			axis_y = axis_z.cross(axis_x);
			axis_y = axis_y / axis_y.norm();
			// and the z axis is the product of the two others
			axis_z = axis_x.cross(axis_y);
		}
		else 
			throw MFTError("CoordinateSystem::make_orthonormal", "Unknown axis specification");
	}
	else if (!primary_axis.compare("y") || (!primary_axis.compare("Y"))) {
		// Normalize the y axis
		axis_y = axis_y / axis_y.norm();
		if (!secondary_axis.compare("z") || (!secondary_axis.compare("Z"))) {
			// The x axis is orthogonal to y and z, and has unit norm
			axis_x = axis_y.cross(axis_z);
			axis_x = axis_x / axis_x.norm();
			// and the z axis is the product of the two others
			axis_z = axis_x.cross(axis_y);
		}
		else if (!secondary_axis.compare("x") || (!secondary_axis.compare("X"))) {
			// The z axis is orthogonal to x and y, and has unit norm
			axis_z = axis_x.cross(axis_y);
			axis_z = axis_z / axis_z.norm();
			// and the x axis is the product of the two others
			axis_x = axis_y.cross(axis_z);
		}
		else
			throw MFTError("CoordinateSystem::make_orthonormal", "Unknown axis specification");
	}
	else if (!primary_axis.compare("z") || (!primary_axis.compare("Z"))) {
		// Normalize the z axis
		axis_z = axis_z / axis_z.norm();
		if (!secondary_axis.compare("x") || (!secondary_axis.compare("X"))) {
			// The y axis is orthogonal to x and z, and has unit norm
			axis_y = axis_z.cross(axis_x);
			axis_y = axis_y / axis_y.norm();
			// and the y axis is the product of the two others
			axis_x = axis_y.cross(axis_z);
		}
		else if (!secondary_axis.compare("y") || (!secondary_axis.compare("Y"))) {
			// The x axis is orthogonal to y and z, and has unit norm
			axis_x = axis_y.cross(axis_z);
			axis_x = axis_x / axis_x.norm();
			// and the y axis is the product of the two others
			axis_y = axis_z.cross(axis_x);
		}
		else
			throw MFTError("CoordinateSystem::make_orthonormal", "Unknown axis specification");
	}
	else
		throw MFTError("CoordinateSystem::make_orthonormal", "Unknown axis specification");
	
	// Check the orthonormality
	set_ortho();
}

// Check if orthonormal
void CoordinateSystem::set_ortho() {

	ortho = false;
	if ((axis_x.norm() == 1) && (axis_y.norm() == 1) && (axis_z.norm() == 1))
		if (axis_x.cross(axis_y) == axis_z)
			ortho = true;
}

// Check if the coordinate system is the "world" one
bool CoordinateSystem::is_world() {

	Eigen::Vector3d vect;
	vect << 0, 0, 0;
	if (origin != vect) return false;
	vect << 1, 0, 0;
	if (axis_x != vect) return false;
	vect << 0, 1, 0;
	if (axis_y != vect) return false;
	vect << 0, 0, 1;
	if (axis_z != vect) return false;
	return true;
}

// Compute the components of v in the coordinate system and store the result in v_proj
void CoordinateSystem::proj_vector(Eigen::Vector3d& v_proj, Eigen::Vector3d& v) {
	double v1 = axis_x.dot(v);
	double v2 = axis_y.dot(v);
	double v3 = axis_z.dot(v);
	v_proj(0) = v1;
	v_proj(1) = v2;
	v_proj(2) = v3;
}

Eigen::Vector3d CoordinateSystem::proj_vector(Eigen::Vector3d& v) {
	Eigen::Vector3d v_proj;
	proj_vector(v_proj, v);
	return v_proj;
}

vector<double> CoordinateSystem::proj_vector(const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("CoordinateSystem::proj_vector", "Vectors must have 3 components");
	Eigen::Vector3d v_proj, v3;
	v3 << v[0], v[1], v[2];
	proj_vector(v_proj, v3);
	vector<double> vp{ v_proj(0) , v_proj(1) , v_proj(2) };
	return vp;
}

// Compute the coordinates of p in the coordinate system and store the result in p_proj
void CoordinateSystem::proj_point(Eigen::Vector3d& p_proj, Eigen::Vector3d &p) {
	
	Eigen::Vector3d op = p - origin;
	double x = axis_x.dot(op);
	double y = axis_y.dot(op);
	double z = axis_z.dot(op);
	p_proj(0) = x;
	p_proj(1) = y;
	p_proj(2) = z;
}

// Compute the coordinates of p in the coordinate system and store the result in p_proj
Eigen::Vector3d CoordinateSystem::proj_point(Eigen::Vector3d& p) {
	Eigen::Vector3d p_proj;
	proj_point(p_proj, p);
	return p_proj;
}

// Compute the coordinates of p in the coordinate system and store the result in p_proj
vector<double> CoordinateSystem::proj_point(vector<double>& p) {
	Eigen::Vector3d p_proj, p3;
	p3 << p[0], p[1], p[2];
	proj_point(p_proj, p3);
	vector<double> pp{ p_proj(0), p_proj(1), p_proj(2) };
	return pp;
}

// Compute the coordinates expressed in another system
void CoordinateSystem::compute_new_coordinates(Eigen::Vector3d& p_new, Eigen::Vector3d& p_old, CoordinateSystem& cs_old) {
	
	// Get the origin and the axes of the sample coordinate system
	Eigen::Vector3d cs_set_o, cs_set_first, cs_set_second, cs_set_third;
	cs_old.get_origin(cs_set_o);
	cs_old.get_axis_x(cs_set_first);
	cs_old.get_axis_y(cs_set_second);
	cs_old.get_axis_z(cs_set_third);
	
	Eigen::Vector3d v3;
	v3 = cs_set_o + cs_set_first * p_old(0) + cs_set_second * p_old(1) + cs_set_third * p_old(2);
	proj_point(p_new, v3);
}

// Compute the coordinates in another system
Eigen::Vector3d CoordinateSystem::compute_new_coordinates(Eigen::Vector3d& p_old, CoordinateSystem& cs_old) {

	Eigen::Vector3d p_new;
	compute_new_coordinates(p_new, p_old, cs_old);
	return p_new;
}

// Compute the coordinates in another system
vector<double> CoordinateSystem::compute_new_coordinates(vector<double>& p_old, CoordinateSystem& cs_old) {

	Eigen::Vector3d p_new, p_old3;
	p_old3 << p_old[0], p_old[1], p_old[2];
	compute_new_coordinates(p_new, p_old3, cs_old);
	vector<double> p{p_new[0], p_new[1], p_new[2]};
	return p;
}

// Apply a space transformation to the coordinate system
void CoordinateSystem::transform(SpaceTransform* tr) {

	// Translation
	if (tr->is_type("translation")) {
		Eigen::Vector3d trans;
		tr->get_axis(trans);
		origin += trans;
	}
	// Rotation
	else if (tr->is_type("rotation")) {
		
		Rotation* rot = (Rotation*)tr;
		// Centre
		Eigen::Vector3d centre;
		rot->get_centre(centre);
		// New position of the origin
		Eigen::Matrix3d m;
		rot->matrix(m, true);
		origin = centre + m * (origin - centre);
		// New axes
		rot->matrix(m, true);
		axis_x = m * axis_x;
		axis_y = m * axis_y;
		axis_z = m * axis_z;
		
	}
	// Other space transformations to be implemented later...
	else
		throw MFTError("CoordinateSystem::transform", "Unknown space transformation");

}

// Apply a space transformation to the coordinate system
void CoordinateSystem::transform(vector<SpaceTransform*> v_tr) {

	size_t n = v_tr.size();

	// Check the type of transformation before applying any transformation to the coordinate system
	for (size_t k = 0; k < n; k++) 
		if (!v_tr[k]->is_type("rotation") && !v_tr[k]->is_type("translation")) 
			throw MFTError("CoordinateSystem::transform", "Unknown space transformation");

	// Apply successive transformations
	for (size_t k = 0; k < n; k++)
		transform(v_tr[k]);

}
