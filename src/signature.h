/*!
   \file		signature.h
   \brief	  Signature2D class
   \author		Gael Le Bec
   \version		1
   \date		2022
   \copyright	GNU Public License.
 */

#pragma once

#include <string>
#include <vector>

#include "mag_field_model.h"
#include "sample_vector.h"
#include "interpolation.h"
#include "mft_errors.h"

using namespace std;

//! Signature class
/*! 
This virtual class is for signatures based on interplators. 
It is the mother class of Signature2D and Signature3D.
*/
class Signature : public MagFieldModel {
public:

  //! Set the coefficient
  void set_coef(double c) { coef = c; };

  //! Get the coefficient
  double get_coef() { return coef; };

  //! Set the coordinate system
  void set_coordinate_system(CoordinateSystem & c) { coord_system = c; };

  // Field
  void field(Sample& s, CoordinateSystem& cs) { 
    throw MFTError("Signature::field", "field(s, cs) not defined for Signature", 0);
  };

  //! Compute the field at the position of a Sample
  /*!
  @param s Sample
  @note this function should not be used to compute the field at several sample locations (use SampleVectors).
  */
  void field(Sample& s);

  virtual void field(SampleVector& s) = 0;
  // Potential
  void scalar_potential(Sample& s) {
    throw MFTError("Signature::scalar_potential", "scalar_potential(s) not defined for Signature", 0);
  };

  // Potential
  void scalar_potential(Sample& s, CoordinateSystem& cs) {
    throw MFTError("Signature::scalar_potential", "scalar_potential(s, cs) not defined for Signature", 0);
  }

  // Reference set
  void init_reference_set(SampleVector& s_set) {
    throw MFTError("Signature::init_reference_set", "init_reference_set(s) not defined for Signature", 0);
  }

  //! Move the object
  /*!
  @param tr space transform 
  */
  void move(SpaceTransform* tr);

  //! Apply a vector of space transformations (tr0, tr1, etc) to the multipole
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void move(vector<SpaceTransform*> v_tr);

  //! Transform the coordinate system and compute new multipole coefficients
	/*! 
	@param tr Space transform
	*/
	void transform_coordinate_system(SpaceTransform* tr) { move(tr); };

  //! Apply a vector of space transformations (tr0, tr1, etc) to the coordinate system and compute the coefficients in the new system
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void transform_coordinate_system(vector<SpaceTransform*> v_tr) { move(v_tr); }; 

  //! Get the vector direction
  void get_vector_dir(Eigen::Vector3d &v) { v = vector_dir; };

  //! Get the vector direction
  vector<double> get_vector_dir() { 
    vector<double> v(3); 
    v[0] = vector_dir(0);
    v[1] = vector_dir(1);
    v[2] = vector_dir(2); 
    return v;};

  //! Set the vector direction
  void set_vector_dir(double vx, double vy, double vz) { 
    vector_dir(0) = vx;
    vector_dir(1) = vy;
    vector_dir(2) = vz;
  }

  //! Get the values interpolator (load and save)
  Interpolation & get_values_interpolator() { return values_interpolator; };

  //! Get the derivatives interpolator (load and save)
  Interpolation & get_derivatives_interpolator() { return derivatives_interpolator; };

  //! Get the range 
  pair<double, double> get_range() { return range; };

protected:
  //! Compute and set the coordinate system
  void compute_coord_system(SampleVector & s); 

  //! Compute the range from the interpolator values
  void compute_range();

  Interpolation values_interpolator;
  Interpolation derivatives_interpolator;
  Eigen::Vector3d vector_dir;
  double coef = 1;
  pair<double, double> range;
};


//! Signature2D class 
/*! 
This class compute magnetic signatures based on interplators

Field values are computed from the field profile in one direction and its derivative.
For instance, the field integral Iz(x, z) is defined by
    Iz(x, z) = Iz(x, z0) + (z - z0) * dIz / dz

The orientation and the position of the model are defined by its coordinate system:
    - The position is set by the origin
    - The x axis defines the line defined by the initial samples
    - The z axis is defined in the initial samples, as the direction perpendicular to the 2D plane
    - The y axis is perpendicular to the two others 

An overall multiplicative coefficient is apply to the signature. 
*/

class Signature2D : public Signature {
public:

  //! Signature2D Default constructor
  Signature2D();

  //! Signature2D constructor
  /*!
  The coordinates, values and derivalrives are used to initialize the interpolators, 
    and the coordinate system defines the main plane and origin of the Signature.
  @param s signature 
  @param ds derivative of the signature 
  @param v_dir vector direction
  @param c coef
  */
  Signature2D(SampleVector & s, SampleVector & ds, double c=1);

  //! Signature2D constructor
  /*!
  The coordinates, values and derivalrives are used to initialize the interpolators, 
    and the coordinate system defines the main plane and origin of the Signature.
  @param s signature 
  @param ds derivative of the signature 
  @param v_dir vector direction
  @param c coef
  */
  Signature2D(SampleVector & s, SampleVector & ds, double v_dir[3], double c=1):
    Signature2D(s, ds, c) {
      vector_dir(0) = v_dir[0];
      vector_dir(1) = v_dir[1];
      vector_dir(2) = v_dir[2];
      vector_dir /= vector_dir.norm();
    };

  //! Approximate a field from this signature
  void field(SampleVector& s);

  //! Approximate a field from this signature
  void field(Sample& s);

  // -- Defines the virtual functions of the MagFiedModel class 
  
  //! Initialize from a sample set
  void init_from_set(SampleVector & s, SampleVector & ds, double c);
  
  //! Initialize from a sample set
  void init_from_set(SampleVector& s);

  //! Set the field units
  void set_unit_field(Unit& u); 

  //! Set the distance unit
  void set_unit_distance(Unit& u);

  //! Scale the coefficients by a scalar
  /*!
  @param f scale factor
  */
  void scale(double d);

private:

};

//! Signature3D class 
/*! 
This class compute magnetic signatures based on interplators

Field values are computed from the field profile in one direction and its derivative.
For instance, the field Bz(x, y, z) is defined by
    Bz(x, y, z) = Bz(x, y0, z0) + (y - y0) * dBz / dy + (z - z0) * dBz / dz 

The orientation and the position of the model are defined by its coordinate system:
    - The position is set by the origin
    - The x axis defines the line defined by the initial samples
    - The z axis is defined in the initial samples, as the direction perpendicular to the 2D plane
    - The y axis is perpendicular to the two others 

An overall multiplicative coefficient is apply to the signature. 
*/

class Signature3D : public Signature {

public:
  //! Signature3D Default constructor
  Signature3D();

  //! Signature3D constructor
  /*!
  The coordinates, values and derivalrives are used to initialize the interpolators, 
    and the coordinate system defines the main plane and origin of the Signature.
  @param s signature along the longitudinal axis
  @param ds_dy derivative of the signature vs the vertical axis
  @param ds_dz derivative of the signature vs the transverse horizontal axis
  @param c coef
  */
  Signature3D(SampleVector & s, SampleVector & ds_dy, SampleVector & ds_dz, double c=1);

  //! Signature3D constructor
  /*!
  The coordinates, values and derivalrives are used to initialize the interpolators, 
    and the coordinate system defines the main plane and origin of the Signature.
  @param s signature along the longitudinal axis
  @param ds_dy derivative of the signature vs the vertical axis
  @param ds_dz derivative of the signature vs the transverse horizontal axis
  @param v_dir vector direction
  @param c coef
  */
  Signature3D(SampleVector & s, SampleVector & ds_dy, SampleVector & ds_dz, double v_dir[3], double c=1):
    Signature3D(s, ds_dy, ds_dz, c) {
      vector_dir(0) = v_dir[0];
      vector_dir(1) = v_dir[1];
      vector_dir(2) = v_dir[2];
      vector_dir /= vector_dir.norm();
    };

  //! Initialize from sample sets
  /*!
  @param s signature along the longitudinal axis
  @param ds_dy derivative of the signature vs the vertical axis
  @param ds_dz derivative of the signature vs the transverse horizontal axis
  @param c coefficient
  */
  void init_from_set(SampleVector & sz, SampleVector & ds_dy, SampleVector & ds_dz, double c);

  //! Initialize from a sample set
  void init_from_set(SampleVector& s);

  //! Set the coefficient
  void set_coef(double c) { coef = c; };

  //! Get the coefficient
  double get_coef() { return coef; };

  //! Approximate a field from this signature
  void field(SampleVector& s);

  //! Set the field units
  void set_unit_field(Unit& u); 

  //! Set the distance unit
  void set_unit_distance(Unit& u);

  //! Scale the coefficients by a scalar
  /*!
  @param f scale factor
  */
  void scale(double d);

private:
  Interpolation derivatives_interpolator2;
};
