/* File : mag_field_tools.i */
%module mag_field_tools

namespace Eigen {
}

%{
#include "coord_system.h"
#include "units.h"
#include "sample.h"
#include "sample_vector.h"
#include "space_transform.h"
#include "symmetries.h"
#include "mft_errors.h"
#include "mag_field_model.h"
#include "multipole.h"
#include "strength.h"
#include "bem.h"
#include "signature.h"
%}

%include stl.i
%include std_string.i
%include <exception.i>
%include "mft_errors.h"

%include "typemaps.i"
%include "std_vector.i"
%include "std_complex.i"
%include "std_pair.i"
%include "std_string.i"

%ignore Eigen::Vector3d;
%ignore Eigen::Matrix3d;
%ignore Eigen::MatrixXd;

typedef long time_t;

/* instantiate the required template specializations */
%template(DoubleVector) std::vector<double>;
%template(CplxDoubleVector) std::vector<std::complex<double>>;
%template(DoubleMatrix) std::vector<std::vector<double>>;
%template(CplxDoubleMatrix) std::vector<std::vector<std::complex<double>>>;
%template(BoolVector) std::vector<bool>;
%template(SpaceTrVector) std::vector<SpaceTransform*>;
%template(SampleVecVector) std::vector<SampleVector*>;
%template(StringVector) std::vector<std::string>;
%template(DoublePair) std::pair<double, double>;
%template(DoublePairVector) std::vector<std::pair<double, double>>;

%exception {
  try {
    $action
  } catch (const exception& e) {
        SWIG_exception(SWIG_UnknownError, e.what());
  } 
}

%exceptionclass MFTError;

%include "coord_system.h"
%include "units.h"
%include "sample.h"
%include "sample_vector.h"
%include "strength.h"
%include "mag_field_model.h"
%include "multipole.h"
%include "space_transform.h"
%include "symmetries.h"
%include "bem.h"
%include "signature.h"
