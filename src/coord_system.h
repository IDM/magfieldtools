/*!
   \file		coord_system.h
   \brief		Coordinate system class implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <Eigen/Dense>
#include <string>
#include <vector>
#include "mft_errors.h"
#include "space_transform.h"
#include <iostream> // Debug

// using namespace Eigen; // This namespace cannot be used du to conflict between Eigen::Translation and SpaceTransform::Translation
using namespace std;

// -------------------------------------------
// Coordinate System
// -------------------------------------------
//! Coordinate system class
/*! 
A coordinate system is defined by
- its origin
- x, y, and z axes
- a bool indicating if it is orthogonal 
*/
class CoordinateSystem
{
public:

	//! Coordinate system constructor.
	/*! 
	Build the trivial coordinate system:
	- centre = {0, 0, 0}
	- x axis = {1, 0, 0}
	- y axis = {0, 1, 0}
	- z axis = {0, 0, 1}
	*/
	CoordinateSystem() { axis_x << 1, 0, 0; axis_y << 0, 1, 0; axis_z<< 0, 0, 1; origin << 0, 0, 0; ortho = true; };
	
	//! Coordinate system constructor.
	/*! 
	@param v1 x axis (main) 
	@param v2 y axis (secondary) 
	@param c origin 
	*/
	CoordinateSystem(const Eigen::Vector3d& v1, const Eigen::Vector3d& v2, const Eigen::Vector3d& o);

	//! Coordinate system constructor.
	/*! 
	@param v1 x axis (main) 
	@param v2 y axis (secondary) 
	@param c origin 
	*/
	CoordinateSystem(const vector<double>& v1, const vector<double>& v2, const vector<double>& o);

	//! Coordinate system constructor.
	/*! 
	This constructor can be usd to build non a orthonormal coordinate system
	@param v1 x axis 
	@param v2 y axis 
	@param v3 z axis 
	@param c origin
	*/
	CoordinateSystem(const Eigen::Vector3d& v1, const Eigen::Vector3d& v2, const Eigen::Vector3d& v3, const Eigen::Vector3d& o) { 
		axis_x = v1; axis_y = v2; axis_z = v3; origin = o; set_ortho();
	};

	//! Coordinate system constructor.
	/*! 
	This constructor can be used to build non a orthonormal coordinate system
	@param v1 x axis 
	@param v2 y axis 
	@param v3 z axis 
	@param c origin 
	*/
	CoordinateSystem(const vector<double>& v1, const vector<double>& v2, const vector<double>& v3, const vector<double>& o);

	//! Coordinate system constructor.
	/*! 
	@param s1 specification for primary axis ("x", "y" or "z")
	@param v1 primary axis 
	@param s2 specification for secondary axis ("x", "y" or "z")
	@param v2 secondary axis
	@param c origin 
	*/
	CoordinateSystem(string s1, const Eigen::Vector3d& v1, string s2, const Eigen::Vector3d& v2, const Eigen::Vector3d& o) 
		{ init(s1, v1, s2, v2, o); };

	//! Coordinate system constructor.
	/*! 
	@param s1 specification for primary axis ("x", "y" or "z")
	@param v1 primary axis 
	@param s2 specification for secondary axis ("x", "y" or "z")
	@param v2 secondary axis 
	@param c origin 
	*/
	CoordinateSystem(string s1, const vector<double>& v1, string s2, const vector<double>& v2, const vector<double>& o);

	//! Initialize coordinate system.
	/*! 
	@param s1 specification for primary axis ("x", "y" or "z")
	@param v1 primary axis 
	@param s2 specification for secondary axis ("x", "y" or "z")
	@param v2 secondary axis 
	@param c origin 
	*/
	void init(string s1, const Eigen::Vector3d& v1, string s2, const Eigen::Vector3d& v2, const Eigen::Vector3d& o);

	//! Set the x axis of the coordinate system
	void set_axis_x(const Eigen::Vector3d& v) { axis_x = v; set_ortho(); };

	//! Set the x axis of the coordinate system 
	void set_axis_x(const vector<double>& v) { axis_x << v[0], v[1], v[2];  set_ortho(); };

	void get_axis_x(Eigen::Vector3d& v) { v = axis_x; }; //!< Get the x axis
	vector<double> get_axis_x() { vector<double> v(3); v[0] = axis_x(0); v[1] = axis_x(1); v[2] = axis_x[2]; return v; }; //!< Return the x axis

	//! Set the y axis of the coordinate system
	void set_axis_y(const Eigen::Vector3d& v) { axis_y = v; set_ortho(); };

	//! Set the y axis of the coordinate system 
	void set_axis_y(const vector<double>& v) { axis_y << v[0], v[1], v[2]; set_ortho(); };
	
	void get_axis_y(Eigen::Vector3d& v) { v = axis_y; }; //!< Get the y axis of the coordinate system
	vector<double> get_axis_y() { vector<double> v(3); v[0] = axis_y(0); v[1] = axis_y(1); v[2] = axis_y[2]; return v; }; //!< Return the y axis 
	
	//! Set the z axis of the coordinate system
	void set_axis_z(const Eigen::Vector3d& v) { axis_z = v; set_ortho(); };

	//! Set the z axis of the coordinate system 
	void set_axis_z(const vector<double>& v) { axis_z << v[0], v[1], v[2]; set_ortho(); };

	void get_axis_z(Eigen::Vector3d& v) { v = axis_z; } //!< Get the z axis of the coordinate system.
	vector<double> get_axis_z() { vector<double> v(3); v[0] = axis_z(0); v[1] = axis_z(1); v[2] = axis_z[2]; return v; }; //!< Return the z axis
	
	void get_axes(Eigen::Vector3d& v1, Eigen::Vector3d& v2, Eigen::Vector3d& v3) { v1 = axis_x; v2 = axis_y, v3 = axis_z; } //!< Get the three axes of the coordinate system

	//! Set the axis specified by s
	/*! 
	@param s "x", "y" or "z"
	@param v axis 
	*/
	void set_axis(string s, const Eigen::Vector3d& v);

	//! Get the axis specified by s
	/*! 
	@param[in] s "x", "y" or "z"
	@param[out] v axis 
	*/
	void get_axis(string s, Eigen::Vector3d& v);

	//! Get the axis specified by s
	/*! 
	@param s "x", "y" or "z"
	@return v axis */
	vector<double> get_axis(string s);

	void set_origin(const Eigen::Vector3d& v) { origin = v; }; 	//!< Set the origin of the coordinate system
	void set_origin(const vector<double>& v) { origin << v[0], v[1], v[2]; }; //!< Set the origin of the coordinate system
	
	void get_origin(Eigen::Vector3d& v) { v = origin; }; //!< Get the origin of the coordinate system
	vector<double> get_origin() { vector<double> v(3); v[0] = origin(0); v[1] = origin(1); v[2] = origin[2]; return v; };  //!< Get the origin of the coordinate system

	bool is_ortho() { return ortho; }; 	//!< Return the ortho bit

	void set_ortho(); //! Check the orthonormality of the system and set the ortho bit accordingly

	//! Check if the coordinate system is the world coordinate system defined as origin = (0, 0, 0), first_axis = (1, 0, 0) and second axis = (0, 1, 0)
	bool is_world();

	//! Make the coordinate system orthonormal
	void make_orthonormal();

	//! Make the coordinate system orthonormal
	/*! 
	@param s1 specification for primary axis ("x", "y" or "z") 
	@param s2 specification for secondary axis ("x", "y" or "z") 
	*/
	void make_orthonormal(string primary_axis, string secondary_axis);

	//! Compute the components of a vector in the coordinate system
	/*! 
	@param[out] vproj vector components in this coordinate system 
	@param[in] v input vector 
	*/
	void proj_vector(Eigen::Vector3d& v_proj, Eigen::Vector3d& v);

	//! Compute the components of a vector in the coordinate system
	/*! 
	@param v input vector
	@return the vector components in this coordinate system
	*/
	Eigen::Vector3d proj_vector(Eigen::Vector3d& v);

	//! Compute the components of a vector in the coordinate system
	/*! 
	@param v vector
	@return the vector components in this coordinate system
	*/
	vector<double> proj_vector(const vector<double>& v);

	//! Coordinates of a point in the coordinate system
	/*! 
	@param[out] pproj Coordinates in this coordinate system 
	@param[in] p Coordinates 
	*/
	void proj_point(Eigen::Vector3d& p_proj, Eigen::Vector3d& p);

	//! Coordinates of a point in the coordinate system
	/*! 
	@param p Coordinates 
	@return pproj Coordinates in this coordinate system 
	*/
	Eigen::Vector3d proj_point(Eigen::Vector3d& p); 

	//! Coordinates of a point in the coordinate system
	/*! 
	@param p Coordinates
	@return pproj Coordinates in this coordinate system 
	*/
	vector<double> proj_point(vector<double> & p);

	//! Compute coordinates expressed in the cs_old coordinate system in this coordinate system
	/*! 
	@param[out] pnew new coordinates 
	@param[in] pold old coordinates
	@param[in] csold old coordinate system 
	*/
	void compute_new_coordinates(Eigen::Vector3d& p_new, Eigen::Vector3d& p_old, CoordinateSystem& cs_old);

	//! Compute coordinates expressed in the cs_old coordinate system in this coordinate system
	/*! 
	@param pold old coordinates
	@param csold old coordinate system 
	@return pnew new coordinates
	*/
	Eigen::Vector3d compute_new_coordinates(Eigen::Vector3d& p_old, CoordinateSystem& cs_old);

	//! Compute coordinates expressed in the cs_old coordinate system in this coordinate system
	/*! 
	@param pold old coordinates
	@param csold old coordinate system
	@return pnew new coordinates
	*/
	vector<double> compute_new_coordinates(vector<double>& p_old, CoordinateSystem& cs_old);
	
	//! Apply a space transform to the coordinate system
	void transform(SpaceTransform* trans);
	
	//! Apply a vector of space transforms (first transform, second transform, ...) to the coordinate system
	void transform(vector<SpaceTransform*> v_trans);

private:
	Eigen::Vector3d axis_x;	// x axis
	Eigen::Vector3d axis_y;	// y axis
	Eigen::Vector3d axis_z;	// z axis
	Eigen::Vector3d origin;		// Origin
	bool ortho;			// True is the coordinate system is orthonormal
};

