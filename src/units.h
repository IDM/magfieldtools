/*!
   \file		units.h
   \brief		Units class
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */
#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include "mft_errors.h"
#include <iostream> // for debug

using namespace std;

//! Units
/*! 
This class manipulate physical units
To be further developed! 
*/
class Unit {

public:
	Unit(); /*!< Unit constructor */

	//! Unit constructor
	/*! 
	@param name unit specification (string, e.g. "Meter", "m", etc.) 
	*/
	Unit(string name);
	
	void init(string name); /*! Initialize the unit with the string name */

	void get_physical_quantity(string& str_q) { str_q = physical_quantity; }; /*!< Get the physical quantity*/

	//! Test the value of the physical quantity
	/*! 
	@param str physical quantity  
	@return b true if the physical quantity is equal to str 
	*/
	bool is_physical_quantity(const string& str_q) { if (!physical_quantity.compare(str_q)) return true; return false; };
	
	//! Get the physical quantity
	/*! 
	@return the physical quantity
	*/
	string get_physical_quantity() { return physical_quantity; } 

	void get_symbol(string& str_s); /*!< Get the symbol of the unit */
	
	//! Get the symbol of the unit
	/*! 
	@return unit symbol 
	*/
	string get_symbol() { string str_s; get_symbol(str_s);  return str_s; }

	void copy(Unit& u); /*!< Copy the unit given in parameter to this unit*/
	
	//! Ratio of the present unit to the reference unit in the SI system
	/*! 
	@return the ratio
	*/
	double get_unit_to_si_ratio() { return get_prefix_value() * unit_to_si_ratio; }; 
	
	//! Factor from the present unit to Unit 
	/*! 
	Scale factor from the present unit to a target one
	@param u the target unit 
	@return the scale factor 
	*/
	double get_scale_factor_to_unit(Unit& u); 

	void set_prefix(const string str_p) { prefix = str_p; }; /*!< Set the unit prefix given in parameter */
	
	//! Return a decimal value from the prefix
	/*! 
	@return a value (1e-3 if "m", 1e-6 if "u", etc.) 
	*/
	double get_prefix_value(); 

	//! Return a prefix from a decimal value
	/*! 
	@param p a decimal value (1e3, 1, 1e-3, 1e-6, etc.)
	@param multiplier=1 multiplicative factor applied to the value
	@param extended=false enable extended prefix, (1e-1, 1e-2, 10, etc.)
	@return the prefix (string) 
	*/
	string get_prefix_from_value(double p, double multiplier=1, bool extended = false);
	
	//! Multiply with another unit
	/*! 
	Multiply this unit by the one given in parameter
	@param u another unit 
	*/
	void multiply(Unit& u);
	
	//! Divide by another unit
	/*! 
	Divide this unit by another one given in parameter 
	@param u another unit 
	*/
	void divide(Unit& u);

	//! Sort the symbols and physical quantities
	void sort_symbols();

	//! Simplify the unit
	void simplify();

	//! Update the physical quantity string
	void update_physical_quantity();
	
protected:
	string physical_quantity;
	vector<string> physical_quantity_up; /*!< The physical quantities at numerator (string) */
	vector<string> physical_quantity_down; /*!< The physical quantities at denominator (vector of string)*/
	vector<string> symbol_up; /*!< Multiplicative symbols, e.g. m, A, etc. (vector of strings) */
	vector<string> symbol_down; /*!< Dividing symbols m^-1, A^-1, etc. (vector of strings) */
	string prefix; /*!< Prefix (string) */
	double unit_to_si_ratio; /*!< Ratio between this unit and reference SI unit (double) */
};

