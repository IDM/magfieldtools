/*!
   \file		bem.cpp
   \brief		Boundary Element Methods (BEM) and derivated classes
   \author		Gael Le Bec
   \version		1
   \date		2021
   \copyright	GNU Public License.
 */

#include "bem.h"
#include <iostream>

// ---------------------------------------------
// BEM2D: base class for 2D BEM models
// ---------------------------------------------
BEM2D::BEM2D(){
 	is_2D = true; 
 	model_type = "bem_2d";
	n = 0;
}

// Copy constructor
BEM2D::BEM2D(const BEM2D& b){

	is_2D = true;
	model_type = "bem_2d";		
	
	coord_system = b.coord_system;

	model_type = b.model_type;
	svd_tol = b.svd_tol;
	
	unit_distance = b.unit_distance;
	unit_field = b.unit_field;
	sources = b.sources;
	vertex_x = b.vertex_x;
	vertex_y = b.vertex_y;
	n = b.n;
}

// Initialize from a sample vector
void BEM2D::init_from_set(SampleVector& field_set){
	set_vertex_from_field(field_set);
	init(field_set);
}

// Initialize from sample vectors
void BEM2D::init_from_set(SampleVector& vertex_set, SampleVector& field_set){
	init(vertex_set, field_set);
}

// Resize
void BEM2D::resize(size_t new_size){

	sources.resize(new_size); 
	vertex_x.resize(new_size);
	vertex_y.resize(new_size);
	n = new_size;
}

// Initialize from a sample vector
void BEM2D::init(SampleVector& vertex_set, SampleVector& field_set){
	
	set_vertex(vertex_set);
	init(field_set); 
}

// Initialize from a sample vector
void BEM2D::init(SampleVector& field_set){

	// Set units and other attributes
	unit_field = field_set.get_unit_sample();

	bool is_2D = true;									
	model_type = "bem_2d";

	// Compute the equivalent sources
	fit_sources(field_set);

	// Update the metadata
	metadata.set_creation_time(); 
}

// Set the vertex points from a sample vector
void BEM2D::set_vertex(SampleVector& s_set){

	// Set the distance unit
	unit_distance = s_set.get_unit_distance(); 
	
	// The model coordinate system is defined by s_set
	s_set.get_coordinate_system(coord_system);	

	Eigen::Vector3d point;
	resize(s_set.get_size());

	for (size_t k = 0; k < n ; k++){
		s_set.get_coordinates(k, point);
		vertex_x[k] = point(0);
		vertex_y[k] = point(1);
	}
}

// Set the vertex points
void BEM2D::set_vertex(string s, const vector<double> xy_coord){

	size_t npts = xy_coord.size();

	// Check the type of component
	bool x_comp;
	if (!s.compare("x"))
		x_comp = true;
	else if (!s.compare("y"))
		x_comp = false;
	else throw MFTError("BEM2D::set_vertex", "Wrong component specification", 0);

	if (!n)
		resize(npts);

	if (n == npts)
		for (size_t k = 0; k < n; k++){
			if (x_comp)
				vertex_x[k] = xy_coord[k];
			else 
				vertex_y[k] = xy_coord[k];
		}
	else throw MFTError("BEM2D::set_vertex", "Wrong number of points", 0);

}

// Set the vertex points
void BEM2D::set_vertex(const vector<double> x_coord, const vector<double> y_coord){

	size_t npts_x = x_coord.size();
	size_t npts_y = y_coord.size();
	if (npts_x != npts_y) throw MFTError("BEM2D::set_vertex", "Wrong number of points", 0);

	if (!n)
		resize(npts_x);
	else if (n == npts_x)
		for (size_t k = 0; k < n; k++){
				vertex_x[k] = x_coord[k];
				vertex_y[k] = y_coord[k];
		}
	else throw MFTError("BEM2D::set_vertex", "Wrong number of points", 0);
}

// Set vertex from field samples
void BEM2D::set_vertex_from_field(SampleVector& field_set){

	size_t npts = field_set.get_size();
	resize(npts);

	CoordinateSystem cs_set;
	field_set.get_coordinate_system(cs_set);

	Eigen::Vector3d point_0, point_1, u0, u0_proj, u1, u1_proj;
	double x0, x1, y0, y1;

	for (size_t k = 0; k < n; k++){

		// first point
		field_set.get_coordinates(k, point_0);
		coord_system.compute_new_coordinates(point_1, point_0, cs_set);
		x0 = point_1(0);
		y0 = point_1(1);

		// second point 
		if (k < n - 1)
			field_set.get_coordinates(k + 1, point_0);
		else
			field_set.get_coordinates(0, point_0);
		coord_system.compute_new_coordinates(point_1, point_0, cs_set);
		x1 = point_1(0);
		y1 = point_1(1);

		// vertex position
		vertex_x[k] = (x0 + x1) / 2;
		vertex_y[k] = (y0 + y1) / 2;
	}

}

// Get the x vertex points
vector<double> BEM2D::get_vertex_x(){

	vector<double> res(vertex_x.begin(), vertex_x.end()); 
	return res;
}

// Get the y vertex points
vector<double> BEM2D::get_vertex_y(){

	vector<double> res(vertex_y.begin(), vertex_y.end()); 
	return res;
}

// Get the vertex points
vector<double> BEM2D::get_vertex(string s){

	if (!s.compare("x"))
		return get_vertex_x();
	else if (!s.compare("y"))
		return get_vertex_y();
	else throw MFTError("BEM2D::get_vertex", "Wrong component specification", 0);
}

// Set the sources
void BEM2D::set_sources(vector<double> s){
	size_t npts = s.size();
	if (!n)
		resize(npts);
	else if (n == npts)
		for (size_t k = 0; k < n; k++)
			sources[k] = s[k];
	else throw MFTError("BEM2D::set_sources", "Wrong number of points", 0);
}

// Check if the boundary is valid
bool BEM2D::boundary_is_valid(){

		if ((vertex_x.size() == n) && (vertex_y.size() == n) && (vertex_x.size() > 0) && (sources.size() == n))
			return true;
		return false;
}

// Field projections
Eigen::VectorXd BEM2D::field_projection(SampleVector& field_set){

	// Coordinate systems
	CoordinateSystem cs_field;
	field_set.get_coordinate_system(cs_field);
	Eigen::Vector3d first_axis, second_axis, third_axis, coord, point;
	Eigen::Vector3d first_axis_field, second_axis_field, third_axis_field;
	coord_system.get_axis_x(first_axis);
	coord_system.get_axis_y(second_axis);
	coord_system.get_axis_z(third_axis);

	cs_field.get_axis_x(first_axis_field);
	cs_field.get_axis_y(second_axis_field);
	cs_field.get_axis_z(third_axis_field);

	// Number of field components
	size_t n_samples = field_set.get_size();
	size_t n_field = 0;
	for (size_t i = 0; i < n_samples; i ++){
		n_field ++;
		if (!field_set.is_proj(i))
			n_field ++;
	}

	Eigen::VectorXd field_val(n_field);
	Eigen::Vector3d v3, v, v3_proj;
	size_t count = 0;
	Sample s;

	// Compute the projections
	for (size_t i = 0; i < n_samples; i++){
		
		field_set.get_sample(i, s);
		s.get_unit_vector(v3);
		coord_system.proj_vector(v3_proj, v3);

		// If the sample is a projection
		if (s.is_proj()){
			double proj = sqrt(v3_proj(0) * v3_proj(0) + v3_proj(1) * v3_proj(1));
			field_val(count) = s.get_value() * proj;
			count ++;
		}
		// If the sample is a vector
		else{
			field_val(count) = v3_proj(0);
			field_val(count + 1) = v3_proj(1);
			count += 2;
		}
	}
	return field_val;
}

// Scalar potential 
Eigen::VectorXd BEM2D::scalar_potential_from_samples(SampleVector& field_samples, double tol, size_t max_wrong_pts){

	size_t n_field = field_samples.get_size();
	Eigen::VectorXd potential(n_field);
	potential(0) = 0;
	Sample s0, s1, s2;
	Eigen::Vector3d u_sample, u_contour, point0, point2, v0, v2, v, u1, u, v1;
	if (tol < 0)
		tol *= -1;
	size_t wrong_points_count = 0;

	// --- Coordinate systems
	CoordinateSystem cs_field;
	Eigen::Vector3d first_axis, second_axis, third_axis, coord, point;
	Eigen::Vector3d first_axis_field, second_axis_field, third_axis_field;
	Eigen::Vector2d bxy, dxy;
	field_samples.get_coordinate_system(cs_field);
	coord_system.get_axis_x(first_axis);
	coord_system.get_axis_y(second_axis);
	coord_system.get_axis_z(third_axis);
	cs_field.get_axis_x(first_axis_field);
	cs_field.get_axis_y(second_axis_field);
	cs_field.get_axis_z(third_axis_field);

	// Remove the average
	field_samples.subtract_mean_vector_value();
	field_samples.subtract_mean_value(); // The integral along the countour should be 0 (B = - grad V)

	// --- Compute the potential on the contour
	for (size_t i = 0; i < n_field; i++){
		
		// Previous sample
		if (i == 0)
			field_samples.get_sample(n_field - 1, s0);
		else
			field_samples.get_sample(i - 1, s0);
		// Next sample
		if (i < n_field - 1)
			field_samples.get_sample(i + 1, s2);
		else
		 	field_samples.get_sample(0, s2);
		// This sample
		field_samples.get_sample(i, s1);
		s1.get_unit_vector(u_sample);

		// Unit vector tangent to the contour
		s0.get_coordinates(v0);
		s2.get_coordinates(v2);
		point0 = coord_system.compute_new_coordinates(v0, cs_field);
		point2 = coord_system.compute_new_coordinates(v2, cs_field);
		v = point2 - point0;
		v(2) = 0; // We disregard the z component perpendicular to the plane of the model
		u_contour = v / v.norm();
		dxy(0) = v(0);
		dxy(1) = v(1);

		// Get the sample vector direction
		s1.get_unit_vector(u1);
		u = u1(0) * first_axis_field + u1(1) * second_axis_field + u1(2) * third_axis_field;
		coord_system.proj_vector(u_sample, u);
		u = u_sample;
		u(2) = 0; // We disregard the z component perpendicular to the plane of the model
		u_sample = u / u.norm();
		
		// If the sample is a projection
		if ((s1.is_proj()) && (abs(u_sample.dot(u_contour)) < 1 - tol))
			wrong_points_count ++;
			
		// Projected field value
		s1.get_vector_values(v1);
		v = v1(0) * first_axis_field + v1(1) * second_axis_field + v1(2) * third_axis_field;
		coord_system.proj_vector(v1, v);
		bxy(0) = v1(0);
		bxy(1) = v1(1);

		// Potential
		potential(i) = bxy.dot(dxy);
		if (i > 0)		
			potential(i) += potential(i - 1);
	}

	if (wrong_points_count > max_wrong_pts)
		throw MFTError("BEM2D::scalar_potential_from_samples", "Projected unit vector of field sample not parallel to contour", 0);

	return potential;
}

// Vector potential 
Eigen::VectorXd BEM2D::vector_potential_from_samples(SampleVector& field_samples, double tol, size_t max_wrong_pts){

	size_t n_field = field_samples.get_size();
	Eigen::VectorXd potential(n_field);
	potential(0) = 0;
	Sample s0, s1, s2;
	Eigen::Vector3d u_sample, u_contour, point0, point2, v0, v2, v, u1, u, v1;
	if (tol < 0)
		tol *= -1;
	size_t wrong_points_count = 0;

	// --- Coordinate systems
	CoordinateSystem cs_field;
	Eigen::Vector3d first_axis, second_axis, third_axis, coord, point;
	Eigen::Vector3d first_axis_field, second_axis_field, third_axis_field;
	Eigen::Vector2d bxy, dxy;
	field_samples.get_coordinate_system(cs_field);
	coord_system.get_axis_x(first_axis);
	coord_system.get_axis_y(second_axis);
	coord_system.get_axis_z(third_axis);
	cs_field.get_axis_x(first_axis_field);
	cs_field.get_axis_y(second_axis_field);
	cs_field.get_axis_z(third_axis_field);

	// Compute the normal component and remove the average
	SampleVector un = field_samples.normal_vectors();
	SampleVector dotp = field_samples.dot(un);
	double ave = dotp.mean_value();
	field_samples.set_unit_vector("x", un.get_unit_vector("x"));
	field_samples.set_unit_vector("y", un.get_unit_vector("y"));
	field_samples.set_unit_vector("z", un.get_unit_vector("z"));
	vector<double> vval = dotp.get_values();
	for (size_t k = 0; k < n_field; k++)
		vval[k] -= ave;
	field_samples.set_values(vval);

	// Remove the average
	// field_samples.subtract_mean_vector_value();
	// field_samples.subtract_mean_value(); // The integral along the contour should be 0 (no sources nor current inside)
	// // Note that the contour should be regularly sampled. This is not checked for the moment.

	// --- Compute the potential on the contour
	for (size_t i = 0; i < n_field; i++){
		// Previous sample
		if (i == 0)
			field_samples.get_sample(n_field - 1, s0);
		else
			field_samples.get_sample(i - 1, s0);
		// Next sample
		if (i < n_field - 1)
			field_samples.get_sample(i + 1, s2);
		else
		 	field_samples.get_sample(0, s2);
		// This sample
		field_samples.get_sample(i, s1);
		s1.get_unit_vector(u_sample);

		// Unit vector tangent to the contour
		s0.get_coordinates(v0);
		s2.get_coordinates(v2);
		point0 = coord_system.compute_new_coordinates(v0, cs_field);
		point2 = coord_system.compute_new_coordinates(v2, cs_field);
		v = point2 - point0;
		v(2) = 0; // We disregard the z component perpendicular to the plane of the model
		u_contour = v / v.norm();
		dxy(0) = - v(1);
		dxy(1) = v(0);

		// Get the sample vector direction
		s1.get_unit_vector(u1);
		u = u1(0) * first_axis_field + u1(1) * second_axis_field + u1(2) * third_axis_field;
		coord_system.proj_vector(u_sample, u);
		u = u_sample;
		u(2) = 0; // We disregard the z component perpendicular to the plane of the model
		u_sample = u / u.norm();
		
		// If the sample is a projection
		if ((s1.is_proj()) && (abs(u_sample.dot(u_contour)) > tol))
			wrong_points_count ++;
			
		// Projected field value
		s1.get_vector_values(v1);
		v = v1(0) * first_axis_field + v1(1) * second_axis_field + v1(2) * third_axis_field;
		coord_system.proj_vector(v1, v);
		bxy(0) = v1(0);
		bxy(1) = v1(1);

		// Potential
		potential(i) = bxy.dot(dxy);
		if (i > 0)		
			potential(i) += potential(i - 1);
	}

	if (wrong_points_count > max_wrong_pts)	{
			cout <<"Wrong points: "<< wrong_points_count << endl;
			throw MFTError("BEM2D::scalar_potential_from_samples", "Projected unit vector of field sample not parallel to contour", 0);
		}
	return potential;
}

// Fit current density
void BEM2D::fit_sources(SampleVector& field_samples){

	// Check the boundary and the samples
	if (!boundary_is_valid()) 
		throw MFTError("BEM2D::fit_sources", "Invalid boundary", 0);

	// Compute the potential
	size_t  ns = field_samples.get_size();
	SampleVector potential;
	field_samples.integral_normal(potential);
	Eigen::VectorXd pot(ns);
	vector<double> pot_std(ns);
	potential.get_values(pot_std);
	for (size_t k = 0; k < ns; k++)
		pot(k) = pot_std[k];
		
	// Source matrix
	Eigen::MatrixXd mat = source_potential_matrix(field_samples);

	// Remove the average
	// double m = mat.mean();
	// for (size_t i = 0; i < (size_t) mat.rows(); i ++)
	// 	for (size_t j = 0; j < (size_t) mat.cols(); j++)
	// 		mat(i, j) -= m;

	
	// // Eigen::BDCSVD<Eigen::MatrixXd> svd(mat, Eigen::ComputeFullU | Eigen::ComputeFullV);
	// Eigen::BDCSVD<Eigen::MatrixXd> svd(mat, Eigen::ComputeThinU | Eigen::ComputeThinV);

	// Eigen::VectorXd s = svd.singularValues();
	// Eigen::MatrixXd u = svd.matrixU();
	// Eigen::MatrixXd v = svd.matrixV();
	// // Filter out small singular values
	// Eigen::MatrixXd sm = Eigen::MatrixXd::Zero(v.cols(), u.cols());
	// for (size_t k = 0; k < (size_t) s.size(); k++){
	// 	if (s(k) / s(0) >= svd_tol){
	// 		sm(k, k) = s(k);
	// 	}
	// }

	// // Pseudo-inverse
	// Eigen::MatrixXd imat = v*sm*u.transpose();
	// Eigen::MatrixXd sources_tmp = imat * pot;

	Eigen::MatrixXd sources_tmp = mat.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(pot);
	//Eigen::MatrixXd sources_tmp = mat.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(pot);

	sources.resize(n);
	for (size_t i = 0; i < n; i++)
		sources[i] = sources_tmp(i);
}

// Complex sign
complex<double> BEM2D::sign(complex<double> z){
	double c = abs(z);
	
	if (c == 0){
		complex<double> res(0, 0);
		return res;
	}
	complex<double> res = z / c;
	return res;
}

// Double sign
double BEM2D::sign(double d){
	if (d < 0)
		return -1;
	return 1;
}

// Complex log
complex<double> BEM2D::ln(complex<double> z){
	
	complex<double> c = log(abs(z)), ii(0, 1);
	double x = z.real(), y = z.imag();
	c += ii * atan2(y, x);

	return c;
}

// Complex potential from a single segment
// This corresponds to the Apl() proc in JC's Igor Pro implementation
complex<double> BEM2D::complex_potential_segment(complex<double> z, complex<double> z1, complex<double> z2, double sigma0, double sigma1){
	
	complex<double> ii(0, 1);
	complex<double> v0 = sign(z2 - z1);
	complex<double> v1 = (z - z1) / v0;
	complex<double> v2 = (z - z2) / v0;
	complex<double> v3 = (v1 + v2) / 2.0;

	double m = abs(z2 - z1);
	double c1 = abs(v1);

	double c2 = abs(v2);
	double cf = sign(v3.imag());
	complex<double> c0 = pi / 2.0 * m * sigma0 * ii;

	if(c1 == 0)
		return  ii * (sigma0 * m * (-1.0 + log(m)) + sigma1 / 2.0 * m); 

	if (c2 == 0)
		return  ii * (sigma0 * m * (-1.0 + log(m)) - sigma1 / 2.0 * m);
	
	if (m == 0)
		return 0;
		
	return sigma0 * (v1 * log(v1) - v2 * log(v2) + (v2 - v1)) + sigma1 / m * (-v3 * m + v1 * v2 * log(v1 / v2));
}

// Complex field from a single segment
// This corresponds to the Bpl() proc in JC's Igor Pro implementation
complex<double> BEM2D::complex_field_segment(complex<double> z, complex<double> z1, complex<double> z2, double sigma0, double sigma1){

	complex<double> ii(0, 1);
	complex<double> v0 = sign(z2 - z1);
	complex<double> v1 = (z - z1) / v0;
	complex<double> v2 = (z - z2) / v0;
	complex<double> v3 = (v1 + v2) / 2.0;
	double m = abs(z2 - z1);

	if (m == 0)
		return 0;
	else
		return  ii * ((sigma0 + 2.0 * v3 / m * sigma1) * ln(v1 / v2) - 2.0 * sigma1) / v0;
}

// Source matrix
Eigen::MatrixXd BEM2D::source_matrix(SampleVector& field_set){

	// Counts the number of projected components
	size_t m = 0;
	size_t n_field = field_set.get_size();
	for (size_t i = 0; i < n_field ; i++){
		m ++;
		if (!field_set.is_proj(i)) // One more component in that case
			m ++;
	}

	// Build and fill the matrix
	CoordinateSystem cs_field;
	field_set.get_coordinate_system(cs_field);
	Eigen::Vector3d first_axis, second_axis, coord, point;
	coord_system.get_axis_x(first_axis);
	coord_system.get_axis_y(second_axis);
	Sample s;
	Eigen::Vector3d axis_x_field, axis_y_field, axis_z_field;
	cs_field.get_axis_x(axis_x_field);
	cs_field.get_axis_y(axis_y_field);
	cs_field.get_axis_z(axis_z_field);

	Eigen::Vector3d v3, v3_proj, u3, u3_perp;
	Eigen::MatrixXd mat(m, n);
	complex<double> b, ii(0, 1), b_proj;
	double bx, by;
	size_t count = 0;
	for (size_t i = 0; i < n_field; i++){

		// Coordinates
		field_set.get_sample(i, s);
		s.get_coordinates(coord);
		coord_system.compute_new_coordinates(point, coord, cs_field);
		complex<double> z(point(0), point(1));
		
		// Field sample unit vectors
		field_set.get_unit_vector(i, u3);
		u3_perp = u3(1) * axis_x_field - u3(0) * axis_y_field + u3(2) * axis_z_field;

		// Matrix coefficients
		for (size_t j = 0; j < n; j++){
			complex<double> z1j(vertex_x[j], vertex_y[j]);
			complex<double> z2j;
			if (j == n - 1){
				z2j.real(vertex_x[0]);
				z2j.imag(vertex_y[0]);
			}
			else{
				z2j.real(vertex_x[j + 1]);
				z2j.imag(vertex_y[j + 1]);
			}
			// --- Contribution from current densities
			b = complex_field_segment(z, z1j, z2j, 1.0, 0);

			// Compute the projections
			bx = b.imag();
			by = b.real();
			v3 = bx * first_axis + by * second_axis;
			
			// Store the values
			mat(count, j) = v3.dot(u3);
			if (!field_set.is_proj(j))
				mat(count + 1, j) = v3.dot(u3_perp);
			
		}
		count ++;
		if(!field_set.is_proj(i))
			count ++;
	}
	return mat;
}

// Complex field from a single segment
// This corresponds to the Bpl() proc in JC's Igor Pro implementation
complex<double> BEM2D::complex_field_segment(complex<double> z, size_t k){

	if (k < n){
		complex<double> z0(vertex_x[k], vertex_y[k]), z1;
		double sigma0 = sources[k], dsigma;
		if (k < n - 1){
			z1.real(vertex_x[k]);
			z1.imag(vertex_y[k]);
			dsigma = sources[k + 1] - sources[k];
			}
		else{
			z1.real(vertex_x[0]);
			z1.imag(vertex_y[0]);
			dsigma = sources[n - 1] - sources[0];
		}
		return complex_field_segment(z, z0, z1, sigma0, dsigma);
	}
	else throw MFTError("BEM2D::complex_field_segments", "Wrong index", 0);
}

// Complex potential from a single segment
complex<double> BEM2D::complex_potential_segment(complex<double> z, size_t k){

	if (k < n){
		complex<double> z0(vertex_x[k], vertex_y[k]), z1;
		double sigma0 = sources[k], dsigma;
		if (k < n - 1){
			z1.real(vertex_x[k]);
			z1.imag(vertex_y[k]);
			dsigma = sources[k + 1] - sources[k];
			}
		else{
			z1.real(vertex_x[0]);
			z1.imag(vertex_y[0]);
			dsigma = sources[n - 1] - sources[0];
		}
		return complex_potential_segment(z, z0, z1, sigma0, dsigma);
	}
	else throw MFTError("BEM2D::complex_potential_segments", "Wrong index", 0);
}

// Complex field
complex<double> BEM2D::complex_field(complex<double> z){
	
	complex<double> res(0, 0),  z2, ii(0, 1);
	for (size_t i = 0; i < n; i++){
		complex<double> z1(vertex_x[i], vertex_y[i]);
		if (i < n - 1){
			z2.real(vertex_x[i + 1]);
			z2.imag(vertex_y[i + 1]);
			}
		else{
			z2.real(vertex_x[0]);
			z2.imag(vertex_y[0]);
			}
		res -= ii *  complex_field_segment(z, z1, z2, sources[i], 0);
	}
	return res;
}

// Compute the sample field
void BEM2D::field(Sample& sample, CoordinateSystem& cs) {

	// Sample coordinates and vector direction
	Eigen::Vector3d coord, vect_dir;
	sample.get_coordinates(coord);
	sample.get_unit_vector(vect_dir);
	
	// Sample coordinates
	Eigen::Vector3d vect;
	coord_system.compute_new_coordinates(vect, coord, cs);
	coord = vect;

	// Direction vector
	Eigen::Vector3d sample_x_axis, sample_y_axis, sample_z_axis;
	cs.get_axes(sample_x_axis, sample_y_axis, sample_z_axis);
	vect = vect_dir(0) * sample_x_axis + vect_dir(1) * sample_y_axis + vect_dir(2) * sample_z_axis;
	vect_dir = vect;

	// Coordinates in the Circular2D coordinate system
	Eigen::Vector3d point_proj;
	coord_system.proj_point(point_proj, coord);
	complex<double> z(point_proj(0), point_proj(1));

	// Compute the field
	complex<double> b = complex_field(z);
		
	// Compute the field in the Circular2D coordinate system
	Eigen::Vector3d b_vect_tmp;
	Eigen::Vector3d axis_x, axis_y;
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	
	b_vect_tmp = b.imag() * axis_x + b.real() * axis_y;

	// The value is the projection of b_vect_tmp to vect_dir
	if (sample.is_proj()) 		
		sample.set_value(b_vect_tmp.dot(vect_dir));

	else {
		if (b.real() != 0 || b.imag() != 0) {
			// Set the vector direction
			sample.set_unit_vector(b_vect_tmp(0), b_vect_tmp(1), 0);
			// Set the value
			sample.set_value(b_vect_tmp.norm());
		}
		else {
			// This defines the direction to y for zero valued samples
			sample.set_unit_vector(axis_y(0), axis_y(1), 0);
			sample.set_value(0);
		}
	}
}

// Field
void BEM2D::field(SampleVector& s){

	size_t n = s.get_size();
	CoordinateSystem cs;
	s.get_coordinate_system(cs);
	Sample si;

	for (size_t i = 0; i < n; i++){
		s.get_sample(i, si);
		field(si, cs);
		s.set_sample(i, si);
	}
	metadata.set_modification_time();
}

// Source matrix
Eigen::MatrixXd BEM2D::source_potential_matrix(SampleVector& field_samples){

	// Declarations and init
	size_t m = field_samples.get_size();
	Eigen::MatrixXd mat(m, n);
	Sample s;
	Eigen::Vector3d coord, coord_field;
	CoordinateSystem cs_field;
	field_samples.get_coordinate_system(cs_field);
	complex<double> a;

	// Build and fill the matrix
	for (size_t i = 0; i < m; i++){

		// Coordinates
		field_samples.get_sample(i, s);
		s.get_coordinates(coord_field);
		coord_system.compute_new_coordinates(coord, coord_field, cs_field);
		complex<double> z(coord(0), coord(1));

		// Matrix coefficients
		for (size_t j = 0; j < n; j++){
			complex<double> z1j(vertex_x[j], vertex_y[j]);
			complex<double> z2j;
			if (j == n - 1){
				z2j.real(vertex_x[0]);
				z2j.imag(vertex_y[0]);
			}
			else{
				z2j.real(vertex_x[j + 1]);
				z2j.imag(vertex_y[j + 1]);
			}
			// Contribution from current densities
			a = complex_potential_segment(z, z1j, z2j, 1.0, 0);

			// Store the values
			mat(i, j) = a.real();
		}
	}
	return mat;
}

// Parse the source matrix to a vector
vector<vector<double>> BEM2D::source_potential_matrix_std(SampleVector& field_samples){
 
	// Compte the matrix
	Eigen::MatrixXd mat = source_potential_matrix(field_samples);
	size_t m = mat.rows();

	// Parse to vectors
	vector<vector<double>> mat_std(m, vector<double>(m, 0));
	
	for (size_t i = 0; i < m; i ++)
		for (size_t j = 0; j < n; j++) 
				mat_std[i][j] = mat(i, j);

	return mat_std;
 }

// Move
 void BEM2D::move(SpaceTransform* tr){

	// Create a sample vector with the coordinates
	SampleVector s;
	s.resize(n);
	vector<double> one(n, 1);
	s.set_coordinates("x", vertex_x);
	s.set_coordinates("y", vertex_y);
	//s.set_coordinates('z', z);
	//s.set_unit_vector('x', one);

	// Apply the transformation
	s.move(tr);

	// Update the coordinates
	vector<double> z;
	s.get_coordinates(vertex_x, vertex_y, z);
 }

 // Scale
 void BEM2D::scale(double d){

	for (size_t k = 0; k < n; k++)
		sources[k] = d * sources[k];
 }