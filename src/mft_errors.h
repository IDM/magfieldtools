/*!
   \file		mft_errors.h
   \brief		Magnetic field tools error class
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <string>
#include <exception>
using namespace std;

class MFTError : public exception
{
public:
    MFTError(string const& from0= "", string const& message0 = "", int level0 = 0) throw()
        : message(message0), from(from), message_long(message0+"::"+message0), level(level0)
    {}

    const char* what() const throw()  {
        return message_long.c_str();
    }
    
    int get_level() const throw()  {
        return level;
    }

private:
    string from;
    string message;   
    string message_long;
    int level;           

};
