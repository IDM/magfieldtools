/*!
   \file		multipole.h
   \brief		Multipole and derivated classes
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <string>
#include <vector>
#include <complex>
#include <cmath>

#include "mag_field_model.h"
#include "sample.h"
#include "coord_system.h"
#include "strength.h"

using namespace std;

// -----------------------------------------
// Base class for 2D multipole objects
// -----------------------------------------
//! Multipole2D class
/*! 
This class is a generic class for multipole based models. It contains:
- a degree
- coefficients
- normalized coefficients
- a reference (radius, semi axis, etc.)
It inherits from MagFieldModel
*/
class Multipole2D : public MagFieldModel{	
	public:
		Multipole2D(); //!< Multipole2D constructor

		//! Multipole2D constructor
		/*! 
		@param cs coordinate system 
		@param d degree
		@param r ref 
		*/
		Multipole2D(CoordinateSystem & cs, size_t d, const vector<double> &r);

		void set_degree(size_t d) { degree = d; coef.resize(d); coef_norm.resize(d); }; //!< Set the degree
		size_t get_degree() { return degree; }; //!< Get the degree
		void set_coef(const vector<complex<double>>& v) { coef = v; degree = coef.size(); normalize_coef(); }; //!< Set the coefficients from a vector
		void get_coef(vector<complex<double>>& v) { v = coef; }; //!< Get the coefficients
		vector<complex<double>> get_coef() { return coef; }; //!< Get the coefficients
		
		//! Set a single coefficient
		/*! 
		@param k index of the coefficient
		@param v complex value 
		*/
		void set_coef(size_t k, complex<double> v); 
		
		//! Get the coefficients
		/*! 
		Return the real or imaginary part of the coefficients, depending on s 
		@param s string specification: 'real' or 'imag' 
		*/
		vector<double> get_coef(string s); 

		void set_coef_norm(const vector<complex<double>>& v) { coef_norm = v; degree = coef.size(); }; //!< Set the normalized coefficients from a vector
		void get_coef_norm(vector<complex<double>>& v) { v = coef_norm; } //!< Get the normalized coefficients
		vector<complex<double>> get_coef_norm() { return coef_norm; } //!< Get the normalized coefficients

		//! Get the normalized coefficients
		/*! 
		Return the real or imaginary part of the coefficients, depending on s
		@param s string specification: 'real' or 'imag' 
		*/
		vector<double> get_coef_norm(string s);

		void set_ref(const vector<double>& v) { ref = v; }; //!< Set the reference from a vector
		void get_ref(vector<double>& v) { v = ref; }; //!< Get the reference
		vector<double> get_ref() { return ref; }; //!< Get the reference

		//! Normalize the coefficients
		/*!	
		@param n reference coefficient (-1 if not specified: find max coefficient)
		@param mode char specification of the reference: 0: abs(max(coef)), 1: real(...), 2: imag(...), 3: max(coef)
		@param u unity 
		*/
		void normalize_coef(int n=-1, unsigned char mode=0, double unity=1); 

		//! Change the field units and update the coefficients
		/*!	
		@param u Unit 
		*/
		void set_unit_field(Unit& u);

		//! Change the distance units and update reference
		/*!	
		@param u Unit 
		*/
		void set_unit_distance(Unit& u);

		//! Initialize with a SampleVector
		/*! 
		@param s SampleVector 
		*/
		void init_from_set(SampleVector& s_set);

		//! Initialize with a SampleVector
		/*! 
		@param s vector of SampleVector pointers 
		*/
		void init_from_set(vector<SampleVector*> s_set);

		virtual Eigen::MatrixXd multipole_matrix(SampleVector& s_set) = 0; //!< Multipole matrix (model dependent)

		//! Scale the coefficients by a scalar
		/*!
		@param f scale factor
		*/
		void scale(double d);

	protected:
		size_t degree = 32;			// Multipole degree
		vector<complex<double>> coef;		// Multipole coefficients
		vector<complex<double>> coef_norm;	// Normalized multipole coefficients
		vector<double> ref;					// Reference radius, semi axis, etc...
};

// -----------------------------------------
// 2D circular multipoles
// -----------------------------------------
//! Circular2D class
class Circular2D : public Multipole2D {

public:
	//! Circular2D constructor.
	Circular2D() : Multipole2D() { set_model_type("circular_2d"); init_units(); metadata.set_creation_time(); };

	//! Circular2D constructor.
	/*! 
	@param u field unit 
	*/
	Circular2D(const Unit& uf) : Circular2D() { unit_field = uf; };

	//! Circular2D constructor.
	/*! 
	@param uf field unit 
	@param ud distance unit 
	*/
	Circular2D(const Unit& uf, const Unit& ud) : Circular2D() { unit_field = uf; unit_distance = ud; };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius 
	*/
	Circular2D(double r0) : Circular2D() { set_ref(vector<double>(1, r0)); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius 
	@param u field unit 
	*/
	Circular2D(double r0, const Unit& u) : Circular2D(u) { set_ref(vector<double>(1, r0)); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param uf field unit 
	@param ud distance unit 
	*/
	Circular2D(double r0, const Unit& uf, const Unit& ud) : Circular2D(uf, ud) { set_ref(vector<double>(1, r0)); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius 
	@param d degree 
	*/
	Circular2D(double r0, size_t d) : Circular2D(r0) { set_degree(d); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param d degree 
	@param uf field unit
	*/
	Circular2D(double r0, size_t d, const Unit& uf) : Circular2D(r0, uf) { set_degree(d); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param d degree
	@param uf field unit
	@param ud distance unit 
	*/
	Circular2D(double r0, size_t d, const Unit& uf, const Unit& ud) : Circular2D(r0, uf, ud) { set_degree(d); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius 
	@param cs coordinate system 
	*/
	Circular2D(double r0, CoordinateSystem& cs) : Circular2D(r0) { set_coordinate_system(cs); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius 
	@param d degree
	@param cs coordinate system 
	*/
	Circular2D(double r0, size_t d, CoordinateSystem& cs) : Circular2D(r0, d) { set_coordinate_system(cs); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param d degree
	@param cs coordinate system 
	@param u field unit
	*/
	Circular2D(double r0, size_t d, CoordinateSystem& cs, const Unit& u) : Circular2D(r0, d, u) { set_coordinate_system(cs); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param d degree
	@param cs coordinate system
	@param ud field unit
	@param uf distance unit
	*/
	Circular2D(double r0, size_t d, CoordinateSystem& cs, const Unit& uf, const Unit& ud) : Circular2D(r0, d, ud, uf) { set_coordinate_system(cs); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param samples SampleVector 
	*/
	Circular2D(double r0, SampleVector& s_set) :Circular2D(r0) { init_from_set(s_set); };

	//! Circular2D constructor.
	/*! 
	@param r0 reference radius
	@param d degree
	@param samples SampleVector 
	*/
	Circular2D(double r0, size_t d, SampleVector& s_set) :Circular2D(r0, d) { init_from_set(s_set); };

	//! Compute the multipole matrix from a SampleVector
	/*! 
	@param s SampleVector with field values
	@return the multipole matrix 
	*/
	Eigen::MatrixXd multipole_matrix(SampleVector& s_set);

	//! Compute the multipole matrix from a vector of SampleVectors
	/*! 
	@param s vector of SampleVector pointers
	@return the multipole matrix 
	*/
	Eigen::MatrixXd multipole_matrix(vector<SampleVector*> s_set);

	//! Compute the multipole matrix from a SampleVector and parse it to a std vector (for debug use only)
	/*! 
	@param s SampleVector
	@return the multipole matrix 
	*/
	vector<vector<double>> multipole_matrix_std(SampleVector& s_set);

	//! Compute the multipole matrix from a SampleVector and parse it to a std vector (for debug use only)
	/*! 
	@param s vector of SampleVector pointers
	@return the multipole matrix 
	*/
	vector<vector<double>> multipole_matrix_std(vector<SampleVector*> s_set);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s Sample
	@param cs coordinate system 
	*/
	void field(Sample& s, CoordinateSystem& cs);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s Sample
	*/
	void field(Sample& s);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param[in][out] s_set SampleVector 
	*/
	void field(SampleVector& s_set){ _field(s_set);};

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	TO BE TESTED
	@param s Sample
	@param cs coordinate system 
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs);

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	@param s Sample
	*/
	void scalar_potential(Sample& s);

	//! Compute the magnetic scalar potential, at the position of the sample
	/*! 
	@param s_set SampleVector 
	*/
	void scalar_potential(SampleVector& s_set){ _scalar_potential(s_set);};

	//! Set the coordinate system
	//! @Warning This function does not compute the multipole coefficients in the new system
	void set_coordinate_system(CoordinateSystem& c) { coord_system = c; }

	//! Change the reference radius and compute new coefficients
	/*! 
	@param r new reference radius
	*/
	void change_ref_radius(double r0);

	//! Initialize a sample set on the reference circle, with radia field values
	/*! 
	param[out] s_set sample set 
	*/
	void init_reference_set(SampleVector& s_set) { s_set.samples_on_circle(ref[0], 2 * degree, 'r'); };

	// //! Initialize and return a sample set on the reference circle, with radia field values
	// /*! return s_set sample set */
	// SampleVector init_reference_set() { SampleVector s_set; s_set.resize(degree); init_reference_set(s_set); return s_set; };

	//! Compute the strengths
	/*!	
	@param[out] s multipole strengths
	*/
	void strength(Strength& s);

	//! Compute the strengths
	/*!	
	@return strengths multipole strengths
	*/
	Strength strength() { Strength s; strength(s); return s; };

	//! Compute the normalized strengths
	/*! 
	@param[in] b_rho magnetic rigidity
	@param[in] u magnetic rigidity unit 
	@param[out] s multipole strength 
	*/
	void normalized_strength(double b_rho, Unit& u, Strength& s);

	//! Compute the normalized strengths
	/*! 
	@param[in] b_rho magnetic rigidity in [T m]
	@param[out] s multipole strength 
	*/
	void normalized_strength(double b_rho, Strength& s) { Unit u("Tm"); normalized_strength(b_rho, u, s); };

	//! Compute the normalized strengths
	/*! 
	@param b_rho magnetic rigidity expressed in (T m)
	@param u magnetic rigidity unit
	@return s multipole strength 
	*/
	Strength normalized_strength(double b_rho, Unit& u) { Strength s; normalized_strength(b_rho, u, s); return s; };

	//! Compute the normalized strengths
	/*! 
	@param b_rho magnetic rigidity expressed in (T m)
	@return s multipole strength 
	*/
	Strength normalized_strength(double b_rho) { Strength s; normalized_strength(b_rho, s); return s; };

private:
	//! Compute the complex potential at sample location and return the complex value
	/*! 
	@param s Sample
	@param cs coordinate system 
	*/
	complex<double> complex_potential(Sample& s, CoordinateSystem& cs);

	// Functions to be implemented for faster parallel computations
	void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) {}; //!< Compute the scalar potential at sample position, using a different coordinate system
	void scalar_potential(Sample& s,  double coef_lm, size_t l, size_t m) {}; //!< Compute the scalar potential at sample position, using a different coordinate system
	void field(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) {}; //!< Compute the field at sample position, using a different coordinate system	
	void field(Sample& s, double coef_lm, size_t l, size_t m) {}; //!< Compute the field at sample position, using a different coordinate system	

};

// -----------------------------------------
// 2D elliptic multipoles
// -----------------------------------------
//! Elliptic2D class
class Elliptic2D : public Multipole2D {

public:
	//! Elliptic2D constructor.
	Elliptic2D() : Multipole2D() { set_model_type("elliptic_2d"); init_units(); metadata.set_creation_time(); };

	//! Elliptic2D constructor.
	/*! 
	@param u field unit 
	*/
	Elliptic2D(const Unit& uf) : Elliptic2D() { unit_field = uf; };

	//! Elliptic2D constructor.
	/*! 
	@param uf field unit 
	@param ud distance unit 
	*/
	Elliptic2D(const Unit& uf, const Unit& ud) : Elliptic2D() { unit_field = uf; unit_distance = ud; };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis 
	@param b semi-minor axis 
	*/
	Elliptic2D(double a, double b) : Elliptic2D() { calc_set_ref_coord(a, b); };
	
	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis 
	@param b semi-minor axis
	@param u field unit 
	*/
	Elliptic2D(double a, double b, const Unit& u) : Elliptic2D(u) { calc_set_ref_coord(a, b); };

	//! Elliptic2D constructor.
	/*! 
	@param r0 reference radius
	@param uf field unit 
	@param ud distance unit 
	*/
	Elliptic2D(double a, double b, const Unit& uf, const Unit& ud) :  Elliptic2D(uf, ud){ calc_set_ref_coord(a, b); };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param d degree 
	*/
	Elliptic2D(double a, double b, size_t d) : Elliptic2D(a, b) { set_degree(d); };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param d degree 
	@param uf field unit
	*/
	Elliptic2D(double a, double b, size_t d, const Unit& uf) : Elliptic2D(a, b, uf) { set_degree(d); };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param d degree
	@param uf field unit
	@param ud distance unit 
	*/
	Elliptic2D(double a, double b, size_t d, const Unit& uf, const Unit& ud) : Elliptic2D(a, b, uf, ud) { set_degree(d); };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param samples SampleVector 
	*/
	Elliptic2D(double a, double b, SampleVector& s_set) :Elliptic2D(a, b) { init_from_set(s_set); };

	//! Elliptic2D constructor.
	/*! 
	@param a semi-major axis
	@param b semi-minor axis
	@param d degree
	@param samples SampleVector 
	*/
	Elliptic2D(double a, double b, size_t d, SampleVector& s_set) :Elliptic2D(a, b, d) { init_from_set(s_set); };

	//! Compute the multipole matrix from a SampleVector
	/*! 
	@param s SampleVector
	@return the multipole matrix 
	*/
	Eigen::MatrixXd multipole_matrix(SampleVector& s_set);

	//! Compute the multipole matrix from a vector of SampleVector
	/*! 
	@param s vector of SampleVector
	@return the multipole matrix 
	*/
	Eigen::MatrixXd multipole_matrix(vector<SampleVector*> vs_set);

	//! Compute and set reference elliptical coordinates
	/*!	
	@param a semi-major axis 
	@param b semi-minor axis
	*/
	void calc_set_ref_coord(double a, double b);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s Sample
	@param cs coordinate system 
	*/
	void field(Sample& s, CoordinateSystem& cs);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s Sample
	*/
	void field(Sample& s) { CoordinateSystem cs; field(s, cs);};

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s_set SampleVector 
	*/
	void field(SampleVector& s_set){ _field(s_set);};

	//! Compute the semi axes from the reference elliptic coordinates
	/*! 
	@return p pair (a, b)
	*/
	pair<double, double> get_semi_axes();

	//! Init a SampleVector on the reference 
	void init_reference_set(SampleVector& s_set){ 
		pair<double, double> ab = get_semi_axes();
		s_set.resize(degree);
		s_set.samples_on_ellipse(ab.first, ab.second, s_set.get_size(), false);
	}

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	TO BE TESTED
	@param s Sample
	@param cs coordinate system 
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs);

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	TO BE TESTED
	@param s Sample
	*/
	void scalar_potential(Sample& s);

	//! Compute the magnetic scalar potential, at the position of the sample
	/*! 
	@param s_set SampleVector 
	*/
	void scalar_potential(SampleVector& s_set){ _scalar_potential(s_set);};


private:
	//! Compute the complex potential at sample location and return the complex value
	/*! 
	@param s Sample
	@param cs coordinate system 
	*/
	complex<double> complex_potential(Sample& s, CoordinateSystem& cs);

	// Functions to be implemented for faster parallel computations
	void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m){}; //!< Compute the scalar potential at sample position, using a different coordinate system
	void scalar_potential(Sample& s,  double coef_lm, size_t l, size_t m){}; //!< Compute the scalar potential at sample position, using a different coordinate system
	void field(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m){}; //!< Compute the field at sample position, using a different coordinate system	
	void field(Sample& s, double coef_lm, size_t l, size_t m){}; //!< Compute the field at sample position, using a different coordinate system	

};

// -----------------------------------------
// Base class for 3D multipole objects
// -----------------------------------------
//! Multipole3D class
/*! 
This class is a generic class for multipole based models. It contains:
- a degree 
- multipole coefficients defined as follows:
	coef[0][] for Y[0, 0] (= 0 for magnetic field)
	coef[1][] for Y[1, 0], Y[1, 1], Y[1, -1]
	coef[2][] for Y[2, 0], Y[2, 1], Y[2, 2], Y[2, -1], Y[2, -2]
	...
- a reference (radius, semi axis, etc.)
It inherits from MagFieldModel
*/
class Multipole3D : public MagFieldModel{	
	public:

		size_t get_degree() { return degree; }; //!< Get the degree
		void get_coef(vector<vector<double>>& v) { v = coef; }; //!< Get the coefficients
		vector<vector<double>> get_coef() { return coef; }; //!< Get the coefficients
		virtual size_t get_coef_size() = 0; //!< Get the number of coefficients
		virtual size_t get_coef_size(size_t deg) = 0; //!< Number of coefficients 

		void set_ref(const vector<double>& v) { ref = v; }; //!< Set the reference from a vector
		void get_ref(vector<double>& v) { v = ref; }; //!< Get the reference
		vector<double> get_ref() { return ref; }; //!< Get the reference

		//! Set coef eps
		/*! The coefficients below this value will be ignored
			Set a value <= 0 to get all coefficients
		@param e tolerance
		*/
		void set_coef_eps(double e){ coef_eps = abs(e); update_coef_idx(); }

		//! Get coef eps
		/*! Return the coefficient tolerances
		@return e tolerance
		*/
		double get_coef_eps() { return coef_eps; }

		//! Change the field units and update the coefficients
		/*!	
		@param u Unit 
		*/
		void set_unit_field(Unit& u);

		//! Change the distance units and update reference
		/*!	
		@param u Unit 
		*/
		void set_unit_distance(Unit& u);

		//! Initialize with a SampleVector
		/*! 
		@param s SampleVector 
		*/
		void init_from_set(SampleVector& s_set);

		//! Initialize with a SampleVector
		/*! 
		@param s vector of SampleVector pointers 
		*/
		void init_from_set(vector<SampleVector*> s_set);

		//! Compute the multipole matrix from a vector of SampleVectors with field values
		/*! 
		@param s_set SampleVector
		@return the multipole matrix 
		*/
		Eigen::MatrixXd multipole_matrix(SampleVector& s_set); 

		//! Mutipole matrix for a set of sample sets
		/*! 
		@param vs_set set of sample vectors
		@return the multipole matrix 
		*/
		Eigen::MatrixXd multipole_matrix(vector<SampleVector*> vs_set);

		//! Compute the multipole matrix from a vector of SampleVectors with field values and return a vector
		/*! 
		@param s_set SampleVector
		@return the multipole matrix as a vector
		*/
		vector<vector<double>> multipole_matrix_std(SampleVector& s_set);

		//! Set the dr / ref ratio to compute small displacements as dref_ref * ref[0] (to compute derivatives)
		/*! 
		@param dref_ref ratio of small displacements to ref
		*/
		void set_dref_ref(double d) {dref_ref = d;}

		//! Get the dr / ref ratio to compute small displacements as dref_ref * ref[0] (to compute derivatives)
		/*! 
		@return dref_ref
		*/
		double get_dref_ref(){return dref_ref;}

		//! Scale the coefficients by a scalar
		/*!
		@param f scale factor
		*/
		void scale(double d);

	protected:
		size_t degree = 8;			// Multipole degree
		vector<vector<double>> coef;		// Multipole coefficients
		vector<double> ref;					// Reference radius, semi axis, etc...
		double dref_ref = 1e-3; 			// Small displacements are defined as dref_ref * ref[0]
		vector<pair<size_t, size_t>> coef_idx;	// Index of non null coefficients
		double coef_eps = -1; 			// Coefficients with absolute values below this will be ignored (take all coefs if this value is <= 0)

		//! Magnetic field as the gradient of the scalar potential, at the position of the sample
		/*! 
		@param s Sample 
		@param cs Coordinate system 
		*/
		void field_from_scalar_potential_grad(Sample& s, CoordinateSystem& cs);	

		//! Magnetic field as the gradient of the scalar potential, at the position of the sample (internal use for parallelization only)
		/*! 
		@param s Sample 
		@param cs Coordinate system 
		@param coef_lm Coefficient at l, m
		@param l Degree 
		@param m Order
		*/
		void field_from_scalar_potential_grad(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m);	

		//! Update coefficient indices
		void update_coef_idx();

		virtual void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) = 0; //!< Compute the scalar potential at sample position, using a different coordinate system
		virtual void scalar_potential(Sample& s,  double coef_lm, size_t l, size_t m) = 0; //!< Compute the scalar potential at sample position, using a different coordinate system
		virtual void scalar_potential(Sample& s, CoordinateSystem& cs) = 0;
		virtual void field(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) = 0; //!< Compute the field at sample position, using a different coordinate system	
		virtual void field(Sample& s, double coef_lm, size_t l, size_t m) = 0; //!< Compute the field at sample position, using a different coordinate system	
		virtual void field(Sample& s, CoordinateSystem& cs) = 0;
};

// -----------------------------------------
// 3D spherical multipoles
// -----------------------------------------
//! Spherical3D class
class Spherical3D : public Multipole3D {

public:
	//! Spherical3D constructor.
	Spherical3D() : Multipole3D() { 
		set_model_type("spherical_3d"); 
		init_units(); 
		set_degree(degree);
		metadata.set_creation_time(); 
		ref.resize(1);
		ref[0] = 1;
		};

	//! Spherical3D contructor
	/*! 
	@param degree degree of the model 
	@param r reference radius 
	*/
	Spherical3D(size_t degree, double r) : Spherical3D(){
		set_degree(degree);
		ref[0] = abs(r);
	} 

	//! Spherical3D contructor
	/*! 
	@param degree degree of the model 
	@param r reference radius 
	@param s sample vector
	*/
	Spherical3D(size_t degree, double r, SampleVector & s) : Spherical3D(degree, r){ init_from_set(s); } 

	void set_degree(size_t d); //!< Set the degree

	size_t get_coef_size(); //!< Get the number of coefficients

	size_t get_coef_size(size_t deg){ return 2 * deg + 1; }; //!< Get the number of coefficients at degree deg
	
	//! Set the spherical harmonic coefficients
	/*! 
	@param v Vector of coefficents, organized as [(c[0][0]), (c[1][0], c[1][1], c[1][-1]), ...]
	*/
	void set_coef(const vector<vector<double>>& v);

	//! Set a single coefficient. The coefficients are organized as [(c[0][0]), (c[1][0], c[1][1], c[1][-1]), ...].  
	/*! 
	@param l degree index of the coefficient (0 <= l < degree)
	@param m order index of the coefficient (0 <= m < 2 * l + 1 or -l <= m <= l)
	@param v coefficient value 
	*/
	void set_coef(size_t l, int m, double v); 

	//! Compute the scalar potential and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs);

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	@param s Sample
	*/
	void scalar_potential(Sample& s);

	//! Compute the scalar potential at l, k, with coef_lm and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	@param coef_lm Multipole coefficient at l, m
	@param l Degree
	@param m Order 
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m);

	//! Compute the scalar potential at l, k, with coef_lm and set its value to the sample
	/*! 
	@param s Sample
	@param coef_lm Multipole coefficient at l, m
	@param l Degree
	@param m Order 
	*/
	void scalar_potential(Sample& s, double coef_lm, size_t l, size_t m);

	//! Compute the magnetic scalar potential, at the position of the sample
	/*! 
	@param[in][out] s_set SampleVector 
	*/
	void scalar_potential(SampleVector& s_set){ _scalar_potential(s_set);};

	//! Compute the field and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	*/
	void field(Sample& s, CoordinateSystem& cs) { field_from_scalar_potential_grad(s, cs); };

	//! Magnetic field as the gradient of the scalar potential, at the position of the sample
	/*! 
	@param s Sample 
	*/
	void field(Sample& s){CoordinateSystem cs; field(s, cs); };		

	//! Magnetic field as the gradient of the scalar potential, at the position of the sample
	/*! 
	@param s_set SampleVector 
	*/
	void field(SampleVector& s_set){ _field(s_set);};	

	//! Compute the field and set its value to the sample (used only in parallelization)
	/*! 
	@param s Sample
	@param cs Coordinate system
	@param coef_lm Multipole coef 
	@param l Degree
	@param m Order
	*/
	void field(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) { 
		field_from_scalar_potential_grad(s, cs, coef_lm, l, m); };

	//! Compute the field and set its value to the sample (used only in parallelization)
	/*! 
	@param s Sample
	@param coef_lm Multipole coef 
	@param l Degree
	@param m Order
	*/
	void field(Sample& s,  double coef_lm, size_t l, size_t m) { 
		CoordinateSystem cs;
		field_from_scalar_potential_grad(s, cs, coef_lm, l, m); };

	//! Init a SampleVector on the reference 
	void init_reference_set(SampleVector& s_set){ 
		// NUMBER OF POINTS TO BE CHECKED
		s_set.samples_on_sphere(ref[0], get_degree(), get_degree(), 'r', false);
	}
};

// -----------------------------------------
// 3D cylindrical multipoles (Bessel functions)
// -----------------------------------------
//! Cylindrical3D class
/*! 
	Magnetic field multipoles inside a cylinder
	- The longitudinally homogeneous multipoles are based on  r^n polynomials
	- The others ones are based on In Bessel functions (non radial oscillation and finite at r = 0)
	Please refer to the documentation for the coefficients and normalization
	To summarise, they define the spatial dependence of the scalar potential terms in 
		[long. dipole	, solenoid (sin(m z))	, ..., solenoid (cos(m z))	, ... ]
		[sin(n theta)	, sin(n theta) sin(m z)	, ..., sin (n theta) cos(mz), ... ]
		[...			, ...					, ..., ..., 				, ... ]
		[cos(n theta)	, cos(n theta) sin(m z)	, ..., cos (n theta) cos(mz), ... ]
		[...			, ...					, ..., ..., 				, ... ]
*/
class Cylindrical3D : public Multipole3D {

public:
	//! Cylindrical3D constructor.
	Cylindrical3D() : Multipole3D() { 
		set_model_type("cylindrical_3d"); 
		init_units(); 
		ref.resize(2);
		ref[0] = 1;
		ref[1] = 1;
		set_degree(degree);
		metadata.set_creation_time(); 
		};

	//! Cylindrical3D contructor
	/*! 
	@param degree degree of the model 
	@param order order of the model (describes the longitudinal field variation)
	@param r reference radius 
	@param l reference length
	*/
	Cylindrical3D(size_t d, size_t o, double r, double l) : Cylindrical3D(){
		ref[0] = abs(r);
		ref[1] = abs(l);
		set_degree(d);
		set_order(o);
	};

	//! Cylindrical3D contructor
	/*! 
	@param degree degree of the model 
	@param order order of the model (describes the longitudinal field variation)
	@param r reference radius 
	@param l reference length
	@param s sample vector
	*/
	Cylindrical3D(size_t d, size_t o, double r, double l, SampleVector& s) : Cylindrical3D(d, o, r, l){ init_from_set(s); };

	void set_ref(const vector<double>& v) { ref = v; compute_normalization_coef(); }; //!< Set the reference from a vector
	
	void set_degree(size_t d); //!< Set the degree

	void set_order(size_t o); //!< Set the order

	size_t get_order() { return order; }; //!< Get the order 

	size_t get_coef_size(); //!< Get the number of coefficients

	size_t get_coef_size(size_t deg){ return 2 * order + 1; }; //!< Get the number of coefficients at degree deg
	
	//! Set the cylindrial harmonic coefficients
	/*! 
	@param v Vector of coefficents, organized as [(c0, A00, A10, ...), (c1, A01, A11), ...]
	*/
	void set_coef(const vector<vector<double>>& v);

	//! Set a single coefficient
	/*! The normalization factors and the storage of the coefficients is defined in the documentation. 
	To summarise, they define the spatial dependence of the scalar potential terms in 
		[long. dipole	, solenoid (sin(m z))	, ..., solenoid (cos(m z))	, ... ]
		[sin(n theta)	, sin(n theta) sin(m z)	, ..., sin (n theta) cos(mz), ... ]
		[...			, ...					, ..., ..., 				, ... ]
		[cos(n theta)	, cos(n theta) sin(m z)	, ..., cos (n theta) cos(mz), ... ]
		[...			, ...					, ..., ..., 				, ... ]
	@param l degree index of the coefficient (0 <= l <= 2 * degree)
	@param m order index of the coefficient (0 <= m < 2 * l + 1 or -l <= m <= l)
	@param v coefficient value 
	@param update update the coefficient list if true
	*/
	void set_coef(size_t l, size_t m, double v, bool update_list=true); 

	//! Set all coefficients to zero
	void set_coef_zero();

	//! Compute the scalar potential and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs);

	//! Compute the scalar potential at sample location and overwrite sample's value
	/*! 
	@param s Sample
	*/
	void scalar_potential(Sample& s);

	//! Compute the scalar potential at l, k, with coef_lm and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	@param coef_lm Multipole coefficient at l, m
	@param l Degree
	@param m Order 
	*/
	void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m);

	//! Compute the scalar potential at l, k, with coef_lm and set its value to the sample
	/*! 
	@param s Sample
	@param coef_lm Multipole coefficient at l, m
	@param l Degree
	@param m Order 
	*/
	void scalar_potential(Sample& s, double coef_lm, size_t l, size_t m);

	//! Compute the magnetic scalar potential, at the position of the sample
	/*! 
	@param[in][out] s_set SampleVector 
	*/
	void scalar_potential(SampleVector& s_set){ _scalar_potential(s_set);};

	//! Compute the field and set its value to the sample
	/*! 
	@param s Sample
	@param cs Coordinate system
	*/
	void field(Sample& s, CoordinateSystem& cs) { field_from_scalar_potential_grad(s, cs); };

	//! Magnetic field as the gradient of the scalar potential, at the position of the sample
	/*! 
	@param s Sample 
	*/
	void field(Sample& s){CoordinateSystem cs; field(s, cs); };		

	//! Magnetic field as the gradient of the scalar potential, at the position of the sample
	/*! 
	@param s_set SampleVector 
	*/
	void field(SampleVector& s_set){ _field(s_set);};	

	//! Compute the field and set its value to the sample (used only in parallelization)
	/*! 
	@param s Sample
	@param cs Coordinate system
	@param coef_lm Multipole coef 
	@param l Degree
	@param m Order
	*/
	void field(Sample& s, CoordinateSystem& cs, double coef_lm, size_t l, size_t m) { 
		field_from_scalar_potential_grad(s, cs, coef_lm, l, m); };

	//! Compute the field and set its value to the sample (used only in parallelization)
	/*! 
	@param s Sample
	@param coef_lm Multipole coef 
	@param l Degree
	@param m Order
	*/
	void field(Sample& s,  double coef_lm, size_t l, size_t m) { 
		CoordinateSystem cs;
		field_from_scalar_potential_grad(s, cs, coef_lm, l, m); };

	//! Init a SampleVector on the reference 
	void init_reference_set(SampleVector& s_set){ 
		// Number of coefficients to check
		s_set.samples_on_cylinder(ref[0], ref[0], 4 * get_degree(), 4 * order, 'r', false, coord_system);
	}

	vector<vector<double>> get_normalization_coef_std(); // For debug

private:

	void compute_normalization_coef(); // Compute the normalisation coefficients
	size_t order = 8;			// Multipole order
	vector<vector<double>> normalization_coef; // Coeffients use to normalize the field
};
