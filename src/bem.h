/*!
   \file		bem.h
   \brief		Boundary Element Methods (BEM) and derivated classes
   \author		Gael Le Bec
   \version		1
   \date		2021
   \copyright	GNU Public License.
 */

#pragma once
#include <string>
#include <vector>
#include <complex>
#include <cmath>

#include "mag_field_model.h"

//#include "strength.h"

using namespace std;

// -----------------------------------------
// Base class for 2D BEM models
// -----------------------------------------
//! BEM2D class
class BEM2D : public MagFieldModel {
public:
	BEM2D(); //!< BEM2D constructor

	BEM2D(const BEM2D& b); //!< BEM2D copy constructor

	//! BEM2D constructor
	/*!
	@param field_set SampleVector with field values defining boundary elements 
	*/
	BEM2D(SampleVector & field_set){ init_from_set(field_set); };

	//! BEM2D constructor
	/*! 
	@param vertex_set vertex points 
	@param field_set field values 
	*/
	BEM2D(SampleVector& vertex_set, SampleVector & field_set){ init_from_set(vertex_set, field_set); };

	//! Get the size of the boundary
	size_t get_size(){return n;};

	//! Set the vertex points from a sample vector
	void set_vertex(SampleVector& s_set);
	
	//! Set the vertex points
	/*! 
	@param s 'x' or 'y' specification 
	@param xy_coord set of vertex point x or y coordinates
	*/
	void set_vertex(string s, const vector<double> xy_coord);

	//! Set the vertex points
	/*! 
	@param x_coord set of vertex point x coordinates 
	@param y_coord set of vertex point y coordinates 
	*/
	void set_vertex(const vector<double> x_coord, const vector<double> y_coord);	
	
	//! Set the vertex points from a sample vector describing the field assuming field are measured between vertex points
	/*! 
	@param field_set field sample values 
	*/
	void set_vertex_from_field(SampleVector& field_set);

	//! Get the vertex points
	/*! 
	@param[out] x_coord set of vertex point x coordinates
	*/
	void get_vertex_x(vector<double>& x_coord){x_coord = vertex_x;};

	//! Get the vertex points
	/*! 
	@return x_coord set of vertex point x coordinates 
	*/
	vector<double> get_vertex_x();

	//! Get the vertex points
	/*! 
	@param[out] y_coord set of vertex point y coordinates 
	*/
	void get_vertex_y(vector<double>& y_coord){y_coord = vertex_y;};

	//! Get the vertex points
	/*! 
	@return y_coord set of vertex point y coordinates 
	*/
	vector<double> get_vertex_y();

	//! Get the vertex points
	/*! 
	@param s select "x" or "y" coordinates
	@return coord set of vertex point x or y coordinates 
	*/
	vector<double> get_vertex(string s);
	
	//! Check boundary attributes
	/*! 
	@return true if the boundary is valid 
	*/
	bool boundary_is_valid();

	// Inherited functions TO BE IMPLEMENTED
	void scalar_potential(Sample& s){}; //!< Compute the scalar potential at sample position
	void scalar_potential(Sample& s, CoordinateSystem& cs){}; //!< Compute the scalar potential at sample position, using a different coordinate system
	//void scalar_potential(Sample& s, CoordinateSystem& cs, double coef_lm, unsigned int l, unsigned int m){}; //!< Compute the scalar potential at sample position, using a different coordinate system
	//void scalar_potential(Sample& s,  double coef_lm, unsigned int l, unsigned int m){}; //!< Compute the scalar potential at sample position, using a different coordinate system

	void set_unit_field(Unit& u){}; //!< Set the field unit
	void set_unit_distance(Unit& u){}; //!< Set the distance unit
	void init_reference_set(SampleVector& s_set){};
	
	void set_coordinate_system(CoordinateSystem & c){};

	//void field(Sample& s, CoordinateSystem& cs, double coef_lm, unsigned int l, unsigned int m){}; //!< Compute the field at sample position, using a different coordinate system	
	//void field(Sample& s, double coef_lm, unsigned int l, unsigned int m){};

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s a Sample
	@param cs a coordinate system 
	*/
	void field(Sample& s, CoordinateSystem& cs);

	//! Compute the field at sample location and overwrite sample's vector values
	/*! 
	@param s a Sample
	*/
	void field(Sample& s){ field(s, coord_system);};

	//! Compute the field at sample locations
	/*! 
	@param s Sample vector 
	*/
	void field(SampleVector& s);

	//! Initialize 
	/*! 
	@param  vertex_set vertex specifications 
	@param field_set field specificatnions 
	*/
	void init(SampleVector& vertex_set, SampleVector& field_set);

	//! Initialize 
	/*! 
	@param field_set field specifications 
	*/
	void init(SampleVector& field_set);

	//! Initialize with a SampleVector
	/*! 
	@param field_set  Field values 
	*/
	void init_from_set(SampleVector& field_set);

	//! Initialize with a SampleVector
	/*! 
	@param vertex_set vertex specifications
	@param field_samples field values specifications 
	*/
	void init_from_set(SampleVector& vertex_set, SampleVector& field_samples);

	//! Compute the source potential matrix
	/*! 
	@param field_set field specifications 
	@return a matrix of source contributions 
	*/
	Eigen::MatrixXd source_potential_matrix(SampleVector& field_set);

	//! Compute the source potential matrix and parse it to std vector (for debug use only)
	/*! 
	@param field_set field specifications
	@return a matrix of source contributions 
	*/
	vector<vector<double>> source_potential_matrix_std(SampleVector& field_set);

	//! Return the sources
	/*! 
	@param[out] s sources
	*/
	void get_sources(vector<double>& s) {s = sources;};

	//! Return the sources
	/*! 
	@return s sources (Re: current density, Im: charges)
	*/
	vector<double> get_sources() {vector<double> res; res = sources; return res;};

	//! Set the sources
	/*! 
	@param s sources (Re: current density, Im: charges)
	*/
	void set_sources(vector<double> s);

	//! Resize the model
	void resize(size_t new_size);

	//! Set SVD tolerance
	void set_svd_tol(double tol){svd_tol = tol;};

	//! Get the SVD toleramce
	double get_svd_tol(){return svd_tol;}

	//! Move the multipole
	/*! 
	@param tr Space transform 
	*/
	void move(SpaceTransform* tr);

	//! Scale the coefficients by a scalar
	/*!
	@param f scale factor
	*/
	void scale(double d);

private:

	//! Return the scalar potential from sample field values 
	/*! 
	The sample set defines a contour. The field components parallel to this countour are needed for this computation.
	@param field_set field values 
	@param tol tolerance for parallelism 
	@param n_worng max number of out of tol points (ie corners)
	@return the scalar potential 
	*/
	Eigen::VectorXd scalar_potential_from_samples(SampleVector& field_set, double tol=1e-2, size_t max_wrong_pts=10);

	//! Return the vector potential from sample field values
	/*! 
	The sample set defines a contour. The field components perpendicular to this countour are needed for this computation
	@param field_set field values 
	@param tol tolerance for parallelism 
	@param n_worng max number of out of tol points (ie corners)
	@return the vector potential
	*/
	Eigen::VectorXd vector_potential_from_samples(SampleVector& field_set, double tol=1e-2, size_t max_wrong_pts=100);	
	
	//! Compute the source field matrix (not used anymore)
	/*! 
	@param field_samples field specifications
	@return a matrix of source contributions 
	*/
	Eigen::MatrixXd source_matrix(SampleVector& field_samples);
	
	//! Return projected values of field
	/*! 
	@param field_set field values 
	@return the field projections 
	*/
	Eigen::VectorXd field_projection(SampleVector& field_set);

	//! Fit the sources
	/*! 
	@param field_samples field values
	*/
	void fit_sources(SampleVector& field_samples);

	//! Complex field from a segment [z1, z2] with a linearly variing charge density (Bpl() proc)
	/*! 
	@param z position 
	@param z0 first extremity of the segment 
	@param z1 second extremity of the segment 
	@param sigma0 average value of sources 
	@param sigma1 difference between sources at extremities 
	@return the complex field from the segment 
	*/
	complex<double> complex_field_segment(complex<double> z, complex<double> z0, complex<double> z1, double sigma0, double sigma1);

	//! Complex field from segment k with a linearly variing charge density
	/*! 
	@param z position 
	@param k index of the segment 
	*/
	complex<double> complex_field_segment(complex<double> z, size_t k);

	//! Complex potential from a segment [z1, z2] with a linearly variing charge density (Apl() proc)
	/*! 
	@param z position  
	@param z0 first extremity of the segment 
	@param z1 second extremity of the segment 
	@param sigma0 average value of sources 
	@param sigma1 difference between sources at extremities 
	@return the complex potential from the segment 
	*/
	complex<double> complex_potential_segment(complex<double> z, complex<double> z1, complex<double> z2, double sigma0, double sigma1);

	//! Complex potential from segment k with a linearly variing current density (Bpl() proc)
	/*! 
	@param z position 
	@param k index of the segment 
	*/
	complex<double> complex_potential_segment(complex<double> z, size_t k);

	//! Complex field
	/*! 
	@param z position 
	@return the total field 
	*/
	complex<double> complex_field(complex<double> z);

	//! Sign of a complex double
	complex<double> sign(complex<double> z);

	//! Sign of a double
	double sign(double d);

	//! Complex log
	complex<double> ln(complex<double> z);

	vector<double> sources; // sources
	vector<double> vertex_x;
	vector<double> vertex_y;
	size_t n;

	double svd_tol = 1e-16;
};