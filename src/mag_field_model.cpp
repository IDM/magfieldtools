/*!
   \file		mag_field_model.cpp
   \brief		Magnetic field model class implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "mag_field_model.h"
#include <cmath>


// --------------------------------------------------
// Field Model
// --------------------------------------------------
// Set the coordinate system
void MagFieldModel::set_coordinate_system(CoordinateSystem & c) {
	if (c.is_ortho())
		coord_system = c;
	else
		throw MFTError("MagFieldModel::set_coordinate_system", "Coordinate system is not valid", 0);;
}
// Get the coordinate system
void MagFieldModel::get_coordinate_system(CoordinateSystem & c) {
	c = coord_system;
}
// Set a 2D model
void MagFieldModel::set_2D() {
	is_2D = true;
}
// Set a 3D model
void MagFieldModel::set_3D() {
	is_2D = false;
}
// Return true for 2D models and false for 3D models
bool MagFieldModel::field_model_is_2D() {
	return is_2D;
}
// Set the model type
void MagFieldModel::set_model_type(const string t) {
	model_type = t;
}
// Get the model type
void MagFieldModel::get_model_type(string & t) {
	t = model_type;
}

// Initialize the units with default values
void MagFieldModel::init_units() {

	// Sample
	string s;
	s = "T";
	try {
		unit_field.init(s);
	}
	catch (MFTError e) {
		throw e;
	}
	// Distance
	s = "m";
	try {
		unit_distance.init(s);
	}
	catch (MFTError e) {
		throw e;
	}
}

// Initialize the units from an array of strings
void MagFieldModel::init_units(string u[2]) {
	// Sample
	try {
		unit_field.init(u[0]);
	}
	catch (MFTError e) {
		throw e;
	}
	// Distance
	try {
		unit_distance.init(u[1]);
	}
	catch (MFTError e) {
		throw e;
	}
}

// Field
void MagFieldModel::_field(SampleVector & s_set) {

	// Units
	s_set.set_unit_sample(unit_field);
	s_set.set_unit_distance(unit_distance);

	// Coordinate system
	CoordinateSystem cs_set;
	s_set.get_coordinate_system(cs_set);
	size_t n = s_set.get_size();

	Sample s;

	if (cs_set.is_world()) {
		for (size_t k = 0; k < n; k++) {
			// Get the sample, compute and store the results
			s_set.get_sample(k, s);
			field(s);
			s_set.set_sample(k, s);
		}
	}
	else {
		for (size_t k = 0; k < n; k++) {
			s_set.get_sample(k, s);
			field(s, cs_set);
			s_set.set_sample(k, s);
		}
	}

	// Update the metadata
	Metadata s_md;
	s_md.set_comment("Built from " + metadata.get_label() + ".");
	s_md.set_creation_time();
	s_md.set_modification_time();
	s_set.set_metadata(s_md);
}

// Compute the scalar potential

void MagFieldModel::_scalar_potential(SampleVector & s_set) {

	// Units
	Unit unit_potential = get_unit_field();
	unit_potential.multiply(unit_distance);
	s_set.set_unit_sample(unit_potential, true);
	s_set.set_unit_distance(unit_distance);

	// Coordinate system
	CoordinateSystem cs_set;
	s_set.get_coordinate_system(cs_set);
	size_t n = s_set.get_size();

	Sample s;

	if (cs_set.is_world()) {
		for (size_t k = 0; k < n; k++) {
			// Get the sample, compute and store the results
			s_set.get_sample(k, s);
			scalar_potential(s);
			s_set.set_sample(k, s);
		}
	}
	else {
		for (size_t k = 0; k < n; k++) {
			s_set.get_sample(k, s);
			scalar_potential(s, cs_set);
			s_set.set_sample(k, s);
		}
	}

	// Update the metadata
	Metadata s_md;
	s_md.set_comment("Built from " + metadata.get_label() + ".");
	s_md.set_creation_time();
	s_md.set_modification_time();
	s_set.set_metadata(s_md);
}

// Transform the coordinate system
void MagFieldModel::transform_coordinate_system(SpaceTransform* tr) {

	// Build a set of samples distributed on a circle
	SampleVector s_set;
	init_reference_set(s_set);

	// Compute the field 
	_field(s_set);

	// Apply the space transform
	s_set.transform_coordinate_system(tr);
	s_set.set_coordinate_system(coord_system);

	// Compute the model
	init_from_set(s_set);

	// Apply to the coordinate system
	coord_system.transform(tr);

	metadata.set_modification_time();
}

// Transform the coordinate system
void MagFieldModel::transform_coordinate_system(vector<SpaceTransform*> v_tr) {

	size_t n = v_tr.size();

	// Check the type of the all space transforms first
	for (size_t k = 0; k < n; k++)
		if (!v_tr[k]->is_type("rotation") && !v_tr[k]->is_type("translation"))
			throw MFTError("Circular2D::transform_coordinate_system", "Unknown space transformation", 0);

	// Apply the transformations
	for (size_t k = 0; k < n; k++)
		transform_coordinate_system(v_tr[k]);
}

// Move the model
void MagFieldModel::move(SpaceTransform* tr) {
	// Build a set of samples distributed on the reference boundary
	SampleVector s_set;

	// Set the units
	s_set.set_unit_sample(unit_field, true);
	s_set.set_unit_distance(unit_distance, true);

	init_reference_set(s_set);

	// Compute the field at the samples
	_field(s_set);

	// Move the samples
	s_set.move(tr);

	// Compute the model
	init_from_set(s_set);

	metadata.set_modification_time();
}

// Move the model 
void MagFieldModel::move(vector<SpaceTransform*> v_tr) {

	size_t n = v_tr.size();

	// Check the type of the all space transforms first
	for (size_t k = 0; k < n; k++)
		if (!v_tr[k]->is_type("rotation") && !v_tr[k]->is_type("translation"))
			throw MFTError("Circular2D::move", "Unknown space transformation", 0);

	// Apply the transformations
	for (size_t k = 0; k < n; k++)
		move(v_tr[k]);
}

// Init reference
SampleVector MagFieldModel::init_reference_set(){
	SampleVector s_set;
	init_reference_set(s_set);
	return s_set;
	}
