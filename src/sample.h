/*!
   \file		sample.h
   \brief		Acquisition paramters, MetaData and Sample classes
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */
#pragma once
#include <omp.h>
#include <string>
#include <vector>
#include <complex>
#include <Eigen/Dense>
#include <cmath>
#include <ctime>
#include "coord_system.h"
#include "units.h"
#include "mft_errors.h"
#include "symmetries.h"

#include <iostream> // Debug

#ifndef PI
const double pi = 3.141592653589793;
#define PI
#endif

using namespace std;

// -----------------------------------------
// Acquisition parameters
// -----------------------------------------
class AcquisitionParameters {

};

// -----------------------------------------
// Metadata
// -----------------------------------------
//! Metadata class.
/*! 
This class implements metadata for sampleVectors and magnetic field models. Metadata are
- data_type
- user
- lab
- source (e.g. SW-bench-01, Radia <filename>, Opera, etc.)
- label
- device (e.g. Quad magnet <S/N>, U35/5, etc.)
- creation_time
- modification_time
- comment
- Acquisition parameters (depends on the source)
*/
class Metadata {
public:
	void set_data_type(const string& s) { data_type = s; } //!< Set the data_type string
	void get_data_type(string& s) { s = data_type; } //!< Get the data_type string
	string get_data_type() { return data_type; }; //!< Get the data_type string
	void set_user(const string& s) { user = s; } //!< Set the user string
	void get_user(string& s) { s = user; } //!< Get the user string
	string get_user() { return user; }; //!< Get the user string
	void set_lab(const string& s) { lab = s; } //!< Set the lab string
	void get_lab(string& s) { s = lab; } //!< Get the lab string
	string get_lab() { return lab; }; //!< Get the lab string
	void set_source(const string& s) { source = s; } //!< Set the source string
	void get_source(string& s) { s = source; } //!< Get the source string
	string get_source() { return source; }; //!< Get the source string
	void set_label(const string& s) { label = s; } //!< Set the label string
	void get_label(string& s) { s = label; } //!< Get the label string
	string get_label() { return label; }; //!< Get the label string
	void set_device(const string& s) { device = s; } //!< Set the device string
	void get_device(string& s) { s = device; } //!< Get the device string
	string get_device() { return device; }; //!< Get the device string
	void set_creation_time(const time_t& t) { creation_time = t; } //!< Set the creation time
	void set_creation_time() { creation_time = time(nullptr); }; //!< Set creation time to now
	void get_creation_time(time_t& s) { s = creation_time; } //!< Get the creation time
	time_t get_creation_time() { return creation_time; } //!< Return the creation time
	void set_modification_time(const time_t& s) { modification_time = s; } //!< Set the modification time
	void set_modification_time() { modification_time = time(nullptr);  } //!< Set the modification time to now
	void get_modification_time(time_t& s) { s = modification_time; } //!< Get the modification time
	time_t get_modification_time() { return modification_time; } //!< Return the modification time
	void set_comment(const string& s) { comment = s; } //!< Set the comment string
	void get_comment(string& s) { s = comment; } //!< Get the comment string
	string get_comment() { return comment; }; //!< Get the comment string
	void set_acq_params(AcquisitionParameters* p) { params = p; }; //!< Set the acquisition parameters
	void get_acq_params(AcquisitionParameters* p) { p = params; }; //!< Get the acquisition parameters
private:
	string data_type;
	string user;
	string lab;
	string source;
	string device;
	string label;
	time_t creation_time;
	time_t modification_time;
	string comment;
	AcquisitionParameters* params = nullptr;
};

// -----------------------------------------
// Sample
// -----------------------------------------
//! Sample class.
/*! 
This class implements magnetic field samples. Samples have 
- a value
- a time stamp
- coordinates
- a vector direction specified by a unit vector
- a projection state indicating if the sample gives the 3 components of the field or only a projection on the unit vector direction
The units of value, time and coordinates are not attribues of individual samples, but of sets of samples (SampleVector)
@see SampleVector
*/
class Sample {

public:
	//! Default constructor
	Sample() { value = 0; coordinates << 0, 0, 0; unit_vector << 1, 0, 0; t = 0; }

	//! Constructor
	/*! 
	@param val value 
	@param c coordinates
	@param u unit vector indicating the direction of the field 
	*/
	Sample(double val, Eigen::Vector3d& c, Eigen::Vector3d& u);

	//! Constructor
	/*! 
	@param val value
	@param c coordinates
	@param u unit vector indicating the direction of the field 
	@param b projection status (projection if true)
	*/
	Sample(double val, Eigen::Vector3d c, Eigen::Vector3d& u, bool b) : Sample(val, c, u) { proj = b; }

	//! Constructor
	/*! 
	@param val value
	@param c coordinates
	@param u unit vector indicating the direction of the field 
	@param t time stamp 
	*/
	Sample(double val, Eigen::Vector3d& c, Eigen::Vector3d& u, double t0) : Sample(val, c, u) { t = t0; }

	//! Constructor
	/*! 
	@param val value
	@param c coordinates 
	@param u unit vector indicating the direction of the field 
	@param b projection status 
	@param t time stamp 
	*/
	Sample(double val, Eigen::Vector3d& c, Eigen::Vector3d& u, bool b, double t0) : Sample(val, c, u, b) { t = t0; }

	//! Constructor
	/*! 
	@param val value
	@param c coordinates
	@param u unit vector indicating the direction of the field 
	*/
	Sample(double val, const vector<double>& c, const vector<double>& u);

	//! Constructor
	/*! 
	@param val value
	@param c coordinates
	@param u unit vector indicating the direction of the field
	@param b projection status (projection if true)
	*/
	Sample(double val, const vector<double>& c, const vector<double>& u, bool b) : Sample(val, c, u) { proj = b; }

	//! Constructor
	/*! 
	@param val value
	@param c coordinates 
	@param u unit vector indicating the direction of the field 
	@param t time stamp 
	*/
	Sample(double val, const vector<double>& c, const vector<double>& u, double t0) : Sample(val, c, u) { t = t0; }

	//! Constructor
	/*! 
	@param val value
	@param c coordinates
	@param u unit vector indicating the direction of the field
	@param b projection status (projection if true)
	@param t time stamp 
	*/
	Sample(double val, const vector<double>& c, const vector<double>& u, bool b, double t0) : Sample(val, c, u, b) { t = t0; }

	//! Set the sample value
	/*! 
	@param a value 
	*/
	void set_value(double val); 

	//! Get the sample value
	/*! 
	@return a value 
	*/
	double get_value();

	//! Set vector values
	/*! 
	@param vx first component 
	@param vy second component
	@param vz third component
	*/
	void set_vector_values(double vx, double vy, double vz);

	//! Set vector values
	/*! 
	@param v 3D vector values 
	*/
	void set_vector_values(vector<double>& v);

	//! Get the vector values
	//! @param[out] v vector value  
	void get_vector_values(Eigen::Vector3d& v);

	//! Get the vector values
	//! @param[out] v vector value  
	void get_vector_values(vector<double> v);

	//! Get the vector values
	//! @return v vector value 
	vector<double> get_vector_values();

	//! Set the sample time
	/*! 
	@param t0 time 
	*/
	void set_time(double t0) { t = t0; }

	//! Get the sample time
	/*! 
	@return a time 
	*/
	double get_time() { return t; };
	
	//! Set the coordinates
	/*! 
	@param v vector of coordinates
	*/
	void set_coordinates(const Eigen::Vector3d& v);

	//! Set the coordinates
	/*! 
	@param v vector of coordinates 
	*/
	void set_coordinates(const vector<double>& v);

	//! Set the coordinates
	/*! 
	@param v array of coordinates 
	*/
	void set_coordinates(const double v[3]);
	
	//! Set the coordinates
	/*! 
	@param x coordinate
	@param y coordinate
	@param z coordinate 
	*/
	void set_coordinates(double x, double y, double z);

	//! Get the coordinates
	/*! 
	@param[out] v vector of coordinates 
	*/
	void get_coordinates(Eigen::Vector3d& v);
	
	//! Get the coordinates
	/*! 
	@param[out] v array of coordinates 
	*/
	void get_coordinates(double v[3]);

	//!Get the coordinates
	/*! 
	@param[out] x coordinate
	@param[out] y coordinate
	@param[out] z coordinate
	*/
	void get_coordinates(double& x, double& y, double& z);
	
	//! Get the coordinates
	/*! 
	@return a vector of coordinates 
	*/
	vector<double> get_coordinates();

	//! Set the unit vector and normalize its components
	/*! 
	@param v vector 
	*/
	void set_unit_vector(const Eigen::Vector3d& v);

	//! Set the unit vector and normalize its components
	/*! 
	@param v vector 
	*/
	void set_unit_vector(const vector<double>& v);
	
	//! Set the unit vector and normalize its components
	/*! 
	@param v array of doubles
	*/
	void set_unit_vector(const double v[3]);
	
	//! Set the unit vector and normalize its components
	/*! 
	@param vx component
	@param vy component
	@param vz component 
	*/
	void set_unit_vector(double vx, double vy, double vz);
	
	//! Get the unit vector
	/*! 
	@param[out] v vector of components 
	*/
	void get_unit_vector(Eigen::Vector3d& v);

	//! Get the unit vector
	/*! 
	@return an vector of components 
	*/
	vector<double> get_unit_vector();

	//! Get the unit vector
	/*! 
	@param[out] an vector of components
	*/
	void get_unit_vector(double v[3]);

	//! Get the unit vector
	/*! 
	@param[out] the x component 
	@param[out] the y component 
	@param[out] the z component 
	*/
	void get_unit_vector(double& vx, double& vy, double& vz);
	
	//! Get the vector projection state. The sample gives a projected value if this state is true.
	/*! 
	@param p the projection state
	*/
	void set_proj(const bool p) { proj = p; };
	
	//! Return the vector projection state. The sample gives a projected value if this state is true.
	/*! 
	@return the projection state 
	*/
	bool is_proj() { return proj; }

	//! Apply a space transformation to the sample
	/*!
	@param tr space transformation 
	@see SpaceTransform 
	*/
	void transform(SpaceTransform* tr);

	//! Apply a translation to the sample
	/*! 
	@param tr translation 
	@see SpaceTransform 
	*/
	void translate(SpaceTransform* tr);
	
	//! Apply a translation to the sample
	/*! 
	@param tr translation 
	*/
	void translate(Eigen::Vector3d& tr);

	//! Apply a translation to the sample
	/*! 
	@param tr translation 
	*/
	void translate(vector<double>& tr);

	//! Apply a rotation to the sample
	/*! 
	@param tr rotation 
	@see SpaceTransform 
	*/
	void rotate(SpaceTransform* tr);

	//! Apply a rotation to the sample
	/*! 
	@param center rotation centre
	@param m rotation matrix
	@param im inverse rotation matrix 
	*/
	void rotate(Eigen::Vector3d& centre, Eigen::Matrix3d& m, Eigen::Matrix3d& im);

	//! Make a sample symmetric to this with respect to sym
	/*! 
	@param[in] sym pointer to a symmetry
	@param[out] Sample 
	*/
	void symmetry(Symmetry* sym, Sample& s);

	//! Make a sample symmetric to this with respect to a symmetry plane
	/*! 
	@param[in] sym symmetry plane 
	@param[out] Sample 
	*/
	void symmetry_plane(Symmetry* sym, Sample& s);

	//! Make a sample symmetric to this with respect to a symmetry centre
	/*! 
	@param[in] sym centre of symmetry
	@param[out] Sample 
	*/
	void symmetry_centre(Symmetry* sym, Sample& s);

private:
	double value = 0;	/*!< Sample value */
	double t = 0;	/*!< Time */
	Eigen::Vector3d coordinates; /*!< Coordinates (Eigen vector) */
	Eigen::Vector3d unit_vector; /*!< Unit vector indicating projection direction or vector field direction, depending on the value of the proj attribute (Eigen vector)*/
	bool proj = 0; /*!< The sample is a 3D vector (proj = false) or a projection of a vector on unit_vector (proj = true)*/ 
};


