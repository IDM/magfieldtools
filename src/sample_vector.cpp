/*!
   \file		sample_vector.cpp
   \brief		SampleVector class implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "sample_vector.h"
#include "mag_field_model.h"

//------------------------------------------------------
// SampleVector
//------------------------------------------------------
//SampleVector::SampleVector() {}

// Copy constructor
SampleVector::SampleVector(const SampleVector & s){

	size = s.size;

	unit_sample =  s.unit_sample;
	unit_distance = s.unit_distance;
	unit_time = s.unit_time;

	values_enable = s.values_enable;
	time_enable = s.time_enable;
	coord_enable = s.coord_enable; 
	vector_enable = s.vector_enable;

	config(values_enable, time_enable, coord_enable, vector_enable, size);

	if (values_enable)
		for (size_t k = 0; k < size ; k++)
			value_array[k] = s.value_array[k];

	if (time_enable)
		for (size_t k = 0; k < size ; k++)
			time_array[k] = s.time_array[k];

	if (coord_enable)
		for (size_t k = 0; k < size; k++){
			x_coord_array[k] = s.x_coord_array[k];
			y_coord_array[k] = s.y_coord_array[k];
			z_coord_array[k] = s.z_coord_array[k];
		}

	if (vector_enable)
		for (size_t k = 0; k < size; k++){
			x_unit_vector_array[k] = s.x_unit_vector_array[k];
			y_unit_vector_array[k] = s.y_unit_vector_array[k];
			z_unit_vector_array[k] = s.z_unit_vector_array[k];
			proj_array[k] = s.proj_array[k];
		}

	coord_system = s.coord_system; 
	metadata = s.metadata;
	metadata.set_modification_time(); 
	
}

// Parsing from std vectors
SampleVector::SampleVector(vector<double>& val, vector<double[3]>& xyz, vector<double[3]>& vect_xyz, string u[3], bool p) {

	metadata.set_creation_time();

	// Check the size of the arrays
	size_t s_val = val.size();
	if ((xyz.size() != s_val) || (vect_xyz.size() != s_val)) {
		// Empty the sample set if the sizes are not coherent
		clear();
		throw MFTError("SampleVector::SampleVector", "Input vector sizes are not coherent", 0);
	}
	else {
		time_enable = false;
		resize(s_val);

		set_values(val);
		set_coordinates(xyz);
		set_unit_vector(vect_xyz);
		set_proj(p);

	}
	// Type and units
	init_units(u);

}

// Constructor
SampleVector::SampleVector(const vector<string>& u) {

	if (u.size() > 3)
		throw MFTError("SampleVector::SampleVector", "Too many units specified");

	string u_ar[3] = {"T", "m", "s"};
	size_t n = u.size();

	if (n > 0)
		u_ar[0] = u[0];
	if (n > 1)
		u_ar[1] = u[1];
	if (n > 2)
		u_ar[2] = u[2];

	init_units(u_ar);
}

// Parsing from std vectors: scalar samples
SampleVector::SampleVector(vector<double>& val, vector<double[3]>& xyz, string u[3]) {

	metadata.set_creation_time();

	// Check the size of the arrays
	size_t s_val = val.size();
	if (xyz.size() != s_val) {
		// Empty the sample set if the sizes are not coherent
		clear();
		throw MFTError("SampleVector::SampleVector", "Input vector sizes are not coherent", 0);
	}
	else {
		vector_enable = false;
		time_enable = false;
		resize(s_val);

		set_values(val);
		set_coordinates(xyz);

	}
	// Type and units
	init_units(u);

}

// Clear vectors
void SampleVector::clear() {

	value_array.clear();
	time_array.clear();
	x_coord_array.clear();
	y_coord_array.clear();
	z_coord_array.clear();
	x_unit_vector_array.clear();
	y_unit_vector_array.clear();
	z_unit_vector_array.clear();
	proj_array.clear();

	size = 0;

	metadata.set_modification_time();
}

// Resize vectors
void SampleVector::resize(size_t n) {

	if (values_enable)
		value_array.resize(n);
	else
		value_array.clear();

	if (time_enable)
		time_array.resize(n);
	else
		time_array.clear();

	if (coord_enable) {
		x_coord_array.resize(n);
		y_coord_array.resize(n);
		z_coord_array.resize(n);
	}
	else {
		x_coord_array.clear();
		y_coord_array.clear();
		z_coord_array.clear();
	}

	if (vector_enable) {
		x_unit_vector_array.resize(n);
		y_unit_vector_array.resize(n);
		z_unit_vector_array.resize(n);
		proj_array.resize(n);
	}
	else {
		x_unit_vector_array.clear();
		y_unit_vector_array.clear();
		z_unit_vector_array.clear();
		proj_array.clear();
	}

	size = n;

	metadata.set_modification_time();
}

// Reserve space vectors
void SampleVector::reserve(size_t n) {

	if (values_enable)
		value_array.reserve(n);
	else
		value_array.clear();

	if (time_enable)
		time_array.reserve(n);
	else
		time_array.clear();

	if (coord_enable) {
		x_coord_array.reserve(n);
		y_coord_array.reserve(n);
		z_coord_array.reserve(n);
	}
	else {
		x_coord_array.clear();
		y_coord_array.clear();
		z_coord_array.clear();
	}

	if (vector_enable) {
		x_unit_vector_array.reserve(n);
		y_unit_vector_array.reserve(n);
		z_unit_vector_array.reserve(n);
		proj_array.reserve(n);
	}
	else {
		x_unit_vector_array.clear();
		y_unit_vector_array.clear();
		z_unit_vector_array.clear();
		proj_array.clear();
	}
}

// Parse values from vector
void SampleVector::set_values(const vector<double>& val) {
	if (!values_enable)
		throw MFTError("SampleVector::set_values", "Sample values are disabled", 0);
	if (size == 0)
		resize(val.size());
	if (val.size() == size)
		value_array.assign(val.begin(), val.end());
	else
		throw MFTError("SampleVector::set_values", "Incompatible dimensions", 0);

	metadata.set_modification_time();
}

void SampleVector::set_values(size_t k, double val) {
	if (k >= size) 
		throw MFTError("SampleVector::set_value", "Index out of range");
	if (!values_enable)
		throw MFTError("SampleVector::set_values", "Sample values are disabled");

	value_array[k] = val;
	metadata.set_modification_time();
}

// Parse values from an array
void SampleVector::set_values(double* val) {
	if (!values_enable)
		throw MFTError("SampleVector::set_values", "Sample values are disabled");
	value_array.assign(val, val + size);
	metadata.set_modification_time();

}

// Set all values
void SampleVector::set_values(double val){
	if (!values_enable)
		throw MFTError("SampleVector::set_values", "Sample values are disabled");
	for (size_t k = 0; k < size; k++)
		value_array[k] = val;
		
	metadata.set_modification_time();

}

// Get values
void SampleVector::get_values(vector<double>& val) {
	if (!values_enable)
		throw MFTError("SampleVector::get_values", "Sample values are disabled");
	val.assign(value_array.begin(), value_array.end());
}

// Get values
vector<double> SampleVector::get_values() {
	if (!values_enable)
		throw MFTError("SampleVector::get_values", "Sample values are disabled");
	vector<double> val;
	val.assign(value_array.begin(), value_array.end());
	return val;
}

// Get values
void SampleVector::get_values(string s, vector<double>& val) {
	if (!values_enable)
		throw MFTError("SampleVector::get_values", "Sample values are disabled");
	val.resize(size);
	for (size_t k = 0; k < size; k++) {
		if (!s.compare("x") || !s.compare("X"))
			val[k] = value_array[k] * x_unit_vector_array[k];
		else if (!s.compare("y") || !s.compare("Y"))
			val[k] = value_array[k] * y_unit_vector_array[k];
		else if (!s.compare("z") || !s.compare("Z"))
			val[k] = value_array[k] * z_unit_vector_array[k];
		else
			throw MFTError("SampleVector::get_values", "Unknown component specification");
	}
}

// Get values
vector<double> SampleVector::get_values(string s) {
	if (!values_enable)
		throw MFTError("SampleVector::get_values", "Sample values are disabled");
	vector<double> val(size);

	get_values(s, val);

	return val;
}

// Get values
void SampleVector::get_vector_values(size_t k, vector<double>& val) {
	if (!values_enable || !vector_enable)
		throw MFTError("SampleVector::get_vector_values", "Sample values or vectors are disabled");
	val[0] = value_array[k] * x_unit_vector_array[k];
	val[1] = value_array[k] * y_unit_vector_array[k];
	val[2] = value_array[k] * z_unit_vector_array[k];
}

// Get values
vector<double> SampleVector::get_vector_values(size_t k) {
	if (k > size) throw MFTError("SampleVector::get_vector_values", "Index out of range", 0);
	vector<double> val(3);
	get_vector_values(k, val);
	return val;
}

// Parse time from vector
void SampleVector::set_time_ref(const vector<double>& val) {
	if (!time_enable)
		throw MFTError("SampleVector::set_time_ref", "Sample time is disabled", 0);
	if (size == 0)
		resize(val.size());
	if (val.size() == size)
		time_array.assign(val.begin(), val.end());
	else
		throw MFTError("SampleVector::set_time_ref", "Incompatible dimensions", 0);

	metadata.set_modification_time();

}

// Parse time from vector
void SampleVector::set_time(size_t k, double val) {
	if (!time_enable)
		throw MFTError("SampleVector::set_time", "Sample time is disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::set_time", "Index out of range", 0);
	
	time_array[k] = val;

	metadata.set_modification_time();

}

// Parse time from an array
void SampleVector::set_time(double* val) {
	if (!time_enable)
		throw MFTError("SampleVector::set_time", "Sample time is disabled", 0);
	time_array.assign(val, val + size);

	metadata.set_modification_time();

}

// Get time k
double SampleVector::get_time(size_t k) {
	if (!time_enable)
		throw MFTError("SampleVector::get_time", "Sample time is disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_time", "Index out of range", 0);
	
	return time_array[k];
}

// Parse coordinates from vector
void SampleVector::set_coordinates(vector<double[3]>& val) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (size == 0)
		resize(val.size());
	if (val.size() == size)
		for (size_t k = 0; k < size; k++) {
			x_coord_array[k] = val[k][0];
			y_coord_array[k] = val[k][1];
			z_coord_array[k] = val[k][2];
		}
	else
		throw MFTError("SampleVector::set_coordinates", "Incompatible dimensions", 0);

	metadata.set_modification_time();

}

void SampleVector::set_coordinates(const size_t k, double c[3]) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (k < size) {
		x_coord_array[k] = c[0];
		y_coord_array[k] = c[1];
		z_coord_array[k] = c[2];
	}
	else
		throw MFTError("SampleVector::set_coordinates", "Index out of range", 0);

	metadata.set_modification_time();

}

// Set sample coordinates
void SampleVector::set_coordinates(const size_t k, const Eigen::Vector3d& v) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (k < size) {
		x_coord_array[k] = v(0);
		y_coord_array[k] = v(1);
		z_coord_array[k] = v(2);
	}
	else
		throw MFTError("SampleVector::set_coordinates", "Index out of range", 0);

	metadata.set_modification_time();

}

// Set sample coordinates
void SampleVector::set_coordinates(const size_t k, const vector<double>& c)
{
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (c.size() != 3)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates must have 3 components");
	if (k < size) {
		x_coord_array[k] = c[0];
		y_coord_array[k] = c[1];
		z_coord_array[k] = c[2];
	}
	else
		throw MFTError("SampleVector::set_coordinates", "Index out of range", 0);

	metadata.set_modification_time();

}


// Parse coordinates from vectors
void SampleVector::set_coordinates(const vector<double>& x, const vector<double>& y, const vector<double>& z) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (x.size() == size && y.size() == size && z.size() == size) {
		if (size == 0)
			resize(x.size());
		x_coord_array.assign(x.begin(), x.end());
		y_coord_array.assign(y.begin(), y.end());
		z_coord_array.assign(z.begin(), z.end());
	}
	else
		throw MFTError("SampleVector::set_coordinates", "Incompatible dimensions", 0);

	metadata.set_modification_time();

}

// Parse coordinates from vectors
void SampleVector::set_coordinates(string s, const vector<double>& v) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);
	if (size == 0)
		resize(v.size());
	if (v.size() != size)
		throw MFTError("SampleVector::set_coordinates", "Incompatible dimensions", 0);
	if (!s.compare("x") || !s.compare("X"))
		x_coord_array.assign(v.begin(), v.end());
	else if (!s.compare("y") || !s.compare("Y"))
		y_coord_array.assign(v.begin(), v.end());
	else if (!s.compare("z") || !s.compare("Z"))
		z_coord_array.assign(v.begin(), v.end());
	else
		throw MFTError("SampleVector::set_coordinates", "Unknown coordinate specification", 0);

	metadata.set_modification_time();
}


// Parse coordinates from array
void SampleVector::set_coordinates(double* x, double* y, double* z) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);

	x_coord_array.assign(x, x + size);
	y_coord_array.assign(y, y + size);
	z_coord_array.assign(z, z + size);

	metadata.set_modification_time();

}

// Parse coordinates from array
void SampleVector::set_coordinates(double* xyz) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_coordinates", "Sample coordinates are disabled", 0);

	x_coord_array.assign(xyz, xyz + size);
	y_coord_array.assign(xyz + size + 1, xyz + 2 * size + 1);
	z_coord_array.assign(xyz + 2 * size + 2, xyz + 3 * size + 2);

	metadata.set_modification_time();

}

// Get coordinates
void SampleVector::get_coordinates(vector<double>& x, vector<double>& y, vector<double>& z) {
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	x = x_coord_array;
	y = y_coord_array;
	z = z_coord_array;
}

// Get coordinates
vector<double> SampleVector::get_coordinates(string s) {

	vector<double> v;
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	if ((!s.compare("x")) || (!s.compare("X")))
		v.assign(x_coord_array.begin(), x_coord_array.end());
	else if ((!s.compare("y")) || (!s.compare("Y")))
		v.assign(y_coord_array.begin(), y_coord_array.end());
	else if ((!s.compare("z")) || (!s.compare("Z")))
		v.assign(z_coord_array.begin(), z_coord_array.end());
	else
		throw MFTError("SampleVector::get_coordinates", "Unknown coordinate specification", 0);
	return v;
}

// Get coordinates
void SampleVector::get_coordinates(size_t k, double& x, double& y, double& z) {
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_coordinates", "Index out of range", 0);
	x = x_coord_array[k];
	y = y_coord_array[k];
	z = z_coord_array[k];
}

// Get coordinates
void SampleVector::get_coordinates(size_t k, double c[3]) {
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_coordinates", "Index out of range", 0);
	c[0] = x_coord_array[k];
	c[1] = y_coord_array[k];
	c[2] = z_coord_array[k];
}

// Get coordinates
void SampleVector::get_coordinates(size_t k, Eigen::Vector3d& c) {
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_coordinates", "Index out of range", 0);
	c(0) = x_coord_array[k];
	c(1) = y_coord_array[k];
	c(2) = z_coord_array[k];
}

// Get coordinates
vector<double> SampleVector::get_coordinates(size_t k) {
	if (!coord_enable)
		throw MFTError("SampleVector::get_coordinates", "Sample coordinates are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_coordinates", "Index out of range", 0);
	vector<double> v(3);
	v[0] = x_coord_array[k];
	v[1] = y_coord_array[k];
	v[2] = z_coord_array[k];

	return v;
}

// Parse vector direction from vector
void SampleVector::set_unit_vector(vector<double[3]>& val) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if (val.size() == size)
		for (size_t k = 0; k < size; k++) {
			double n = sqrt(val[k][0] * val[k][0] + val[k][1] * val[k][1] + val[k][2] * val[k][2]);
			if (n > 0){			
				x_unit_vector_array[k] = val[k][0] / n;
				y_unit_vector_array[k] = val[k][1] / n;
				z_unit_vector_array[k] = val[k][2] / n;
				}
			else{
				x_unit_vector_array[k] = 0;
				y_unit_vector_array[k] = 0;
				z_unit_vector_array[k] = 1;
			}
		}
	else
		throw MFTError("SampleVector::set_unit_vector", "Incompatible dimensions", 0);

	metadata.set_modification_time();
}

// Parse vector direction from vector
void SampleVector::set_unit_vector(double vx, double vy, double vz) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	double n = sqrt(vx * vx + vy * vy + vz * vz);
	for (size_t k = 0; k < size; k++) {
		if (n > 0){
			x_unit_vector_array[k] = vx / n;
			y_unit_vector_array[k] = vy / n;
			z_unit_vector_array[k] = vz / n;
		}
		else{
			x_unit_vector_array[k] = 0;
			y_unit_vector_array[k] = 0;
			z_unit_vector_array[k] = 1;
		}

	}
	metadata.set_modification_time();
}

void SampleVector::set_unit_vector(const size_t k, double v[3]) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if (k < size) {
		double n = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		if (n > 0){
			x_unit_vector_array[k] = v[0] / n;
			y_unit_vector_array[k] = v[1] / n;
			z_unit_vector_array[k] = v[2] / n;
		}
		else{
			x_unit_vector_array[k] = 0;
			y_unit_vector_array[k] = 0;
			z_unit_vector_array[k] = 1;
		}
	}
	else
		throw MFTError("SampleVector::set_unit_vector", "Index out of range", 0);

	metadata.set_modification_time();
}

void SampleVector::set_unit_vector(const size_t k, const Eigen::Vector3d& v) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if (k < size) {
		double n = v.norm();
		if (n > 0){
			x_unit_vector_array[k] = v(0) / n;
			y_unit_vector_array[k] = v(1) / n;
			z_unit_vector_array[k] = v(2) / n;
		}
		else{
			x_unit_vector_array[k] = 0;
			y_unit_vector_array[k] = 0;
			z_unit_vector_array[k] = 1;
		}
	}
	else
		throw MFTError("SampleVector::set_unit_vector", "Index out of range", 0);

	metadata.set_modification_time();
}

void SampleVector::set_unit_vector(const size_t k, const vector<double>& v) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if (k < size) {
		double n = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		if (n > 0){
			x_unit_vector_array[k] = v[0] / n;
			y_unit_vector_array[k] = v[1] / n;
			z_unit_vector_array[k] = v[2] / n;
		}
		else{
			x_unit_vector_array[k] = 0;
			y_unit_vector_array[k] = 0;
			z_unit_vector_array[k] = 1;
		}
	}
	else
		throw MFTError("SampleVector::set_unit_vector", "Index out of range", 0);

	metadata.set_modification_time();
}

// Parse vector direction from vector
void SampleVector::set_unit_vector(const vector<double>& vx, const vector<double>& vy, const vector<double>& vz) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if ((vx.size() == size && vy.size() == size && vz.size() == size) || (size == 0)) {
		size = vx.size();
		x_unit_vector_array.assign(vx.begin(), vx.end());
		y_unit_vector_array.assign(vy.begin(), vy.end());
		z_unit_vector_array.assign(vz.begin(), vz.end());

		normalize_unit_vector();
	}
	else
		throw MFTError("SampleVector::set_unit_vector", "Incompatible dimensions", 0);

	metadata.set_modification_time();
}

void SampleVector::set_unit_vector(string s, const vector<double>& v) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	if (size != v.size() && size > 0)
		throw MFTError("SampleVector::set_unit_vector", "Incompatible dimensions", 0);
	if (!size)
		resize(v.size());

	if (!s.compare("x") || !s.compare("X"))
		x_unit_vector_array.assign(v.begin(), v.end());

	else if (!s.compare("y") || !s.compare("Y"))
		y_unit_vector_array.assign(v.begin(), v.end());

	else if (!s.compare("z") || !s.compare("Z"))
		z_unit_vector_array.assign(v.begin(), v.end());

	else
		throw MFTError("SampleVector::set_unit_vector", "Unknown component specification", 0);

	metadata.set_modification_time();
}

// Parse vector direction from arrays
void SampleVector::set_unit_vector(double* vx, double* vy, double* vz) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_unit_vector", "Vector samples are disabled", 0);
	x_unit_vector_array.assign(vx, vx + size);
	y_unit_vector_array.assign(vy, vy + size);
	z_unit_vector_array.assign(vz, vz + size);

	normalize_unit_vector();

	metadata.set_modification_time();
}

// Parse coordinates from array
void SampleVector::set_unit_vector(double* vxyz) {
	if (!coord_enable)
		throw MFTError("SampleVector::set_unit_vector", "Sample coordinates are disabled", 0);

	x_unit_vector_array.assign(vxyz, vxyz + size);
	y_unit_vector_array.assign(vxyz + size + 1, vxyz + 2 * size + 1);
	z_unit_vector_array.assign(vxyz + 2 * size + 2, vxyz + 3 * size + 2);

	normalize_unit_vector();

	metadata.set_modification_time();
}

// Normalize the vector 
void SampleVector::normalize_unit_vector() {
	for (size_t k = 0; k < size; k++) {
		double n = sqrt(x_unit_vector_array[k] * x_unit_vector_array[k] + y_unit_vector_array[k] * y_unit_vector_array[k] + z_unit_vector_array[k] * z_unit_vector_array[k]);
		if (n > 0){
			x_unit_vector_array[k] /= n;
			y_unit_vector_array[k] /= n;
			z_unit_vector_array[k] /= n;
		}
		else{
			x_unit_vector_array[k] = 0;
			y_unit_vector_array[k] = 0;
			z_unit_vector_array[k] = 1;
		}
	}
}

// Get vector directions
void SampleVector::get_unit_vector(vector<double>& x, vector<double>& y, vector<double>& z) {
	if (!vector_enable)
		throw MFTError("SampleVector::get_unit_vector", "Sample vector directions are disabled", 0);
	x = x_unit_vector_array;
	y = y_unit_vector_array;
	z = z_unit_vector_array;
}

// Get vector directions
vector<double> SampleVector::get_unit_vector(string s) {
	if (!vector_enable)
		throw MFTError("SampleVector::get_unit_vector", "Sample vector directions are disabled", 0);
	if (!s.compare("x") || !s.compare("X"))
		return x_unit_vector_array;
	else if (!s.compare("y") || !s.compare("Y"))
		return y_unit_vector_array;
	else if (!s.compare("z") || !s.compare("Z"))
		return z_unit_vector_array;
	else
		throw MFTError("SampleVector::get_unit_vector", "Unknown vector direction specification", 0);
}

// Get vector directions
void SampleVector::get_unit_vector(size_t k, double c[3]) {
	if (!vector_enable)
		throw MFTError("SampleVector::get_unit_vector", "Sample vector directions are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_unit_vector", "Index out of range", 0);
	c[0] = x_unit_vector_array[k];
	c[1] = y_unit_vector_array[k];
	c[2] = z_unit_vector_array[k];
}

// Get vector directions
void SampleVector::get_unit_vector(size_t k, Eigen::Vector3d& c) {
	if (!vector_enable)
		throw MFTError("SampleVector::get_unit_vector", "Sample vector directions are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_unit_vector", "Index out of range", 0);
	c(0) = x_unit_vector_array[k];
	c(1) = y_unit_vector_array[k];
	c(2) = z_unit_vector_array[k];
}

// Get vector directions
vector<double> SampleVector::get_unit_vector(size_t k) {
	if (!vector_enable)
		throw MFTError("SampleVector::get_unit_vector", "Sample vector directions are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::get_unit_vector", "Index out of range", 0);
	vector<double> v(3);
	v[0] = x_unit_vector_array[k];
	v[1] = y_unit_vector_array[k];
	v[2] = z_unit_vector_array[k];
	return v;
}

// Parse proj from vector
void SampleVector::set_proj(const vector<bool>& p) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_proj", "Vector samples are disabled", 0);
	if (p.size() == size)
		proj_array.assign(p.begin(), p.end());
	else
		throw MFTError("SampleVector::set_proj", "Incompatible dimensions", 0);

	metadata.set_modification_time();
}

// Parse proj from array
void SampleVector::set_proj(bool* p) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_proj", "Vector samples are disabled", 0);
	proj_array.assign(p, p + size);

	metadata.set_modification_time();
}

// Set all proj values to p
void SampleVector::set_proj(bool p) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_proj", "Vector samples are disabled", 0);
	for (size_t k = 0; k < size; k++)
		proj_array[k] = p;

	metadata.set_modification_time();
}

// Set kth proj value to p
void SampleVector::set_proj(size_t k, bool p) {
	if (!vector_enable)
		throw MFTError("SampleVector::set_proj", "Vector samples are disabled", 0);
	if (k >= size)
		throw MFTError("SampleVector::set_proj", "Index out of range", 0);

	proj_array[k] = p;

	metadata.set_modification_time();
}

// Initialize the units with default values
void SampleVector::init_units() {

	// Sample
	string s;
	s = "T";
	try {
		unit_sample.init(s);
	}
	catch (MFTError e) {
		throw e;
	}
	// Distance
	s = "m";
	try {
		unit_distance.init(s);
	}
	catch (MFTError e) {
		throw e;
	}
	// Time
	s = "s";
	try {
		unit_time.init(s);
	}
	catch (MFTError e) {
		throw e;
	}

}

// Initialize the units from an array of strings
void SampleVector::init_units(string u[3]) {
	// Sample
	try {
		unit_sample.init(u[0]);
	}
	catch (MFTError e) {
		throw e;
	}
	// Distance
	try {
		unit_distance.init(u[1]);
	}
	catch (MFTError e) {
		throw e;
	}
	// Time
	try {
		unit_time.init(u[2]);
	}
	catch (MFTError e) {
		throw e;
	}
}

// Configure the arrays
void SampleVector::config(bool values_on, bool time_on, bool coord_on, bool vector_on, size_t n) {
	values_enable = values_on;
	time_enable = time_on;
	coord_enable = coord_on;
	vector_enable = vector_on;
	resize(n);
	metadata.set_modification_time();
}

// Build the sample k from arrays
void SampleVector::get_sample(size_t k, Sample& s) {

	if (k > size) throw MFTError("SampleVector::get_sample", "Index out of range", 0);

	if (values_enable)
		s.set_value(value_array[k]);
	else
		s.set_value(0);

	if (time_enable)
		s.set_time(time_array[k]);
	else
		s.set_time(0);

	if (coord_enable)
		s.set_coordinates(x_coord_array[k], y_coord_array[k], z_coord_array[k]);
	else
		s.set_coordinates(0, 0, 0);

	if (vector_enable) {
		s.set_unit_vector(x_unit_vector_array[k], y_unit_vector_array[k], z_unit_vector_array[k]);
		s.set_proj(proj_array[k]);
	}
	else {
		s.set_unit_vector(0, 0, 1);
		s.set_proj(true);
	}

}

Sample SampleVector::get_sample(size_t k) {
	if (k > size) throw MFTError("SampleVector::get_sample", "Index out of range", 0);
	Sample s;
	get_sample(k, s);
	return s;
}

// Fill the array location k from sample
void SampleVector::set_sample(size_t k, Sample& s) {

	if (k > size) throw MFTError("SampleVector::get_sample", "Index out of range", 0);

	if (values_enable)
		value_array[k] = s.get_value();

	if (time_enable)
		time_array[k] = s.get_time();

	if (coord_enable) {
		double x, y, z;
		s.get_coordinates(x, y, z);
		x_coord_array[k] = x;
		y_coord_array[k] = y;
		z_coord_array[k] = z;
	}

	if (vector_enable) {
		double vx = 0, vy = 0, vz = 0;
		s.get_unit_vector(vx, vy, vz);
		x_unit_vector_array[k] = vx;
		y_unit_vector_array[k] = vy;
		z_unit_vector_array[k] = vz;
		proj_array[k] = s.is_proj();
	}

	metadata.set_modification_time();
}

// Add a sample
void SampleVector::push_back(Sample& s) {

	// Value
	if (values_enable)
		value_array.push_back(s.get_value());

	// Time
	if (time_enable)
		time_array.push_back(s.get_time());

	double v[3];
	// Coordinates
	if (coord_enable) {
		s.get_coordinates(v);
		x_coord_array.push_back(v[0]);
		y_coord_array.push_back(v[1]);
		z_coord_array.push_back(v[2]);
	}

	if (vector_enable) {
		// Direction
		s.get_unit_vector(v);
		x_unit_vector_array.push_back(v[0]);
		y_unit_vector_array.push_back(v[1]);
		z_unit_vector_array.push_back(v[2]);
		// Projection
		proj_array.push_back(s.is_proj());
	}

	size += 1;

	metadata.set_modification_time();
}

// Remove the last element
void SampleVector::pop_back() {

	if (size > 0) {
	
		// Value
		if (values_enable)
			value_array.pop_back();

		// Time
		if (time_enable)
			time_array.pop_back();

		// Coordinates
		if (coord_enable) {
			x_coord_array.pop_back();
			y_coord_array.pop_back();
			z_coord_array.pop_back();
		}

		if (vector_enable) {
			// Direction
			x_unit_vector_array.pop_back();
			y_unit_vector_array.pop_back();
			z_unit_vector_array.pop_back();
			// Projection
			proj_array.pop_back();
		}

		size -= 1;

		metadata.set_modification_time();
	}

}

// Erase element k
void SampleVector::erase(size_t k) {

	if (k >= size)
		throw MFTError(" SampleVector::erase", "Index out of range");

	// Value
	if (values_enable)
		value_array.erase(value_array.begin() + k);

	// Time
	if (time_enable)
		time_array.erase(time_array.begin() + k);

	// Coordinates
	if (coord_enable) {
		x_coord_array.erase(x_coord_array.begin() + k);
		y_coord_array.erase(y_coord_array.begin() + k);
		z_coord_array.erase(z_coord_array.begin() + k);
	}

	if (vector_enable) {
		// Direction
		x_unit_vector_array.erase(x_unit_vector_array.begin() + k);
		y_unit_vector_array.erase(y_unit_vector_array.begin() + k);
		z_unit_vector_array.erase(z_unit_vector_array.begin() + k);
		// Projection
		proj_array.erase(proj_array.begin() + k);
	}

	size -= 1;

	metadata.set_modification_time();
}

// Add another array
void SampleVector::add(SampleVector& set_1) {

	// Checks
	if ((values_enable != set_1.get_values_enable()) || (time_enable != set_1.get_time_enable())
		|| (coord_enable != set_1.get_coord_enable()) || (vector_enable != set_1.get_vector_enable()))
		throw MFTError(" SampleVector::add", "Incompatible arrays", 0);

	// Units
	double f;
	Unit u;
	if (values_enable) {
		set_1.get_unit_sample(u);
		// Check the physical quantities
		try {
			f = unit_sample.get_scale_factor_to_unit(u);
			// Change the units if needed
			if (f != 1)
				set_1.set_unit_sample(unit_sample);
		}
		catch (MFTError e) {
			throw e;
		}

	}
	if (time_enable) {
		set_1.get_unit_time(u);
		try {
			f = unit_time.get_scale_factor_to_unit(u);
			if (f != 1)
				set_1.set_unit_time(unit_time);
		}
		catch (MFTError e) {
			throw e;
		}

	}
	if (coord_enable) {
		set_1.get_unit_distance(u);
		try {
			f = unit_distance.get_scale_factor_to_unit(u);
			if (f != 1)
				set_1.set_unit_distance(unit_distance);
		}
		catch (MFTError e) {
			throw e;
		}

	}

	// Insert
	Sample s;
	reserve(size + set_1.get_size());

	for (size_t k = 0; k < set_1.get_size(); k++) {
		set_1.get_sample(k, s);
		push_back(s);
	}

	metadata.set_modification_time();
}

// Set the coordinate system
void SampleVector::set_coordinate_system(CoordinateSystem& c) {
	if (c.is_ortho())
		coord_system = c;
	metadata.set_modification_time();
}

// Get the coordinate system
void SampleVector::get_coordinate_system(CoordinateSystem& c) {
	c = coord_system;
}

// Get the coordinate system
CoordinateSystem SampleVector::get_coordinate_system() {
	return coord_system;
}

void SampleVector::set_unit_sample(Unit& u, bool do_not_check) {

	if (!do_not_check) {
		// Check the compatibility of the units and get the scaling factor
		double f;
		try {
			f = unit_sample.get_scale_factor_to_unit(u);
		}
		catch (MFTError e) {
			throw e;
		}

		// Change the sample values
		if (values_enable)
			for (int i = 0; i < size; i++)
				value_array[i] *= f;
	}

	// Change the units
	unit_sample.copy(u);

	metadata.set_modification_time();
}

void SampleVector::set_unit_distance(Unit& u, bool do_not_check) {

	if (!do_not_check) {
		// Check the compatibility of the units and get the scaling factor
		double f = 0;
		try {
			f = unit_distance.get_scale_factor_to_unit(u);
		}
		catch (MFTError e) {
			throw e;
		}

		// Change the sample values
		if (coord_enable)
			for (int i = 0; i < size; i++) {
				x_coord_array[i] *= f;
				y_coord_array[i] *= f;
				z_coord_array[i] *= f;
			}
	}

	// Change the units
	unit_distance.copy(u);

	metadata.set_modification_time();
}

void SampleVector::set_unit_time(Unit& u, bool do_not_check) {

	if (!do_not_check) {
		// Check the compatibility of the units and get the scaling factor
		double f = 0;
		try {
			f = unit_time.get_scale_factor_to_unit(u);
		}
		catch (MFTError e) {
			throw e;
		}

		// Change the sample values
		double t = 0;
		if (time_enable)
			for (int i = 0; i < size; i++)
				time_array[i] *= f;
	}

	// Change the units
	unit_time.copy(u);

	metadata.set_modification_time();
}

// Transform the coordinate system
void SampleVector::transform_coordinate_system(SpaceTransform* tr) {

	// Transform the coordinate system
	coord_system.transform(tr);

	Sample s;

	// Translate all samples
	if (tr->is_type("translation")) {
		Eigen::Vector3d trans;
		tr->get_axis(trans);

		for (size_t k = 0; k < size; k++) {
			get_sample(k, s);
			s.translate(trans);
			set_sample(k, s);
		}
	}

	// Rotate all samples
	else if (tr->is_type("rotation")) {
		// Centre of the rotation
		Rotation* rot = (Rotation*)tr;
		Eigen::Vector3d centre;
		rot->get_centre(centre);

		// Rotation matrices
		Eigen::Matrix3d m, im;
		rot->matrix(m, false);
		rot->matrix(im, true); // im = m^-1

		// Apply to the samples
		for (size_t k = 0; k < size; k++) {
			get_sample(k, s);
			s.rotate(centre, m, im);
			set_sample(k, s);
		}
	}
	else
		throw MFTError(" SampleVector::transform_coordinate_system", "Unknown space transformation", 0);

	metadata.set_modification_time();
}

// Transform the coordinate system
void SampleVector::transform_coordinate_system(vector<SpaceTransform*> v_tr) {

	size_t n = v_tr.size();

	// Check the type of the all space transforms first
	for (size_t k = 0; k < n; k++)
		if (!v_tr[k]->is_type("rotation") && !v_tr[k]->is_type("translation"))
			throw MFTError("SampleVector::transform_coordinate_system", "Unknown space transformation", 0);

	// Apply the transformations
	for (size_t k = 0; k < n; k++)
		transform_coordinate_system(v_tr[k]);
}

// Transform the sample coordinates and unit vectors
void SampleVector::move(SpaceTransform* tr) {

	Sample s;

	// Translate all samples
	if (tr->is_type("translation")) {
		Eigen::Vector3d trans;
		tr->get_axis(trans);
		trans *= -1;

		for (size_t k = 0; k < size; k++) {
			get_sample(k, s);
			s.translate(trans);
			set_sample(k, s);
		}
	}

	// Rotate all samples
	else if (tr->is_type("rotation")) {
		// Centre of the rotation
		Rotation* rot = (Rotation*)tr;
		Eigen::Vector3d centre;
		rot->get_centre(centre);

		// Rotation matrices
		Eigen::Matrix3d m, im;
		rot->matrix(m, true);
		rot->matrix(im, false); // im = m^-1

		// Apply to the samples
		for (size_t k = 0; k < size; k++) {
			get_sample(k, s);
			s.rotate(centre, m, im);
			set_sample(k, s);
		}
	}
	else
		throw MFTError(" SampleVector::move", "Unknown space transformation", 0);

	metadata.set_modification_time();
}

// Transform the coordinate system
void SampleVector::move(vector<SpaceTransform*> v_tr) {

	size_t n = v_tr.size();

	// Check the type of the all space transforms first
	for (size_t k = 0; k < n; k++)
		if (!v_tr[k]->is_type("rotation") && !v_tr[k]->is_type("translation"))
			throw MFTError("SampleVector::move", "Unknown space transformation", 0);

	// Apply the transformations
	for (size_t k = 0; k < n; k++)
		move(v_tr[k]);
}

// Create samples with zero value distributed on a circle
void SampleVector::samples_on_circle(double r, size_t n_samples, const char& c_xyrt, bool proj, const CoordinateSystem& cs) {

	// Create the sample set
	config(true, true, true, true, n_samples);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);
	Eigen::Vector3d origin_to_point(axis_x);

	// Initialize the samples
	double angle = 2 * pi / n_samples;
	Eigen::Matrix3d m, m1;
	m = Eigen::AngleAxisd(angle, axis_z);
	m1 = Eigen::AngleAxisd(pi / 2, axis_z);

	// This sample vector contains field projections
	set_proj(proj);

	for (size_t k = 0; k < n_samples; k++) {

		// Sample value
		value_array[k] = 0;
		// Sample position
		point_k = origin + r * origin_to_point;
		set_coordinates(k, point_k);
		// Direction
		if (c_xyrt == 'x')
			set_unit_vector(k, axis_x);
		else if (c_xyrt == 'y')
			set_unit_vector(k, axis_y);
		else if (c_xyrt == 'z')
			set_unit_vector(k, axis_z);
		else if (c_xyrt == 'r') {
			Eigen::Vector3d vect;
			vect = origin_to_point / origin_to_point.norm();
			set_unit_vector(k, vect);
		}
		else if (c_xyrt == 't') {
			Eigen::Vector3d vect;
			vect = m1 * origin_to_point;
			vect /= vect.norm();
			set_unit_vector(k, vect);
		}
		else
			throw MFTError("SampleVector::samples_on_circle", "Unknown xyrt specifications", 0);
		// Rotate for next step
		origin_to_point = m * origin_to_point;

	}

	metadata.set_modification_time();
};

// Create samples with zero value distributed on a disk
void SampleVector::samples_on_disk(double r, size_t n_samples_x, const char& c_xyz, bool proj, const CoordinateSystem& cs) {

	// Create the sample set
	config(true, true, true, true, 0);

	// Reserve space
	size_t n = round( n_samples_x * n_samples_x * pi / 4 * 1.1); // Number of samples + 10 %
	reserve(n);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);
	Eigen::Vector3d origin_to_point(axis_x);

	// Initialize the samples
	double x, y;
	double dxy = 2 * r / (n_samples_x - 1);
	Sample s_k;

	for (size_t i = 0; i < n_samples_x; i++)
		for (size_t j = 0; j < n_samples_x; j++) {
			x = -r + dxy * i;
			y = -r + dxy * j;
			if (x * x + y * y <= r * r) {
				point_k = origin + x * axis_x + y * axis_y;

				// Set sample attributes
				s_k.set_value(0);
				s_k.set_coordinates(point_k);
				s_k.set_proj(proj);

				// Direction
				if (c_xyz == 'x')
					s_k.set_unit_vector(axis_x);
				else if (c_xyz == 'y')
					s_k.set_unit_vector(axis_y);
				else if (c_xyz == 'z')
					s_k.set_unit_vector(axis_z);
				else
					throw MFTError("SampleVector::samples_on_disk", "Unknown xyz specifications", 0);

				// Pushback
				push_back(s_k);
			}
		}

	metadata.set_modification_time();
};

// Create samples with zero value distributed on an ellipse
void SampleVector::samples_on_ellipse(double a, double b, size_t n_samples, const char& c_xyrt, bool proj, const CoordinateSystem& cs) {

	if (a <= b)
		throw MFTError("SampleVector::samples_on_ellipse", "The semi-major axis must be larger than the semi-minor axis", 0);

	// Create the sample set
	config(true, true, true, true, n_samples);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k, origin_to_point;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);
	Eigen::Matrix3d m, m1;
	m1 = Eigen::AngleAxisd(- pi / 2, axis_z);

	// Initialize the samples
	double angle = 2 * pi / n_samples;
	double eps = pow(a * a - b * b, 0.5);
	double eta = atanh(b / a);
	complex<double> z, dz_dphi;
	complex<double> ii(0, 1);
	double phi;

	// This sample vector contains field projections
	set_proj(proj);

	for (size_t k = 0; k < n_samples; k++) {

		// Sample value
		value_array[k] = 0;
		// Point in the complex plane
		phi = 2 * k * pi / n_samples;
		z = eps * cosh(eta + ii * phi);
		// Sample position
		point_k = origin + z.real() * axis_x + z.imag() * axis_y;
		set_coordinates(k, point_k);
		origin_to_point = point_k - origin;
		// Direction
		if (c_xyrt == 'x')
			set_unit_vector(k, axis_x);
		else if (c_xyrt == 'y')
			set_unit_vector(k, axis_y);
		else if (c_xyrt == 'z')
			set_unit_vector(k, axis_z);
		else if (c_xyrt == 'r') {
			dz_dphi = eta * ii * sinh(eta + ii * phi);
			Eigen::Vector3d vect;
			vect = dz_dphi.real() * axis_x + dz_dphi.imag() * axis_y;
			vect /= vect.norm();
			vect = m1 * vect;
			set_unit_vector(k, vect);
		}
		else if (c_xyrt == 't') {
			dz_dphi = eta * ii * sinh(eta + ii * phi);
			Eigen::Vector3d vect;
			vect = dz_dphi.real() * axis_x + dz_dphi.imag() * axis_y;
			vect /= vect.norm();
			set_unit_vector(k, vect);
		}
		else
			throw MFTError("SampleVector::samples_on_ellipse", "Unknown xyrt specifications", 0);

	}

	metadata.set_modification_time();
};

// Samples on a sphere
void SampleVector::samples_on_sphere(double r, size_t n_samples_theta, size_t n_samples_phi, const char& c_xyzr, bool proj, const CoordinateSystem& cs) {

	size_t n_samples = n_samples_theta * n_samples_phi;

	// Create the sample set
	config(true, true, true, true, n_samples);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);
	Eigen::Vector3d radial_vect;

	// Rotation matrices
	double dtheta = pi / (n_samples_theta - 1);
	double dphi = 2 * pi / (n_samples_phi - 1);
	//Eigen::Matrix3d m_theta, m_phi, m_pi_2;
	//m_theta = Eigen::AngleAxisd(dtheta, axis_z);
	//m_phi = Eigen::AngleAxisd(dphi, axis_z);
	//m_pi_2 = Eigen::AngleAxisd(pi / 2, axis_z);

	// Set the projection state
	set_proj(proj);
	size_t idx = 0;
	for (size_t k = 0; k < n_samples_theta; k++) 
		for (size_t l = 0; l < n_samples_phi; l++ ) { 

			// Sample value
			value_array[idx] = 0;
			// Sample position
			radial_vect << 	sin(k * dtheta) * cos(l * dphi), 
							sin(k * dtheta) * sin(l * dphi),
							cos(k * dtheta);
			point_k = origin + r * radial_vect;
			set_coordinates(idx, point_k);
			// Direction
			if (c_xyzr == 'x')
				set_unit_vector(idx, axis_x);
			else if (c_xyzr == 'y')
				set_unit_vector(idx, axis_y);
			else if (c_xyzr == 'z')
				set_unit_vector(idx, axis_z);
			else if (c_xyzr == 'r') {
				Eigen::Vector3d vect;
				vect = radial_vect / radial_vect.norm();
				set_unit_vector(idx, vect);
			}
			else
				throw MFTError("SampleVector::samples_on_sphere", "Unknown xyrt specifications", 0);

			idx++;
			

		}

	metadata.set_modification_time();
};

// Samples on a cylinder
void SampleVector::samples_on_cylinder(double r, double len, size_t n_samples_theta, size_t n_samples_z, const char& c_xyzr, bool proj, const CoordinateSystem& cs) {

	size_t n_samples = n_samples_theta * n_samples_z;

	// Create the sample set
	config(true, true, true, true, n_samples);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);
	Eigen::Vector3d radial_vect;

	// Rotation matrices
	double dtheta = 2 * pi / n_samples_theta;
	double dz = len / (n_samples_z - 1);

	// Set the projection state
	set_proj(proj);
	size_t idx = 0;
	for (size_t k = 0; k < n_samples_theta; k++) 
		for (size_t l = 0; l < n_samples_z; l++ ) { 

			// Sample value
			value_array[idx] = 0;
			// Sample position
			radial_vect = axis_x * cos(k * dtheta) + axis_y * sin(k * dtheta);
			point_k = origin + r * radial_vect + axis_z * (- len / 2 + dz * l);
			set_coordinates(idx, point_k);
			// Direction
			if (c_xyzr == 'x')
				set_unit_vector(idx, axis_x);
			else if (c_xyzr == 'y')
				set_unit_vector(idx, axis_y);
			else if (c_xyzr == 'z')
				set_unit_vector(idx, axis_z);
			else if (c_xyzr == 'r') {
				Eigen::Vector3d vect;
				vect = radial_vect / radial_vect.norm();
				set_unit_vector(idx, vect);
			}
			else
				throw MFTError("SampleVector::samples_on_cylinder", "Unknown xyrt specifications", 0);

			idx++;
		
		}

	metadata.set_modification_time();
};


// Samples on a rectangle
void SampleVector::samples_on_rectange(double x0, double y0, double x1, double y1, size_t nx, size_t ny, const char& c_xyzr, bool proj, const CoordinateSystem& cs) {

	size_t n_samples = nx * ny;

	if (!n_samples)
		throw MFTError("SampleVector::samples_on_rectangle", "Number of samples must be positive", 0);

	if (x1 - x0 == 0)
		throw MFTError("SampleVector::samples_on_rectangle", "Width must be positive", 0);

	if (y1 - y0 == 0)
		throw MFTError("SampleVector::samples_on_rectangle", "Height must be positive", 0);

	// Create the sample set
	config(true, true, true, true, n_samples);

	// Set the coordinate system 
	coord_system = cs;
	coord_system.make_orthonormal();

	// Get the origin and first axis
	Eigen::Vector3d origin, axis_x, axis_y, axis_z, point_k, pos;
	coord_system.get_origin(origin);
	coord_system.get_axis_x(axis_x);
	coord_system.get_axis_y(axis_y);
	coord_system.get_axis_z(axis_z);

	// Steps
	double dx = (x1 - x0) / (nx - 1);
	double dy = (y1 - y0) / (ny - 1);

	// Set the projection state
	set_proj(proj);
	size_t idx = 0;
	for (size_t k = 0; k < nx; k++) 
		for (size_t l = 0; l < ny; l++ ) { 

			// Sample value
			value_array[idx] = 0;
			// Sample position
			pos << 	x0 + dx * k, y0 + dy * l, 0;
			point_k = origin + pos;
			set_coordinates(idx, point_k);
			// Direction
			if (c_xyzr == 'x')
				set_unit_vector(idx, axis_x);
			else if (c_xyzr == 'y')
				set_unit_vector(idx, axis_y);
			else if (c_xyzr == 'z')
				set_unit_vector(idx, axis_z);
			else
				throw MFTError("SampleVector::samples_on_rectangle", "Unknown xyrt specifications", 0);

			idx++;
		}
	metadata.set_modification_time();
};

// Samples on a racetrack
void SampleVector::segment_to_racetrack(SampleVector &res, double l0,  double r, MagFieldModel & amb, bool above, double eps){

	// Check the configuration
	if (!coord_enable)
		throw MFTError("SampleVector::segment_to_racetrack", "Coordinates must be enabled", 0);

	if (!vector_enable)
		throw MFTError("SampleVector::segment_to_racetrack", "Vectors must be enabled", 0);

	if (!values_enable)
		throw MFTError("SampleVector::segment_to_racetrack", "Values must be enabled", 0);

	// Check the number of points
	if (size < 2 )
		throw MFTError("SampleVector::segment_to_racetrack", "Segments must have at leat 2 points", 0);
	
	// The segment is defined by the first and last points
	Eigen::Vector3d xyz0, xyz1, dxyz, centre, centre_arc, point_k;
	get_coordinates(0, xyz0);
	get_coordinates(size - 1, xyz1);
	dxyz = (xyz1 - xyz0) / (size - 1);
	centre = (xyz0 + xyz1) / 2;
	double step = dxyz.norm();

	if (step < eps)
		throw MFTError("SampleVector::segment_to_racetrack", "Segment with null length", 0);
	// Check the first length
	double r0 = (xyz1 - centre).norm();
	if (l0 / 2 + step <= r0 )
		l0 = 2 * r0 + 2 * step;

	// Number of points to add 
	size_t n_seg = (size_t) round((r - r0) / step); // segment points, one side
	size_t n_arc = (size_t) round((pi * r / step) - 1); // arc points
	size_t n_seg0 = (size_t) round((l0 / 2 - r0) / step); // segment points (existing points to l0)
	size_t n_seg1 = (size_t) round(l0 / step); // segment points 
	
	// --- Reserve space
	res.x_coord_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.y_coord_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.z_coord_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.x_unit_vector_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.y_unit_vector_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.z_unit_vector_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);
	res.proj_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);

	if (time_enable)
		res.time_array.reserve(size + 2 * n_seg0 + 4 * n_arc + n_seg1);

	// --- Axis
	Eigen::Vector3d a_rot, z_axis, h_axis, v_axis, r_vect;
	r_vect = xyz1 - centre;

	// Rotation axis
	coord_system.get_axis_z(z_axis);
	a_rot = (z_axis.cross(r_vect)).cross(r_vect);
	double a_rot_n = a_rot.norm();
	if (a_rot_n < eps)
		a_rot = z_axis;
	else	
		a_rot /= a_rot_n;

	// Horizontal axis
	h_axis = r_vect / r_vect.norm();

	// Vertical axis
	v_axis = a_rot.cross(r_vect);
	v_axis /= v_axis.norm();

	// --- Initial points
	for (size_t k = 0; k < size; k++){
		res.x_coord_array.push_back(x_coord_array[k]);
		res.y_coord_array.push_back(y_coord_array[k]);
		res.z_coord_array.push_back(z_coord_array[k]);
		res.value_array.push_back(value_array[k]);

		res.size++;
	}

	// --- Add aligned points to reach l0 / 2
	Eigen::Vector3d dir_k;
	point_k = xyz1;

	for (size_t k = 0; k < n_seg0; k++){
		point_k =  xyz1 + (k + 1) * dxyz;
		res.x_coord_array.push_back(point_k(0));
		res.y_coord_array.push_back(point_k(1));
		res.z_coord_array.push_back(point_k(2));

		res.size++;
	}

	// --- Add points on the arc 0
	Eigen::Matrix3d m_rot;
	coord_system.get_axis_z(z_axis);

	// Angle
	double a = pi / n_arc;
	if (above) a *= -1;

	centre_arc = point_k - r * v_axis;
	r_vect = point_k - centre_arc;
	
	for (size_t k = 0; k < n_arc; k++){

		// Rotation matrix
		m_rot = Eigen::AngleAxisd((k + 1) * a, a_rot);
		
		point_k = m_rot * r_vect + centre_arc;
		res.x_coord_array.push_back(point_k(0));
		res.y_coord_array.push_back(point_k(1));
		res.z_coord_array.push_back(point_k(2));
		
		res.size++;
	}

	// --- Add aligned points on the second segment
	for (size_t k = 0; k < n_seg1; k++){
		point_k -= h_axis * step;
		res.x_coord_array.push_back(point_k(0));
		res.y_coord_array.push_back(point_k(1));
		res.z_coord_array.push_back(point_k(2));

		res.size++;
	}

	// --- Add points on the second arc
	centre_arc -= l0 * h_axis;
	r_vect = point_k - centre_arc;
	
	for (size_t k = 0; k < n_arc - 1; k++){

		// Rotation matrix
		m_rot = Eigen::AngleAxisd((k + 1) * a, a_rot);
		
		point_k = m_rot * r_vect + centre_arc;
		res.x_coord_array.push_back(point_k(0));
		res.y_coord_array.push_back(point_k(1));
		res.z_coord_array.push_back(point_k(2));

		res.size++;
	}

	// --- Add aligned points on the last segment
	for (size_t k = 0; k < n_seg0; k++){
		point_k =  xyz0 - (n_seg0 - k) * dxyz;
		res.x_coord_array.push_back(point_k(0));
		res.y_coord_array.push_back(point_k(1));
		res.z_coord_array.push_back(point_k(2));

		res.size++;
	}

	if (time_enable)
		res.time_array.resize(res.size);

	res.metadata.set_modification_time();

	if (vector_enable){
		res.x_unit_vector_array.resize(res.size);
		res.y_unit_vector_array.resize(res.size);
		res.z_unit_vector_array.resize(res.size);
		res.proj_array.resize(res.size);
	}

	if (values_enable)
		res.value_array.resize(res.size);

	res.set_proj(true);

	// --- Vector directions
	SampleVector un = res.normal_vectors();
	res.x_unit_vector_array = un.x_unit_vector_array;
	res.y_unit_vector_array = un.y_unit_vector_array;
	res.z_unit_vector_array = un.z_unit_vector_array;

	SampleVector dotp = res.dot(un);
	for (size_t k = 0; k < size; k++)
		res.value_array[k] = dotp.value_array[k];

	// --- Background field
	Sample sk;
	for (size_t k = size; k < res.size; k++){
		res.get_sample(k, sk);
		amb.field(sk);
		res.value_array[k] = sk.get_value();
	}

	// --- Div B = 0
	res.zero_integral_normal();
	
}

// Subtract a field
void SampleVector::subtract_field(MagFieldModel & fmod){

	Sample sk;
	vector<double> v(3), vm(3), vcorr(3);
	
	for (size_t k = 0; k < size; k++){
		
		// Field values
		get_vector_values(k, v);

		// Compute the field model at kth position
		get_sample(k, sk);
		fmod.field(sk); 
		get_vector_values(k, vm);

		// Corrected field
		vcorr[0] = v[0] - vm[0];
		vcorr[1] = v[1] - vm[1];
		vcorr[2] = v[2] - vm[2];
		set_vector_values_ref(k, vcorr);
	}
}

// Symmetry
void SampleVector::symmetry(Symmetry* sym, SampleVector& s_vect) {

	if (sym->is_type("symmetry plane"))
		symmetry_plane(sym, s_vect);
	else if (sym->is_type("symmetry centre"))
		symmetry_centre(sym, s_vect);
	else
		throw MFTError("SampleVector::symmetry", "Unknown symmetry specification");
}

// Symmetry plane
void SampleVector::symmetry_plane(Symmetry* sym, SampleVector& s_vect) {

	if (!sym->is_type("symmetry plane"))
		throw MFTError("SampleVector::symmetry_plane", "Not a symmetry plane");

	SymmetryPlane* sym_plane = (SymmetryPlane*)sym;

	// --- Coordinate system
	// Build a vector (vx, vy, vz) in the symmetry plane
	double nx = 0, ny = 0, nz = 0;
	double vx, vy, vz;
	sym_plane->get_normal_vector(nx, ny, nz);
	if (nx != 0) {
		vy = 0;
		vz = 1;
		vx = -nz / nx;
	}
	else if (ny != 0) {
		vx = 0;
		vz = 1;
		vy = -nz / ny;
	}
	else if (nz != 0) {
		vy = 0;
		vx = 1;
		vz = -nx / nz;
	}
	else 
		throw MFTError("SampleVector::symmetry_plane", "Null normal vector", 0);
	

	// Configure and resize
	s_vect.set_coord_enable(coord_enable);
	s_vect.set_time_enable(coord_enable);
	s_vect.set_values_enable(values_enable);
	s_vect.set_vector_enable(vector_enable);
	s_vect.set_coordinate_system(coord_system);
	s_vect.resize(size);
	s_vect.set_unit_distance(unit_distance);
	s_vect.set_unit_sample(unit_sample);
	s_vect.set_unit_time(unit_time);

	// Build a coordinate system attached to the symmetry plane
	Eigen::Vector3d v, n, c;
	n << nx, ny, nz;
	v << vx, vy, vz;
	sym->get_centre(c);
	CoordinateSystem cs(n, v, c);
	
	Eigen::Vector3d p_proj, new_coordinates, coordinates, unit_vector;
	Eigen::Vector3d new_unit_vector, axis_x, axis_y, axis_z, o;
	cs.get_axis_x(axis_x);
	cs.get_axis_y(axis_y);
	cs.get_axis_z(axis_z);
	cs.get_origin(o);

	// --- New samples
	for (size_t k = 0; k < size; k++) {
	
		// New coordinates
		if (coord_enable) {
		
			get_coordinates(k, coordinates);
			cs.proj_point(p_proj, coordinates); // Projection of the coordinates in the symmetry plane
			
			new_coordinates = o - p_proj(0) * axis_x + p_proj(1) * axis_y + p_proj(2) * axis_z;
		
			s_vect.set_coordinates(k, new_coordinates);
		}

		// New unit vector
		if (vector_enable) {
		
			get_unit_vector(k, unit_vector);
			cs.proj_vector(p_proj, unit_vector);

			if ((! sym_plane->is_zero_para())&&(! sym_plane->is_zero_perp())){
				if (sym_plane->is_antisymmetric())
					new_unit_vector = -p_proj(0) * axis_x - p_proj(1) * axis_y - p_proj(2) * axis_z;
				else
					new_unit_vector = p_proj(1) * axis_y + p_proj(2) * axis_z - p_proj(0) * axis_x;
			}
			else{
				if (sym_plane->is_zero_para())
					new_unit_vector = p_proj(0) * axis_x - p_proj(1) * axis_y - p_proj(2) * axis_z;

				else if (sym_plane->is_zero_perp())
					new_unit_vector = -p_proj(0) * axis_x + p_proj(1) * axis_y + p_proj(2) * axis_z;
			}

			s_vect.set_unit_vector(k, new_unit_vector);

			s_vect.set_proj(k, proj_array[k]);
		}

		if (values_enable)
			s_vect.set_values(k, value_array[k]);
		if (time_enable)
			s_vect.set_time(k, time_array[k]);

	}

	metadata.set_modification_time();
}

// Symmetry Centre
void SampleVector::symmetry_centre(Symmetry* sym, SampleVector& s_vect) {

	if (!sym->is_type("symmetry centre"))
		throw MFTError("SampleVector::symmetry_centre", "Not a centre of symmetry");

	SymmetryCentre* sym_centre = (SymmetryCentre*)sym;

	// Configure and resize
	s_vect.set_coord_enable(coord_enable);
	s_vect.set_time_enable(coord_enable);
	s_vect.set_values_enable(values_enable);
	s_vect.set_vector_enable(vector_enable);
	s_vect.set_coordinate_system(coord_system);
	s_vect.resize(size);
	s_vect.set_unit_distance(unit_distance);
	s_vect.set_unit_sample(unit_sample);
	s_vect.set_unit_time(unit_time);

	// Centre of symmetry
	Eigen::Vector3d centre, coordinates, new_coordinates;
	Eigen::Vector3d unit_vector, new_unit_vector;
	sym->get_centre(centre);

	// --- New samples
	for (size_t k = 0; k < size; k++) {

		// Coordinates
		if (coord_enable) {
			get_coordinates(k, coordinates);
			new_coordinates = 2 * centre - coordinates;
			s_vect.set_coordinates(k, new_coordinates);
		}

		// Vector direction
		if (vector_enable) {
			get_unit_vector(k, unit_vector);
			new_unit_vector = -unit_vector;
			s_vect.set_unit_vector(k, new_unit_vector);

			s_vect.set_proj(k, proj_array[k]);
		}

		if (values_enable)
			s_vect.set_values(k, value_array[k]);
		if (time_enable)
			s_vect.set_time(k, time_array[k]);
		
	}

	metadata.set_modification_time();
}

// Average
double SampleVector::mean_value(){

	if (!values_enable)
		throw MFTError("SampleVector::mean_value", "Values are disabled");

	double m0 = 0;
	for (size_t i = 0; i < size; i++)
		m0 += value_array[i];
	m0 /= size;
	return m0;
}

// Subtract the average
void SampleVector::subtract_mean_value(){

	if (!values_enable)
		return;

	double m0 = mean_value();
	for (size_t i = 0; i < size; i++)
		value_array[i] -= m0;
}

// Average
double SampleVector::mean_value(string s){
	if (!values_enable)
		throw MFTError("SampleVector::mean_value", "Values are disabled");

	if (!vector_enable)
		throw MFTError("SampleVector::mean_value", "Vectors are disabled");

	double m0 = 0;
	if (!s.compare("x"))
		for (size_t i = 0; i < size; i++)
			m0 += value_array[i] * x_unit_vector_array[i];
	else if (!s.compare("y"))
		for (size_t i = 0; i < size; i++)
			m0 += value_array[i] * y_unit_vector_array[i];
	else if (!s.compare("z"))
		for (size_t i = 0; i < size; i++)
			m0 += value_array[i] * z_unit_vector_array[i];
	else
		throw MFTError("SampleVector::mean_value", "Unknown component specification");
	m0 /= size;
	return m0;
}

// Subtract the average
void SampleVector::subtract_mean_vector_value(){

	if ((!values_enable) || (!vector_enable))
		return;

	// Compute the mean values
	double mx = mean_value("x");
	double my = mean_value("y");
	double mz = mean_value("z");

	// Get the vector components
	vector<double> vx(size), vy(size), vz(size);
	get_values("x", vx);
	get_values("y", vy);
	get_values("z", vz);

	// Subtract and set
	for (size_t i = 0; i < size; i++){
		vx[i] -= mx;
		vy[i] -= my;
		vz[i] -= mz;
	}
	set_vector_values_ref(vx, vy, vz);
}

// Subtract the average normal value
void SampleVector::subtract_mean_normal_value(){

	// Compute the average normal value
	SampleVector un = normal_vectors();
	SampleVector dotp = dot(un);
	double ave = dotp.mean_value();
	
	set_unit_vector("x", un.get_unit_vector("x"));
	set_unit_vector("y", un.get_unit_vector("y"));
	set_unit_vector("z", un.get_unit_vector("z"));

	vector<double> vval = dotp.get_values();
	// vector<double> vval = field_samples.get_values();
	for (size_t k = 0; k < size; k++)
		vval[k] -= ave;

	set_values(vval);
}

// Set vector values
void SampleVector::set_vector_values_ref(size_t k, vector<double>& v){

	if (k > size + 1)
		throw MFTError("SampleVector::set_vector_values_ref", "Wrong sample index");

	if (v.size() != 3)
		throw MFTError("SampleVector::set_vector_values_ref", "Vectors must have 3 components");

	double val = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]); 
	
	value_array[k] = val;
	x_unit_vector_array[k] = v[0] / val;
	y_unit_vector_array[k] = v[1] / val;
	z_unit_vector_array[k] = v[2] / val;
}

// Set vector values
void SampleVector::set_vector_values_ref(vector<double>& vx, vector<double>& vy, vector<double>& vz){

	size_t nx = vx.size();
	if ((nx == vy.size()) && (nx == vz.size())){
		vector_enable = true;
		resize(nx);
	}
	else 
		throw MFTError("SampleVector::set_vector_values_ref", "Incoherent vector sizes");

	vector<double> v3(3);
	for (size_t i = 0; i < nx; i++){
		v3[0] = vx[i];
		v3[1] = vy[i];
		v3[2] = vz[i];
		set_vector_values_ref(i, v3);
	}
}

// Mean
void SampleVector::mean(Sample& s){

	// Scalar values
	if (values_enable && !vector_enable){
		double val = 0;
		for (size_t i = 0; i < size; i++)
			val += value_array[i];
		val /= size;
		s.set_value(val);
	}
	// Vector values
	if (vector_enable){
		double mx = mean_value("x");
		double my = mean_value("y");
		double mz = mean_value("z");
		double val = sqrt(mx * mx + my * my + mz * mz);
		
		s.set_value(val);
		if (val > 0)
			s.set_unit_vector(mx, my, mz);
		else
			s.set_unit_vector(1, 0, 0);
	}
	// Coordinates
	if (coord_enable){
		double mx = 0, my = 0, mz = 0;
		for (size_t i = 0; i < size; i++){
			mx += x_coord_array[i];
			my += y_coord_array[i];
			mz += z_coord_array[i];
		}
		mx /= size;
		my /= size;
		mz /= size;
		s.set_coordinates(mx, my, mz);
	}
	// Time
	if (time_enable){
		double mt = 0;
		for (size_t i = 0; i < size; i++)
			mt += time_array[i];
		mt /= size;
		s.set_time(mt);
	}
	// Projection
	double p = false;
	for (size_t i = 0; i < size; i++)
		if (proj_array[i])
			p = true;
	s.set_proj(p);
}

// Curvilinear unit vectors
void SampleVector::frenet_basis(SampleVector& vect_t, SampleVector& vect_n, SampleVector& vect_b, double tol){

	// Check if enough samples are provided
	if (size < 3)
		throw MFTError("SampleVector::curvilinear_unit_vectors", "Needs at least 3 samples");

	vect_t.resize(size);
	vect_n.resize(size);
	vect_b.resize(size);

	vect_t.set_coordinates(x_coord_array, y_coord_array, z_coord_array);
	vect_n.set_coordinates(x_coord_array, y_coord_array, z_coord_array);
	vect_b.set_coordinates(x_coord_array, y_coord_array, z_coord_array);

	Eigen::Vector3d p0, p1, p2, u0, u1, u2, u;
	vector<bool> finite_radius(size, true);
	double n;

	// Compute the vectors
	for (size_t i = 0; i < size ; i ++){
		// First point
		if (i > 0)
			p0 << x_coord_array[i - 1], y_coord_array[i - 1], z_coord_array[i - 1]; 
		else
			p0 << x_coord_array[size - 1], y_coord_array[size - 1], z_coord_array[size - 1]; 
		// Central point
		p1 << x_coord_array[i], y_coord_array[i], z_coord_array[i]; 
		// Last point
		if (i < size - 1)
			p2 << x_coord_array[i + 1], y_coord_array[i + 1], z_coord_array[i + 1]; 
		else
			p2 << x_coord_array[0], y_coord_array[0], z_coord_array[0]; 
		// Tangent vector
		u = p2 - p0;
		n = u.norm();
		u0 = u / n;
		vect_t.set_unit_vector(i, u0);
		// Curvature
		u = (p1 - p2).cross(p1 - p0);
		n = u.norm();
		if (n > tol){
			u2 = u / n;
			vect_b.set_unit_vector(i, u2);
			// The last one...
			u1 = u2.cross(u0);
			vect_n.set_unit_vector(i, u1);
		}
		else
			finite_radius[i] = false;
	}
	// Check if the binornal vector is defined
	bool binormal_found = false;
	for (size_t i = 0; i < size; i++){
		if (finite_radius[i]){
			vect_b.get_unit_vector(i, u2);
			binormal_found = true;
			}
	}

	// Define the binormal vector if needed
	vect_t.get_unit_vector(0, u0);
	if (!binormal_found){
		u2 << 0, 0, 1;
		u1 = u2.cross(u0);
		u2 = u0.cross(u1);
		n = u2.norm();
		u2 /= n;
		if ((u0.cross(u2)).norm() < tol){
			u2 << 0, 1, 0;
			u1 = u2.cross(u0);
			u2 = u0.cross(u1);
			n = u2.norm();
			u2 /= n;
		}
	}
		
	// Set normal and binormal vectors for aligned points
	for (size_t i = 0; i < size; i++){
		if (finite_radius[i])
			vect_b.get_unit_vector(i, u2);
		else{
			vect_t.get_unit_vector(i, u0);
			u1 = u2.cross(u0);
			n = u1.norm();
			u1 /= n;
			vect_n.set_unit_vector(i, u1);
			vect_b.set_unit_vector(i, u2);
		}
	}

	// Set values to 1
	vector<double> ones(size, 1);
	vect_t.set_values(ones);
	vect_n.set_values(ones);
	vect_b.set_values(ones);
}

// Tangent vectors
SampleVector SampleVector::tangent_vectors(double tol){
	SampleVector res0, res1, res2;
	frenet_basis(res0, res1, res2, tol);
	return res0;
}

// Normal vectors
SampleVector SampleVector::normal_vectors(double tol){
	SampleVector res0, res1, res2;
	frenet_basis(res0, res1, res2, tol);
	return res1;
}

// Binormal vectors
SampleVector SampleVector::binormal_vectors(double tol){
	SampleVector res0, res1, res2;
	frenet_basis(res0, res1, res2, tol);
	return res2;
}

// Dot product
void SampleVector::dot(SampleVector& res, SampleVector& s){

	if (size != s.get_size())
		throw MFTError("SampleVector::dot", "The two vectors must have the same number of samples");

	if (!vector_enable || !s.get_vector_enable())
		throw MFTError("SampleVector::dot", "Needs vector values");

	res.config(true, false, true, false, size);
	res.set_coordinates(x_coord_array, y_coord_array, z_coord_array);

	//Eigen::Vector3d u0, u1;
	vector<double> v0(3), v1(3);
	for (size_t i = 0; i < size; i ++){
		get_vector_values(i, v0);		
		s.get_vector_values(i, v1);
		double d = v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2];
		res.set_values(i, d);
	}
}

// Dot product
SampleVector SampleVector::dot(SampleVector& s){

	SampleVector res;	
	dot(res, s);
	return res;
}

// Cross product
void SampleVector::cross(SampleVector& res, SampleVector& s){

	if (size != s.get_size())
		throw MFTError("SampleVector::cross", "The two vectors must have the same number of samples");

	if (!vector_enable || !s.get_vector_enable())
		throw MFTError("SampleVector::cross", "Needs vector values");

	res.config(true, false, true, true, size);
	res.set_coordinates(x_coord_array, y_coord_array, z_coord_array);

	Eigen::Vector3d u0, u1;
	vector<double> v0(3), v1(3), v(3);
	for (size_t i = 0; i < size; i ++){
		get_vector_values(i, v0);		
		s.get_vector_values(i, v1);
		
		v[0] = v0[1] * v1[2] - v0[2] * v1[1];
		v[1] = v0[2] * v1[0] - v0[0] * v1[2];
		v[2] = v0[0] * v1[1] - v0[1] * v1[0];
		
		res.set_vector_values_ref(i, v);
	}
}

// Cross product
SampleVector SampleVector::cross(SampleVector& s){

	SampleVector res;	
	cross(res, s);
	return res;
}

// Norm product
void SampleVector::norm(SampleVector& res){

	if (!vector_enable)
		throw MFTError("SampleVector::norm", "Needs vector values");

	res.config(true, false, true, false, size);
	res.set_coordinates(x_coord_array, y_coord_array, z_coord_array);

	vector<double> v(3);
	double n;
	for (size_t i = 0; i < size; i ++){
		get_vector_values(i, v);	
		n = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
		res.set_values(i, n);
	}
}

// Norm
SampleVector SampleVector::norm(){

	SampleVector res;	
	norm(res);
	return res;
}

// Normal component integral
void SampleVector::integral_normal(SampleVector& s){

	// Normal vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Field integral
	s = dot(un);
	s.set_values(0, 0);
	Eigen::Vector3d xyz0, xyz1;
	for (size_t k = 1; k < size; k++){
		get_coordinates(k - 1, xyz0);
		get_coordinates(k, xyz1);
		s.set_values(k, s.get_values(k - 1) + s.get_values(k) * (xyz1 - xyz0).norm());
	}
}

//Normal component integral
SampleVector SampleVector::integral_normal(){
	SampleVector s;
	integral_normal(s);
	return s;
}

// Tangent component integral
void SampleVector::integral_tangent(SampleVector& s){

	// Tangent vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Field integral
	s = dot(ut);
	s.set_values(0, 0);
	Eigen::Vector3d xyz0, xyz1;
	for (size_t k = 1; k < size; k++){
		get_coordinates(k - 1, xyz0);
		get_coordinates(k, xyz1);
		s.set_values(k, s.get_values(k - 1) + s.get_values(k) * (xyz1 - xyz0).norm());
	}
}

// Tangent component integral
SampleVector SampleVector::integral_tangent(){
	SampleVector s;
	integral_tangent(s);
	return s;
}

// Binormal component integral
void SampleVector::integral_binormal(SampleVector& s){

	// Normal vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Field integral
	s = dot(un);
	s.set_values(0, 0);
	Eigen::Vector3d xyz0, xyz1;
	for (size_t k = 1; k < size; k++){
		get_coordinates(k - 1, xyz0);
		get_coordinates(k, xyz1);
		s.set_values(k, s.get_values(k - 1) + s.get_values(k) * (xyz1 - xyz0).norm());
	}
}

// Binormal component integral
SampleVector SampleVector::integral_binormal(){
	SampleVector s;
	integral_binormal(s);
	return s;
}

// Scalar integral
void SampleVector::integral(SampleVector& s){

	// Configuration
	s.config(true, values_enable, true, false, size);
	s.x_coord_array = x_coord_array;
	s.y_coord_array = y_coord_array;
	s.z_coord_array = z_coord_array;
	
	// Field integral
	s.set_values(0, 0);
	Eigen::Vector3d xyz0, xyz1;
	for (size_t k = 1; k < size; k++){
		get_coordinates(k - 1, xyz0);
		get_coordinates(k, xyz1);
		s.set_values(k, s.get_values(k - 1) + s.get_values(k) * (xyz1 - xyz0).norm());
	}
}

// Binormal component integral
SampleVector SampleVector::integral(){
	SampleVector s;
	integral(s);
	return s;
}

// Set normal component integral to zero
void SampleVector::zero_integral_normal(){

	// Unit vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Project this to the normal vectors
	SampleVector dotp;
	dotp = dot(un);
	set_proj(true);
	value_array = dotp.value_array;
	x_unit_vector_array = un.x_unit_vector_array;
	y_unit_vector_array = un.y_unit_vector_array;
	z_unit_vector_array = un.z_unit_vector_array;

	// Close the contour
	push_back(get_sample(0));

	// Contour integral
	SampleVector cint;
	integral_normal(cint);
	double i0 = cint.get_values(size - 1);

	// Contour length
	SampleVector clen;
	vector<double> values(size);
	for (size_t k = 0; k < size; k++){
		values[k] = value_array[k];
		value_array[k] = 1;
	}
	integral_normal(clen);	
	double l0 = clen.get_values(size - 1);

	// Average
	double v0 = i0 / l0;

	// Correction
	double step;
	for (size_t k = 0; k < size; k++){	
		if (k > 0)
			step = clen.get_values(k) - clen.get_values(k - 1);
		else
			step = clen.get_values(1);
		value_array[k] =  values[k] - v0 * step / (l0 / size); 
	}

	// Remove the added point
	erase(size - 1);
}

// Set tangent component integral to zero
void SampleVector::zero_integral_tangent(){
	// Unit vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Project this to the normal vectors
	SampleVector dotp;
	dotp = dot(ut);
	set_proj(true);
	value_array = dotp.value_array;
	x_unit_vector_array = ut.x_unit_vector_array;
	y_unit_vector_array = ut.y_unit_vector_array;
	z_unit_vector_array = ut.z_unit_vector_array;

	// Close the contour
	push_back(get_sample(0));

	// Contour integral
	SampleVector cint;
	integral_tangent(cint);
	double i0 = cint.get_values(size - 1);

	// Contour length
	SampleVector clen;
	vector<double> values(size);
	for (size_t k = 0; k < size; k++){
		values[k] = value_array[k];
		value_array[k] = 1;
	}
	integral_tangent(clen);	
	double l0 = clen.get_values(size - 1);

	// Average
	double v0 = i0 / l0;

	// Correction
	double step;
	for (size_t k = 0; k < size; k++){	
		if (k > 0)
			step = clen.get_values(k) - clen.get_values(k - 1);
		else
			step = clen.get_values(1);
		value_array[k] =  values[k] - v0 * step / (l0 / size); 
	}

	// Remove the added point
	erase(size - 1);
}

// Set binormal component integral to zero
void SampleVector::zero_integral_binormal(){
	// Unit vectors
	SampleVector ut, un, ub;
	frenet_basis(ut, un, ub);

	// Project this to the normal vectors
	SampleVector dotp;
	dotp = dot(ub);
	set_proj(true);
	value_array = dotp.value_array;
	x_unit_vector_array = ub.x_unit_vector_array;
	y_unit_vector_array = ub.y_unit_vector_array;
	z_unit_vector_array = ub.z_unit_vector_array;

	// Close the contour
	push_back(get_sample(0));

	// Contour integral
	SampleVector cint;
	integral_binormal(cint);
	double i0 = cint.get_values(size - 1);

	// Contour length
	SampleVector clen;
	vector<double> values(size);
	for (size_t k = 0; k < size; k++){
		values[k] = value_array[k];
		value_array[k] = 1;
	}
	integral_binormal(clen);	
	double l0 = clen.get_values(size - 1);

	// Average
	double v0 = i0 / l0;

	// Correction
	double step;
	for (size_t k = 0; k < size; k++){	
		if (k > 0)
			step = clen.get_values(k) - clen.get_values(k - 1);
		else
			step = clen.get_values(1);
		value_array[k] =  values[k] - v0 * step / (l0 / size); 
	}

	// Remove the added point
	erase(size - 1);
}

// Product by a scalar
void SampleVector::times(double d){

	if (values_enable){
		for (size_t k = 0; k < size; k++)
			value_array[k] *= d;
	}
}