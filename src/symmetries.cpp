/*!
   \file		symmetries.cpp
   \brief		Symmetry classes implementation
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#include "symmetries.h"

// ----------------------------------------
// Symmetries
// ----------------------------------------
// Set symmetry centre point
void Symmetry::set_centre(const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("Symmetry::set_centre", "Centre must have 3 components");

	for (int k = 0; k < 3; k++)
		centre(k) = v[k];
}

// Get symmetry centre point
vector<double> Symmetry::get_centre() {
	
	vector<double> v(3);
	for (int k = 0; k < 3; k++)
		v[k] = centre(k);

	return v;
}

// ----------------------------------------
// Symmetry planes
// ----------------------------------------
// Constructor 
SymmetryPlane::SymmetryPlane(const vector<double>& n){ 
	if (n.size() != 3)
		throw MFTError("SymmetryPlane::SymmetryPlane", "Normal vector must have 3 components");

	for (int k = 0; k < 3; k++) 
		normal_vector(k) = n[k];

	centre << 0, 0, 0;
	antisymmetry = false; 
	sym_type = "symmetry plane"; 

};

// Set symmetry normal vector
void SymmetryPlane::set_normal_vector(const vector<double>& v) {
	if (v.size() != 3)
		throw MFTError("SymmetryPlane::set_normal_axis", "Normal vector must have 3 components");

	for (int k = 0; k < 3; k++)
		normal_vector(k) = v[k];
}

// ----------------------------------------
// Symmetry centre
// ----------------------------------------
// Constructor 
SymmetryCentre::SymmetryCentre(const vector<double>& c) {
	if (c.size() != 3)
		throw MFTError("SymmetryPlane::SymmetryPlane", "Centre must have 3 components");

	for (int k = 0; k < 3; k++)
		centre(k) = c[k];

	sym_type = "symmetry centre";

};