/*!
   \file		space_transform.h
   \brief		Space transform classes interface
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <Eigen/Dense>
#include <string>
#include <vector>
#include "mft_errors.h"
#include <iostream> // Debug

// using namespace Eigen; // This namespace cannot be used du to conflict between Eigen::Translation and SpaceTransform::Translation
using namespace std;

// -------------------------------------------
// Space transforms
// -------------------------------------------
//! Space transform class.
/*! 
Class for rotations and translations. A space transform contains:
- an axis (Eigen::Vector3d )
- a type (string)
*/
class SpaceTransform {

public:

	void get_axis(Eigen::Vector3d& v) { v = axis;}; //!< Get transform axis
	vector<double> get_axis(); //! Get transform axis
 	
	void set_axis(const Eigen::Vector3d& v) { axis = v; }; //!< Set transform axis
	void set_axis(const vector<double>& v) { axis << v[0], v[1], v[2]; }; //!< Set transform axis

	void get_type(string& s) { s = transform_type; }; //!< Get the transform type
	string get_type() { return transform_type; } //!< Get the transform type

	void get_centre(Eigen::Vector3d& v) { v = centre;}; //!< Get transform centre
	vector<double> get_centre(); //! Get transform centre
 	
	void set_centre(const Eigen::Vector3d& v) { centre = v; }; //!< Set transform centre
	void set_centre(const vector<double>& v) { centre << v[0], v[1], v[2]; }; //!< Set transform centre

	void set_angle(double a) { angle = a; }; //!< Set the transform angle
	double get_angle() { return angle; }; //!< Get the transform angle

	//! Test the transform type
	/*! 
	@param s string 
	@return true if the type is equal to s, else false 
	*/
	bool is_type(const string c) {	if (!transform_type.compare(c)) return true; return false; }; 

	virtual void transform_coordinates(Eigen::Vector3d& v) {}; //!< Apply to coordinates

	virtual void inverse(){}; //!< Inverse the transform

protected:
	Eigen::Vector3d axis = Eigen::Vector3d::Zero();
	Eigen::Vector3d centre = Eigen::Vector3d::Zero();
	double angle = 0;
	string transform_type = "undefined";
};

//! Rotation class.
/*! 
A rotation contains
- a centre
- an angle
- an axis (inherited from SpaceTransform)
- a type (inherited from SpaceTransform) 
*/
class Rotation : public SpaceTransform {
public:
	
	Rotation() { transform_type = "rotation"; axis << 0, 0, 1; centre << 0, 0, 0; }
	//Rotation(Rotation&) = default;
	//Rotation(Rotation&&) = default;
	//~Rotation() = default;
	
	//! Constructor assuming the centre at (0, 0, 0)
	/*! 
	@param a angle (rad)
	@param v axis 
	*/
	Rotation(double a, const Eigen::Vector3d& v) { axis = v; transform_type = "rotation"; centre << 0, 0, 0; angle = a; };

	//! Constructor assuming the centre at (0, 0, 0)
	/*! 
	@param a angle (rad)
	@param v axis 
	*/
	Rotation(double a, const vector<double>& v);

	//! Constructor 
	/*! 
	@param a angle (rad)
	@param v axis
	@param c centre
	*/
	Rotation(double a, const Eigen::Vector3d& v, const Eigen::Vector3d& o) { axis = v; transform_type = "rotation"; centre = o; angle = a; };

	//! Constructor 
	/*! 
	@param a angle (rad)
	@param v axis
	@param c centre 
	*/
	Rotation(double a, const vector<double>& v, const vector<double>& o);

	//! Constructor 
	/*! 
	@param tr space transform
	*/
	Rotation(SpaceTransform & tr) { tr.get_axis(axis); transform_type = "rotation"; tr.get_centre(centre); angle = tr.get_angle(); };

	void get_centre(Eigen::Vector3d& c) { c = centre; }; //!< Get the centre
	vector<double> get_centre(); //!< Get the centre
	void set_centre(const Eigen::Vector3d& c) { centre = c; }; //!< Set the centre
	void set_centre(const vector<double>& c) { centre << c[0], c[1], c[2]; }; //!< Set the centre

	double get_angle() { return angle; }; //!< Get the rotation angle
	void set_angle(double a) { angle = a; }; //!< Set the rotation angle

	void matrix(Eigen::Matrix3d& m, bool covariant); //!< Compute the rotation matrix

	void transform_coordinates(Eigen::Vector3d& v); //!< Apply to a Eigen::Vector3d of coordinates
	void transform_coordinates(vector<double> & v); //!< Apply to a vector of coordinates

	void transform_vector_cov(Eigen::Vector3d& v); //!< Apply to a covariant vector
	void transform_vector_cov(vector<double>& v); //!< Apply to a covariant vector

	void transform_vector_contrav(Eigen::Vector3d& v); 	//!< Apply to a contravariant vector
	void transform_vector_contrav(vector<double>& v); // !< Apply to a contravariant vector	

	//! Inverse transform
	void inverse() { angle *= -1; };
};

//! Translation class
class Translation : public SpaceTransform {
public:
	Translation() { axis << 0, 0, 0; transform_type = "translation"; };	
	//~Translation() = default;
	//Translation(Translation& ) = default;
	//Translation(Translation&&) = default;

	//! Constructor
	/*! 
	@param v Translation vector 
	*/
	Translation(const Eigen::Vector3d& v) { axis = v; transform_type = "translation"; };

	//! Constructor
	/*! 
	@param v Translation vector 
	*/
	Translation(const vector<double>& v) { axis << v[0], v[1], v[2]; transform_type = "translation"; };

	//! Constructor
	/*!
	@param tr space transform
	*/
	Translation(SpaceTransform & tr) { tr.get_axis(axis); transform_type = "translation"; };

	void transform_coordinates(Eigen::Vector3d& v); //!< Apply to a vector of coordinates
	void transform_coordinates(vector<double>& v); //!< Apply to a vector of coordinates

	//! Inverse the translation
	void inverse() { axis *= -1; };
	
};

