/*!
   \file		mag_field_model.h
   \brief		Magnetic field model class
   \author		Gael Le Bec
   \version		1
   \date		2020
   \copyright	GNU Public License.
 */

#pragma once
#include <string>
#include <vector>
#include <complex>
#include "coord_system.h"
#include "sample_vector.h"

using namespace std;

// -------------------------------------------
// Magnetic Field Model
// -------------------------------------------
//! Magnetic field model class
/*! 
This class is a generic class for magnetic field models. It has the following attributes:
- a coordinate system
- a bool indicating if the model is 2D or 3D
- a type (string) 
*/
class MagFieldModel
{
public:
	virtual void set_coordinate_system(CoordinateSystem & c); //!< Set the coordinate system
	void get_coordinate_system(CoordinateSystem & c); //!< Get the coordinate system
	CoordinateSystem get_coordinate_system() { return coord_system; }; //!< Get the coordinate system
	void set_2D(); //!< Set is_2D to true
	void set_3D(); // !< Set is_2D to false
	bool field_model_is_2D(); //!< Get is_2D
	void set_model_type(const string t); //!< Set the model_type string
	void get_model_type(string & t); //!< get the model type string
	string get_model_type() { return model_type; }; //!< get the model type string
	virtual void init_from_set(SampleVector& s_set) = 0; //!< Initialize with a SampleVector
	virtual void field(Sample& s) = 0; //!< Compute the field at sample position
	virtual void field(Sample& s, CoordinateSystem& cs) = 0; //!< Compute the field at sample position, using a different coordinate system	
	virtual void scalar_potential(Sample& s) = 0; //!< Compute the scalar potential at sample position
	virtual void scalar_potential(Sample& s, CoordinateSystem& cs) = 0; //!< Compute the scalar potential at sample position, using a different coordinate system
	virtual void set_unit_field(Unit& u) = 0; //!< Set the field unit
	virtual void set_unit_distance(Unit& u) = 0; //!< Set the distance unit
	virtual void init_reference_set(SampleVector& s_set) = 0; //!< Initialize a sample set on reference boundary
	SampleVector init_reference_set(); //!< Initialize and return a sample set on reference boundary
	
	//! Transform the coordinate system and compute new multipole coefficients
	/*! 
	@param tr Space transform
	*/
	void transform_coordinate_system(SpaceTransform* tr);

	//! Apply a vector of space transformations (tr0, tr1, etc) to the coordinate system and compute the coefficients in the new system
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void transform_coordinate_system(vector<SpaceTransform*> v_tr); 

	//! Move the multipole
	/*! 
	@param tr Space transform 
	*/
	void move(SpaceTransform* tr);

	//! Apply a vector of space transformations (tr0, tr1, etc) to the multipole
	/*! 
	@param v_tr Vector of space transforms 
	*/
	void move(vector<SpaceTransform*> v_tr);

	Unit get_unit_field() { return unit_field; }; //!< Return the field unit
	Unit get_unit_distance() { return unit_distance; }; //!< Return the distance unit
	void set_metadata(const Metadata& m) { metadata = m; }; //!< Set the metadata
	void get_metadata(Metadata& m) { m = metadata; } //!< Get the metadata
	Metadata get_metadata() { return metadata; } //!< Return the metadata
	void init_units(); /*!< Initialize units*/
	
	/*! Initialize units*/
	/*! 
	@param s Array of strings for [sample, distance ] (e.g. {"T", "m"} etc.)
	*/
	void init_units(string s[2]);

protected:
	void _field(SampleVector& s_set); //!< Compute the field at sample location and overwrite sample's value
	void _scalar_potential(SampleVector& s_set); 	//!< Compute the scalar potential at sample location and overwrite sample's value
	
	Unit unit_field; // field unit
	Unit unit_distance; // Distance unit

	CoordinateSystem coord_system = CoordinateSystem();	// Coordinate system
	bool is_2D = false;									// 2D model if True | 3D model if False
	string model_type = "undefined";					// Multipole type

	Metadata metadata; // Model medatada
};
