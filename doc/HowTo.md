﻿MagFieldTools How To
====================

Gaël Le Bec, ESRF, 2022

## Contents

- [Getting started](#getting-started)
  - [Introduction](#introduction)
  - [Installation as a Python module](#installation-as-a-python-module)
  - [Hello world](#hello-world)
- [Samples and SampleVectors](#samples-and-samplevectors)
  - [Samples](#samples)
  - [SampleVectors](#samplevectors)
- [Magnetic field models](#magnetic-field-models)
  - [Multipole models](#multipole-models)
  - [Boundary element models](#boundary-element-models)
  - [Magnetic signatures](#magnetic-signatures)
- [Coordinate systems](#coordinate-systems)
- [Units](#units)
- [References](#references)

## Getting started

### Introduction
The Magnetic Field Tool (*MagFieldTools*) library was developed for processing magnetostatic field data from accelerator magnet measurements and simulations, NMR/MRI magnet sources and others. Its purpose was to homogenized methods for data acquisition and simulation, and to provide extendable field models.

Magnetic field data are represented by **Samples**. A sample may include a value, a position, a unit vector, etc. Samples can be grouped in **SampleVectors**. These **SampleVectors** can be used to build **magnetic field models** of different types, such as field multipoles. In return, field models can compute field (or potential) values and return it in SampleVectors.

*MagFieldTools* is a C++ library which can be embedded in other programs. All the C++ objects and methods are wrapped to Python with SWIG. This documentation presents Python examples. In most cases, the syntax is the same in C++. All the examples documented here are written in "Python style" and several functions return values. For C++ developments, we encourage to get results as output parameters to avoid unecessary copies of the data. This syntax can also be used with the Python interface.

### Installation as a Python module
Clone or download the [project repository](https://gitlab.esrf.fr/IDM/magfieldtools) from the ESRF GitLab server and follow the instructions given in the *ReadMe*. Building this module requires the *Eigen* matrix library (see the *ReadMe*).

The easiest way is to use *make*. If not available (*e.g.* for Windows), type the following lines in a shell:

```shell
# Set Conda environment
conda activate myenv

# Call the wrapper
# this line can be skipped if the source files were not changed
swig -c++ -python -doxygen -outdir ./ ./src/mag_field_tools.i

# Build and install
python setup.py build_ext install
```

### Hello world
Lets create a set of samples and use it for initializing a 2D multipole model.

```python
# Imports 
import mag_field_tools as mft 
from math import pi, sin, cos 
from matplotlib.pyplot import quiver, show 
import numpy as np

# Create a SampleVector 
s0 = mft.SampleVector() 
s0.samples_on_circle(10, 32, 'r') # Generate 32 points on a 10 mm radius circle, with unit vectors pointing radially 
s0.set_values([sin(2 * k * 2 * pi / 32) for k in range(32)]) # Set the radial field for a pure quadrupole

# Initialize a 2D Circular multipole model 
c = mft.Circular2D(10, 8, s0) # 10 mm reference radius and 8 coefficients

# Compute field samples 
s1 = mft.SampleVector() 
s1.samples_on_disk(10, 31, False) # Generate points on a 10 mm radius disk, with 31 points on the horizontal axis, results not projected
c.field(s1)

# Plot 
x, y = s1.get_coordinates('x'), s1.get_coordinates('y') 
ux, uy = s1.get_values('x'), s1.get_values('y') 
quiver(x, y, ux, uy, pivot='middle') 
show()
```
This example use [SampleVectors](#samplevectors) and [Circular2D](#2d-circular-multipoles) multipole models. These objects are presented in more details in the documentation.

### Doc strings

All objects and methods have doc strings. 

```python
# Python doc strings
help(mft)

# ipython style
?mft 
```

## Samples and SampleVectors

[Samples](#samples) and [SampleVectors](#samplevectors) are important building blocks of *MagFieldTools*. The library is implemented in such a way that *Samples* are by-products of *SampleVectors*. However, *Samples* will be presented first for clarity.

### Samples

#### Attributes

In the *MagFieldTool* library, magnetic data are represented by Samples. In most cases, a *Sample* is composed of:

- Three space coordinates
- A unit vector
- A value
- A projection state (see below)
- A time stamp.

Samples do not have necessarily all these attributes. The configuration of the Sample attributes are done at the [SampleVector](#samplevectors) level.

The code below instantiate a *Sample*:

```python
import mag_field_tools as mft

# This is a first sample
s0 = mft.Sample()

# This is another one (all arguments are optional)
val = 0 # Value
coord = [0, 0, 0] # Coordinates
unit_vect = [1, 0, 0] # Unit vector
proj = True # Projection state
t0 = 0.1 # Time stamp
s1 = mft.Sample(val, coord, unit_vect, proj, t0)
```

#### Scalar samples

Scalar samples have no vector direction. These samples can be used for representing scalar fields like scalar potential. They have only a scalar value that can be accessed as follows:

```python
 # Create a sample
s = mft.Sample()

# Set and get its value
s.set_value(1) 
val = s.get_value()
```

The sample coordinates can be modified easily:

```python
# Set the coordinates
s.set_coordinates(1, 1, 1)

# Set the same coordinates with an array
s.set_coordinates([1, 1, 1])

# Get the coordinates
coord = s.get_coordinates()
```
#### Samples with vector values

Samples can have vector values: this is the case of 2D and 3D magnetic fields. In *MagFieldTools*, vectors are described as unit vectors multiplied by a scalar value:
$$
\mathbf{v}=k\mathbf{u}
$$
where $\mathbf{v}$ is the vector value, $k$ is the amplitude and $\mathbf{u}$ is a unit vector. 

The vector values can be read and write as follows:

```python
# Set and get vector values
s.set_vector_values(2, 0, 0)
v = s.get_vector_values()
```

It is also possible to set independently the amplitude and the unit vector. The two following samples describe the same vector.

```python
# A vector
s0 = mft.Sample()
s0.set_unit_vector(1, 0, 0)
s0.set_value(2)

# Another representation of the same vector
s1 = mft.Sample()
s1.set_unit_vector(-1, 0, 0)
s1.set_value(-2)
```
One may ask why *MagfieldTools* offers the possibility to represent the same vector element in different manners:  three components are sufficient for describing a vector. This additional freedom, however, allows to easily define unit vectors perpendicular to a given boundary or to a sensor motion, etc.

#### Projection state

Vector samples can be of two type:

- **Projected samples**, if they give the projection of a vector on the unit vector
- **Not projected samples**, if they give the 3 components of a vector

The type of a given sample is determined by the **proj** attribute. The following code defines the projection of a field at the origin, along the three axes:

```python
# Vector projection on X
s0 = mft.Sample(1, [0, 0, 0], [1, 0, 0], True)

# Vector projection on Y
s1 = mft.Sample(1, [0, 0, 0], [0, 1, 0], True)

# Vector projection on Z
s2 = mft.Sample(1, [0, 0, 0], [0, 0, 1], True)
```

These projections can be described by a single, non-projected sample:

```python
s3 = mft.Sample(3 ** 0.5, [0, 0, 0], [1, 1, 1], False)
```

Projected samples are very useful in the case the field is partially known. This is the case in numerous accelerator magnet measurements, when only one component of the field is known and the other ones are free.

**By default, the samples have projected values.**


### SampleVectors

*SampleVectors* are sets of *Samples*. The samples of a *SampleVector* must be similar: 

- They must represent the same physical quantity
- They must have the same configuration (scalar/vector, time stamps or not)
- They are expressed in the same coordinate system
- Projected and non-projected samples can be combined

#### Attributes

*SampleVectors* are composed of:

- Arrays of coordinates, values, unit vectors, projection state and time stamps
- Units
- A coordinate system
- Metadata

#### A simple SampleVector

The example code below shows how to access the attributes of a *SampleVector*.

```python
# initialise a Sample vector and set coordinates along a line
s = mft.SampleVector()
s.set_coordinates('y', [k for k in range(10)])
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'), '\n')

# Values
s.set_values([0.1 for k in range(10)])
print('New values:', s.get_values(), '\n')

# Unit vector
vx = [1 for k in range(10)]
vy = [1 for k in range(10)]
vz = [0 for k in range(10)]
s.set_unit_vector(vx, vy, vz)
print('Unit vector for the first sample:', s.get_unit_vector(0))
```
#### Copy

The simpliest way to copy a *SampleVector* is to use the copy constructor:

```python
s1 = mft.SampleVector(s)
```

#### Advanced initialization

##### Predifined shapes

It is often useful to initialize samples with values on specific geometrical shapes. A few methods are provided.

```python
# Initialise an empty set
# Projected values by default
s = mft.SampleVector() 

# 32 Samples with null vector values on a circle of radius 10 mm
s.samples_on_circle(10, 32, 'r') # Radial unit vectors
s.samples_on_circle(10, 32, 't') # Tangential unit vectors
s.samples_on_circle(10, 32, 'x') # Horizontal unit vectors, try also 'y' and 'z'

# 32 samples with null samples on a disk of 10 mm, with 31 points on the diameter
s.samples_on_disk(10, 31, False) # Not projected samples

# 32 samples with null 32 vector values on an ellipse of 10 mm axis and 5 mm axis
s.samples_on_ellipse(10, 5, 32, 'r') # Radial unit vectors, try 't', 'x', ...

# 64 samples on a sphere of 10 mm radius
s.samples_on_sphere(10, 8, 8)

# 121 samples on a rectange with corners (-10, -5) and (10, 5)
s.samples_on_rectangle(-10, -5, 10, 5, 11, 11)

# Initialization of samples on boxes to be added
```
This list of initialization methods will be extended depending on the needs.

The values can be set:

```python
# Set 32 sinusoidal values 
s.set_values([sin(4 * pi / 32) for k in range(32)])
```
The samples above correspond to a quadrupole if the unit vectors are radial. 

More complex initialization examples are given in the *example/sample_set.py* file.

One can also set vector values:

```python
# A quadrupole
x = [sin(2 * pi / 32) for k in range(32)]
y = [cos(2 * pi / 32) for k in range(32)]
z = [0 for k in range(32)]
s.set_vector_values(x, y, z)
```

#### Size

The size of a *SampleVector* can be modified as follows:

```python
s.resize(10) # Resize to 10 samples
```

The *get_size()* method returns the size:

```python
size = s.get_size()
```

#### Configuration

The arrays of a *SampleVector* are configurable: they can be used or not. This configuration is set by the following boolean attributes:

- *values_enable*: an array of values is built if *True*
- *time_enable*: an array of time stamps is built if *True*
- *coord_enable*: an array of coordinates is built if *True*
- *vector_enable*: an array of unit vectors is built if *True*

All these attributes are *True* by default. The configuration can be modified by the *config()* method:
```python
# C++ Prototype
# config(bool values_on, bool time_on, bool coord_on, bool vector_on, size_t n)

# Configure a set of 10 scalar samples without time stamp
s.config(True, False, True, False, 10)

# Configure Trajectories with 10 points
s.config(False, False, True, False, 10) #3D
s.config(False, True, True, False, 10) # 4D

# A set of unit vectors defined at some positions, e.g. a curve
s.config(False, False, True, True, 10)
```

#### Adding or removing samples

A few methods are available for adding samples to a set.

```python
sv = mft.SampleVector()

# Add a sample at the end 
s = mft.Sample()
sv.push_back(s)

# Add another SampleVector at the end
sv1 = mft.SampleVector()
sv.add(sv1)
```

Let us now remove samples from a *SampleVector*:

```python
# Remove the last element
sv.pop_back()

# Remove the kth element
sv.erase(k)

# Remove all elements
sv.clear()
```

#### Moving Samples and SampleVectors

The samples of a *SampleVectors* can be moved as follows.

```python
# Define a translation and move the sample s
tr = mft.Translation([1, 0, 0])
s.move(tr)

# Combination of motions
rz90 = mft.Rotation(pi/2, [0, 0, 1]) # 90 deg rotation arround z
s.move([tr, rz90])
```

Similar methods are available for individual samples:

```python 
s = mft.Sample()
s.move(tr)
```

#### Transforming the coordinate system

The samples' coordinates and values are expressed in a given [coordinate system](#coordinate-systems). This system can be transformed and the coordinate and vector modified accordingly:

```python
# Rotation matrix to be applied 
rz90 = mft.Rotation(pi/2, [0, 0, 1])

# Apply to the SampleVector s
s.transform_coordinate_system(rz90)
```

#### Play with the units

A *SampleVector* include [units](#units) of

- Sample (*e.g.* Tesla, Volt, etc. )
- Distance
- Time

It can be accessed as follows:

```python
# Set the unit of samples
u_sample = mft.Unit('mT') # millitesla, not meter x Tesla!
s.set_unit_sample(u_sample)

# Set the unit of coordinates
u_coord = mft.Unit('mm')
s.set_unit_distance(u_coord)

# Get the time unit
u_time = s.get_unit_time()
```

By default, the *set_unit_xxx()* methods check for that the new unit is coherent with the previous one and change the values accordingly: changing unit from Tesla to millitesla will multiply the values by a factor of 1000.

One can use the following syntax to avoid this behavior:

```python
# Check the unit and change the values, assuming previous units are for field
u_sample = mft.Unit('mT') 
s.set_unit_sample(u_sample)

# Force to another unit without changing the values
u_sample = mft.Unit('V')
s.set_unit_sample(u_sample, True)
```

Please refer to the [Units](#units) section for more details on units and associated methods.

#### Symmetries

Symmetries can be applied to a *SampleVector*. Applying a symmetry will generate new samples. The following symmetries are available:

- Symmetry plane
- Anti-symmetry plane
- Central symmetry
- Magnetic symmetry planes with zero parallel or perpendicular field

 Lets build and apply a symmetry plane.

```python
# Define the plane with [1, 0, 0] normal vector and passing by point [0.5, 0, 0]
sym = mft.SymmetryPlane([1, 0, 0], [0.5, 0, 0])

# Build the symmetric to s with respect to sym
s_sym = s.symmetry(sym)

# Add this SampleVector to s
s.add(s_sym)
```

The same method can build an anti-symmetry plane:

```python
sym = mft.SymmetryPlane([1, 0, 0], [0.5, 0, 0]) # Symmetry
sym = mft.SymmetryPlane([1, 0, 0], [0.5, 0, 0], True) # Anti-symmetry
```

Central symmetries can also be applied:

```python
# Central symmetry with centre at the origin
csym = mft.SymmetryCentre([0, 0, 0])

# Apply
s_csym = s.symmetry(csym) 
```

Now let us build a plane where the vectors have a null parallel component. This corresponds to a material with an infinite permeability, sometimes called *perfect conductor*.

```python
# Zero parallel field (perfect conductor)
sym = mft.SymmetryPlaneZeroPara([1, 0, 0], [0.5, 0, 0])
```

Finally, it is also possible to build a plane with zero perpendicular vector components, corresponding to a perfect insulator.

```python
# Magnetic symmetry: zero perpendicular field (perfect insulator)
sym = mft.SymmetryPlaneZeroPerp([1, 0, 0], [0.5, 0, 0])
```


#### Processing samples

TO DO: Drift correction, interpolation, etc.

##### Mean

```python
# Mean value
val = sv.mean_value()

# Mean x vector value
val_x = sv.mean_value('x')

# Compute and subtract the mean 
sv.subtract_mean_value() # Value
sv.subtract_mean_vector_values() # Vector values
```

##### Multiply by a scalar
```python
# Multiply the values of sv by 2
sv.times(2)
```

##### Frenet basis

In the case the coordinates represent a 3D curve, *MagFieldTools* can build a Frenet basis. It builds three sets of unit vectors such that: 

- The vectors $\mathbf{t}$  of the first set are tangent to the curve
- The vectors $\mathbf{n}$ of the second set are normal to the curve and point towards the centre
- The vectors $\mathbf{b}$ of the last set are normal to $\mathbf{t}$ and $\mathbf{n}$

```python
# Create a simple curve (a circle)
s = mft.SampleVector()
s.samples_on_circle(10, 32, 'r')

# Build the Frenet basis
ut, un, ub = mft.SampleVector(), mft.SampleVector(), mft.SampleVector()
s.frenet_basis(ut, un, ub)

# Plot the normal vectors
ux, uy = un.get_values('x'), un.get_values('y')
x, y = un.get_coordinates('x'), un.get_coordinates('y')
quiver(x, y, ux, uy, pivot='middle')
show()
```

In many cases, it is convenient to get only one of these sets of vectors. They can be obtained as follows:

```python
ut = s.tangent_vectors()
un = s.normal_vectors()
ub = s.binormal_vectors()
```

##### Dot and cross product

Dot and cross products of vectors from two *SampleVectors* can be done:

```python
s_res = s1.dot(s2)
s_res = s1.cross(s2)
```

 These methods checks that the two *SampleVectors* have the same number of elements. However, they does not check that the coordinates of the two sets are coherent.

##### Norm

The norm of the vectors of a *SampleVector* can be computed:

```python
s_norm = s.norm()
```

##### Integration

Various integrals can be computed on sample vectors. Use the following to compute the integral of scalar values:

```python
i = s.integral()
```

Now, let us compute the contour integrals defined as
$$
I_t = \oint \mathbf{v} \cdot \mathbf{t} dl \\
I_n = \oint \mathbf{v} \cdot \mathbf{n} dl \\
I_b = \oint \mathbf{v} \cdot \mathbf{b} dl \\
$$
It writes

```python
i_t = s.integral_tangent().
i_n = s.integral_normal().
i_b = s.integral_binormal().
```
##### Enforce zero contour integral

Fields with zero contour integral have a particular interest in magnetostatics. Let us consider a 2D field. In this case, the Gauss's law writes $\nabla \cdot \mathbf{B} = 0$. The following command will enforce a null divergence for a 2D field, using Ostrogradsky's theorem:

```python
s.zero_integral_normal()
```

Similar commands are available for the other contour integrals:

```python
s.zero_integral_tangent()
s.zero_integral_binormal()
```

##### Line to racetrack conversion

The following example shows how to convert field integral measurements on a straight line to samples on a racetrack contour which can be used to initialize a [2D boundary element model](#2D-boundary-element-models).

```python
# The initial samples are in the SampleVector s

# Assumes an ambient field with dipole and quads
r0 = 100
amb = mft.Circular2D(r0, 2, s)

# Build a racetrack
l = 100 # length of the racetrack
r = 25 # radius of the racetrack
s1 = s.segment_to_racetrack(l, r, amb)

# Build a 2D model
bem = mft.BEM2D(s1)
```


#### Metadata

*SampleVectors* include metadata. 

```python
mdata = mft.Metadata()
s.set_metadata(mdata)
mdata1 = s.get_metadata()
```

## Magnetic field models

Computing fields from magnetic models is one of the main purpose of *MagFieldTools*. Different types of field models can be built: 2D and 3D multipoles with different geometries, boundary element models, and other types will be developped. All of these models inherit from the *MagFieldModel* class, as shown in the UML diagram below. 
```mermaid
classDiagram
      MagFieldModel <|-- Multipole2D
      MagFieldModel <|-- Multipole3D
      MagFieldModel <|-- BEM2D
      MagFieldModel : -coord_system
      MagFieldModel : -unit_field
      MagFieldModel : -unit_distance
      MagFieldModel : -metadata
      MagFieldModel : +field()
      MagFieldModel : +scalar_potential()
      MagFieldModel : +move() 
      MagFieldModel : +init_reference_set()
      Multipole2D <|-- Circular2D
      Multipole2D <|-- Elliptic2D
      Multipole3D <|-- Spherical3D
      class Multipole2D{
          -degree
          -coef
          -coef_norm
          -ref
      }
      class Circular2D{
           +strength()
           +normalized_strength()
      }
      class Elliptic2D{
      }
      class Multipole3D{
          -degree
          -coef
          -ref
      }
      class BEM2D{
          -sources
          -vertex_x
          -vertex_y
          -n
          -svd_tol
      }
```
*Simplified UML diagram of the MagFieldModel class. (In this diagram, only the main methods are represented. The setters and getters are not shown for clarity.)* 

Please see the file *src/mag_field_model.h* for more details.

### Multipole models

#### 2D circular multipoles

##### Definition

2D multipoles are based on the complex representation of fields:
$$
B = \sum_{n=1}^{N}{\left( b_n + i a_n\right)\left(\frac{z}{r_0}\right)^{n - 1}}
$$
were $b_n$ and $a_n$ are the normal and skew multipole coefficients, $z=x+iy$ is the position in the complex plane and $r_0$ is a reference radius.

The multipole coefficients are stored in the *coef* attribute: its real part contains the normal coefficients and its imaginary part the skew coefficients. 

The reference radius is stored in the *ref* attribute:

```python
ref[0] = r_0
```

The estimation of multipoles from projected values is presented in details in [[Le Bec, 2012](#lebec12)].

##### Simple examples

Lets build a circular multipole model for a normal dipole field.

```python
# Build a model with r0 = 10
c = mft.circular(10) 

# Set the dipole coef (index 0) to 1
c.set_coef(0, 1)

# Compute the field on a disk
s = mft.SampleVector()
s.samples_on_disk(10, 31, False) 
c.field(s)

# Plot
x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
```

Circular multipole models can be initialized from sample vectors, as shown below. Note that only the radial component of the field is used. The method does not need all components to be specified, and works with projected and non-projected values.

```python
# Create a SampleVector with a quadrupolar field
s = mft.SampleVector()
s.samples_on_circle(10, 32, 'r') 
s.set_values([sin(2 * k * 2 * pi / 32) for k in range(32)]) 

# Build the model from the samples
r0 = 10 # Reference radius
n = 8 # Number of coefficients
c = mft.Circular2D(r0, n, s)
```

##### Moving a multipole

Magnetic field models can be moved the same way as *SampleVectors*.

```python
# Build a quadrupole with 10 mm reference radius
c = mft.circular(10) 
c.set_coef(1, 1)

# Move the model by 5 mm
tr = mft.Translation([5, 0, 0])
c.move(tr)

# Check the multipole coefficients 
print(c.get_coef())
```

##### Defining the orientation of a 2D magnetic field model

The orientation of a 2D magnetic field model is defined by its coordinate system:

- The third axis gives the direction of projection
- The two other axes specify the orientation in the plane

The code below shows how to modify the coordinate system of a multipole.

```python
# Instantiate a quadrupole
c = mft.Circular2D(10, 8)
c.set_coef([0, 1, 0, 0, 0, 0, 0, 0])

# Rotate its coordinate system
tr = mft.Rotation(pi/4, [0, 0, 1])
c.transform_coordinate_system(tr)
```

##### Changing the reference radius

Now, let us change the reference radius of a multipole object. The method presented here can be adapted to multipoles of any types.

```python
# Build a quadrupole with 10 mm reference radius
c = mft.circular(10) 
c.set_coef(1, 1)

# Create the target multipole with 20 mm reference radius
c1 = mft.circular(20)

# Create samples on the boundary
c1.init_reference_set(s1)

# Initialize the multipole coefficients
c1.init_from_set(s1)
```

These three steps can be done by a single function:

```python
# Build a quadrupole with 10 mm reference radius
c = mft.circular(10) 
c.set_coef(1, 1)

# Change the radius in place
c.change_ref_radius(20)
```

#### 2D elliptic multipoles

##### Definition

Elliptic coordinates are defined such that
$$
x = e \cosh \eta \cos \psi \\
y = s \sinh \eta \sin \psi
$$
were $e$ is the eccentricity and $\eta$ and $\psi$ are the elliptic coordinates. With this notation, a number $z=x+iy$ in the complex plane writes $z = e \cosh w$ and
$$
w = \mathrm{arccosh} (z/e).
$$
The elliptic multipoles are defined as [[Schnizer, 2009](#schnizer09)]
$$
B = \frac{B_0}{2} + \sum_{n=1}^{N}{E_n \frac{\cosh{n (\eta + i \psi)}}{\cosh n \eta_0}}.
$$
The elliptic parameters are stored as

```python
ref[0] = e 
ref[1] = eta_0 
```

and the coefficients $E_n$ are stored in the *coef* attribute, with complex values.

##### Example code

Lets build a 2D elliptic multipole and play with it.

```python
# Build an elliptic multipole with 16 coefficients
# 10 mm major axis and 5 mm minor axis
e = mft.Elliptic2D(10, 5, 16)

# Define a normal quadrupole
e.set_coef(1, 1)

# Define positions for computation
s.samples_on_ellipse(10, 5, 16, 'r', False)

# Compute the field
e.field(s)

# Close the loop: compute the multipoles from the field samples
e_back = mft.Elliptic2D(10, 5, 16, s)
e_back.get_coef()
```

 ##### Motions, orientation and change of reference

Please refer to the dedicated sections in the [2D circular multipoles](#2d-circular-multipoles) sections. The syntaxes are similar.

#### 3D spherical multipoles

##### Definition

The spherical coordinates are given by the radius $r$, the colatitude $\theta$ and the longitude $\varphi$ . The spherical multipole expansion is derived from the following expression of the scalar potential:
$$
V = \sum_{l=0}^N\sum_{m=-l}^{l}{S_{lm}\left(\frac{r}{r_0}\right)^l P_l^m(\theta) \cos(m \varphi)}
$$
The field expressions are derived from this potential. The reference radius is stored in the *ref* attribute:

```python
ref[0] = r_0
```

and the multipole coefficients are stored in *coef*:

```python
coef[0] = [S(0, 0)]
coef[1] = [S(0, 1), S(1, 1), S(1, -1)]
#...
```

##### Example code

```python
# Build a spherical multipole model
m = mft.Spherical3D()

# Set the dipole coef[1][0] to 5
m.set_coef(1, 0, 5) 

# Create samples on a sphere
sph = mft.SampleVector()
sph.samples_on_sphere(10, 8, 8) 
sph.set_proj(False) # Disable projections

# Compute the field
m.field(sph) 

# Compute the scalar potential 
m.scalar_potential(sph) 
```

Please refer to the file *examples/spherical_multipoles.py* for a more detailed example, including 3D plots.

#### 3D cylindrical multipoles

##### Definition

The 3D cylindrical multipoles are defined as a sum of harmonic functions in cylindrical coordinates, where the longitudinal direction is given by the beam axis and the transverse positions are given by a radius and angle. 

*Bessel* multipoles were introduces by Davies [[Davies, 1992](#davies09)]. In this paper, the bessel functions were series expanded and the "multipole"coefficients were the coefficients of the terms of the series expansions of the Bessel functions. In this library, the multipole coefficients are for the Bessel functions, i.e. the number of terms is reduced.

In 3D cylindrical coordinates, the expression of the scalar potential at longitudinal position $z$, radius $r$ and angle $\theta$ is:
$$
V_{Bes} = \sum_{m=0}^{M-1}\sum_{n=0}^{N-1} \frac{A_{mn}}{a_{mn}} I_n\left(\frac{2 \pi m r}{L_0}\right) CS\left({n \theta}\right) CS\left({\frac{2 \pi m r}{L_0}}\right)
$$
were the $I_n$ are the modified Bessel functions of the first kind, the $CS$ are for $\sin$ or $\cos$ functions depending on the coefficients (see below), $L_0$ is a reference length and $a_{mn}$ is a normalization coefficient choseen in order to normalize the field to one on a reference radius $r_0$:
$$
a_{mn} = \frac{m \pi}{L_0} \left(I_{n - 1}\left(\frac{2 \pi m r_0}{L_0}\right) + I_{n + 1}\left(\frac{2 \pi m r_0}{L_0}\right)\right).
$$
This potential does not describe correctly the longitudinally homogeneous terms ("2D" dipoles, quadrupoles, etc.) for which we add the well know 2D terms:
$$
V_{2D} = \sum_{n=0}^{N-1}\frac{C_n}{n r_0^{n - 1}} r^n CS\left({n \theta}\right).
$$
The last term to add is the longitudinal dipole:
$$
V_Z = C \frac{z}{L_0}.
$$
Using these notations, the potential is defined as 
$$
V = V_{Bes} + V_{2D} + V_Z.
$$
The coefficients are stored as follow:
| 2D                  | Normal longitudinal         | Skew longitudinal       |
|---                  |---                          | ---                     |
| Longitudinal dipole | Solenoid                    | Solenoid                |
| Normal transverse   | Norm. trans., norm. long.   | Norm trans., skew long. |  
| Skew transverse     | Skew trans., norm. long     | Skew trans., skew long. | 

The spatial dependences of these terms are
| 2D     | Normal longitudinal         | Skew longitudinal       |
|---     |---                          | ---                     |
| $z $    | $I_0\left(\frac{2 \pi m r}{L_0}\right) \sin\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M $ | $ I_0 \left(\frac{2 \pi m r}{L_0}\right) \cos\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M $ |
| $r^n \sin\left({n \theta}\right)$   | $I_n\left(\frac{2 \pi m r}{L_0}\right) \sin\left({n \theta}\right) \sin\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M \\ n = 1, \ldots, N $   | $ I_n\left(\frac{2 \pi m r}{L_0}\right) \sin\left({n \theta}\right) \cos\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M \\ n = 1, \ldots, N$ |  
| $r^n \cos\left({n \theta}\right)$     | $ I_n\left(\frac{2 \pi m r}{L_0}\right) \cos\left({n \theta}\right) \sin\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M \\ n = 1, \ldots, N$     | $ I_n\left(\frac{2 \pi m r}{L_0}\right) \cos\left({n \theta}\right) \cos\left({\frac{2 \pi m r}{L_0}}\right) \\ m = 1, \ldots M \\ n = 1, \ldots, N$ |

The number of coefficients are defined by the degree $N$ and the order $M$.

##### Example code

```python
# Build a cylindrical multipole model
m = mft.Cylindrical3D(4, 3, 10, 100) # Degree = 4, order = 3, ref. radius = 10, ref. length = 100

# Set coefficients
m.set_coef(1, 0, 1) # Set 2D normal dipole to 1
m.set_coef(1 + 4, 0, 1) # Set 2D skew dipole to 1
m.set_coef(1, 1, 1) # Set the normal longit. mode of normal dipole to 1
m.set_coef(1, 1 + 3, 1) # Set the skew longit. mode of normal dipole to 1
m.set_coef_zero() # Clear
m.set_coef(0, 0, 1) # Set the longitudinal field to 1

# Create samples on a cylinder
v = mft.SampleVector() 
v.samples_on_cylinder(10, 100, 16, 64)

b = mft.SampleVector() # Another samples, will be used for field
b.samples_on_cylinder(10, 100, 16, 64)
b.set_proj(False)

# Compute the field
m.field(b) 

# Compute the scalar potential 
m.scalar_potential(v) 
```

#### Other 3D multipoles

To be implemented. These models are based on analytical expressions of the scalar potential, which are easier to implement that field and vector potentials.

### Boundary element models

Boundary element models rely on values of field or potentials on a boundary. The building blocks of the magnetostatic code Radia [[Chubar, 1998](#chubar98)], i.e. its individual magnetic elements, belongs to this category. In another context, measuring the field on a closed contour allows the reconstruction of the field inside this contour, as shown in the next section.

#### 2D boundary element models

##### Theory

A method based on Cauchy's integral was proposed by J Chavanne in 2017 [[Chavanne, 2017](#chavanne17)]. According to Cauchy's theorem, the values of an analytic function inside a contour is given by its values on the contour:
$$
B(z)=\frac{1}{2\pi i}\oint{\frac{B(z_c)}{z_c - z}dz_c}.
$$
In practice, the contour is subdivided in small elements defined by vertex points and a current density $j_s$.This sources defines the value of the potential in the middle of the elements. In the present version of the method, the vector potential is estimated by integrating the normal component of the field.

In the *MagFieldTools* module, these models are named *BEM2D*.

##### Important note

In its present version, this method needs:

- The samples to be sorted along the contour
- The component of the field normal to the contour

These two conditions are needed to estimate the vector potential from the field values. We will try to relax these constrains in later versions of this code.

##### Example

First, let us load integrated field values measured along a racetrack contour, with a stretched wire.

```python
import csv

# Read the data in a CSV file
f = open('bem2d_test_data.dat') 
reader = csv.reader(f, delimiter='\t')
tab = [row for row in reader]	
x = [float(tab[k][0]) for k in range(1, len(tab))]	
y = [float(tab[k][1]) for k in range(1, len(tab))]
z = [float(tab[k][2]) for k in range(1, len(tab))]
field_int = [float(tab[k][3]) for k in range(1, len(tab))]

# Create a SampleVector
s = mft.SampleVector()
s.set_coordinates('x', x)
s.set_coordinates('y', y)
s.set_coordinates('z', z)
s.set_values(field_int)

# Compute and set normal vectors
un = s.normal_vectors()
ux, uy = un.get_vector_values('x'), un.get_vector_values('x')
s.set_unit_vector('x', ux)
s.set_unit_vector('y', uy)
```

Now, let us build a boundary element model:

```python
# Initialize the model
bem = mft.BEM2D(s)

# Plot the sources
plot(bem.get_sources())
```

This model can be used for computing the field, for instance on a rectangular grid:

```python
s = mft.SampleVector()

# Define a rectangle with corners (-45, -11) and (45, -100) and 21 x 21 points
s.samples_on_rectange(-45, -11, 45, -100, 21, 21)

# Non-projected values
s.set_proj(False)

# Compute the field
bem.field(s)

# Plot
xs = r.get_coordinates('x')
ys = r.get_coordinates('y')
vxs = r.get_vector_values('x')
vys = r.get_vector_values('y')
quiver(xs, ys, vxs, vys, pivot='middle')
```

##### Move BEM2D models

The *BEM2D* models are not move the same way as other models. Here, the motions are applied to the vertex points of the models. The syntax is the same as for other models:

```python
tr = mft.Rotation(pi, [0, 0, 1])
bem.move(tr)
```

### Magnetic signatures

Magnetic signatures can be used to describe a field near a refence line, for instance close to the gap of an undulator. The "fields" computed with signatures do not fulfill the Maxwel equations and they should be considered as rough approximations. They could be useful for undulator shimming, for instance.

#### 2D signatures

##### Theory

*Signature2D* objects are defined with field and field derivative values along a line. In the case this line is parallel to the x axis, the approximation is:
$$
B(x, y) = c \left(B(x, y_0) + \frac{dB}{dy}(x, y_0) (y - y0)\right),
$$
where $c$ can be used to scale the signature, $e.g.$ to change the value of a magnet angle error.  

The functions $B$ and $dB/dy$ are implemented as spline interpolators. 

The points used to initialize a *Signature2D* object must be aligned.

##### Coordinate system
The coordinate system of 2D signatures plays an important role in the field computations. When the field is computed at a given point, this point is projected to the *Signature2D* coordinate system to determine its position. As detailled below, *Signature2D* objects are initialized with a set of field values stored in a *SampleVector* and a set of field derivatives in another *SampleVector*. The coordinate system is defined from the field values set, as follows:
- The origin is the average value of the coordinates
- The first axis is parallel to the line defined by the samples' coordinates 
- The third axis is parallel to third axis of the data set's coordinate system
- The second axis is perpendicular to the two others

##### Example

The signatures are initialized with field values and their derivatives stored in *SampleVectors*:
```python
val, dval = mft.SampleVector(), mft.SampleVector()
val.set_coordinates('x', x)
val.set_values(bz) # Field values
dval.set_values(dbz_dy) # Field derivatives

s2d = mft.Signature2D(val, dval)
```

Let us now use the signature for field computations:
```python
# Prepare a SampleVector with the coordinates
s = mft.SampleVector()
s.set_coordinates('x', np.linspace(-10, 10, 21))
s.set_coordinates('y', 0.5 * np.ones(21)) # 0.5 mm above the initial data
s.set_unit_vector('y', np.ones(21))

# Compute the field
s2d.field(s)

# Change the multiplicative coef
s2d.set_coef(2)
s2d.field(s)
```
##### Moving a Signature2D

*Signature2D* objects can be moved with the same commands as other models:
```python
# Translation
tr = mft.Translation([0, 1, 0])
s2d.move(tr)
```
These commands move the coordinate system of the model, which is used for computing the fields.

#### 3D signatures

##### Theory

*Signature3D* objects are defined with field and field derivative values along a line. In the case this line is parallel to the x axis, the approximation is:
$$
B(x, y, z) = c \left(B(x, y_0, z_0) + (y - y0) \frac{dB}{dy}(x, y_0) + (z - z0) \frac{dB}{dz}(x, y_0, z_0) \right),
$$
where $c$ can be used to scale the signature.  

The functions $B$, $dB/dy$ and $dB/dz$ are implemented as spline interpolators. 

The points used to initialize a *Signature3D* object must be aligned.

##### Example

The signatures are initialized with field values and their derivatives stored in *SampleVectors*:
```python
val, dval_dy, dval_dz = mft.SampleVector(), mft.SampleVector(), mft.SampleVector()
val.set_coordinates('x', x)
val.set_values(bz) # Field values
dval_dy.set_values(dbz_dy) # Field derivatives
dval_dz.set_values(dbz_dz) # Field derivatives

s3d = mft.Signature3D(val, dval_dy, dval_dz)
```

### Processing models

#### Scaling a model
A scalar scale factor can be apply to magnet models. It will overwrite the model coefficients or sources.
```python
# Scale
mod.scale(2) # Field values will be twice higher 
```

##  Coordinate systems

Coordinates systems contain

- A point which defines its origin
- Vectors defining the $\mathbf{x}$, $\mathbf{y}$ and $\mathbf{z}$ axis
- A boolean which is True if the axis define an orthonormal basis.

The example below shows a simple example of coordinate system.

```python
# Build the default coordinate system
cs = mft.CoordinateSystem()

# Build a custom coordinate system
axis_main = [1, 0, 0]
axis_secondary = [0, 1, 0]
origin = [1, 0, 0]
cs = mft.CoordinateSystem('x', axis_main, 'y', axis_secondary, origin)

# Check the values
print('axis x:', cs.get_axis('x'))
print('axis y:', cs.get_axis('y'))
print('axis z:', cs.get_axis('z'))
print('origin:', cs.get_origin())
```

Combinations of translations and rotations can be applied to a coordinate system.

```python
# Translate a coordinate system
tr = mft.Translation([1, 0, 0])
cs.transform(tr)
```

Please see *examples/coord_sys.py* for more details, and *src/coord_system.h* for an exhautive list of methods.

## Units

Documentation to be written. Please see *examples/units.py* 

# References

<a name="lebec12"></a>G Le Bec *et al.*, PRST-AB 15, 022401 (2012)

<a name="schnizer09"></a>P Schnizer *et al.*, NIMA 607, 505–516 (2009)

<a name="davies92"></a>WG Davies, NIMA 311, 399–436 (1992)

<a name="chubar98"></a>O Chubar *et al.*, J Synchrotron Rad 5, 481–484 (1998)

<a name="chavanne17"></a>J Chavanne, presented by G Le Bec at IMMW20, Didcot, UK (2017)
