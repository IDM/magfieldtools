from distutils.core import setup, Extension
import distutils.ccompiler
import platform


# Source directory
src = './src'

# Get the compiler and select the corresponding options
comp = distutils.ccompiler.get_default_compiler()
print('Compiler: ', comp)
print(platform.platform())

if platform.platform().lower().find('windows') == 0:
       if comp == 'msvc':
              # MSBuild options
              c_args = ['/bigobj', '/std:c++17']
       elif comp == 'gcc':
              # gcc and g++ options
              c_args = ['-std=c++17', '-Wa,-mbig-obj'] 

else:
	c_args = ['-std=c++17']

mag_field_tools_module = Extension('_mag_field_tools',
                           sources=[
                            src+'/mag_field_tools_wrap.cxx', 
                            src+'/bem.cpp',
                            src+'/coord_system.cpp',
                            src+'/mag_field_model.cpp',
                            src+'/multipole.cpp',
                            src+'/sample.cpp',
                            src+'/sample_vector.cpp',
                            src+'/space_transform.cpp',
                            src+'/strength.cpp',
                            src+'/symmetries.cpp',
                            src+'/units.cpp'
                            ],
                            extra_compile_args=c_args
                           )

setup (name = 'mag_field_tools',
       version = '0.1',
       author      = "Gael Le Bec",
       description = """Tools for magnetic field analysis""",
       include_dirs = [src, '../external_libs/eigen-eigen'],
       ext_modules = [mag_field_tools_module],
       py_modules = ["mag_field_tools"],
       )