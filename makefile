SRC_DIR 	= ./src
SWIG 		= swig

all: 
	$(SWIG) -c++ -python -doxygen -outdir ./ $(SRC_DIR)/mag_field_tools.i
	python setup.py build_ext install