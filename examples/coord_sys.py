# --- Imports
import mag_field_tools as mft
from math import pi
from matplotlib.pyplot import quiver, show

# --- Build a default coordinate system
cs0 = mft.CoordinateSystem()
print('Default coordinate system')
print('axis x:', cs0.get_axis('x'))
print('axis y:', cs0.get_axis('y'))
print('axis 3:', cs0.get_axis('z'))

# --- Rotate it by 90 degrees
rz90 = mft.Rotation(pi/2, [0, 0, 1])
cs0.transform(rz90)
print('\n90 degrees rotation')
print('axis x:', cs0.get_axis('x'))
print('axis y:', cs0.get_axis('y'))
print('axis 3:', cs0.get_axis('z'))

# --- Another coordinate system build from two non orthogonal vectors
axis_main = [1, 0, 0]
axis_secondary = [1, 1, 0]
origin = [0, 0, 0]
cs1 = mft.CoordinateSystem('x', axis_main, 'y', axis_secondary, origin)
print('\nNew coordinate system from not orthonormal vectors')
print('axis x:', cs1.get_axis('x'))
print('axis y:', cs1.get_axis('y'))
print('axis 3:', cs1.get_axis('z'))
print('origin:', cs1.get_origin())

# --- Translation and rotation
print('\nRotation and translation')
tr = mft.Translation([1, 0, 0])
cs1.transform(tr)
cs1.transform(rz90)
print('Is this coordinate system is the trivial one?', cs1.is_world(), '\n')
print('Space transforms can be combined in a tuple.')
cs2 = mft.CoordinateSystem()
print('Origin: ', cs2.get_origin())
cs2.transform([tr, tr])
print('Origin after 2 translations: ', cs2.get_origin())



