# --- Imports
import sys
sys.path.append('../python/') 

import mag_field_tools as mft
import radia as rad

import matplotlib.pyplot as plt
import numpy as np
from math import pi, cos, sin

# --- Open a Radia file
# Open
with open('quad_solved.rad', 'rb') as f:
    obj_bin = f.read()
    quad = rad.UtiDmpPrs(obj_bin)

# Plot the geometry
rad.ObjDrwOpenGL(quad)

# --- Parameters
# References
r0 = 7 # Reference radius
l0 = 700 # 

# Number of points (2D)
n_theta = 32
n_coef = 16

# Number of points (3D)
n_theta_3d = 4 # for 1/8 of the magnet
n_l_3d = 128 # Number of points in the longitudinal direction 

# --- 2D Multipoles
# Build a circle with radius r0
print('Initialize SampleVectors...')
circle = mft.SampleVector(['Tmm', 'mm'])
circle.samples_on_circle(r0, n_theta, 'x', False)
x, y = circle.get_coordinates('x'), circle.get_coordinates('y')

# Compute field integrals
print('Field computations with Radia...')
ix = [rad.FldInt(quad, 'inf', 'ibx', [x[k], - l0 / 2, y[k]], [x[k], l0 / 2, y[k]]) for k in range(n_theta)]
iy = [rad.FldInt(quad, 'inf', 'ibz', [x[k], - l0 / 2, y[k]], [x[k], l0 / 2, y[k]]) for k in range(n_theta)]
iz = [0 for k in range(n_theta)]
plt.figure()
plt.plot(ix, label='I_X')
plt.plot(iy, label='I_Y')
plt.legend()
plt.xlabel('# point')
plt.ylabel('Field int. (Tmm)')
plt.show()

# Store the field integrals in the sample vectors
print('Multipole computations...')
circle.set_vector_values(ix, iy, iz)

# Compute the multipoles
c = mft.Circular2D(r0, n_coef, circle)

# Print the results
c_coef = c.get_coef()
for k in range(n_coef):
    print('n =', k, ': bn =', np.real(c_coef[k]))

# Gradient
g = c.strength().get(1)
ug = g.get_unit_sample().get_symbol() # Gradient unit
print('\nNormal gradient: {0} {1}'.format(g.get_values()[0], ug))

# --- Compute the field from multipoles
# Create samples on a disk
disk = mft.SampleVector(['Tmm', 'mm'])
disk.samples_on_disk(r0, 21, 'x', False)

# Compute the field
c.field(disk)

# Plot
x, y = disk.get_coordinates('x'), disk.get_coordinates('y')
ux, uy = disk.get_values('x'), disk.get_values('y')
plt.figure()
plt.quiver(x, y, ux, uy, pivot='middle')
plt.show()

# Create samples on a rectangle
rect = mft.SampleVector(['Tmm', 'mm'])
rect.samples_on_rectange(0, 0, 7, 7, 101, 101)
rect.set_proj(False)

# Compute the field
c.field(rect)

# Plot the field amplitude
x, y = rect.get_coordinates('x'), rect.get_coordinates('y')
ux, uy = rect.get_values('x'), rect.get_values('y')
plt.figure()
plt.imshow((np.reshape(ux, (101, 101)) ** 2 + np.reshape(uy, (101, 101)) ** 2 ) ** 0.5, extent=[0, 7, 0, 7])
plt.xlabel('x (mm)')
plt.ylabel('y (mm)')
plt.show()

# --- 3D Multipoles
print('Field computations with Radia...')
# Define sample positions
xyz = [[0, 0, 0] for k in range(n_theta_3d * n_l_3d)]
idx = 0
for k in range(n_theta_3d):
    for l in range(n_l_3d):
        xyz[idx][0] = r0 * cos(pi / 4 * k / (n_theta_3d - 1))
        xyz[idx][1] = l * l0 / 2 / (n_l_3d - 1)
        xyz[idx][2] = r0 * sin(pi / 4 * k / (n_theta_3d - 1))
        idx += 1

# Compute the field
bx = rad.Fld(quad,'bx', xyz) 
plt.figure()
plt.imshow(np.reshape(bx, (n_theta_3d, n_l_3d)))
plt.show()

# Create a set and apply symmetries
# Extract the coordinates
n_3d = n_theta_3d * n_l_3d
x = [xyz[k][0] for k in range(n_3d)]
y = [xyz[k][2] for k in range(n_3d)]
z = [xyz[k][1] for k in range(n_3d)]

# Initialize a vector of samples with projected values
s = mft.SampleVector(n_3d)
s.set_coordinates(x, y, z)
zeros, ones = [0 for k in range(n_3d)], [1 for k in range(n_3d)]
s.set_unit_vector(ones, zeros, zeros)
s.set_values(bx)

# Apply symmetries
sym_z = mft.SymmetryPlane([0, 0, 1])
s_sym_z = s.symmetry(sym_z)
s.add(s_sym_z)

sym_45 = mft.SymmetryPlane([1, -1, 0])
s_sym_45 = s.symmetry(sym_45)
s.add(s_sym_45)

sym_x = mft.SymmetryPlane([1, 0, 0], [0, 0, 0], True)
s_sym_x = s.symmetry(sym_x)
s.add(s_sym_x)

sym_y = mft.SymmetryPlane([0, 1, 0], [0, 0, 0], True)
s_sym_y = s.symmetry(sym_y)
s.add(s_sym_y)

x, y, z = s.get_coordinates('x'), s.get_coordinates('y'), s.get_coordinates('z')
vx, vy, vz = s.get_vector_values('x'), s.get_vector_values('y'), s.get_vector_values('z')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.quiver(x, y, z, vx, vy, vz)  
plt.show()

# Compute the multipoles
m = mft.Cylindrical3D(3, 128, r0, l0, s)
plt.imshow(m.get_coef())