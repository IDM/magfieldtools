# %%

# --- Imports
import mag_field_tools as mft
from math import pi, sin, cos
from matplotlib.pyplot import quiver, show, figure, imshow, plot
import numpy as np

def print_coef(multipole):
	"""Print the multipole coefficients
	:param multipole: Multipole object
	"""
	coef = multipole.get_coef()
	for k in range(len(coef)):
		print('{0:2}-pole: {1:2.3f} + {2:.3f} i'.format(2 * (k+1), coef[k].real, coef[k].imag))
	print('\n')

# --- Initialize samples on an ellipse
print('Create a set of samples with unit vectors normal to an ellipse...')
s = mft.SampleVector()
s.samples_on_ellipse(10, 5, 16, 'r') # Generate 16 points on a 10 mm maj axis and 5 mm min axis ellipse
# x, y = s.get_coordinates('x'), s.get_coordinates('y')
# ux, uy = s.get_unit_vector('x'), s.get_unit_vector('y')
# quiver(x, y, ux, uy, pivot='middle')
# show()

# --- Initialize an elliptic multipole object
e = mft.Elliptic2D(10, 5, 16)
e.set_coef(1, 1) # Normal quad
s.set_proj(False)
e.field(s)
x, y = s.get_coordinates('x'), s.get_coordinates('y')
vx, vy = s.get_vector_values('x'), s.get_vector_values('y')
quiver(x, y, vx, vy, pivot='middle')
show()

# --- Round turn: compute the multipoles from the field
e_back = mft.Elliptic2D(10, 5, 16, s)
print_coef(e_back)

# %%
