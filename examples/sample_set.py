# --- Imports
import mag_field_tools as mft
from math import pi, sin, cos
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.pyplot import quiver, show, figure

# --- SampleVector
# Coordinates
print('Initialize a sample set with coordinates...') 
s = mft.SampleVector()
s.set_coordinates('y', [k for k in range(10)])
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'), '\n')
# Values
print('Set the values...')
s.set_values([0.1 for k in range(10)])
print('New values:', s.get_values(), '\n')
# Unit vector
print('Set vector directions (unit vectors)...')
vx = [1 for k in range(10)]
vy = [1 for k in range(10)]
vz = [0 for k in range(10)]
s.set_unit_vector(vx, vy, vz)
print('Unit vector for the first sample:', s.get_unit_vector(0))
print('and for the second sample:', s.get_unit_vector(1))
print('Note that the vectors were normalized.\n')

# --- Change the coordinates
print('Rotate the coordinate system by pi/2 arround the z axis and express the sample in the new system...')
rz90 = mft.Rotation(pi/2, [0, 0, 1])
s.transform_coordinate_system(rz90)
cs = s.get_coordinate_system()
print('New x axis:', cs.get_axis('x'))
print('New y axis:', cs.get_axis('y'))
print('New z axis:', cs.get_axis('z'))
print('New x coordinates:', s.get_coordinates('x'))
print('New y coordinates:', s.get_coordinates('y'))
print('New z coordinates:', s.get_coordinates('z'))
print('New unit vector for the first sample:', s.get_unit_vector(0))
print('Note that the points are the same, only the coordinates have moved! And the same for the unit vectors.\n')

# --- Move the sample
rz_90 = mft.Rotation(-pi/2, [0, 0, 1])
s.transform_coordinate_system(rz_90)
print('Rotate back to the initial coordinate system...')
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'))
tr = mft.Translation([1, 0, 0])
print('Translate the sample...')
s.move(tr)
print('x coordinates:', s.get_coordinates('x'))
print('Back to the initial position...')
tr_back = mft.Translation([-1, 0, 0])
s.move(tr_back)
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'))
print('Unit vector for the first sample:', s.get_unit_vector(0), '\n')
print('Rotate the sample by 90 degrees arround z...')
s.move(rz90)
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'))
print('Unit vector for the first sample:', s.get_unit_vector(0), '\n')
print('Combine several displacements...')
s.move([rz90, rz90, rz90])
print('x coordinates:', s.get_coordinates('x'))
print('y coordinates:', s.get_coordinates('y'))
print('z coordinates:', s.get_coordinates('z'))
print('Unit vector for the first sample:', s.get_unit_vector(0), '\n')

# --- Time stamps
print('Initialize a sample set with time stamps...') 
s = mft.SampleVector()
s.set_time([t for t in range(10)])
print('Time stamps:', s.get_time(), '\n')

# --- Generate samples on a circle
print('Generate samples on a circle...') 
s = mft.SampleVector()
r0 = 10 # radius (mm)t 
s.samples_on_circle(r0, 32, 'r') # 'r' for radial vectors, 't' tangential, 'x', 'y' or 'z' for horizontal, vertical or longitudinal
print('Plot the unit vectors...')
x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_unit_vector('x'), s.get_unit_vector('y')
quiver(x, y, ux, uy, pivot='middle')
show()

print('Define values for a quadrupole field, and plot...')
print('(These values are for the radial components measured for instance by a moving stretched wire.)\n')
s.set_values([sin(2 * k * 2 * pi / 32) for k in range(32)])
vx, vy = s.get_values('x'), s.get_values('y')
quiver(x, y, vx, vy, pivot='middle')
show()

print('This does the same for the x and y components in a quadrupole...\n')
sx, sy = mft.SampleVector(), mft.SampleVector()
sx.samples_on_circle(r0, 32, 'x')
sy.samples_on_circle(r0, 32, 'y')
sx.set_values([sin(k * 2 * pi / 32) for k in range(32)])
sy.set_values([cos(k * 2 * pi / 32) for k in range(32)])
vxx, vxy = sx.get_values('x'), sx.get_values('y')
vyx, vyy = sy.get_values('x'), sy.get_values('y')
quiver(x, y, vxx, vxy, pivot='middle')
quiver(x, y, vyx, vyy, pivot='middle')
show()

print('NOTE: In all the cases above, the sample set give projections of the field in the direction spedified by the unit vector.')
print('This can be clarified by setting the is_proj attribute to True.')
print('This can be done for all the samples of the set, or for each samples individually')
print('Different field model construction methods will be called depending of this bit (see later).\n')
s.set_proj(True)	# All sample values are projections
sx.set_proj(True)
sy.set_proj([True for k in range(32)]) # Different values can be assigned to different samples

print('Build samples for a 3D normal dipole...')
print('Build a slice...')
r0 = 10
s_slice = mft.SampleVector()
s_slice.samples_on_circle(r0, 32, 'y')
s_slice.set_proj(False) # The samples give the field, not its projection
s_slice.set_values([1 for k in range(32)])
print('Build the dipole by translating the slice')
s_dipole = mft.SampleVector()
tr = mft.Translation([0, 0, 10]) # Translation
for k in range(10):
	s_dipole.add(s_slice)
	s_slice.move(tr)
print('Add zero field samples at the end ')
s_slice.set_values([0 for k in range(32)])
for k in range(10):
	s_dipole.add(s_slice)
	s_slice.move(tr)
print('Plot the dipole...')
x, y, z = s_dipole.get_coordinates('x'), s_dipole.get_coordinates('y'), s_dipole.get_coordinates('z')
vx, vy, vz = s_dipole.get_values('x'), s_dipole.get_values('y'), s_dipole.get_values('z')
fig = figure()
ax = fig.add_subplot(111, projection='3d')
quiver(x, y, z, vx, vy, vz)  
show()

# --- Units
print('\nBuild a sample set with custom units...')
s = mft.SampleVector(['Gm', 'mm'])
print('The sample units are ',  s.get_unit_sample().get_symbol())
