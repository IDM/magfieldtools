#%%
# --- Imports
import sys
sys.path.append('../python')

import mag_field_tools as mft
# # from math import pi, sin, cos, sqrt
from matplotlib.pyplot import plot, quiver, show, figure, imshow, axes, axes
from matplotlib.pyplot import contour, contourf
import numpy as np
# Note: avoid numpy during debug!
import csv

# --- Load SW data
f = open('HB0_0002_IxIz.dat') 
reader = csv.reader(f, delimiter='\t')
tab = [row for row in reader]	
x = [float(tab[k][0]) for k in range(1, len(tab))]	
ix = [float(tab[k][1]) for k in range(1, len(tab))]
iy = [float(tab[k][2]) for k in range(1, len(tab))]

n = len(x)
x0 = 0
for xk in x:
    x0 += xk / n
for k in range(n):
    x[k] -= x0

# --- Initialize a sample vector
s = mft.SampleVector(["Tmm", "mm"])
s.set_coordinates('x', x)
s.set_unit_vector('y', [1 for k in range(n)])
s.set_values(iy)
s.set_proj(True)

# Vector plot
# xs = s.get_coordinates('x')
# ys = s.get_coordinates('y')
# vxs = s.get_vector_values('x')
# vys = s.get_vector_values('y')
# quiver(xs, ys, vxs, vys, pivot='middle')
# show()

# --- Ambient field
r0 = 100
amb = mft.Circular2D(r0, 3, s) # keep dipole and quad

# --- Compute samples on the boundary of a racetrack
l, r = 100, 25
s1 = s.segment_to_racetrack(l, r, amb)

xs1 = s1.get_coordinates('x')
ys1 = s1.get_coordinates('y')
zs1 = s1.get_coordinates('z')
vxs1 = s1.get_vector_values('x')
vys1 = s1.get_vector_values('y')
vzs1 = s1.get_vector_values('z')
# vxs1 = s1.get_unit_vector('x')
# vys1 = s1.get_unit_vector('y')
# vzs1 = s1.get_unit_vector('z')
quiver(xs1, ys1, vxs1, vys1, pivot='middle')

# --- Build a boundary element model
bem = mft.BEM2D(s1)
# bem = mft.BEM2D()
# bem.set_svd_tol(1e-10)
# bem.init_from_set(s1)
figure()
plot(bem.get_sources())

m = np.array(bem.source_potential_matrix_std(s1))
a = np.array(s1.integral_normal().get_values())
figure()
plot(np.linalg.pinv(m) @ a)

# --- Test the values
st = mft.SampleVector(s)
bem.field(st)
figure()
plot(s.get_values())
plot(st.get_values())

#%%