# --- Imports
import mag_field_tools as mft
from math import pi, sin, cos
from matplotlib.pyplot import quiver, show, figure, imshow, plot
import numpy as np

def print_coef(multipole):
	"""Print the multipole coefficients
	:param multipole: Multipole object
	"""
	coef = multipole.get_coef()
	for k in range(len(coef)):
		print('{0:2}-pole: {1:2.3f} + {2:.3f} i'.format(2 * (k+1), coef[k].real, coef[k].imag))
	print('\n')

# --- Initialize a multipole from a sample set
print('Create a set of samples corresponding to a pure quadrupole...')
s = mft.SampleVector()
s.samples_on_circle(10, 32, 'r') # Generate 32 points on a 10 mm radius circle
s.set_values([sin(2 * k * 2 * pi / 32) for k in range(32)]) # Set the radial field for a pure quadrupole
print('Initialize a 2D circular multipole...')
c = mft.Circular2D(10, 8, s) # Use the sample vector s to initialize a circular multipole with 10 mm reference radius and 8 complex multipole coefficients
print('Multipole coefficients: ')
print_coef(c)

# --- Compute field samples from a multipole
print('Compute the field from the multipole...')
s = mft.SampleVector()
s.samples_on_disk(10, 31, False) # Generate points on a 10 mm radius disk (with 31 points on the horizontal axis), with not projected values
#s.set_proj(False)	# For 2D field calculations (not needed because of the proj=False in samples_on_disk)
c.field(s)

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Transform the coordinate system
print('\nTransform the coordinate system (without changing the field)...')
c = mft.Circular2D(10, 8)
c.set_coef([0, 1, 0, 0, 0, 0, 0, 0])
# tr = mft.Translation([1, 0, 0])
tr = mft.Rotation(pi/4, [0, 0, 1])
c.transform_coordinate_system(tr)
s.samples_on_disk(10, 31, False) # Generate points on a 10 mm radius disk, not projected
c.field(s)

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

print('\nFlip the coordinate system arrount the vertical axis')
c = mft.Circular2D(10, 8)
c.set_coef([1, 1, 1, 1, 1, 1, 1, 1])
print('Multipole coefficients before transformation:')
print_coef(c)
rz180 = mft.Rotation(pi, [0, 1, 0])
c.transform_coordinate_system(rz180)
print('Multipole coefficients after transformation:')
print_coef(c)

# --- Move the multipole
print('\nMove the multipole...')
c = mft.Circular2D(10, 8)
c.set_coef(1, 1)
# tr = mft.Translation([1, 0, 0])
tr = mft.Rotation(pi/4, [0, 0, 1])
c.move(tr)
s.samples_on_disk(10, 31, False) # Generate points on a 10 mm radius disk
c.field(s)

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Compute a sextupole field
print('\nSextupole field...')
c = mft.Circular2D(10, 8)
c.set_coef(2, 1)
# Vector plot on a disk
s = mft.SampleVector()
s.samples_on_disk(10, 31, False) # Generate points on a 10 mm radius disk (with 31 points on the horizontal axis)
c.field(s)
# Vertical field on horizontal axis
sx = mft.SampleVector()
sx.set_coordinates('x', [k for k in range(-10, 11)])
sx.set_proj(False)
c.field(sx)

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

xx = sx.get_coordinates('x')
by = sx.get_values('y')
plot(xx, by)
show()
