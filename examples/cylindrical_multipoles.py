#%%
 
# --- Imports
import sys
from turtle import width
sys.path.append('../python/') 

import mag_field_tools as mft
from math import pi, sin, cos
import matplotlib.pyplot as plt
import numpy as np

# # --- Create samples on a cylinder
# print('Create a set of samples on a cylinder...')
phi = mft.SampleVector()
phi.samples_on_cylinder(10, 100, 16, 64) # Generate 16 * 64 points on a cylinder with radius 10 and length 100

# --- Cylindrical multipole, based on Bessel functions
m = mft.Cylindrical3D(4, 3, 10, 100)
m.set_coef(1, 0, 1) # Set the coefficient (1, 0) to 1
m.scalar_potential(phi) #  Plot the scalar potential
v = phi.get_values()
v = np.reshape(v, (16, 64))
plt.imshow(v)
plt.show()

# Change the coefficients and compute the field
b = mft.SampleVector()
b.samples_on_cylinder(10, 100, 16, 64)
b.set_proj(False)

m.set_coef_zero()
m.set_coef(1, 1, 1) # Normal dipole mode of normal order 1
m.field(b) # Compute the field
x0, y0, z0 = b.get_coordinates('x'), b.get_coordinates('y'), b.get_coordinates('z')   
vx0, vy0, vz0 = b.get_vector_values('x'), b.get_vector_values('y'), b.get_vector_values('z')

m.set_coef_zero()
m.set_coef(1 + m.get_degree(), 1, 1) # Skew dipole mode of normal order 1
m.field(b) # Compute the field
x1, y1, z1 = b.get_coordinates('x'), b.get_coordinates('y'), b.get_coordinates('z')   
vx1, vy1, vz1 = b.get_vector_values('x'), b.get_vector_values('y'), b.get_vector_values('z')

m.set_coef_zero()
m.set_coef(1, 1 + m.get_order(), 1) # Normal dipole mode of skew order 1
m.field(b) # Compute the field
x2, y2, z2 = b.get_coordinates('x'), b.get_coordinates('y'), b.get_coordinates('z')   
vx2, vy2, vz2 = b.get_vector_values('x'), b.get_vector_values('y'), b.get_vector_values('z')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.quiver(x0, y0, z0, vx0, vy0, vz0, pivot='middle', length=5, color='blue')
plt.quiver(x1, y1, z1, vx1, vy1, vz1, pivot='middle', length=5, color='red')
plt.quiver(x2, y2, z2, vx2, vy2, vz2, pivot='middle', length=5, color='green')

# --- Compute the multipoles from a sample vector
m.set_coef_zero()
m.set_coef(1, 1, 1)
m.field(b)
m1 = mft.Cylindrical3D(4, 3, 10, 100, b)
plt.imshow(m1.get_coef())
# plt.imshow(m1.get_coef())

# %%
