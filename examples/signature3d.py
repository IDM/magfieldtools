# --------------------
# Test the Signature2D and Signature3D classes
# G Le Bec, ESRF, Jan 2023
# --------------------
#%%
# Add path (assuming the module is not installed in the site-packages)
import sys
sys.path.append('../python/') 

# Imports
import matplotlib.pylab as plt
import numpy as np
import mag_field_tools as mft
import radia_id as rid
from math import pi, sin

# Build a magnet with radia
print('Build a magnet with Radia...')
p = rid.HybridUndParam(18, n_periods=1) # Define parameters, see ?rid.HybUndParam for details
u = rid.HybridUndulator(p, print_switch=False) # Build an undulator but do not solve it
pm = u.build_block() # Build a H magnet
tr = rid.rad.TrfTrsl([0, 0, -p.mag_height]) # Move the block down
rid.rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, 0]) # Appy a Radia symmetry
rid.rad.TrfOrnt(pm, tr)
rid.rad.Solve(pm, 0.00001, 1000) # Solve

# -----------------------
# 2D signatures
# Sampling in the longitudinal direction
# -----------------------

# Compute the integrated field
ymin, ymax, ny = -100, 100, 101
z_pos = 5 # Vertical position
dz = 0.2

print('Compute the field with Radia...')
y = np.linspace(ymin, ymax, ny)
bz_radia, dbz_radia = np.zeros(ny), np.zeros(ny)
for k in range(ny):
    # Bz
    bz_radia[k] = rid.rad.Fld(pm, 'bz', [0, y[k], z_pos])
    
    # d Bz / dz
    dbz_radia[k] = rid.rad.Fld(pm, 'bz', [0, y[k], z_pos + dz / 2]) 
    dbz_radia[k] -= rid.rad.Fld(pm, 'bz', [0, y[k], z_pos - dz / 2])
    dbz_radia[k] /= dz

# Store the values in SampleVectors
val, dval = mft.SampleVector(), mft.SampleVector()
val.set_coordinates('y', z_pos * np.ones(ny))
val.set_coordinates('z', y)
val.set_values(bz_radia)
dval.set_values(dbz_radia)

# Build a signature
s2d = mft.Signature2D(val, dval)

# Use the signature to interpolate the field
ny2 = 201
y2 = np.linspace(ymin, ymax, ny2)

# Sample Vector and field values
s = mft.SampleVector()
s.set_coordinates('y', z_pos * np.ones(ny2))
s.set_coordinates('z', y2)
s.set_unit_vector('y', np.ones(ny2))

s2d.field(s)

plt.plot(y, bz_radia, label='Radia')
plt.plot(s.get_coordinates('z'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field (T)')
plt.title('H magnet field')
plt.legend()
plt.show()

print('Compute the field at another vertical position...')
# Compute with Radia
bz_radia_test = np.zeros(ny)
z_pos_test = 4
for k in range(ny):
    bz_radia_test[k] = rid.rad.Fld(pm, 'bz', [0, y[k], z_pos_test])

# Compute with the signature
s.set_coordinates('y', z_pos_test * np.ones(ny2))
s2d.field(s)
plt.plot(y, bz_radia_test, label='Radia')
plt.plot(s.get_coordinates('z'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field (T)')
plt.legend()
plt.show()

# The result should be the same if we move the model
print('Translate the signature...')
tr = mft.Translation([0, z_pos - z_pos_test, 0])
s2d.move(tr)

s.set_coordinates('y', z_pos * np.ones(ny2))
s2d.field(s)
plt.plot(y, bz_radia_test, label='Radia')
plt.plot(s.get_coordinates('z'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field (Tmm)')
plt.title('Translation')
plt.legend()
plt.show()

tr = mft.Translation([0, - z_pos + z_pos_test, 0])
s2d.move(tr) # Back to the initial position

# -----------------------
# 3D signatures
# Sampling in the longitudinal direction
# -----------------------
# Compute the integrated field
ymin, ymax, ny = -100, 100, 101
x_pos = 20 # Horizontal position
z_pos = 5 # Vertical position
dx = 0.2
dz = 0.2

print('Compute the field with Radia, off-axis...')
y = np.linspace(ymin, ymax, ny)
bz_radia, dbz_dy_radia, dbz_dx_radia = np.zeros(ny), np.zeros(ny), np.zeros(ny)
for k in range(ny):
    # Bz
    bz_radia[k] = rid.rad.Fld(pm, 'bz', [x_pos, y[k], z_pos])
    
    # d Bz / dy
    dbz_dy_radia[k] = rid.rad.Fld(pm, 'bz', [x_pos, y[k], z_pos + dz / 2]) 
    dbz_dy_radia[k] -= rid.rad.Fld(pm, 'bz', [x_pos, y[k], z_pos - dz / 2])
    dbz_dy_radia[k] /= dz

    # d Bz / dx
    dbz_dx_radia[k] = rid.rad.Fld(pm, 'bz', [x_pos + dx / 2, y[k], z_pos]) 
    dbz_dx_radia[k] -= rid.rad.Fld(pm, 'bz', [x_pos - dx / 2, y[k], z_pos])
    dbz_dx_radia[k] /= dz

# Initial SampleVectors
# Note: the axis are not the sames in Radia and MFT
# RADIA |   MFT     |
# Y     |   z       |   Longitudinal
# Z     |   y       |   Vertical
# Y     |   -x      |   Transverse horizontal
val, dval0, dval1 = mft.SampleVector(), mft.SampleVector(), mft.SampleVector()
val.set_coordinates('x', x_pos * np.ones(ny))
val.set_coordinates('y', z_pos * np.ones(ny))
val.set_coordinates('z', y)
val.set_values(bz_radia)
dval0.set_values(dbz_dy_radia)
dval1.set_values(- np.array(dbz_dx_radia))

# Build a signature
s3d = mft.Signature3D(val, dval0, dval1)

# Sample Vector and field values
s = mft.SampleVector()
s.set_coordinates('x', x_pos * np.ones(ny2))
s.set_coordinates('y', z_pos * np.ones(ny2))
s.set_coordinates('z', y2)
s.set_unit_vector('y', np.ones(ny2))

s3d.field(s)

plt.plot(y, bz_radia, label='Radia')
plt.plot(s.get_coordinates('z'), s.get_values(), label='Signature3D')
plt.xlabel('position (mm)')
plt.ylabel('Field (T)')
plt.title('H magnet field')
plt.legend()
plt.show()

#%%
print('Compute the field at another transverse position...')
# Compute with Radia
bz_radia_test = np.zeros(ny)
x_pos_test = 21
z_pos_test = 5
for k in range(ny):
    bz_radia_test[k] = rid.rad.Fld(pm, 'bz', [x_pos_test, y[k], z_pos_test])

# Compute with the signature
s.set_coordinates('x', x_pos_test * np.ones(ny2))
s.set_coordinates('y', z_pos_test * np.ones(ny2))
s.set_coordinates('z', y2)
s3d.field(s)
plt.plot(y, bz_radia_test, label='Radia')
plt.plot(s.get_coordinates('z'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field (T)')
plt.legend()
plt.show()

#%%
