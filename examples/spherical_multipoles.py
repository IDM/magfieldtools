#%%
 
# --- Imports
import mag_field_tools as mft
from math import pi, sin, cos
# from mpl_toolkits.mplot3d import Axes3D
# from matplotlib.pyplot import quiver, show, figure, imshow, plot, hot
# import numpy as np

# --- Create samples on a sphere
print('Create a set of samples on a sphere...')
sph = mft.SampleVector()
sph.samples_on_sphere(10, 8, 8) # Generate 32 * 32 points on a 10 radius sphere

x, y, z = sph.get_coordinates('x'), sph.get_coordinates('y'), sph.get_coordinates('z')
vx, vy, vz = sph.get_unit_vector('x'), sph.get_unit_vector('y'), sph.get_unit_vector('z')

# fig = figure()
# ax = fig.add_subplot(111, projection='3d')
# quiver(x, y, z, vx, vy, vz)  
# show()

# --- Spherical multipole
m = mft.Spherical3D()
m.set_coef(1, 0, 5) # Set dipole coef[1][0] to 5
# print('Multipole coefficients')
# print(m.get_coef())

# Scalar potential
m.scalar_potential(sph) 
x, y, z = sph.get_coordinates('x'), sph.get_coordinates('y'), sph.get_coordinates('z')   
c = sph.get_values() # The values are represented by the color

# fig = figure()
# ax = fig.add_subplot(111, projection='3d')
# img = ax.scatter(x, y, z, c=c, cmap=hot())
# fig.colorbar(img)
# show()

# Magnetic field
m.set_coef(1, 0, 0)
m.set_coef(2, 1, 5)
print('Multipole coefficients')
print(m.get_coef())

sph = mft.SampleVector()
sph.samples_on_sphere(10, 8, 8)
sph.set_proj(False)
m.field(sph) 
x, y, z = sph.get_coordinates('x'), sph.get_coordinates('y'), sph.get_coordinates('z')   
vx, vy, vz = sph.get_vector_values('x'), sph.get_vector_values('y'), sph.get_vector_values('z')

# fig = figure()
# ax = fig.add_subplot(111, projection='3d')
# quiver(x, y, z, vx, vy, vz)  
# show()

m1 = mft.Spherical3D()
m1.init_from_set(sph)
print(m1.get_coef())

#%%