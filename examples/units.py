# --- Imports
import mag_field_tools as mft

# --- Distance units
u1 = mft.Unit('m')
u2 = mft.Unit('mm')
print('One [{0}] is {1} [{2}]\n'.format(u1.get_symbol(), u1.get_scale_factor_to_unit(u2), u2.get_symbol()))

# --- Try to convert [m] to a not compatible unit
u3 = mft.Unit('s')
# The line below will produce an error
# u1.get_scale_factor_to_unit(u3)

# --- Magnetic field units
u4 = mft.Unit('G')
u5 = mft.Unit('uT')
print('One [{0}] is {1} [{2}]\n'.format(u4.get_symbol(), u4.get_scale_factor_to_unit(u5), u5.get_symbol()))

# --- Multiply units
u6 = mft.Unit('m')
u6.multiply(mft.Unit('G'))
print('[m] times [G] gives [{0}]'.format(u6.get_symbol()))
print('This unit is for', u6.get_physical_quantity(), '\n')

# --- Divide units
u7 = mft.Unit('T')
u7.divide(mft.Unit('m'))
print('[T] divided by [m] gives [{0}]'.format(u7.get_symbol()))
print('This unit is for', u7.get_physical_quantity(), '\n')

# --- Unit conversion
u8 = mft.Unit('G')
u8.divide(mft.Unit('mm'))
print('One [{0}] is {1} [{2}]\n'.format(u7.get_symbol(), u7.get_scale_factor_to_unit(u8), u8.get_symbol()))

# --- Simplify units 
u9 = mft.Unit('m')
u9.divide(mft.Unit('m'))
print('[m] divided by [m] gives [{0}].'.format(u9.get_symbol()))


