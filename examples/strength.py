# --- Imports
import mag_field_tools as mft
from math import pi, sin, cos
from matplotlib.pyplot import quiver, show, figure, imshow, plot
import numpy as np


# --- Initialize a multipole
print('Initialize a multipole field...')
c = mft.Circular2D(10, 8)
c.set_coef(1, 1)

s = c.strength() # Strengths
g = s.get(1) # Gradient
ug = g.get_unit_sample().get_symbol() # Gradient unit
print('Normal gradient: {0} {1}'.format(g.get_values()[0], ug))
print('Skew gradient: {0} {1}'.format(g.get_values()[1], ug))

# --- Normalized strengths
print('\nNormalized strengths...')
c = mft.Circular2D(10, 8, mft.Unit("Tm")) # Normalized strengths require integrated field multipoles
c.set_coef(1, 1) 

s = c.normalized_strength(20) # B rho in [Tm] (other units can be specified)
g = s.get(1) # Focal length
ug = g.get_unit_sample().get_symbol() # Focal length unit
print('Normal gradient: {0} {1}'.format(g.get_values()[0], ug))
print('Skew gradient: {0} {1}'.format(g.get_values()[1], ug))

print('Add a dipole component...')
c.set_coef(0, 1) 

s = c.normalized_strength(20) # B rho in [Tm] (other units can be specified)
a = s.get(0) # Angle
ua = g.get_unit_sample().get_symbol() # Angle unit
print('Normal angle: {0} {1}'.format(a.get_values()[0], ua))
print('Skew angle: {0} {1}'.format(a.get_values()[1], ua))