# --- Imports
import sys
from turtle import width
sys.path.append('../python/') 
import mag_field_tools as mft
from math import pi, sin, cos
from matplotlib.pyplot import quiver, show, figure, imshow, plot
# import numpy as np

# # --- Initialize data
print('Build a SampleVector with a single Sample...')
s = mft.SampleVector()
sp = mft.Sample(1, [1, 1, 0], [1, 1, 0]) # Sample with value 1, located a [1, 0, 0] with unit vector [1, 1, 0]
s.push_back(sp) # Push sp in s

# --- Symmetry plane with zero parallel field
print('Create and apply a symmetry plane with zero parallel field...')
sym = mft.SymmetryPlaneZeroPara([0, 1, 0], [0.5, 0, 0]) # Symmetry plane with normal vector [0, 1, 0] and passing through the [0.5, 0, 0]
s_sym = s.symmetry(sym) # Build a SampleVector symmetric to the initial one
s.add(s_sym) # Join s and s_sym

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Symmetry plane with zero perpendicular field
print('Build a SampleVector with a single Sample...')
s = mft.SampleVector()
sp = mft.Sample(1, [1, 1, 0], [1, 1, 0]) # Sample with value 1, located a [1, 0, 0] with unit vector [1, 1, 0]
s.push_back(sp)
print('Create and apply a symmetry plane with zero perpendicular field...')
sym = mft.SymmetryPlaneZeroPerp([1, 0, 0], [0.5, 0, 0]) # Symmetry plane with normal vector [0, 1, 0] and passing through the [0.5, 0, 0]
s_sym = s.symmetry(sym) # Build a SampleVector symmetric to the initial one
s.add(s_sym) # Join s and s_sym

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Symmetry plane
print('Build a SampleVector with a single Sample...')
s = mft.SampleVector()
sp = mft.Sample(1, [1, 1, 0], [1, 1, 0]) # Sample with value 1, located a [1, 0, 0] with unit vector [1, 1, 0]
s.push_back(sp)
print('Create and apply a symmetry plane...')
sym = mft.SymmetryPlane([1, 0, 0], [0.5, 0, 0]) # Symmetry plane with normal vector [1, 0, 0] and passing through the [0.5, 0, 0]
s_sym = s.symmetry(sym) # Build a SampleVector symmetric to the initial one
s.add(s_sym) # Join s and s_sym

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Antisymmetry plane
print('Build a SampleVector with a single Sample...')
s = mft.SampleVector()
sp = mft.Sample(1, [1, 0, 0], [1, 0, 0]) # Sample with value 1, located a [1, 0, 0] with unit vector x
print('Create and apply an antisymmetry plane...')
s.push_back(sp) # Push sp in s
asym = mft.SymmetryPlane([0, 1, 0], [0, 1, 0], True) # Anti symmetry plane with normal vector [0, 1, 0] and passing through the [0, 1, 0]
s_asym = s.symmetry(asym) # Build a SampleVector symmetric to the initial one
s.add(s_asym) # Join s and s_sym

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

# --- Central symmetry 
print('Build a new Vector corresponding to 1/2 quadrupole radial field...')
s = mft.SampleVector()
s.set_coordinates('x', [cos(2 * pi * k / 32) for k in range(16)])
s.set_coordinates('y', [sin(2 * pi * k / 32) for k in range(16)])
vx, vy = [cos(2 * pi * k / 32)  for k in range(16)], [sin(2 * pi * k / 32)  for k in range(16)]
vz = [0 for k in range(16)]
s.set_unit_vector(vx, vy, vz)
s.set_values([sin(4 * pi * k / 32) for k in range(16)])
x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()

csym = mft.SymmetryCentre([0, 0, 0]) # Symmetry centre at the origin
s_csym = s.symmetry(csym) # Build a symmetric set
s.add(s_csym) # Join the sets of samples

x, y = s.get_coordinates('x'), s.get_coordinates('y')
ux, uy = s.get_values('x'), s.get_values('y')
quiver(x, y, ux, uy, pivot='middle')
show()
