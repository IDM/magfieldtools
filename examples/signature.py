# --------------------
# Test the Siganture2D class
# G Le Bec, ESRF, Nov 2022
# --------------------
#%%
# Add path (assuming the module is not installed in the site-packages)
import sys
sys.path.append('../python/') 

# Imports
import matplotlib.pylab as plt
import numpy as np
import mag_field_tools as mft
import radia_id as rid
from math import pi, sin

# Build a magnet with radia
print('Build a magnet with Radia...')
p = rid.HybridUndParam(18, n_periods=1) # Define parameters, see ?rid.HybUndParam for details
u = rid.HybridUndulator(p, print_switch=False) # Build an undulator but do not solve it
pm = u.build_block() # Build a H magnet
tr = rid.rad.TrfTrsl([0, 0, -p.mag_height]) # Move the block down
rid.rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, 0]) # Appy a Radia symmetry
rot = rid.rad.TrfRot([0, 0, -p.mag_height / 2], [1, 0, 0], pi / 180) # Define a 1 deg rotation
rid.rad.TrfOrnt(pm, tr)
rid.rad.TrfOrnt(pm, rot)
rid.rad.Solve(pm, 0.00001, 1000) # Solve

# -----------------------
# 2D signatures
# -----------------------

# Compute the integrated field
xmin, xmax, nx = -50, 50, 101
z_pos = 5 # Vertical position
dz = 0.2

print('Compute the field with Radia...')
x = np.linspace(xmin, xmax, nx)
iz_radia, diz_radia = np.zeros(nx), np.zeros(nx)
for k in range(nx):
    # int. Bz
    iz_radia[k] = rid.rad.FldInt(pm, 'inf', 'ibz', [x[k], -1000, z_pos], [x[k], 1000, z_pos])
    
    # d Int. Bz / dz
    diz_radia[k] = rid.rad.FldInt(pm, 'inf', 'ibz', [x[k], -1000, z_pos + dz / 2], [x[k], 1000, z_pos + dz / 2]) 
    diz_radia[k] -= rid.rad.FldInt(pm, 'inf', 'ibz', [x[k], -1000, z_pos - dz / 2], [x[k], 1000, z_pos - dz / 2])
    diz_radia[k] /= dz

# Store the values in SampleVectors
val, dval = mft.SampleVector(), mft.SampleVector()
val.set_coordinates('x', x)
val.set_coordinates('y', z_pos * np.ones(nx))
val.set_values(iz_radia)
dval.set_values(diz_radia)

# Build a signature
s2d = mft.Signature2D(val, dval)

# Use the signature to interpolate the field
nx2 = 201
x2 = np.linspace(xmin, xmax, nx2)
ones2 = np.ones(nx2)

# Sample Vector and field values
s = mft.SampleVector()
s.set_coordinates('x', x2)
s.set_coordinates('y', z_pos * ones2)
s.set_unit_vector('y', ones2)

s2d.field(s)

plt.plot(x, iz_radia, label='Radia')
plt.plot(s.get_coordinates('x'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field int. (Tmm)')
plt.title('1 degree rotation')
plt.legend()
plt.show()

print('Compute the field integral for a 10 deg rotation...')
s2d.set_coef(sin(10 * pi / 180) / sin(pi / 180))
s2d.field(s)
plt.plot(s.get_coordinates('x'), s.get_values(), label='Signature2D, 10 deg')
plt.xlabel('position (mm)')
plt.ylabel('Field int. (Tmm)')
plt.title('10 degrees rotation')
plt.legend()
plt.show()

print('Compute the field at another vertical position...')
# Compute with Radia
iz_radia_test = np.zeros(nx)
z_pos_test = 3
for k in range(nx):
    iz_radia_test[k] = rid.rad.FldInt(pm, 'inf', 'ibz', [x[k], -1000, z_pos_test], [x[k], 1000, z_pos_test])

# Compute with the signature
s2d.set_coef(1) # Reset the coeffient to 1 deg angle
s.set_coordinates('y', z_pos_test * ones2)
s2d.field(s)
plt.plot(x, iz_radia_test, label='Radia')
plt.plot(s.get_coordinates('x'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field int. (Tmm)')
plt.legend()
plt.show()

# The result should be the same if we move the model
print('Translate the signature...')
tr = mft.Translation([0, z_pos - z_pos_test, 0])
s2d.move(tr)

s.set_coordinates('y', z_pos * ones2)
s2d.field(s)
plt.plot(x, iz_radia_test, label='Radia')
plt.plot(s.get_coordinates('x'), s.get_values(), label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field int. (Tmm)')
plt.title('Translation')
plt.legend()
plt.show()

tr = mft.Translation([0, - z_pos + z_pos_test, 0])
s2d.move(tr) # Back to the initial position

#%%

# Signature of a magnet rotation
print('Rotate the signature...')
rotz = rid.rad.TrfRot([0, 0, 0], [0, 1, 0], 2* pi / 180) # Define a 2 deg rotation
rid.rad.TrfOrnt(pm, rotz)
iz_radia_rot = np.zeros(nx)
for k in range(nx):
    iz_radia_rot[k] = rid.rad.FldInt(pm, 'inf', 'ibz', [x[k], -1000, z_pos], [x[k], 1000, z_pos])

# Back to the initial position
rotz = rid.rad.TrfRot([0, 0, 0], [0, 1, 0], -2 * pi / 180)
rid.rad.TrfOrnt(pm, rotz)

rz = mft.Rotation(-2 * pi/180, [0, 0, 1])
s2d.move(rz)
s2d.field(s)
iz_rot = np.array(s.get_values())

plt.plot(x, iz_radia_rot, label='Radia')
plt.plot(s.get_coordinates('x'), iz_rot, label='Signature2D')
plt.xlabel('position (mm)')
plt.ylabel('Field int. (Tmm)')
plt.title('Rotation')
plt.legend()
plt.show()

#%%
