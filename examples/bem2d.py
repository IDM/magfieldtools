#%%
# --- Imports
import sys
sys.path.append('../python')

import mag_field_tools as mft
from math import pi, sin, cos, sqrt
from matplotlib.pyplot import plot, quiver, show, figure, imshow, contour
# import numpy as np
import csv

# --- Perpendicular unit vectors
def perp_unit_vectors(x, y):
	n_points = len(x)
	ux = [0 for k in range(n_points)]
	uy = [0 for k in range(n_points)]

	# Compute vector direction
	for k in range(1, n_points - 1):
		ux[k] = - y[k + 1] + y[k - 1] 
		uy[k] =  x[k + 1] - x[k - 1]
	ux[0] = - y[1] + y[n_points - 1] 
	ux[n_points - 1] =  - y[0] + y[n_points - 2] 
	uy[0] = x[1] - x[n_points - 1] 
	uy[n_points - 1] = x[0] - x[n_points - 2] 
	
	# Normalize
	for k in range(n_points):
		un = sqrt(ux[k] ** 2 + uy[k] ** 2)
		ux[k] /= un
		uy[k] /= un

	return ux, uy

# --- Load real data from SW benches
# x, y, z, field_int = np.loadtxt('bem2d_test_data.dat', delimiter='\t', unpack=True)
# Same without np, more suitable for debug...
f = open('bem2d_test_data.dat') 
reader = csv.reader(f, delimiter='\t')
tab = [row for row in reader]	
x = [float(tab[k][0]) for k in range(1, len(tab) - 1)]	
y = [float(tab[k][1]) for k in range(1, len(tab) - 1)]
z = [float(tab[k][2]) for k in range(1, len(tab) - 1)]
field_int = [float(tab[k][3]) for k in range(1, len(tab) - 1)]

ux, uy = perp_unit_vectors(x, y)
s = mft.SampleVector()
s.set_coordinates('x', x)
s.set_coordinates('y', y)
s.set_coordinates('z', z)
s.set_unit_vector('x', ux)
s.set_unit_vector('y', uy)
s.set_values(field_int)
s.set_proj(True)

s.zero_integral_normal()

# Plot the field
xs = s.get_coordinates('x')
ys = s.get_coordinates('y')
vxs = s.get_vector_values('x')
vys = s.get_vector_values('y')

quiver(xs, ys, vxs, vys, pivot='middle')


# Build a boundary model
bem = mft.BEM2D(s)
# plot(bem.get_sources())
# show()
# m = bem.source_potential_matrix_std(s)
	
# Initialize samples on a rectangle
r = mft.SampleVector()
r.samples_on_rectange(-45, -11, 45, -100, 21, 21)
r.set_proj(False)

# Compute the field at sample locations
bem.field(r)

# Plot the field
xr = r.get_coordinates('x')
yr = r.get_coordinates('y')
vxr = r.get_vector_values('x')
vyr = r.get_vector_values('y')
figure()
quiver(xr, yr, vxr, vyr, pivot='middle')

tr = mft.Rotation(pi, [0, 0, 1])
bem.move(tr)

r1 = mft.SampleVector()
r1.samples_on_rectange(-45, 11, 45, 100, 21, 21)
r1.set_proj(False)
bem.field(r1)

x1s = r1.get_coordinates('x')
y1s = r1.get_coordinates('y')
vx1s = r1.get_vector_values('x')
vy1s = r1.get_vector_values('y')
quiver(x1s, y1s, vx1s, vy1s, pivot='middle')


#%%

# 